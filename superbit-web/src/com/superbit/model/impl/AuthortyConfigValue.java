package com.superbit.model.impl;

import java.io.Serializable;

public class AuthortyConfigValue implements Serializable {
	private static final long serialVersionUID = -717876643143316916L;
	
	/**
	 * ID
	 */
	private int id;
	/**
	 * 资源路径（去除ContextPath)
	 */
	private String uri;
	/**
	 * cmd命令参数
	 */
	private String cmd;
	/**
	 * 支持的角色，多个角色以，间隔
	 */
	private String roles;
	/**
	 * 是否需要记录
	 */
	private boolean needLog;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getCmd() {
		return cmd;
	}
	public void setCmd(String cmd) {
		this.cmd = cmd;
	}
	public String getRoles() {
		return roles;
	}
	public void setRoles(String roles) {
		this.roles = roles;
	}
	public boolean isNeedLog() {
		return needLog;
	}
	public void setNeedLog(boolean needLog) {
		this.needLog = needLog;
	}
}
