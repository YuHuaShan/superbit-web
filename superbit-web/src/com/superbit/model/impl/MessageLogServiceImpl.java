package com.superbit.model.impl;

import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.superbit.model.feature.MessageLogService;

@Service("messageLogService")
public class MessageLogServiceImpl implements MessageLogService {
	
	private Logger errorlLgger = LoggerFactory.getLogger("WEBERROR");


	@Override
	public void logUserError(Integer userid, String remoteIp, Throwable exp, Map<String, String> requestMap) {
//		messageLogDao.saveUserError(userid,remoteIp,exp,requestMap);
	}

	@Override
	public void logUserOpertiaon(String uri, String cmd, Integer loginUserId, Map<String, String> parammap,	boolean success) {
//		String params = parammap==null?"":parammap.toString();
//		messageLogDao.saveUserOperationLog(loginUserId,uri,cmd,success,params);
	}

	@Override
	public void logUserAccess(Date accessDate, long durationtime, Integer userid, String loginName) {
//		messageLogDao.saveUserAcessLog(userid,loginName,accessDate,durationtime);
	}

	@Override
	public void logDevlopError(Throwable exp, Map<String, String> map) {
		errorlLgger.error(exp.getMessage()+","+map,exp);
	}

	@Override
	public void logServerError(Throwable exp, Map<String, String> map) {
		errorlLgger.error(exp.getMessage()+","+map,exp);
	}

}
