package com.superbit.model.feature;

import java.util.Date;
import java.util.Map;

/**
 * 记录日志服务
 */
public interface MessageLogService {
	
	/**记录用户访问错误
	 * @param userid
	 * @param remoteIp
	 * @param exp
	 * @param requestMap
	 */
	void logUserError(Integer userid,String remoteIp,Throwable exp,Map<String,String> requestMap);

	/**记录用户操作
	 * @param uri
	 * @param cmd
	 * @param loginUserId
	 * @param parammap
	 * @param sucess
	 */
	void logUserOpertiaon(String uri, String cmd, Integer loginUserId, Map<String, String> parammap,boolean sucess);

	/**记录用户访问
	 * @param date
	 * @param durationtime
	 * @param userid 登陆用户ID 可为空
	 * @param loginName 登陆用户名 可为空
	 */
	void logUserAccess(Date accessDate, long durationtime, Integer userid, String loginName);

	/**记录开发异常
	 * @param exp
	 * @param map
	 */
	void logDevlopError(Throwable exp, Map<String, String> map);

	/**记录服务异常
	 * @param exp
	 * @param map
	 */
	void logServerError(Throwable exp, Map<String, String> map);
}
