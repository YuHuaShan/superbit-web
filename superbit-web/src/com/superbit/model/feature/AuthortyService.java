package com.superbit.model.feature;

import java.util.List;

/**
 * 判断用户权限使用的工具类
 */
public interface AuthortyService {
	static String ROLE_ADMIN = "admin"; //高级管理员
	static String ROLE_MANAGER = "manager"; //运维人员
	static String ROLE_USER = "user"; //一般用户
	static String ROLE_NOLOGIN = "nologin"; //未登陆人员
	
	/**获取对应URL支持的用户角色
	 * @param uri
	 * @param cmd
	 * @return
	 */
	List<String> getAuthortyRoleByURI(String uri,String cmd);
	
	/**判断对应角色是否有权限
	 * @param uri
	 * @param cmd
	 * @param role
	 * @return
	 */
	boolean hasAuthorty(String uri,String cmd,String role);
	
	/**是否需要记录操作日志
	 * @param uri
	 * @param cmd
	 * @return
	 */
	boolean needLogOperation(String uri,String cmd);
	
	/**更新角色权限和日志情况
	 * @param uri
	 * @param cmd
	 * @param roles
	 */
	void updateAuthorty(String uri,String cmd,List<String> roles,boolean needLogOperation);
}
