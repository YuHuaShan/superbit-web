package com.superbit.web.request;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import com.superbit.core.constant.TradeLogConstants;

/**
 * 交易记录获取请求
 * 
 * @author zhangdaoguang
 *
 */
public class TradeLogGetRequest implements Serializable {
	private static final long serialVersionUID = 2369005858384366444L;

	/* 交易类型, 必传, 0买入,1卖出 */
	private Integer tradeType;
	/* 状态, 非必传, 不传表示全部 (默认), 0未付款, 1已付款, 2已完成, 3已取消 */
	private Integer payStatus;
	/* 申诉中, 非必传, 不传或0表示没选(默认), 1表示勾选 */
	private Integer hasComplain;
	/* 付款参考号 (精确搜索)，非必传 */
	private Long payNumber;
	/* 币种code,必传 */
	private String coinCode;
	/* 用户ID,必传 */
	private Integer userId;
	/* 页号,必传 */
	private Integer pageNo;
	/* 每页大小,必传 */
	private Integer pageSize;

	public Boolean validate() {
		if (!TradeLogConstants.TRADE_TYPE_BUY.equals(this.tradeType)
				&& !TradeLogConstants.TRADE_TYPE_SELL.equals(this.tradeType)) {
			return Boolean.FALSE;
		}

		if (this.pageNo == null || this.pageNo <= 0 || this.pageSize == null
				|| this.pageSize <= 0) {
			return Boolean.FALSE;
		}

		if (StringUtils.isBlank(coinCode)) {
			return Boolean.FALSE;
		}

		if (userId == null) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public String toString() {
		return "TradeLogGetRequest [tradeType=" + tradeType + ", payStatus="
				+ payStatus + ", hasComplain=" + hasComplain + ", payNumber="
				+ payNumber + ", coinCode=" + coinCode + ", userId=" + userId
				+ ", pageNo=" + pageNo + ", pageSize=" + pageSize + "]";
	}

	public Integer getTradeType() {
		return tradeType;
	}

	public void setTradeType(Integer tradeType) {
		this.tradeType = tradeType;
	}

	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public Integer getHasComplain() {
		return hasComplain;
	}

	public void setHasComplain(Integer hasComplain) {
		this.hasComplain = hasComplain;
	}

	public Long getPayNumber() {
		return payNumber;
	}

	public void setPayNumber(Long payNumber) {
		this.payNumber = payNumber;
	}

	public String getCoinCode() {
		return coinCode;
	}

	public void setCoinCode(String coinCode) {
		this.coinCode = coinCode;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

}
