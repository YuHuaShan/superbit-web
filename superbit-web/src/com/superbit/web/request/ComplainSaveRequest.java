package com.superbit.web.request;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

/**
 * 提交申诉请求
 * 
 * @author zhangdaoguang
 *
 */
public class ComplainSaveRequest implements Serializable {
	private static final long serialVersionUID = 376650863441413734L;
	/* 订单号 */
	private String orderNumber;
	/* 申诉人UID */
	private Integer uid;
	/* 申诉对象UID */
	private Integer complainUid;
	/* 申诉理由内容 */
	private String complainContent;

	public Boolean validate() {
		if (StringUtils.isBlank(orderNumber) || uid == null
				|| complainUid == null || StringUtils.isBlank(complainContent)) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public String toString() {
		return "ComplainSaveRequest [orderNumber=" + orderNumber + ", uid="
				+ uid + ", complainUid=" + complainUid + ", complainContent="
				+ complainContent + "]";
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public Integer getComplainUid() {
		return complainUid;
	}

	public void setComplainUid(Integer complainUid) {
		this.complainUid = complainUid;
	}

	public String getComplainContent() {
		return complainContent;
	}

	public void setComplainContent(String complainContent) {
		this.complainContent = complainContent;
	}

}
