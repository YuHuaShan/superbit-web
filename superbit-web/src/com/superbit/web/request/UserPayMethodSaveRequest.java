package com.superbit.web.request;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import com.superbit.core.constant.UserStatusConstant;
import com.superbit.web.utils.StringValidateUtil;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月20日 下午1:39:22  
  * @descrip 
  */
public class UserPayMethodSaveRequest implements Serializable{

	private static final long serialVersionUID = 161406594714102026L;
	/*真实姓名*/
	private String realName;
	/*支付账号：银行卡号，支付宝账号，微信账号*/
	private String payAccount;
	/*支付二维码(开户行)*/
	private String payQrCode;
	/*二维码存储地址(开户支行)*/
	private String payQrAddr;
	/*支付方式编号：0银行卡 1支付宝 2微信*/
	private Integer chanelID;
		
	public Boolean validate() {
		if (StringUtils.isBlank(realName) || !StringValidateUtil.isRealname(realName)) {
			return Boolean.FALSE;
		}

		if (StringUtils.isBlank(payAccount) || (chanelID.equals(UserStatusConstant.PAY_METHOD_CARD) && !StringValidateUtil.isBankCard(payAccount))) {
			return Boolean.FALSE;
		}
		
		if (StringUtils.isBlank(payAccount) || (chanelID.equals(UserStatusConstant.PAY_METHOD_ALI) && !StringValidateUtil.isAliAccount(payAccount))) {
			return Boolean.FALSE;
		}
		
		if (StringUtils.isBlank(payAccount) || (chanelID.equals(UserStatusConstant.PAY_METHOD_WX) && !StringValidateUtil.isWechatAccount(payAccount))) {
			return Boolean.FALSE;
		}
		
		if (chanelID < 0 || chanelID > 3) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
	
	
	
	@Override
	public String toString() {
		return "UserPayMethodSaveRequest [realName=" + realName + ", payAccount=" + payAccount + ", payQrCode="
				+ payQrCode + ", payQrAddr=" + payQrAddr + ", chanelID=" + chanelID + "]";
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getPayAccount() {
		return payAccount;
	}

	public void setPayAccount(String payAccount) {
		this.payAccount = payAccount;
	}

	public String getPayQrCode() {
		return payQrCode;
	}

	public void setPayQrCode(String payQrCode) {
		this.payQrCode = payQrCode;
	}

	public String getPayQrAddr() {
		return payQrAddr;
	}

	public void setPayQrAddr(String payQrAddr) {
		this.payQrAddr = payQrAddr;
	}

	public Integer getChanelID() {
		return chanelID;
	}

	public void setChanelID(Integer chanelID) {
		this.chanelID = chanelID;
	}
	
	
	
}
