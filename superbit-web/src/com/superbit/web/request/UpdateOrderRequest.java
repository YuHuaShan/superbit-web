package com.superbit.web.request;

import java.io.Serializable;
import org.apache.commons.lang.StringUtils;
import com.superbit.core.constant.TradeLogConstants;

/**
 * 更新订单请求
 * 
 * @author zhangdaoguang
 *
 */
public class UpdateOrderRequest implements Serializable {
	private static final long serialVersionUID = 6821972776733372235L;
	private String orderNumber;
	private Integer payStatus;
	private String fundPassword;

	public Boolean validate() {
		if (StringUtils.isBlank(orderNumber)) {
			return Boolean.FALSE;
		}

		if (!TradeLogConstants.PAY_STATUS_HAVE_PAY.equals(payStatus)
				&& !TradeLogConstants.PAY_STATUS_DONE.equals(payStatus)
				&& !TradeLogConstants.PAY_STATUS_BUY_CANCEL.equals(payStatus)) {
			return Boolean.FALSE;
		}

		// 放币必须填入资金密码
		if (TradeLogConstants.PAY_STATUS_DONE.equals(payStatus)
				&& StringUtils.isBlank(fundPassword)) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public String toString() {
		return "UpdateOrderRequest [orderNumber=" + orderNumber
				+ ", payStatus=" + payStatus + ", fundPassword=" + fundPassword
				+ "]";
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public String getFundPassword() {
		return fundPassword;
	}

	public void setFundPassword(String fundPassword) {
		this.fundPassword = fundPassword;
	}

}
