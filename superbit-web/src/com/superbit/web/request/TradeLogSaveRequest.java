package com.superbit.web.request;

import java.io.Serializable;

/**
 * 交易记录保存请求
 * 
 * @author zhangdaoguang
 *
 */
public class TradeLogSaveRequest implements Serializable {
	private static final long serialVersionUID = 147583789220903548L;
	/* 报价ID */
	private Long quoteId;
	/* 买家UID */
	private Integer buyserUid;
	/* 卖家UID */
	private Integer sellerUid;
	/* 交易类型 */
	private Integer tradeType;
	/* 币种Code */
	private String coinCode;
	/* 币种名称 */
	private String coinName;
	/* 价格CNY(单价) */
	private Double unitPrice;
	/* 数量BTC */
	private Double coinAmount;
	/* 金额CNY(总价) */
	private Double totalPrice;
	/* 交易方式： 记录id多个逗号分隔,0银行卡,1支付宝,2微信 */
	private String payType;

	@Override
	public String toString() {
		return "TradeLogSaveRequest [quoteId=" + quoteId + ", buyserUid="
				+ buyserUid + ", sellerUid=" + sellerUid + ", tradeType="
				+ tradeType + ", coinCode=" + coinCode + ", coinName="
				+ coinName + ", unitPrice=" + unitPrice + ", coinAmount="
				+ coinAmount + ", totalPrice=" + totalPrice + ", payType="
				+ payType + "]";
	}

	public Long getQuoteId() {
		return quoteId;
	}

	public void setQuoteId(Long quoteId) {
		this.quoteId = quoteId;
	}

	public Integer getBuyserUid() {
		return buyserUid;
	}

	public void setBuyserUid(Integer buyserUid) {
		this.buyserUid = buyserUid;
	}

	public Integer getSellerUid() {
		return sellerUid;
	}

	public void setSellerUid(Integer sellerUid) {
		this.sellerUid = sellerUid;
	}

	public Integer getTradeType() {
		return tradeType;
	}

	public void setTradeType(Integer tradeType) {
		this.tradeType = tradeType;
	}

	public String getCoinCode() {
		return coinCode;
	}

	public void setCoinCode(String coinCode) {
		this.coinCode = coinCode;
	}

	public String getCoinName() {
		return coinName;
	}

	public void setCoinName(String coinName) {
		this.coinName = coinName;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Double getCoinAmount() {
		return coinAmount;
	}

	public void setCoinAmount(Double coinAmount) {
		this.coinAmount = coinAmount;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

}
