 package com.superbit.web.excep;

public interface ErrorCodeConstant {
	
	static final String PARAMINVALID = "RT101"; //参数不合法      前端提示用户"参数不合法"
	static final String NOFITRECORD = "RT102"; //无匹配记录        前端提示用户"找不到匹配记录”
	static final String CONFIGERROR = "RT103"; //服务器内部错误      前端提示用户"服务器配置错误，请联系管理员"
	static final String SERVERERROR = "RT104"; //服务器内部错误      前端提示用户"服务器配置错误，请联系管理员"
	
	static final String NOTLOGIN = "WE101"; //无访问权限         前端提示用户"访问权限不足"  后跳转登陆页面
	static final String ACCESSLIMIT  = "WE102"; //用户访问受限   前端提示用户 "请绑定手机或谷歌验证码"，后跳转至绑定页面
	static final String IPLIMIT  = "WE103"; //用户IP不在允许范围内
	static final String ACCESSTOOBUSY  = "WE104"; //访问过于频繁   前端提示用户 "访问频率过快"
	static final String PAGENOTFOUND = "WE104"; //找不到对应页面    前端提示用户"找不到服务器资源"
	
	static final String NULLPOINTER = "RT301"; //空指针       
	static final String ARRAYOUTINDEX = "RT302"; //数组下边越界    
	static final String DATAFORMAT = "RT303"; //转换异常
	static final String MATHERROR = "RT304"; //数学运算错误
	
	static final String ERRORTHROW = "RT398"; //未处理抛出异常     
	static final String UNDEFINED  = "RT399"; //未定义异常    前端提示用户
	
	
	/*	
	 * 1开头和3开头的错误属于系统错误， 上文中有明确提示的进行提示， 无明确提示的统一提示 "未定义错误"
	 * 2开头的属于业务异常， 每种Code都有相应异常提示， 针对每一个URL需要分别处理， 请向开发人员索取文档
	 * */
	//资产
	static final String PARAMOUTOFRANGE  = "UA200"; //用户资产模块字段超出限定长度，或长度不匹配
	//提现
	static final String VERIFICATIONCODE  = "WD200"; //用户资产提现模块，验证码发送异常
	static final String INSUFFICIENTBALANCE  = "WD201"; //用户资产提现余额不足
	static final String FREEZEBALANCE  = "WD202"; //用户资产冻结余额不足
	static final String UNFREEZEBALANCE  = "WD203"; //用户资产解冻余额不足
	static final String FREEZDATE  = "WD204"; //用户修改密码导致资产解冻不能提现
	static final String WITHDRAWSTATUSERROR  = "WD205"; //提现状态修改数据校验不一致
	static final String WITHDRAWSTATUSFAIL  = "WD206"; //提现状态修改失败
	//币种信息
	static final String NOUNIQUE  = "CI200"; //coinCode或coinName不唯一
	static final String DELETCOIN  = "CI201"; //删除币种信息异常
	static final String UPDATECOIN  = "CI202"; //更新币种信息异常
	static final String EFFECTCOIN  = "CI203"; //币种状态修改失败（生效/不可用）
	static final String WITHDRAWMIN  = "CI204"; //小于币种提现最小额度
	static final String WITHDRAWMAX  = "CI205"; //大于币种提现最大额度
}
