package com.superbit.web.excep;

public class IPLimitException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7659740058423813903L;
	
	private String logUserId;
	
	private String localIp;

	public IPLimitException(String logUserId,String localIp) {
		this.logUserId = logUserId;
		this.localIp = localIp;
	}

	public String getLogUserId() {
		return logUserId;
	}

	public void setLogUserId(String logUserId) {
		this.logUserId = logUserId;
	}

	public String getLocalIp() {
		return localIp;
	}

	public void setLocalIp(String localIp) {
		this.localIp = localIp;
	}

	
}
