package com.superbit.web.excep;

/**
 * 用户无权限访问，一般为未登录，
 */
@SuppressWarnings("serial")
public class NoLoginException extends RuntimeException{
	
	private String uri;
	private String cmd;

	public NoLoginException(String uri, String cmd) {
		super(" user not login,URL: "+uri+"&cmd="+cmd, null);
		this.uri = uri;
		this.cmd = cmd;
	}
	
	public String getUri() {
		return uri;
	}
	public String getCmd() {
		return cmd;
	}
}
