package com.superbit.web.excep;

/**
 * 用户无权限访问，一般为未登录，
 */
@SuppressWarnings("serial")
public class AuthorityException extends RuntimeException{
	
	private String uri;
	private String cmd;
	private Integer userid;

	public AuthorityException(String uri, String cmd, Integer loginUserId) {
		super("user-"+loginUserId+" cannot access URL: "+uri+"&cmd="+cmd, null);
		this.uri = uri;
		this.cmd = cmd;
		this.userid = loginUserId;
	}
	
	public String getUri() {
		return uri;
	}
	public String getCmd() {
		return cmd;
	}
	public Integer getUserid() {
		return userid;
	}
}
