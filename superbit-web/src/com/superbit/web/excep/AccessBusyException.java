package com.superbit.web.excep;

@SuppressWarnings("serial")
public class AccessBusyException extends RuntimeException {

	private Integer longinUserId;
	private int accesscount;
	public AccessBusyException(Integer loginUserId,int count) {
		this.longinUserId = loginUserId;
		this.accesscount = count;
	}
	public Integer getLonginUserId() {
		return longinUserId;
	}
	public int getAccesscount() {
		return accesscount;
	}
}
