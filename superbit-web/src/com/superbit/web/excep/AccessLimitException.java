package com.superbit.web.excep;

/**
 * 用户访问有限制
 */
@SuppressWarnings("serial")
public class AccessLimitException extends RuntimeException {
	
	private int userid;
	private String uri;
	private String cmd;
	
	public AccessLimitException(int userid, String uri,String cmd) {
		this.userid = userid;
		this.uri = uri;
		this.cmd = cmd;
	}

	public int getUserid() {
		return userid;
	}

	public String getUri() {
		return uri;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
	
	
}
