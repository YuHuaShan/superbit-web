package com.superbit.web.utils;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.superbit.core.exception.ParamInvalidException;

public class StringValidateUtil {
	
	/**@function 字符串是小数时返回double，否则抛出异常
	 * @param str
	 * @param paramName
	 * @return
	 * @throws ParamInvalidException
	 */
	public static double strToDouble(String str,String paramName)throws ParamInvalidException{
		try{
			double dou = Double.parseDouble(str.trim());
			if (Double.isNaN(dou)||Double.isInfinite(dou))throw new Exception();
			return dou;
		}catch(Exception e){
			throw new ParamInvalidException(paramName, "Not Double");
		}
	}
	
	/**@function 字符串是整数时返回整数，否则抛出异常
	 * @param str
	 * @param paramName
	 * @return
	 * @throws ParamInvalidException
	 */
	public static long strToLong(String str,String paramName)throws ParamInvalidException{
		try{
			Long lou = Long.parseLong(str);
			return lou;
		}catch(Exception e){
			throw new ParamInvalidException(paramName, "Not Long");
		}
	}
	

	/**@function 字符串为空时直接抛出异常（包含字符串为"null")的情况
	 * @param str
	 * @throws ParamInvalidException
	 */
	public static void validateNotEmpty(String str,String paramName)throws ParamInvalidException{
		if(str==null || str.trim().equals("") || str.equals("null")) {
			throw new ParamInvalidException(paramName, "Not Allowed Null");
		}	
	}
	
	/**
	 * @function 字符串是否符合手机号格式
	 * @param str
	 * @return
	 */
	public static boolean isPhone(String str) {
		if(StringValidateUtil.isNull(str)) {
			return false;
		}
		if(str.length()<8 || str.length()>25) {
			return false;
		}
		if(StringValidateUtil.isLong(str)) {
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 判断对象是否为空
	 * @param str
	 * @return
	 */
	public static boolean isNull(Object str) {
		return str==null || str.toString().trim().equals("");
	}
	
	/**
	 * @function 判断字符串是否超长，集合
	 * @param length
	 * @param objs
	 */
	public static void validateLength(int length,String...objs){
		for (String obj : objs) {
			if(obj.length()>length){
				throw new ParamInvalidException("params", "Too Long");
			}
		}
	}
	
	/**
	 * @function 判断对象是否是Long类型
	 * @param str
	 * @return
	 */
	public static boolean isLong(String str) {
		try{
			validateIsLong(str, null);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	/**@function 判断是否是小数（包含整数）
	 * @param str
	 * @return
	 */
	public static boolean isDouble(String str){
		try{
			validateIsDouble(str, null);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	/**@function 字符串不是数字时（整数+小数），抛出异常
	 * @param str
	 * @throws ParamInvalidException
	 */
	public static void validateIsDouble(String str,String paramName)throws ParamInvalidException{
		try{
			Double.parseDouble(str);
		}catch(Exception e){
			throw new ParamInvalidException(paramName, "Not Double");
		}
	}
	/**@function 字符串不是Long类型时抛出异常
	 * @param str
	 * @throws ParamInvalidException
	 */
	public static void validateIsLong(String str, String paramName)throws ParamInvalidException {
		try{
			Long.parseLong(str);
		}catch(Exception e) {
			throw new ParamInvalidException(paramName, "Not Long");
		}
	}
	
	/**@function 字符串是整数时返回整数，否则抛出异常
	 * @param str
	 * @param paramName
	 * @return
	 * @throws ParamInvalidException
	 */
	public static Integer strToInt(String str, String paramName)throws ParamInvalidException {
		try{
			Integer inte = Integer.parseInt(str);
			return inte;
		}catch(Exception e) {
			throw new ParamInvalidException(paramName, "Not Int");
		}
	}
	
	
	/**@function 判断是否是整数
	 * @param str
	 * @return
	 */
	public static boolean isInteger(String str){
		try{
			validateIsInt(str, null);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	/**@function 字符串不是整数时抛出异常
	 * @param str
	 * @throws ParamInvalidException
	 */
	public static void validateIsInt(String str,String paramName)throws ParamInvalidException{
		try{
			Integer.parseInt(str);
		}catch(Exception e){
			throw new ParamInvalidException(paramName,"Not Int");
		}
	}
	
	/**@function 字符串是否符合邮件格式
	 * @param str
	 */
	public static boolean isEmail(String str){
		String pattern = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);
		return m.matches();
	}
	
	/**@function 字符串是否符合登录密码规则格式
	 * @param str
	 */
	public static boolean isLoginPassword(String str){
		String pattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[\\s\\S]{8,32}$";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);
		return m.matches();
	}
	
	/**@function 字符串是否符合资金密码规则格式,仅要求8-32位,不允许有空格
	 * @param str
	 */
	public static boolean isFundPassword(String str){
		String pattern = ".{8,32}";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);
		return m.matches();
	}
	
	/**@function 字符串是否符合昵称规则,仅要求1-16位，待修改
	 * @param str
	 */
	public static boolean isNickname(String str){
		if(str.isEmpty() || "".equals(str.trim()) || str.length()==0 || str.trim().length()>16){
			return false;
		}
		return true;
	}
	
	/**@function 真实姓名，字限10个
	 * @param str
	 */
	public static boolean isRealname(String str){
		if(str.isEmpty() || "".equals(str.trim()) || str.length()==0 || str.trim().length()>10){
			return false;
		}
		return true;
	}
	
	/**@function 银行卡号，字限20个
	 * @param str
	 */
	public static boolean isBankCard(String str){
		if(str.isEmpty() || "".equals(str.trim()) || str.length()==0 || str.trim().length()>20){
			return false;
		}
		return true;
	}
	
	/**@function 支付宝账号，字限16个
	 * @param str
	 */
	public static boolean isAliAccount(String str){
		if(str.isEmpty() || "".equals(str.trim()) || str.length()==0 || str.trim().length()>16){
			return false;
		}
		return true;
	}
	
	/**@function 微信账号，字限16个
	 * @param str
	 */
	public static boolean isWechatAccount(String str){
		if(str.isEmpty() || "".equals(str.trim()) || str.length()==0 || str.trim().length()>16){
			return false;
		}
		return true;
	}
	
	
}
