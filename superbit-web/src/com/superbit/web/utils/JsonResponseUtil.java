package com.superbit.web.utils;

import com.superbit.core.exception.BusinessException;

import net.sf.json.JSONObject;

public class JsonResponseUtil {
	
	
	
	/**
	 * @param data
	 * @return
	 */
	public static JSONObject buildSucessResonpe(Object data){
		JSONObject result = new JSONObject();
		result.put("success", true);
		result.put("status", 200);
		result.put("data", data);
		return result;
	}
	
	/**
	 * @param data
	 * @param count 返回符合条件的记录个数
	 * @return
	 */
	public static JSONObject buildSucessResonpe(Object data,int count){
		JSONObject result = new JSONObject();
		result.put("success", true);
		result.put("data", data);
		result.put("count", count);
		return result;
	}
	
	
	/**
	 * @param exp
	 * @return
	 */
	public static JSONObject buildBusiExcepResonpe(BusinessException exp){
		JSONObject result = new JSONObject();
		result.put("success", false);
		result.put("code", exp.getErrorcode());
		result.put("message", exp.getMessage());
		return result;
	}
	
	/**
	 * @param code
	 * @param message
	 * @return
	 */
	public static JSONObject buildNormalExcepResonpe(String code,String message){
		JSONObject result = new JSONObject();
		result.put("success", false);
		result.put("code", code);
		result.put("message", message);
		return result;
	}
}
