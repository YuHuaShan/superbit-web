package com.superbit.web.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.superbit.core.exception.ParamInvalidException;



/**
 * 处理分页
 */
public class PageInfoObjct {
	
	private int start;
	private int sizeperpage;
	private int currentpage;
	private int count;
	private int maxpage;
	
	/**创建PageInfo对象
	 * @param page
	 * @param size
	 * @return
	 */
	public static PageInfoObjct getPageInfo(String page,String size){
		if(StringUtils.isEmpty(page)){
			page = "1";
		}
		if(StringUtils.isEmpty(size)){
			size = "20";
		}
		int currentpage = StringValidateUtil.strToInt(page, "当前页");
		int sizeperpage = StringValidateUtil.strToInt(size, "每页个数");
		if(sizeperpage<=0||currentpage<=0){
			throw new ParamInvalidException("page", "不符合规范");
		}
		int startnum = (currentpage-1)*sizeperpage;
		PageInfoObjct p = new PageInfoObjct();
		p.setCurrentpage(currentpage);
		p.setSizeperpage(sizeperpage);
		p.setStart(startnum);
		return p;
	}
	
	/**初始化count方法
	 * @param count
	 */
	public void initCount(int count){
		this.count = count;
		this.maxpage = ((count%sizeperpage==0)?0:1) + count/sizeperpage;
	}
	
	/**组装Page页面信息
	 * @param req
	 * @param pagParamName
	 * @param sizeParamName
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map<String, Object> buildPageMetaData(HttpServletRequest req, String pagParamName, String sizeParamName) {
		if(this.count<=this.sizeperpage&&this.currentpage==1){
			//当数据只有一页时，不返回分页信息
			return null;
		}
		int page = currentpage;
		
		String uri = req.getRequestURI();
		Map<String,String[]> requestmap = new HashMap(req.getParameterMap());
		requestmap.remove(pagParamName);
		requestmap.remove(sizeParamName);
		StringBuffer sb = new StringBuffer(uri).append("?");
		for(String key:requestmap.keySet()){
			String[] values = requestmap.get(key);
			for(int i=0;i<values.length;i++){				
				sb.append(key+"="+values[i]+"&");
			}
		}
		String baseUrl = sb.toString();
		
		Map<String,Object> pageMetaData = new HashMap<String,Object>(); 
		if(page>1){
			String url = baseUrl+pagParamName+"="+(page-1)+"&"+sizeParamName+"="+sizeperpage;
			pageMetaData.put("per_url", url);
		}
		if(page<maxpage){
			String url = baseUrl+pagParamName+"="+(page+1)+"&"+sizeParamName+"="+sizeperpage;
			pageMetaData.put("next_url", url);
		}
		List<Map<String,Object>> links = new ArrayList<Map<String,Object>>();
		if(page>2){
			String url = baseUrl+pagParamName+"="+1+"&"+sizeParamName+"="+sizeperpage;
			Map<String,Object> m = CollectionUtils.buildMap("is_active",false,"number",1,"url",url);
			links.add(m);
		}
		if(page>3){
			links.add(null);
		}
		if(page>1){			
			String url1 = baseUrl+pagParamName+"="+(page-1)+"&"+sizeParamName+"="+sizeperpage;
			Map<String,Object> m1 = CollectionUtils.buildMap("is_active",false,"number",page-1,"url",url1);
			links.add(m1);
		}
		String url2 = baseUrl+pagParamName+"="+(page)+"&"+sizeParamName+"="+sizeperpage;
		Map<String,Object> m2 = CollectionUtils.buildMap("is_active",true,"number",page,"url",url2);
		links.add(m2);
		if(page<maxpage){			
			String url3 = baseUrl+pagParamName+"="+(page+1)+"&"+sizeParamName+"="+sizeperpage;
			Map<String,Object> m3 = CollectionUtils.buildMap("is_active",false,"number",page+1,"url",url3);
			links.add(m3);
		}
		if(page<maxpage-2){
			links.add(null);
		}
		if(page<maxpage-1){
			String url = baseUrl+pagParamName+"="+maxpage+"&"+sizeParamName+"="+sizeperpage;
			Map<String,Object> m = CollectionUtils.buildMap("is_active",false,"number",maxpage,"url",url);
			links.add(m);
		}
		pageMetaData.put("page_links", links);
		return pageMetaData;
	}
	
	
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getSizeperpage() {
		return sizeperpage;
	}
	public void setSizeperpage(int sizeperpage) {
		this.sizeperpage = sizeperpage;
	}
	public int getCurrentpage() {
		return currentpage;
	}
	public void setCurrentpage(int currentpage) {
		this.currentpage = currentpage;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
}
