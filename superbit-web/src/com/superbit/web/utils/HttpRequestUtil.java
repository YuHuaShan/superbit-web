package com.superbit.web.utils;

import javax.servlet.http.HttpServletRequest;

public class HttpRequestUtil {

	/**
	 * 获取真实URI，防止斜杠注入
	 * @param uri
	 * @return
	 */
	public static String getActualURI(HttpServletRequest req){
		String uri = req.getRequestURI().substring(req.getContextPath().length());
		String[] uris = uri.split("/");
		String actualURI = "";
		for (String string : uris) {
			if(!isBlank(string)){
				actualURI += "/" + string;
			}
		}
		return actualURI.toString();
	}
	
	private static boolean isBlank(String str){
		if(null == str){
			return true;
		}
		if(str.trim().equals("")){
			return true;
		}
		if(str.contains("\t")||str.contains("\n")||str.contains("\f")||str.contains("\r")){
			return true;
		}
		return false;
	}

}
