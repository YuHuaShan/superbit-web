package com.superbit.web.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import com.superbit.model.feature.AuthortyService;

public class SessionUserUtil {
	
	//SessionKey
	private static final String userInfoKey = "UserInfo";
	
	//存放多个用户的session对象
	private static Map<String,Map<String,Object>> sessionMap = new HashMap<String,Map<String,Object>>();
	
	/**更新Session中用户信息
	 * @param session
	 * @param userInfo
	 */
	public static void updateUserInfo(HttpSession session,Map<String,Object> userInfo){
		session.setAttribute(userInfoKey, userInfo);
		String userId = userInfo.get("id")+"";
		Map<String,Object> map = sessionMap.get(userId);
		if(map==null){
			map = new HashMap<String,Object>();
			sessionMap.put(userId,map);
		}
		if(!map.containsKey(session.getId())){
			map.put(session.getId(), session);
		}
	}
	

	/**在session时效时，移除session的引用
	 * @param userId
	 * @param sessionID
	 */
	public static void removeUserSession(String userId,String sessionID){
		Map<String,Object> map = sessionMap.get(userId);
		if(map!=null){
			map.remove(sessionID);
			if(map.size()==0){
				sessionMap.remove(userId);
			}
		}
	}
	
	/**更新一个用户同时登录的多个session的值
	 * @param userId
	 * @param key
	 * @param value
	 */
	@SuppressWarnings("unchecked")
	public static void updateUserSession(int userid,String key,Object value){
		Map<String,Object> map = sessionMap.get(userid+"");
		if(map!=null){
			for(Object obj:map.values()){
				try{
					HttpSession hs = (HttpSession)obj;
					Map<String,Object> userinfo =  (Map<String,Object>)hs.getAttribute(userInfoKey);
					if(userinfo!=null){
						userinfo.put(key, value);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void removeUserSession(String userid){
		Map<String,Object> map = sessionMap.get(userid);
		if(map==null){
			return;
		}
		for (Entry<String,Object> entry : map.entrySet()) {
			HttpSession session = (HttpSession) entry.getValue();
			if(session != null)
				session.invalidate();
		}
		map.remove(userid);
	}
	
	@SuppressWarnings("unchecked")
	public static void sendUpdateUserMessage(int userid,String key,Object value,HttpSession httpSession){
		if(httpSession!=null&&userid == getLoginUserId(httpSession)){
			Map<String,Object> userinfo =  (Map<String,Object>)httpSession.getAttribute(userInfoKey);
			if(userinfo!=null){
				userinfo.put(key, value);
			}
		}
		//向相同登陆用户的Session发出同步消息
		updateUserSession(userid,key,value);
		/*
		 * 集群状态时，向MQ中的Topic发送相关消息
		 * */
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getUserInfo(HttpSession session){
		Object obj = session.getAttribute(userInfoKey);
		if(null == obj){
			return null;
		}else{			
			Map<String,Object> userInfo = (Map<String,Object>)obj;
			return userInfo;
		}
	}
	
	/**返回是否登录
	 * @param sesison
	 * @return
	 */
	public static boolean isLogin(HttpSession session){
		return getUserInfo(session)!=null;
	}
	
	/**返回登录角色
	 * @param sesison
	 * @return
	 */
	public static String[] getLoginRole(HttpSession session){
		Map<String,Object> map = getUserInfo(session);
		if(map==null){
			return new String[]{AuthortyService.ROLE_NOLOGIN}; 
		}else{
			String[] role = (String[])map.get("roles");
			if(role==null||role.length==0){
				return new String[]{AuthortyService.ROLE_USER};
			}else{
				String[] roles = new String[role.length+1];
				for(int i=0;i<role.length;i++){
					roles[i] = role[i];
				}
				roles[role.length] = AuthortyService.ROLE_USER;
				return roles;
			}
		}
	}
	
	/**返回登录ID
	 * @param sesison
	 * @return
	 */
	public static Integer getLoginUserId(HttpSession session){
		Map<String,Object> map = getUserInfo(session);
		if(map==null){
			return null; 
		}else{
			Integer userid = (Integer)map.get("id");
			if(userid==null){
				return null;
			}else{
				return userid;
			}
		}
	}
	
	/**返回登录用户名
	 * @param sesison
	 * @return
	 */
	public static String getLoginName(HttpSession session){
		Map<String,Object> map = getUserInfo(session);
		if(map==null){
			return null; 
		}else{
			String name = (String)map.get("name");
			if(name==null){
				return null;
			}else{
				return name;
			}
		}
	}
	
	/**是否处于充值未绑定状态
	 * @param session
	 * @return
	 */
	public static boolean isUnBindStatus(HttpSession session){
		Map<String,Object> map = getUserInfo(session);
		if(map==null){
			return false; 
		}else{
			Boolean unbind = (Boolean)map.get("unbinding");
			if(unbind==null){
				return false;
			}else{
				return unbind;
			}
		}
	}
	
	/**获取安全验证方式
	 * @param session
	 * @return
	 */
	public static int getAuthType(HttpSession session){
		Map<String,Object> map = getUserInfo(session);
		if(map==null){
			return -1; 
		}else{
			Integer authtype = (Integer)map.get("authtype");
			if(authtype==null){
				return -1;
			}else{
				return authtype;
			}
		}
	}
}
