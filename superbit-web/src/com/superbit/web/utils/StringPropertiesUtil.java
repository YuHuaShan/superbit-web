package com.superbit.web.utils;

import java.util.UUID;

import javax.servlet.http.HttpSession;

public class StringPropertiesUtil {
	/**
	 * 生成32位uuid
	 * @return
	 */
	public static String createIdUtil() {
		return createIdUtil(32);
	}
	/**
	 * 生成指定长度uuid
	 * @param length
	 * @return
	 */
	public static String createIdUtil(int length) {
		length = length>0?length:8;
		return UUID.randomUUID().toString().replaceAll("-", "").substring(0, length);
	}
	/**
	 * 获取用户id，
	 * @param session
	 * @return
	 */
	public static Integer getUserId(HttpSession session) {
		Integer userId = SessionUserUtil.getLoginUserId(session);
		if(userId == null) {
			userId = 1;//临时测试使用
		}
		return userId;
	}
}
