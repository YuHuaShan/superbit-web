package com.superbit.web.utils;

import java.beans.PropertyDescriptor;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

public class CollectionUtils {
	
	@SuppressWarnings("unchecked")
	public static <T> Map<String,T> buildMap(Object ... objs){
		Map<String,T> map = new LinkedHashMap<String,T>();
		if(objs!=null){
			int size = objs.length/2;
			for(int i=0;i<size;i++){
				Object key = objs[i*2];
				T value = (T) objs[i*2+1];
				if(key!=null){
					map.put(key.toString(), value);
				}
			}
		}
		return map;
	}
	public static void bean2map(Map<String,Object> m,Object obj){
		BeanWrapper beanWrapper = new BeanWrapperImpl(obj);
		PropertyDescriptor[] descriptor = beanWrapper.getPropertyDescriptors();
		for (int i = 0; i < descriptor.length; i++) {
			m.put(descriptor[i].getName(),beanWrapper.getPropertyValue(descriptor[i].getName()));
		}
	}
}
