package com.superbit.web.listener;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
public class AccessListener implements HttpSessionListener {

	private static Map<String,HttpSession> sessions = new HashMap<String,HttpSession>();
	
	
	@Override
	public void sessionCreated(HttpSessionEvent se) {
//		System.out.println("sessionCreated");
		HttpSession session = se.getSession();
		sessions.put(se.getSession().getId(), session);
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
//		HttpSession session = se.getSession();
//		Integer userid = SessionUserUtil.getLoginUserId(session);
//		if(userid!=null){
//			SessionUserUtil.removeUserSession(userid+"", session.getId());
//		}
//		long time1 = session.getCreationTime();
//		long time2 = new Date().getTime();
//		int second = (int)(time2-time1)/1000;
//		if(userid==null){
//			logAccess4NoUser(time1,second);
//		}else{
//			logAccess4LoginUser(time1,second,userid,SessionUserUtil.getLoginName(session));
//		}
	}

//	private void logAccess4LoginUser(long createtime, long durationtime, Integer userid, String loginName) {
//		MessageLogService	messageLogService = ServiceBeanFactory.getBean(MessageLogService.class);
//		messageLogService.logUserAccess(new Date(createtime),durationtime,userid,loginName);
//	}
//
//	private void logAccess4NoUser(long createtime, int durationtime) {
//		MessageLogService	messageLogService = ServiceBeanFactory.getBean(MessageLogService.class);
//		messageLogService.logUserAccess(new Date(createtime),durationtime,null,null);
//	}
	
	public static HttpSession  getSession(String sessionId){
		return sessions.get(sessionId);
	}
}
