package com.superbit.web.listener;

import java.util.TimeZone;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class TimeZoneSetListener implements ServletContextListener{
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		String imezone = sce.getServletContext().getInitParameter("timeZone");
		if(imezone==null){
			imezone = "GMT";
		}
		//设置默认时区
		TimeZone.setDefault(TimeZone.getTimeZone(imezone));
		System.out.println("设定默认时区为："+imezone);
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
	}

}
