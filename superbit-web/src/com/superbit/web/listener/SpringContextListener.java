package com.superbit.web.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

public class SpringContextListener extends ContextLoaderListener {
	private Log logger = LogFactory.getLog(this.getClass());

	@Override
	public void contextInitialized(ServletContextEvent event) {
		logger.info("加载SpringBean模块");
		//配置spring文件地址
		ServletContext sc = event.getServletContext();
		sc.setInitParameter("contextConfigLocation", "classpath*:configs/bean/*.xml");
		//加载父类方法
		super.contextInitialized(event);
		//将上下文对象放入新的静态工具类
		String key = WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE;
		ApplicationContext ac = (ApplicationContext)sc.getAttribute(key);
		ServiceBeanFactory.intApplicationContext(ac);
		//从Application中移除上下文的引用
		sc.removeAttribute(key);
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		logger.info("销毁SpringBean模块");
		ServiceBeanFactory.destoryApplilcationContext();
		super.contextDestroyed(event);
	}
}
