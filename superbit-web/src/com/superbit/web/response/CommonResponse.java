package com.superbit.web.response;

import java.io.Serializable;

import com.superbit.web.excep.ErrorCodeConstant;

/**
 * 通用返回响应
 * 
 * @author zhangdaoguang
 */
public class CommonResponse<T> implements Serializable {
	private static final long serialVersionUID = 839724278530442183L;

	private Boolean success;
	private String code;
	private String message;
	private T data;
	private Integer count;

	public static <T> CommonResponse<T> createSuccess() {
		CommonResponse<T> commonResponse = new CommonResponse<>();
		commonResponse.setSuccess(Boolean.TRUE);
		return commonResponse;
	}

	public static <T> CommonResponse<T> createSuccess(T data) {
		CommonResponse<T> commonResponse = new CommonResponse<>();
		commonResponse.setSuccess(Boolean.TRUE);
		commonResponse.setData(data);
		return commonResponse;
	}

	public static <T> CommonResponse<T> createPageSuccess(T data, Integer count) {
		CommonResponse<T> commonResponse = new CommonResponse<>();
		commonResponse.setSuccess(Boolean.TRUE);
		commonResponse.setData(data);
		commonResponse.setCount(count);
		return commonResponse;
	}

	public static <T> CommonResponse<T> createFail(String code, String message) {
		CommonResponse<T> commonResponse = new CommonResponse<>();
		commonResponse.setSuccess(Boolean.FALSE);
		commonResponse.setCode(code);
		commonResponse.setMessage(message);
		return commonResponse;
	}

	public static <T> CommonResponse<T> createFail(String code) {
		CommonResponse<T> commonResponse = new CommonResponse<>();
		commonResponse.setSuccess(Boolean.FALSE);
		commonResponse.setCode(code);
		commonResponse.setMessage("");
		return commonResponse;
	}

	public static <T> CommonResponse<T> paramError() {
		CommonResponse<T> commonResponse = new CommonResponse<>();
		commonResponse.setSuccess(Boolean.FALSE);
		commonResponse.setCode(ErrorCodeConstant.PARAMINVALID);
		commonResponse.setMessage("param illegal");
		return commonResponse;
	}

	public void setSystemError() {
		this.setSuccess(Boolean.FALSE);
		this.setCode(ErrorCodeConstant.SERVERERROR);
	}

	public static <T> CommonResponse<T> createSystemError() {
		CommonResponse<T> commonResponse = new CommonResponse<>();
		commonResponse.setSuccess(Boolean.FALSE);
		commonResponse.setCode(ErrorCodeConstant.SERVERERROR);
		commonResponse.setMessage("system error");
		return commonResponse;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}
