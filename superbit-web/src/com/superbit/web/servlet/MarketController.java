package com.superbit.web.servlet;



import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.superbit.core.exception.BusinessException;
import com.superbit.core.service.MarketService;
import com.superbit.core.vo.CoinTypeVO;
import com.superbit.web.listener.ServiceBeanFactory;
import com.superbit.web.utils.JsonResponseUtil;
/**
 * 市场及行情相关controller层
 * @author Administrator
 *
 */

@Controller
@RequestMapping("/market.act")
public class MarketController {
	
	private MarketService marketService;
	
	@PostConstruct
	private void init(){
	 marketService = ServiceBeanFactory.getBean(MarketService.class);
	}
	/**
	 * 获取市场币种详情
	 * @param sortName  排序名称
	 * @param sortType  排序方式 
	 * @return
	 */
	@RequestMapping(params = "cmd=getMarketCoinInfo")
	@ResponseBody
	public String getMarketCoinInfo(HttpServletRequest request){
		List<CoinTypeVO> list= new ArrayList<CoinTypeVO>();
		try{
		 list= this.marketService.getCoinMarketInfo();
		}catch(BusinessException e){
			JsonResponseUtil.buildBusiExcepResonpe(e);
		}
		//TODO 统一规范返回值格式
		return JsonResponseUtil.buildSucessResonpe(list).toString();
	}
	
}
