package com.superbit.web.servlet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.superbit.core.service.UserPayMethodService;
import com.superbit.core.service.UserRealAuthenService;
import com.superbit.web.listener.ServiceBeanFactory;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月20日 下午4:28:15  
  * @descrip 
  */
@Controller
@RequestMapping("/userRealAuthen.act")
public class UserRealAuthenController {

	private UserRealAuthenService realAuthenService;
	
	/**
	 * @function 构造器注入service对象
	 */
	@PostConstruct
	private void init(){
		realAuthenService = ServiceBeanFactory.getBean(UserRealAuthenService.class);
	}
	
	
	/**
	 * 
	 * 
	 */
}
