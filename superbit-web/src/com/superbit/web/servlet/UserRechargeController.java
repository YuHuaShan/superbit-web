package com.superbit.web.servlet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.superbit.core.entry.Coin_Base_Info;
import com.superbit.core.entry.User_Asset;
import com.superbit.core.entry.User_Recharge;
import com.superbit.core.tools.PageListRes;
import com.superbit.service.coin.define.Coin_Base_InfoService;
import com.superbit.service.userasset.service.define.User_AssetService;
import com.superbit.service.userrecharge.define.User_RechargeService;
import com.superbit.web.excep.ErrorCodeConstant;
import com.superbit.web.listener.ServiceBeanFactory;
import com.superbit.web.utils.JsonResponseUtil;
import com.superbit.web.utils.StringPropertiesUtil;
/**
 * 用户充值
 * @author hongzhen
 *
 */
@Controller
@RequestMapping("/user_recharge.act")
public class UserRechargeController {
	
	private static Logger logger = LoggerFactory.getLogger(UserRechargeController.class);
	
	
	private User_RechargeService user_RechargeService;
	private User_AssetService user_AssetService;
	private Coin_Base_InfoService coin_Base_InfoService;
	@PostConstruct
	private void init(){
		user_RechargeService = ServiceBeanFactory.getBean(User_RechargeService.class);
		user_AssetService = ServiceBeanFactory.getBean(User_AssetService.class);
		coin_Base_InfoService = ServiceBeanFactory.getBean(Coin_Base_InfoService.class);
	}
	
	/**
	 * 获取充值地址
	 * @param session
	 * @param req
	 * @param assetId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=getRechargeAddress")
	public String getRechargeAddress(HttpSession session, HttpServletRequest req, 
			String assetId){
		logger.info("cmd=getRechargeAddress");
		Integer userId = StringPropertiesUtil.getUserId(session);
		//TODO 0 参数判断，assetId长度为32
		if(assetId.length()!=32) {
			return JsonResponseUtil.buildNormalExcepResonpe(ErrorCodeConstant.PARAMOUTOFRANGE, "assetId invalid").toString(); 
		}
		//TODO 1 读取资产信息，获取充值地址
		User_Asset ua = user_AssetService.getUser_Asset(userId, assetId, null, null);
		String address = "";
		if(ua.getRecharge_address()==null) {
			//TODO 1.1 如果获取不到充值地址，则发送mq消息
			user_RechargeService.sendRechargeAddressMessage(userId, ua.getCoinId());
//			sendMQmessage
			try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//TODO 1.2 读取MQ消息，update入资产信息
//			getMQmessage
			
//			user_AssetService.updateUser_Asset(ua.getId(), addressMap); //保存用户充值地址
			address = "xxxxxxxxxxx";
			Map<String, Object> addressMap = new HashMap<String, Object>();
			addressMap.put("recharge_address", address);
			

		}else {
			address = ua.getRecharge_address();
		}
		
		Coin_Base_Info coin_Base_Info = coin_Base_InfoService.getCoin_Base_InfoList(3, ua.getCoinId(), null, null).get(0);
		Map<String, String> map = new HashMap<String, String>();
		map.put("address", address);
		map.put("rechargeDesc", coin_Base_Info.getCoinRechargeDesc());
		
		
		return JsonResponseUtil.buildSucessResonpe(map).toString();
	}
	
	
	
	/**
	 * 获取充值记录
	 * @param session
	 * @param req
	 * @param coinId
	 * @param status
	 * @param stime
	 * @param etime
	 * @param pageNo
	 * @param pageSize
	 * @param order
	 * @return
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=getRechargeList")
	public String getRechargeList(HttpSession session, HttpServletRequest req,
			String coinId,
			int status,
			String stime,
			String etime,
			int pageNo,
			int pageSize,
			String order
			){
		logger.info("cmd=getRechargeList");
		Integer userId = StringPropertiesUtil.getUserId(session);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date sDate = null;
		Date eDate = null;
		try {
			sDate = stime.length()==19?simpleDateFormat.parse(stime):null;
			eDate = etime.length()==19?simpleDateFormat.parse(etime):null;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PageListRes<User_Recharge> result = new PageListRes<User_Recharge>();
		result.setPageSize(pageSize);
		result.setPageNo(pageNo);
		result.setTotal(user_RechargeService.getUser_RechargeCount(userId, coinId, status, sDate, eDate));
		List<User_Recharge> uRecharges = null;
		if(result.getTotal()>0) {
			uRecharges = user_RechargeService.getUser_Recharge(userId, coinId, status, sDate, eDate, (pageNo-1)*pageSize, pageSize, order);
		}
		result.setList(uRecharges);
		return JsonResponseUtil.buildSucessResonpe(result).toString();
	}
	
	

}
