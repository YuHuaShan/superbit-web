//package com.superbit.web.servlet;
//
//import javax.annotation.PostConstruct;
//import javax.servlet.http.HttpServletRequest;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import com.superbit.core.entry.QuoteOrderRecord;
//import com.superbit.service.example.define.ExampleService;
//import com.superbit.web.listener.ServiceBeanFactory;
//import com.superbit.web.utils.JsonResponseUtil;
//
//@Controller
//@RequestMapping("/test.act")
//public class ExampleController {
//	@Autowired
//	@Qualifier("exampleService")
//	private ExampleService boardService;
//	
//	@PostConstruct
//	private void init(){
//		boardService = ServiceBeanFactory.getBean(ExampleService.class);
//	}
//	
//	/**
//	 * 测试使用
//	 * @param request
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(params = "cmd=test1")
//	public String listBoard(HttpServletRequest request, int id){
//		QuoteOrderRecord qor = boardService.getExampleRecord(id);
//		return JsonResponseUtil.buildSucessResonpe(qor).toString();
//	}
//}
