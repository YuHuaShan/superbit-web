package com.superbit.web.servlet;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.superbit.core.entry.Common_Platform;
import com.superbit.core.entry.User_Withdraw_Plat;
import com.superbit.service.platform.define.Common_PlatformService;
import com.superbit.service.withdraw.plat.define.User_Withdraw_PlatService;
import com.superbit.web.excep.ErrorCodeConstant;
import com.superbit.web.listener.ServiceBeanFactory;
import com.superbit.web.utils.JsonResponseUtil;
import com.superbit.web.utils.StringPropertiesUtil;

@Controller
@RequestMapping("/plat.act")
public class UserWithDrawPlatController {
	
	private static Logger logger = LoggerFactory.getLogger(UserWithDrawPlatController.class);
	
	
	private User_Withdraw_PlatService user_Withdraw_PlatService;
	private Common_PlatformService common_PlatformService;
	
	@PostConstruct
	private void init(){
		user_Withdraw_PlatService = ServiceBeanFactory.getBean(User_Withdraw_PlatService.class);
		common_PlatformService = ServiceBeanFactory.getBean(Common_PlatformService.class);
	}
	/**
	 * 添加用户自定义平台
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=addUser_Withdraw_Plat")
	public String addUser_Withdraw_Plat(HttpSession session, HttpServletRequest request,
			String platformName,
			String assetId){
		logger.info("addUser_Withdraw_Plat");
		
		if(platformName.length()>7) {
			return JsonResponseUtil.buildNormalExcepResonpe(ErrorCodeConstant.PARAMOUTOFRANGE, "platformName out of range").toString(); 
		}
		if(assetId.length()!=32) {
			return JsonResponseUtil.buildNormalExcepResonpe(ErrorCodeConstant.PARAMOUTOFRANGE, "coinId invalid").toString(); 
		}
		Integer userId = StringPropertiesUtil.getUserId(session);
		
		User_Withdraw_Plat uwp = new User_Withdraw_Plat();
		uwp.setCreateTime(new Date());
		uwp.setUserId(userId);
		uwp.setAssetId(assetId);
		uwp.setPlatformName(platformName);
		uwp.setPlatformId(StringPropertiesUtil.createIdUtil());
		
		boolean flag = user_Withdraw_PlatService.addUser_Withdraw_Plat(uwp);
		
//		根据返回结果对前端做不同响应动作
		
		if(flag) {
			return JsonResponseUtil.buildSucessResonpe("ok").toString();
		}else {
			return JsonResponseUtil.buildSucessResonpe("error").toString();
		}
	}
	/**
	 * 删除用户自定义平台
	 * @param session
	 * @param request
	 * @param platformId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=deleteUser_Withdraw_Plat")
	public String deleteUser_Withdraw_Plat(HttpSession session, HttpServletRequest request,
			String platformId
			){
		logger.info("deleteUser_Withdraw_Plat");
		if(platformId.length()!=32) {
			return JsonResponseUtil.buildNormalExcepResonpe(ErrorCodeConstant.PARAMOUTOFRANGE, "platformId invalid").toString(); 
		}
		boolean result = user_Withdraw_PlatService.deleteUser_Withdraw_Plat(platformId);

		return JsonResponseUtil.buildSucessResonpe(result).toString();
	}
	
	/**
	 * 获取用户自定义平台
	 * @param session
	 * @param request
	 * @param assetId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=getUser_Withdraw_PlatList")
	public String getUser_Withdraw_PlatList(HttpSession session, HttpServletRequest request,
			String assetId){
		logger.info("getUser_Withdraw_PlatList");
		
		Integer userId = StringPropertiesUtil.getUserId(session);
		if(assetId.length()!=32) {
			return JsonResponseUtil.buildNormalExcepResonpe(ErrorCodeConstant.PARAMOUTOFRANGE, "coinId invalid").toString(); 
		}
		List<User_Withdraw_Plat> result = user_Withdraw_PlatService.getUser_Withdraw_PlatList(userId, assetId);

		return JsonResponseUtil.buildSucessResonpe(result).toString();
	}
	
	
	/**
	 * 获取所有提现平台
	 * @param session
	 * @param request
	 * @param assetId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=getWithdraw_PlatList")
	public String getWithdraw_PlatList(HttpSession session, HttpServletRequest request,
			String assetId){
		logger.info("getWithdraw_PlatList");
		//用户自定义提现平台
		Integer userId = StringPropertiesUtil.getUserId(session);
		if(assetId.length()!=32) {
			return JsonResponseUtil.buildNormalExcepResonpe(ErrorCodeConstant.PARAMOUTOFRANGE, "coinId invalid").toString(); 
		}
		List<User_Withdraw_Plat> result = user_Withdraw_PlatService.getUser_Withdraw_PlatList(userId, assetId);
		//公共提现平台
		List<Common_Platform> common_Platforms = common_PlatformService.getCommon_Platforms();
		Map<String, Object> results = new HashMap<String,Object>();
		results.put("user_withdraw", result);
		results.put("common_withdraw", common_Platforms);
		
		return JsonResponseUtil.buildSucessResonpe(results).toString();
	}
}
