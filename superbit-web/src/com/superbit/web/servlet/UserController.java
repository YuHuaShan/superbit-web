/*package com.superbit.web.servlet;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.superbit.core.constant.UserStatusConstant;
import com.superbit.core.entry.UserActivityInfo;
import com.superbit.core.entry.UserBaseInfo;
import com.superbit.core.entry.UserPayMethod;
import com.superbit.core.entry.UserSafeInfo;
import com.superbit.core.exception.BusinessException;
import com.superbit.core.exception.ParamInvalidException;
import com.superbit.core.service.LoginService;
import com.superbit.core.service.RegisterService;
import com.superbit.core.service.TradeLogProxy;
import com.superbit.core.service.UserAssetService;
import com.superbit.core.service.UserPayMethodService;
import com.superbit.core.service.UserRealAuthenService;
import com.superbit.core.service.UserRoleService;
import com.superbit.core.service.UserSafeService;
import com.superbit.service.util.StringTools;
import com.superbit.utils.constantutil.CollectionUtils;
import com.superbit.utils.mailutil.EmailFailException;
import com.superbit.web.excep.ErrorCodeConstant;
import com.superbit.web.listener.ServiceBeanFactory;
import com.superbit.web.utils.CheckLoginMode;
import com.superbit.web.utils.CommonUtil;
import com.superbit.web.utils.JsonResponseUtil;
import com.superbit.web.utils.PictureCheckCode;
import com.superbit.web.utils.SessionUserUtil;
import com.superbit.web.utils.StringValidateUtil;

import net.sf.json.JSONObject;

*//** @author  WangZhenwei 
  * @date 创建时间：2018年1月5日 下午5:35:35  
  * @descrip 用户相关的所有controller层方法
  *//*
@Controller
@RequestMapping("/user.act")
public class UserController {
	
	private static Logger log = LoggerFactory.getLogger(TradeLogController.class);
			
	
	private RegisterService registerService; //不需要加Autowired注解
	private LoginService loginService; //不需要加Autowired注解
	private UserRoleService userRoleService; //不需要加Autowired注解
	private UserSafeService userSafeService; //不需要加Autowired注解
	private UserAssetService userAssertService; //不需要Autowired注解
	private TradeLogProxy tradeLogProxy;
	private UserRealAuthenService realAuthenService;
	
	
	*//**
	 * @function 构造器注入service对象
	 *//*
	@PostConstruct
	private void init(){
		registerService = ServiceBeanFactory.getBean(RegisterService.class);
		loginService = ServiceBeanFactory.getBean(LoginService.class);
		userRoleService = ServiceBeanFactory.getBean(UserRoleService.class);
		userSafeService = ServiceBeanFactory.getBean(UserSafeService.class);
		userAssertService = ServiceBeanFactory.getBean(UserAssetService.class);
		tradeLogProxy = ServiceBeanFactory.getBean(TradeLogProxy.class);
		realAuthenService = ServiceBeanFactory.getBean(UserRealAuthenService.class);
	}
	
	*//**
	 * @function 生成图片验证码
	 * @param session
	 * @return
	 *//*
	@RequestMapping(params = "cmd=createValidateCode")
	public void createCheckCode(HttpSession session, HttpServletResponse resp) {
		log.info("cmd=createValidateCode");
		PictureCheckCode vCode = new PictureCheckCode(); 
    	vCode.createCode();
    	BufferedImage buffImg = vCode.getBuffImg();
		String code = vCode.getCode();
    	//将生成的验证码存在session中
		session.setAttribute("checkcode", code);
	    //设置响应头通知浏览器以图片的形式打开
		resp.setContentType("image/jpeg");//等同于response.setHeader("Content-Type", "image/jpeg");
	    //设置响应头控制浏览器不要缓存
		resp.setDateHeader("expries", -1);
		resp.setHeader("Cache-Control", "no-cache");
		resp.setHeader("Pragma", "no-cache");
	    //将图片写给浏览器
	    try {
			ImageIO.write(buffImg, "png", resp.getOutputStream());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	*//**
	 * @function 手机注册
	 * @param req
	 * @param areaCode		区号
	 * @param phone			手机号
	 * @param consumerCode	图片验证码
	 * @return
	 *//*
	@ResponseBody
	@RequestMapping(params = "cmd=phoneRegister")
	public String phoneRegister(HttpServletRequest req, String areaCode, 
			final String phone, final String consumerCode) {
		log.info("phoneRegister");
		
		//Map<String,Object> codeMap = new HashMap<String,Object>();
		//JSONObject obj = new JSONObject();
		String result = "";
		// 1.根据IP确定国家，如果是中国确定，+86显示
		
		// 2.手机校验(为不为空串；是不是手机号；该手机号是否注册过)
		StringValidateUtil.validateNotEmpty(areaCode, "areaCode");
		CommonUtil.areaCodeCheck(areaCode);
		StringValidateUtil.validateNotEmpty(phone, "phone");
		 
		boolean isPhone = StringValidateUtil.isPhone(areaCode + phone);
		if(!isPhone){
			return JsonResponseUtil.buildNormalExcepResonpe("US651", "The mobile is not mobile").toString();
		}
		// 3.图形验证码 是否正确
		String pictureCaptcha = (String)req.getSession().getAttribute("checkcode");
		if(StringUtils.isEmpty(pictureCaptcha) || !pictureCaptcha.equalsIgnoreCase(consumerCode)){
			// 从session中去除验证码
			req.getSession().removeAttribute("checkcode");
			return JsonResponseUtil.buildNormalExcepResonpe("US601", "Image verification code does not match!").toString();
		}
		
		
		try {
			registerService.phoneRegister(areaCode, phone);
			result = JsonResponseUtil.buildSucessResonpe(true).toString();
		} catch (BusinessException e) {			
			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		}	
		
		req.getSession().removeAttribute("checkcode");
		return result;
	}

	*//**
	 * 手机注册激活
	 * @param mobile    手机号
	 * @param captcha   验证码
	 * @param password  密码
	 *//*
	@ResponseBody
	@RequestMapping(params = "cmd=activePhone")
	public String activePhone(HttpServletRequest req,String phone, final String activeCode, final String password, final String repassword, final String userProtocol){
		log.info("activePhone");
		//Map<String, Object> codeMap = new HashMap<String,Object>();
		//JSONObject obj = new JSONObject();
		String result = "";
		StringValidateUtil.validateNotEmpty(phone, "phone");
		StringValidateUtil.validateLength(32, password, "password");
		// 手机号格式是否正确
		boolean isMobile = StringValidateUtil.isPhone(phone);
		if(!isMobile){			
			throw new ParamInvalidException("mobile", " not mobile");
		}
		
		
		if(!StringValidateUtil.isLoginPassword(password)){					
			req.getSession().removeAttribute("password");
			return JsonResponseUtil.buildNormalExcepResonpe("US616", "validate password").toString();
		}
				
		StringValidateUtil.validateNotEmpty(repassword, "repassword");
		if(!password.equals(repassword)){					
			req.getSession().removeAttribute("repassword");
			return JsonResponseUtil.buildNormalExcepResonpe("US617", "different password").toString();
		}
				
		if(!UserStatusConstant.USER_PROTOCOL_SELECT.equals(userProtocol)){					
			req.getSession().getAttribute("userProtocol"); 
			return JsonResponseUtil.buildNormalExcepResonpe("US618", "please read the user protocol").toString();
		}
		
		StringValidateUtil.validateNotEmpty(activeCode, "captcha");
		StringValidateUtil.validateNotEmpty(password, "password");
		try {
					
			registerService.activePhone(phone, activeCode, password);							
			result = JsonResponseUtil.buildSucessResonpe(true).toString();
		} catch (BusinessException e) {			
			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		}
		return result;		
	}
	
	*//**
	 * @function 邮箱注册
	 * @param req
	 * @param email			邮箱
	 * @param consumerCode	图片验证码
	 * @return
	 *//*
	@ResponseBody
	@RequestMapping(params = "cmd=emailRegister")
	public String emailRegister(HttpServletRequest req, String email, 
			final String consumerCode) {
		log.info("cmd=emailRegister");
		//Map<String, Object> codeMap = new HashMap<String,Object>();
		//JSONObject obj = new JSONObject();
		String result = "";
		// 1.根据IP确定国家，如果是中国确定，+86显示
		
		// 2.邮箱校验(为不为空串；是不是邮箱；该邮箱是否注册过)
		StringValidateUtil.validateNotEmpty(email, "email");
		boolean isEmail = StringValidateUtil.isEmail(email);
		if(!isEmail){
			throw new ParamInvalidException("email", "not email ");
		}
		// 3.图形验证码 是否正确
		String pictureCaptcha = (String) req.getSession().getAttribute("checkcode");
		if(StringUtils.isEmpty(pictureCaptcha) || !pictureCaptcha.equalsIgnoreCase(consumerCode)){
			
			req.getSession().removeAttribute("checkcode");
			//codeMap.put("code", "US601");
			//codeMap.put("message", "Image verification code does not match!");
			//obj.clear();
			//obj.put("error", codeMap);
			//return obj.toString();
			return JsonResponseUtil.buildNormalExcepResonpe("US601", "Image verification code does not match!").toString();
		}
		
		try {
			try {
				registerService.emailRegister(email);
			} catch (EmailFailException e) {
				//result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
				throw new ParamInvalidException("email", "not email ");
			}
			//obj.clear();
			//obj.put("data", true);
			result = JsonResponseUtil.buildSucessResonpe(true).toString();
		} catch (BusinessException e) {
			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
			if(e.getErrorcode().equals("US702")){
				codeMap.put("code", e.getErrorcode());
				codeMap.put("message", "registed");
			}
			if(e.getErrorcode().equals("US619")){
				codeMap.put("code", e.getErrorcode());
				codeMap.put("message", "count limited");
			}
			obj.clear();
			obj.put("error", codeMap);
		}
		
		return result;
	}
	
	*//**
	 * 邮箱注册激活
	 * @param email        邮箱号
	 * @param activeCode   激活码
	 * @param password     密码
	 *//*
	@ResponseBody
	@RequestMapping(params = "cmd=activeEmail")
	public String activeEmail(HttpServletRequest req,String email, final String activeCode, final String password, final String repassword, final String userProtocol){
		log.info("cmd=activeEmail");
		//Map<String, Object> codeMap = new HashMap<String,Object>();
		//JSONObject obj = new JSONObject();
		String result = "";
		StringValidateUtil.validateNotEmpty(email, "email");
		StringValidateUtil.validateLength(32, password, "password");
		// 邮箱格式是否正确
		boolean isEmail = StringValidateUtil.isEmail(email);
		if(!isEmail){			
			throw new ParamInvalidException("email", " not email");
		}
		
		if(!StringValidateUtil.isLoginPassword(password)){
					
			req.getSession().removeAttribute("password");
			//codeMap.put("code", "US616");   
			//codeMap.put("message", "validate password");
			//obj.clear();
			//obj.put("error", codeMap);
			//return obj.toString();
			return JsonResponseUtil.buildNormalExcepResonpe("US616", "validate password").toString();
		}
				
		StringValidateUtil.validateNotEmpty(repassword, "repassword");
		if(!password.equals(repassword)){
					
			req.getSession().removeAttribute("repassword");
			//codeMap.put("code", "US617");   
			//codeMap.put("message", "different password");
			//obj.clear();
			//obj.put("error", codeMap);
			return JsonResponseUtil.buildNormalExcepResonpe("US617", "different password").toString();
		}
				
		if(!"1".equals(userProtocol)){
					
			//req.getSession().getAttribute("userProtocol");
			//codeMap.put("code", "US618");   
			//codeMap.put("message", "please read the user protocol"); 
			return JsonResponseUtil.buildNormalExcepResonpe("US618", "please read the user protocol").toString();
				}
		StringValidateUtil.validateNotEmpty(activeCode, "captcha");
		StringValidateUtil.validateNotEmpty(password, "password");
		try {
					
			registerService.activeEmail(email, activeCode, password);							
		
			//obj.clear();
			//obj.put("data", true);
			result = JsonResponseUtil.buildSucessResonpe(true).toString();
		} catch (BusinessException e) {
			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
			if(e.getErrorcode().equals("US652")){
				codeMap.put("code", e.getErrorcode());
				codeMap.put("message", " is registed");
			}
			if(e.getErrorcode().equals("US653")){
				throw new ParamInvalidException("account", " not found");
			}
			if(e.getErrorcode().equals("US603") || e.getErrorcode().equals("US604") || e.getErrorcode().equals("US605")){
				codeMap.put("code", e.getErrorcode());
				codeMap.put("message", "Activecode is error or invalid!");
			}
			obj.clear();
			obj.put("error", codeMap);
		}
		return result;		
	}
	
	*//**
	 * 登录
	 * @param req
	 * @param resp
	 * @param account
	 * @param accountType
	 * @param password
	 * @return
	 *//*
	@ResponseBody
	@RequestMapping(params = "cmd=login")
	public String login(HttpServletRequest req, HttpServletResponse resp, final String account, final String accountType, final String password){
		
		Map<String, Object> codeMap = new HashMap<String, Object>();
		JSONObject obj = new JSONObject();
		
		// 1.验证账号和密码
		StringValidateUtil.validateNotEmpty(account, "account");
		int type = StringValidateUtil.strToInt(accountType, "accountType");
		StringValidateUtil.validateNotEmpty(password, "password");
		
		if(type == 1){
			boolean isEmail = StringValidateUtil.isEmail(account);
			if(! isEmail){
				throw new ParamInvalidException("email", "not email");
			}
		}else{
			boolean isPhone = StringValidateUtil.isPhone(account);
			if(! isPhone){
				throw new ParamInvalidException("mobile", " not mobile");
			}
		}
		
		// 获取用户远程IP
		String remoteIP = CommonUtil.getRemoteHost(req);
		// 判断登录方式
		int loginMode = 0;
		// 判断是否为移动端访问
		String userAgent = req.getHeader("USER-AGENT").toLowerCase();
		if(null == userAgent){
			userAgent = "";
		}
		boolean isHtml = CheckLoginMode.check(userAgent);
		// 判断是否为移动端访问
		if(isHtml){
			// 手机html
			loginMode = 2;
		}else{
			loginMode = 0;
		}
		
		Map<String, Object> accessData = new ConcurrentHashMap<String, Object>();
		accessData.put("remoteIP", remoteIP);
		accessData.put("loginMode", (byte)loginMode);
		
		try {
			Map<String, Object> result = loginService.login(account, (byte)type, password, accessData);
			// 0未绑定 1已绑定
			String hasPhone = result.get("bindPhone").toString();
			if("0".equals(hasPhone)){
				// 没有手机，直接更新用户session信息
				this.updateUserSession(req, (int)result.get("userId"));
				result.put("code", 0);				// 0 仅有密码验证
				result.put("message", "verifyPass");
				//JsonResponseUtil.buildSucessResonpe(result);
			}else{
				// 开启双重认证（密码和验证码）有手机可以为手机验证或谷歌验证，将userid临时写入Session【此时页面不清楚要那种验证】
				HttpSession session = req.getSession();
				session.setAttribute("userId", result.get("userId"));
				if(null == result.get("googlePrivateKey") || result.get("googlePrivateKey").toString().length()<=0){
					// 此时认证手机
					result.put("code", 1);				// 1 提示页面进入手机验证码验证页面
					result.put("message", "verifyPhone");
					//JsonResponseUtil.buildSucessResonpe(result);
				}else{
					// 此时谷歌认证
					result.put("code", 2);				// 2 提示页面进入谷歌验证码验证页面
					result.put("message", "verifyGoogle");
					//JsonResponseUtil.buildSucessResonpe(result);
				}
				
			}
			
			
			//obj.put("data", result);
			//return obj.toString();	
			return JsonResponseUtil.buildSucessResonpe(result).toString();
		} catch (BusinessException e) {
			
			return JsonResponseUtil.buildBusiExcepResonpe(e).toString();
			//codeMap.put("code", e.getErrorcode());
			//codeMap.put("message", e.getMessage());
			//obj.put("error", codeMap);
			//return obj.toString();
		}
		// 
		// 2.账号或密码错误
		// 3.账号和密码正确
		//  3.1 谷歌验证（查看安全等级说明进行判断）
		//  3.2 手机验证（）
		//  3.3 不需要验证（邮箱账户且没有绑定谷歌、手机）
	}

	*//**
	 * 更新用户session信息
	 * @param req
	 * @param resp
	 * @param object
	 * @return
	 *//*
	private Map<String, Object> updateUserSession(HttpServletRequest req, int userId) {
		// 获取用户基础信息对象
		UserBaseInfo userBaseInfo = loginService.getUserBaseInfo(userId);
		// 获取用户安全信息
		Map<String, Object> userSafeInfoMap = loginService.getUserSafeInfo(userId);
		// 获取用户活动对象信息
		UserActivityInfo userActivityInfo = loginService.getUserActivityInfo(userId);
		
		
		 * 组建Map
		 
		Map<String, Object> map = new HashMap<String, Object>();
		// 用户基础信息
		map.put("id", userBaseInfo.getUserId());
		map.put("name", userBaseInfo.getNickName());
		map.put("email", userBaseInfo.getEmail());
		map.put("accountType", userBaseInfo.getAccountType());
		map.put("areaCode", userBaseInfo.getAreaCode());
		map.put("phone", this.replacePhone(userBaseInfo.getPhone()));
		map.put("registerDate", userBaseInfo.getRegisterDate());
		
		
		// 用户安全信息
		map.put("bindPhone", userSafeInfoMap.get("bindPhone"));
		map.put("pwdLevel", userSafeInfoMap.get("passwordLevel"));
		// 是否有资金密码
		boolean hasFundPassword = (boolean) userSafeInfoMap.get("hasFundPassword");
		map.put("hasFundPwd", hasFundPassword);
		// 是否实名认证
		map.put("authenStatus", userSafeInfoMap.get("authenStatus"));
		// 是否谷歌验证
		boolean hasGooglePrivateKey = (boolean) userSafeInfoMap.get("hasGooglePrivateKey");
		map.put("hasGoogleCode", hasGooglePrivateKey);
		byte securityLevel = (byte) userSafeInfoMap.get("securityLevel");
		if(securityLevel == 0){
			if(hasGooglePrivateKey && StringTools.isNotBlank(userBaseInfo.getPhone())){
				securityLevel = 3;
			}else if(hasGooglePrivateKey && !StringTools.isNotBlank(userBaseInfo.getPhone())){
				securityLevel = 2;
			}else if(!hasGooglePrivateKey && StringTools.isNotBlank(userBaseInfo.getPhone())){
				securityLevel = 1;
			}else{
				securityLevel = -1;
			}
		}
		// 账户安全级别
		map.put("authType", securityLevel);
		map.put("loginAuthentication", userSafeInfoMap.get("loginAuthentication"));
		
		// 用户活动信息
		map.put("userLevel", userActivityInfo.getVIPLevel());
		map.put("score", userActivityInfo.getScore());
		
		// 用户角色
		String[] userRoles = userRoleService.getUserRoles(userId);
		if(userRoles != null && userRoles.length > 0){
			map.put("roles", userRoles);
		}
		
		// 存入session中
		SessionUserUtil.updateUserInfo(req.getSession(), map);
		return map;
	}

	*//**
	 * 隐藏手机号码的部分信息
	 * @param phone
	 * @return
	 *//*
	private Object replacePhone(String phone) {
		if(phone == null || phone.equals("")){
			phone = "";
		}else if(phone.length()<8){
			phone = "********";
		}else{
			String pre = phone.substring(0, phone.length()-8);
			String post = phone.substring(phone.length()-4);
			phone = pre + "****" + post;
		}
		return phone;
	}
	
	*//**
	 * 设置登录认证
	 *//*
	@ResponseBody
	@RequestMapping(params = "cmd=setLoginAuthentication")
	public String setLoginAuthentication(HttpServletRequest req, final String loginAuthentication){
		int type = StringValidateUtil.strToInt(loginAuthentication, "loginAuthentication");
		
		JSONObject obj = new JSONObject();
		Map<String, Object> codeMap = new HashMap<>();
		
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		
		try {
			loginService.setLoginAuthentication(userId, type);
			obj.clear();
			obj.put("data", true);
			// 更新用户状态
			SessionUserUtil.sendUpdateUserMessage(userId, "loginAuthentication", type, req.getSession());
		} catch (BusinessException e) {
			codeMap.put("code", e.getErrorcode());
			codeMap.put("message", e.getMessage());
			obj.clear();
			obj.put("error", codeMap);
		}
		return obj.toString();
	}
	
	*//**
	 * 校验登录认证时发送手机验证码
	 *//*
	@ResponseBody
	@RequestMapping(params = "cmd=checkLoginAuthenticationSendMessage")
	public String checkLoginAuthenticationSendMessage(HttpServletRequest req){
		//JSONObject obj = new JSONObject();
		//Map<String, Object> codeMap = new HashMap<String, Object>();
		String result = "";
		HttpSession session = req.getSession();
		Integer userId = (Integer) session.getAttribute("userId");
		if(userId == null){
			String errcode = ErrorCodeConstant.NOTLOGIN;
			String errmessage = "User not login";
			//codeMap = CollectionUtils.buildMap("code", errcode, "message", errmessage);
			//Map<String, Object> returnmap = CollectionUtils.buildMap("error", codeMap);
			//JSONObject jb = JSONObject.fromObject(returnmap);
			//return jb.toString();
			return JsonResponseUtil.buildNormalExcepResonpe(errcode, errmessage).toString();
		}
		
		try {
			loginService.checkLoginAuthenticationSendMessage(userId);
			result = JsonResponseUtil.buildSucessResonpe(true).toString();
			//obj.clear();
			//obj.put("data", true);
		} catch (BusinessException e) {
			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
			//codeMap.put("code", e.getErrorcode());
			//codeMap.put("message", e.getMessage());
			//obj.clear();
			//obj.put("error", codeMap);
		}
		return result;
	}
	
	*//**
	 * 校验登录认证
	 *//*
	@ResponseBody
	@RequestMapping(params = "cmd=checkLoginAuthentication")
	public String checkLoginAuthentication(HttpServletRequest req, final String captcha, final String pageType){
		//JSONObject obj = new JSONObject();
		//Map<String, Object> codeMap = new HashMap<>();
		String result = "";
		// pageType 为0 仅有密码验证  为1  手机验证码  为2 谷歌验证码
		HttpSession session = req.getSession();
		Integer userId = (Integer) session.getAttribute("userId");
		if(userId == null){
			String errcode = ErrorCodeConstant.NOTLOGIN;
			String errmessage = "User not Login";
			//codeMap = CollectionUtils.buildMap("code", errcode, "message", errmessage);
			//Map<String, Object> returnmap = CollectionUtils.buildMap("error", codeMap);
			//JSONObject jb = JSONObject.fromObject(returnmap);
			//return jb.toString();
			return JsonResponseUtil.buildNormalExcepResonpe(errcode, errmessage).toString();
		}
		
		try {
			loginService.checkLoginAuthentication(userId, captcha, pageType);
			if(userId != null){
				session.removeAttribute("userId");
			}
			// 更新用户session信息
			this.updateUserSession(req, userId);
			
			//obj.clear();
			//obj.put("data", true);
			result = JsonResponseUtil.buildSucessResonpe(true).toString();
			//【认证通过跳转登陆后首页】
		} catch (BusinessException e) {
			//codeMap.put("code", e.getErrorcode());
			//codeMap.put("message", e.getMessage());
			//obj.clear();
			//obj.put("error", codeMap);
			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		}
		return result;
	}
	
	*//**
	 * 用户登录时候忘记登录密码
	 *//*
	@ResponseBody
	@RequestMapping(params = "cmd=forgetLoginPassword")
	public String forgetLoginPassword(HttpServletRequest req, String account){
		//JSONObject obj = new JSONObject();
		//Map<String, Object> codeMap = new HashMap<>();
		String result = "";
		// 1.验证账户是手机号还是邮箱，去数据库查询是否存在账户
		HttpSession session = req.getSession();
		StringValidateUtil.validateNotEmpty(account, "account");
		boolean isEmail = StringValidateUtil.isEmail(account);
		boolean isPhone = StringValidateUtil.isPhone(account);
		if(isEmail || isPhone){
			// 邮箱账号的情况   手机账号的情况
			try {
				UserBaseInfo userBaseInfo = loginService.forgetLoginPassword(account);
				if(req.getSession().getAttribute("userId") != null){
					session.removeAttribute("userId");
				}
				// 账户真实有效且发送验证码的情况下更新用户Id到session
				Map<String, Object> map = new HashMap<String, Object>();
				// 用户基础信息
				map.put("id", userBaseInfo.getUserId());
				map.put("email", userBaseInfo.getEmail());
				map.put("areaCode", userBaseInfo.getAreaCode());
				map.put("phone", userBaseInfo.getPhone());
				SessionUserUtil.updateUserInfo(req.getSession(), map);
				
				//obj.clear();
				//obj.put("data", true);	
				result = JsonResponseUtil.buildSucessResonpe(true).toString();
			} catch (BusinessException e) {
				result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
				//codeMap.put("code", e.getErrorcode());
				//codeMap.put("message", e.getMessage());
				//obj.clear();
				//obj.put("error", codeMap);
			}			
		}else{
			// 无效账号
			throw new ParamInvalidException("account", " not validate account!");
		}
		// 2.存在账户向相应的手机号、或邮箱发送验证码		
		return result;
	}
	
	*//**
	 * 用户登录时候忘记登录密码更改密码的请求
	 *//*
	@ResponseBody
	@RequestMapping(params = "cmd=applyChangeLoginPassword")
	public String applyChangeLoginPassword(HttpServletRequest req, String activeCode, String password, String rePassword){
		//JSONObject obj = new JSONObject();
		//Map<String, Object> codeMap = new HashMap<>();
		String result = "";
		// 1.验证用户输入项是否有效，验证码是否匹配
		boolean isEmpty = StringUtils.isEmpty(activeCode);
		if(isEmpty){
			throw new ParamInvalidException("activeCode", "not null");
		}
		boolean isBlank = StringUtils.isEmpty(activeCode.trim());
		if(isBlank){
			throw new ParamInvalidException("activeCode", "not blank");
		}
		// 2.密码是否符合规则
		// password = (String) req.getSession().getAttribute("password");
		if(!StringValidateUtil.isLoginPassword(password)){
			// 从session中去除密码
			req.getSession().removeAttribute("password");
			//codeMap.put("code", "US616");   
			//codeMap.put("message", "validate password");
			//obj.clear();
			//obj.put("error", codeMap);
			//return obj.toString();
			return JsonResponseUtil.buildNormalExcepResonpe("US616", "validate password").toString();
		}
		// 3.两次密码是否一致
		String repassword = (String) req.getSession().getAttribute("repassword");
		StringValidateUtil.validateNotEmpty(repassword, "repassword");
		if(!password.equals(repassword)){
			// 从session中移除确认密码
			req.getSession().removeAttribute("repassword");
			//codeMap.put("code", "US617");   
			//codeMap.put("message", "different password");
			//obj.clear();
			//obj.put("error", codeMap);
			//return obj.toString();
			return JsonResponseUtil.buildNormalExcepResonpe("US617", "different password").toString();
		}
		
		// 验证session有效性，获取用户Id更新用户登录密码
		if(req.getSession(false)==null){
			throw new ParamInvalidException("session", "Session has been invalidated!");
		}else{
			// 从session中获取用户信息，更新数据库中的用户密码信息
			HttpSession session = req.getSession();
			Integer userId = (Integer) session.getAttribute("userId");
			String email = (String) session.getAttribute("email");
			String phone = (String) session.getAttribute("phone");
			String areaCode = (String) session.getAttribute("areaCode");
			Map<String, String> map = new HashMap<String, String>();
			map.put("phone", phone);
			map.put("areaCode", areaCode);
			map.put("email", email);
			// 调用业务逻辑实现
			try {
				loginService.changeLoginPassword(userId, activeCode, password, map);
				result = JsonResponseUtil.buildSucessResonpe(true).toString();
				//obj.clear();
				//obj.put("data", true);	
			} catch (BusinessException e) {
				//codeMap.put("code", e.getErrorcode());
				//codeMap.put("message", e.getMessage());
				//obj.clear();
				//obj.put("error", codeMap);
				result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
			}
		}
			
		// 2.若通过验证操作数据库更新用户登录密码
		return result;
	}
	
	//显示用户总资产、
	@ResponseBody
	@RequestMapping(params = "cmd=showTotalAssert")
	public String showTotalAssert(HttpServletRequest req, HttpServletResponse resp){
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		Map<String, Object> totalAsset = userAssertService.getUser_Asset(userId);
		return JsonResponseUtil.buildSucessResonpe(totalAsset).toString();
	}
	
	// 用户昵称、安全等级、注册时间、邮箱、手机号、谷歌验证码、登录密码、资金密码、实名认证
	//、交易次数、平均放行时间、最近交易时间、
	@ResponseBody
	@RequestMapping(params = "cmd=showCompInfo")
	public String showCompInfo(HttpServletRequest req, HttpServletResponse resp){
		// 
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		// 总资产
		//Map<String, Object> user_Asset = userAssertService.getUser_Asset(userId);
		Map<Object, Object> result = new HashMap<>();
		Long avgDoneTime = tradeLogProxy.avgDoneTime(userId);
		Timestamp lastTradeTime = tradeLogProxy.getLastTradeTime(userId);
		Integer tradeCount = tradeLogProxy.getTradeCount(userId);
		UserBaseInfo userBaseInfo = loginService.getUserBaseInfo(userId);
		Map<String, Object> userSafeInfo = loginService.getUserSafeInfo(userId);
		result.put("userId", userId);
		result.put("nickname", userBaseInfo.getNickName());
		result.put("avgDoneTime", avgDoneTime);
		result.put("lastTradeTime", lastTradeTime);
		result.put("tradeCount", tradeCount);
		result.put("securityLevel", userSafeInfo.get("securityLevel"));
		result.put("email", userBaseInfo.getEmail());
		result.put("phone", userBaseInfo.getPhone());
		result.put("hasGooglePrivateKey", userSafeInfo.get("googlePrivateKey"));
		result.put("hasLoginPassword", userBaseInfo.getPassword());
		result.put("hasFundPassword", userSafeInfo.get("fundPassword"));
		result.put("authenStatus", userSafeInfo.get("authenStatus"));
		if(UserStatusConstant.AUTHEN_STATUS_FAIL.equals(userSafeInfo.get("authenStatus").toString())){
			// 审核失败时添加失败理由
			String failReason = realAuthenService.getFailReasonByUserId(userId);
			result.put("failReason", failReason);
		}
		
		return JsonResponseUtil.buildSucessResonpe(result).toString();
	}
		
	
	// 查询昵称、设置昵称（需验证昵称是否存在）
	@ResponseBody
	@RequestMapping(params = "cmd=setNicknameAndFundPwd")
	public String setNicknameAndFundPassword(HttpServletRequest req, String nickname, String fundPassword, String reFundPassword){
		//JSONObject obj = new JSONObject();
		//Map<String, Object> codeMap = new HashMap<>();
		String result = "";
		Integer userId = (Integer) req.getSession().getAttribute("userId");
		// 1.验证昵称格式，资金密码格式
		boolean condition1 = StringValidateUtil.isNull(nickname);
		boolean condition2 = StringValidateUtil.isNull(fundPassword);
		boolean condition3 = StringValidateUtil.isNull(reFundPassword);
		if(condition1 || condition2 || condition3){
			throw new ParamInvalidException("parameter", " not null");
		}
		 boolean condition4 = StringValidateUtil.isNickname(nickname);
		 boolean condition5 = StringValidateUtil.isFundPassword(fundPassword);
		 boolean condition6 = StringValidateUtil.isFundPassword(fundPassword);
		 if(condition4&&condition5&&condition6){
			 if(!fundPassword.equals(reFundPassword)){
				 //codeMap.put("code", "US617");   
				 //codeMap.put("message", "different password");
				 //obj.clear();
				 //obj.put("error", codeMap);
				 //return obj.toString();
				 return JsonResponseUtil.buildNormalExcepResonpe("US617", "different password").toString();
			 }
			 
			 // 输入通过有效性验证
			 boolean existNickname = userSafeService.checkNickname(nickname);
			// 2.后台验证昵称是否存在，不存在更新昵称和资金密码
			 if(!existNickname){
				 // 昵称存在
				 req.getSession().removeAttribute("nickname");
				 //codeMap.put("code", "US622");
				 //codeMap.put("message", "Nickname exists!");
				 //obj.clear();
				 //obj.put("error", codeMap);
				 //return obj.toString();
				 return JsonResponseUtil.buildNormalExcepResonpe("US622", "Nickname exists!").toString();
			 }
			 userSafeService.setNicknameAndFundPassword(userId, nickname, fundPassword);
			 
			 //obj.clear();
			 // obj.put("data", true);	
			 result = JsonResponseUtil.buildSucessResonpe(true).toString();
		 }else{
			 throw new ParamInvalidException("parameter", "The input does not meet the requirements!");
		 }		
		//return obj.toString();
		 return result;
	}
	
	
	// 查询登录密码、修改登录密码
	@ResponseBody
	@RequestMapping(params = "cmd=changePasswordByOldPassword")
	public String changePasswordByOldPassword(HttpServletRequest req, String oldPassword, String newPassword){
		//JSONObject obj = new JSONObject();
		//Map<String, Object> codeMap = new HashMap<>();
		String result = "";
		// 验证密码有效性
		boolean isPassword = StringValidateUtil.isLoginPassword(oldPassword);
		boolean isNewPassword = StringValidateUtil.isLoginPassword(newPassword);
		boolean isRePassword = StringValidateUtil.isLoginPassword((String)req.getAttribute("rePassword"));
		if(isPassword && isNewPassword && isRePassword){
			// 三个密码有效性验证通过，比较新密码是否一致
			if(!newPassword.equals(req.getAttribute("rePassword"))){
				// 从session中移除确认密码
				req.getSession().removeAttribute("repassword");
				//codeMap.put("code", "US617");   
				//codeMap.put("message", "different password");
				//obj.clear();
				//obj.put("error", codeMap);
				//return obj.toString();
				return JsonResponseUtil.buildNormalExcepResonpe("US617", "different password").toString();
			}
			
			// 在service里验证旧密码是否正确；若正确则更新登录密码
			Integer userId = (Integer) req.getSession().getAttribute("userId");
			
			try {
				userSafeService.changePasswordByOldPassword(userId, oldPassword, newPassword);
				
				 //obj.clear();
				 //obj.put("data", true);
				 result = JsonResponseUtil.buildSucessResonpe(true).toString();
			} catch (BusinessException e) {
				result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
				//codeMap.put("code", e.getErrorcode());
				//codeMap.put("message", e.getMessage());
				//obj.clear();
				//obj.put("error", codeMap);
			}
		}
		return result;
		//return obj.toString();
	}
	
	// 查询资金密码、修改资金密码
	*//**
	 * 用户点击修改资金密码的初始化操作
	 *//*
	@ResponseBody
	@RequestMapping(params = "cmd=initFundPasswordChange")
	public String initFundPasswordChange(HttpServletRequest req, HttpServletResponse resp){
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		// eamil bindPhone hasGoogleCode
		Map<String, Object> userInfo = SessionUserUtil.getUserInfo(req.getSession());
		boolean hasBind = (boolean) userInfo.get("bindPhone");
		String email = (String) userInfo.get("email");
		boolean hasGoogleCode = (boolean) userInfo.get("hasGoogleCode");
		//String phone = (String) userInfo.get("phone");
		String result = "";
		//JSONObject obj = new JSONObject();
		HashMap<Object, Object> codeMap = new HashMap<>();
		if(! hasBind){
			// 没有手机提示去绑定手机
			codeMap.put("code", 1);
			codeMap.put("message", "bindPhone");
			//obj.clear();
			//obj.put("data", codeMap);
			//return obj.toString();
			return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
		}
		// 判断有没有邮箱【有手机】有没有谷歌
		if(null == email && email.isEmpty()){
			// 没有邮箱
			if(hasGoogleCode){
				// 有谷歌
				codeMap.put("code", 2);  // 有谷歌没邮箱
				codeMap.put("message", "hasGoogle");
				//obj.clear();
				//obj.put("data", codeMap);
				//return obj.toString();
				return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
			}else if(hasBind){
				// 有手机
				codeMap.put("code", 3);  // 有手机没邮箱
				codeMap.put("message", "hasPhone");
				//return obj.toString();
				return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
			}else{
				// 用户不存在
				codeMap.put("code", "US753");
				codeMap.put("message", "The user is not exist!");
				//obj.clear();
				//obj.put("err", codeMap);
				//return obj.toString();
				return JsonResponseUtil.buildNormalExcepResonpe("US753", "The user is not exist!").toString();
			}
		}else{
			// 有邮箱
			if(hasGoogleCode){
				// 有谷歌
				codeMap.put("code", 4);
				codeMap.put("message", "hasGoogleAndEmail");
				//obj.clear();
				//obj.put("data", codeMap);
				//return obj.toString();
				return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
			}else if(hasBind){
				// 有手机
				codeMap.put("code", 5);
				codeMap.put("message", "hasPhoneAndEmail");
				//return obj.toString();
				return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
			}else{
				// 用户不存在
				codeMap.put("code", "US753");
				codeMap.put("message", "The user is not exist!");
				//obj.clear();
				//obj.put("err", codeMap);
				//return obj.toString();
				return JsonResponseUtil.buildNormalExcepResonpe("US753", "The user is not exist!").toString();
			}			
		}
	}
	
	*//**
	 * 修改用户资金密码【type 获取页面类型的参数】2 有谷歌没邮箱 3 有手机没邮箱 4有谷歌有邮箱  5 有手机有邮箱
	 * 加传一个参数页面类型	 pageType 
	 *//*
	@ResponseBody
	@RequestMapping(params = "cmd=changeFundPassword")
	public String changeFundPassword(HttpServletRequest req, HttpServletResponse resp, final String newFundPassword, final String reFundPassword, final String emailCaptcha, final String googleCaptcha, final String phoneCaptcha, final String pageType){
		JSONObject obj = new JSONObject();
		Map<Object, Object> codeMap = new HashMap<>();
		String result = "";
		// 验证密码
		boolean isPassword = StringValidateUtil.isFundPassword(newFundPassword);
		boolean isRePassword = StringValidateUtil.isFundPassword(reFundPassword);
		if(reFundPassword.contains(" ") || !isPassword || newFundPassword.contains(" ") || !isRePassword){
			//codeMap.put("code", "US616");
			//codeMap.put("message", "Validate password!");
			//obj.clear();
			//obj.put("err", codeMap);
			//return obj.toString();
			return JsonResponseUtil.buildNormalExcepResonpe("US616", "Validate password!").toString();
		}
		// 验证两次密码是否一致
		if(!newFundPassword.equals(reFundPassword)){
			//codeMap.put("code", "US617");
			//codeMap.put("message", "Different password!");
			//obj.clear();
			//obj.put("err", codeMap);
			//return obj.toString();
			return JsonResponseUtil.buildNormalExcepResonpe("US617", "Different password!").toString();
		}
		String verifyType = pageType.toString();
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		Map<String, Object> userInfo = SessionUserUtil.getUserInfo(req.getSession());
		String email = (String) userInfo.get("email");
		try{
			if("2".equals(verifyType)){
				// 只验证谷歌验证码;保存新密码
				userSafeService.verifyGoogleCodeByUserId(userId, googleCaptcha);
			}else if("3".equals(verifyType)){
				// 只验证手机验证;保存新密码
				userSafeService.verifyPhoneCodeByUserId(userId, phoneCaptcha);
			}else if("4".equals(verifyType)){
				// 验证谷歌和邮箱;保存新密码
				userSafeService.verifyGoogleCodeByUserId(userId, googleCaptcha);
				userSafeService.verifyEmailCodeByUserId(userId, email, emailCaptcha);
			}else if("5".equals(verifyType)){
				// 验证手机和邮箱;保存新密码
				userSafeService.verifyPhoneCodeByUserId(userId, phoneCaptcha);
				userSafeService.verifyEmailCodeByUserId(userId, email, emailCaptcha);
			}
			userSafeService.changeFundPassword(userId, newFundPassword);
			
			 //obj.clear();
			 //obj.put("data", true);	
			 result = JsonResponseUtil.buildSucessResonpe(true).toString();
		}catch(BusinessException e){
			 //codeMap.put("code", e.getErrorcode());
			 //codeMap.put("message", e.getMessage());
			 //obj.clear();
			 //obj.put("error", codeMap);
			 result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		}		
		return obj.toString();
	}
	
	// 查询谷歌验证、绑定谷歌验证码
	*//**
	 * 绑定谷歌验证码之判断手机号，初始化谷歌验证器二维码
	 * 
	 *//* 
	@ResponseBody
	@RequestMapping(params = "cmd=initGoogleBind")
	public String initGoogleBind(HttpServletRequest req, HttpServletResponse resp){
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		JSONObject obj = new JSONObject();
		Map<String, Object> codeMap = new HashMap<>();
		String result = "";
		Map<String, Object> userInfo = SessionUserUtil.getUserInfo(req.getSession());
		boolean isBindPhone = (boolean) userInfo.get("bindPhone");
		if(!isBindPhone){
			// 没有绑定手机提示去绑定手机
			codeMap.put("code", 1);
			codeMap.put("message", "bindPhone");
						
			//obj.clear();
			//obj.put("data", codeMap);
			//return obj.toString();
			return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
		}
		// 生成谷歌验证器二维码，存到redis；显示到页面
		try {
			Map<String,Object> map = userSafeService.getGoogleAuth(userId);
			//obj.clear();
			//obj.put("data", map);
			result = JsonResponseUtil.buildSucessResonpe(true).toString();
			
		} catch (BusinessException e) {
			//codeMap.put("code", e.getErrorcode());
			//codeMap.put("message", e.getMessage());
			//obj.clear();
			//obj.put("error", codeMap);
			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		}
		//return obj.toString();
		return result;
	}
	
	*//**
	 * 用户点击绑定谷歌的时候绑定的实现
	 *//*
	@ResponseBody
	@RequestMapping(params = "cmd=bindGooglePrivateKey")
	public String bindGooglePrivateKey(HttpServletRequest req, HttpServletRequest resp, String googleCaptcha, String phoneCaptcha){
		// 1.验证谷歌验证码和手机验证码
		// 2.保存谷歌私钥到MySQL数据库，删除redis中存储信息			
		StringValidateUtil.validateNotEmpty(googleCaptcha, "googleCaptcha");
		StringValidateUtil.validateNotEmpty(phoneCaptcha, "phoneCaptcha");
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		//JSONObject obj = new JSONObject();
		String result = "";
		
		try {
			userSafeService.verifyGoogleCodeByUserId(userId, googleCaptcha);
			userSafeService.verifyPhoneCodeByUserId(userId, phoneCaptcha);
			userSafeService.bindGoogleAuth(userId);
			// 3.更新session中用户状态
			SessionUserUtil.sendUpdateUserMessage(userId, "hasGoogleCode", true, req.getSession());
			SessionUserUtil.sendUpdateUserMessage(userId, "authType", 3, req.getSession());
			//obj.put("data", true);
			result = JsonResponseUtil.buildSucessResonpe(true).toString();
		} catch (BusinessException e) {
			//HashMap<Object, Object> codeMap = new HashMap<>();
			//codeMap.put("code", e.getErrorcode());
			//codeMap.put("message", e.getMessage());
			//obj.put("error", codeMap);
			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		}		
		//return obj.toString();
		return result;
	}
	
	*//**
	 * 绑定手机之输入手机号
	 * 
	 *//* 
	@ResponseBody
	@RequestMapping(params = "cmd=initPhoneBind")
	public String initPhoneBind(HttpServletRequest req, HttpServletResponse resp,String phone, String areaCode){
		StringValidateUtil.validateNotEmpty(phone, "phone");
		phone = phone.replaceAll(" ", "");
		boolean isPhone = StringValidateUtil.isPhone(phone);
		if(!isPhone){
			throw new ParamInvalidException("phone", " incorrect format");
		}
		if(phone.lastIndexOf("+")==0){
			phone = phone.substring(1);
		}
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		//JSONObject obj = new JSONObject();
		String result = "";
		boolean bool;
		
		try {
			bool = userSafeService.applyForPhoneBind(userId, phone, areaCode);
			//obj.put("data", bool);
			result = JsonResponseUtil.buildSucessResonpe(bool).toString();
		} catch (BusinessException e) {
			//Map<String,Object> codeMap = new HashMap<String,Object>();
			//codeMap.put("code", e.getErrorcode());
			//codeMap.put("message", e.getMessage());
			//obj.clear();
			//obj.put("error", codeMap);
			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		}
		//return obj.toString();
		return result;
	}
	
	*//**
	 * 绑定手机之获取验证码
	 *//*
	@ResponseBody
	@RequestMapping(params = "cmd=bindPhoneByCode")
	public String bindPhoneByCode(HttpServletRequest req, HttpServletResponse resp, String phone, final String captcha){
		StringValidateUtil.validateNotEmpty(captcha, "captcha");
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		//JSONObject obj = new JSONObject();
		String result = "";
		
		try {
			userSafeService.bindPhone(userId, captcha, phone);
			// 更新用户状态 成功后更新session中用户信息
			UserBaseInfo userBaseInfo = loginService.getUserBaseInfo(userId);
			UserSafeInfo userSafeInfo = userSafeService.getUserSafeInfo(userId);
			SessionUserUtil.sendUpdateUserMessage(userId, "phone", this.replacePhone(userBaseInfo.getPhone()), req.getSession());
			SessionUserUtil.sendUpdateUserMessage(userId, "bindPhone", "1", req.getSession());//手机绑定状态：1 -- 已绑定
			// 更改安全级别
			if(user.getAuthType()==0){
				if(StringTools.isBlank(userSafeInfo.getGooglePrivateKey())){
					SessionUserUtil.sendUpdateUserMessage(userId, "authtype", 1,req.getSession());
				}else{
					SessionUserUtil.sendUpdateUserMessage(userId, "authtype", 3,req.getSession());
				}				
			}
//			obj.put("data", true);
			result = JsonResponseUtil.buildSucessResonpe(true).toString();
		} catch (BusinessException e) {
//			Map<String,Object> codeMap = new HashMap<String,Object>();
//			codeMap.put("code", e.getErrorcode());
//			codeMap.put("message", e.getMessage());
//			obj.clear();
//			obj.put("error", codeMap);
			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		}
		
		return result;
//		return obj.toString();
	}
	

	*//**
	 * 绑定邮箱之输入邮箱:1.绑定邮箱之前判断手机及谷歌是否绑定都不存在去绑定手机；2.存在谷歌验证谷歌；3.存在手机不存在谷歌
	 * 
	 *//* 
	@ResponseBody
	@RequestMapping(params = "cmd=initEmailBind")
	public String initEmailBind(HttpServletRequest req, HttpServletResponse resp,String email){
		StringValidateUtil.validateNotEmpty(email, "email");
		email = email.replaceAll(" ", "");
		boolean isEmail = StringValidateUtil.isEmail(email);
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		//有谷歌验证谷歌
		Map<String, Object> userInfo = SessionUserUtil.getUserInfo(req.getSession());
		boolean hasGoogleCode = (boolean) userInfo.get("hasGoogleCode");
		boolean bindPhone = (boolean) userInfo.get("bindPhone");
		//JSONObject obj = new JSONObject();
		Map<String, Object> codeMap = new HashMap<String, Object>();
		if(!bindPhone){
			// 没有绑定手机提示去绑定手机
//			codeMap.put("code", 1);
//			codeMap.put("message", "bindPhone");
			
//			obj.clear();
//			obj.put("data", codeMap);
//			return obj.toString();
			return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
		}
		if(hasGoogleCode){
			// 去验证谷歌验证码页面
//			codeMap.put("code", 2);
//			codeMap.put("message", "verifyGoogle");
			
//			obj.clear();
//			obj.put("data", codeMap);
//			return obj.toString();
			return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
		}
		
		codeMap.put("code", 3);
		codeMap.put("message", "verifyPhone");
		
		//obj.clear();
		//obj.put("data", codeMap);
		return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
		//return obj.toString();
	}
	
	*//**
	 * 点击"获取验证码"获取邮箱验证码
	 * 
	 *//*
	@ResponseBody
	@RequestMapping(params= "cmd=getEmailCode")
	public String getEmailCode(HttpServletRequest req, String email){
		StringValidateUtil.validateNotEmpty(email, "email");
		boolean isEmail = StringValidateUtil.isEmail(email);
		if(!isEmail){
			throw new ParamInvalidException("email", "not email ");
		}
		//JSONObject obj = new JSONObject();
		String result = "";
//		Map<String, Object> codeMap = new HashMap<String, Object>();
		try {
			registerService.emailRegister(email);
			//obj.clear();
			//obj.put("data", true);
			result = JsonResponseUtil.buildSucessResonpe(true).toString();
		} catch (BusinessException e) {
			// 1.验证码错误；<此处不比较>2.超过次数限制；3.注册过
			if(e.getErrorcode().equals("US702")){
					codeMap.put("code", e.getErrorcode());
					codeMap.put("message", "registed");
			}
			if(e.getErrorcode().equals("US619")){
					codeMap.put("code", e.getErrorcode());
					codeMap.put("message", "count limited");
			}
			obj.clear();
			obj.put("error", codeMap);
			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		}
//		return obj.toString();
		return result;
	}
	
	*//**
	 * 点击"获取验证码"获取短信验证码【绑定邮箱】
	 * 点击"获取验证码"获取短信验证码【绑定谷歌】
	 *//*
	@ResponseBody
	@RequestMapping(params = "cmd=getPhoneCode")
	public String getPhoneCode(HttpServletRequest req){
//		JSONObject obj = new JSONObject();
//		Map<String, Object> codeMap = new HashMap<String, Object>();
		String result = "";
		
		Map<String, Object> userInfo = SessionUserUtil.getUserInfo(req.getSession());
		//String areaCode = (String) userInfo.get("areaCode");
		//String phone = (String) userInfo.get("phone");
		Integer userId = (Integer) userInfo.get("id");
		try {
			//registerService.phoneRegister(areaCode, phone);
			userSafeService.getPhoneCodeByUserId(userId);
//			obj.clear();
//			obj.put("data", true);
			result = JsonResponseUtil.buildSucessResonpe(true).toString();
		} catch (BusinessException e) {
			// 1.验证码错误；<此处不比较>2.超过次数限制；3.注册过
			if(e.getErrorcode().equals("US602") || e.getErrorcode().equals("US652")) {
				codeMap.put("code", e.getErrorcode());
				codeMap.put("message", "count limited");
			}
			if(e.getErrorcode().equals("US652")) {
				codeMap.put("code", e.getErrorcode());
				codeMap.put("message", "registed");
			}
			obj.clear();
			obj.put("error", codeMap);
			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		}
		
		return result;
//		return obj.toString();
	}
	
	*//**
	 * 绑定邮箱之获取验证码【谷歌验证码或手机验证码】
	 *//*
	@ResponseBody
	@RequestMapping(params = "cmd=bindEmailByCode")
	public String bindEmailByCode(HttpServletRequest req, HttpServletResponse resp, String email, final String emailCaptcha, final String phoneCaptcha, final String googleCaptcha){
//		JSONObject obj = new JSONObject();
//		Map<String, Object> codeMap = new HashMap<String, Object>();
		String result = "";
		// 验证邮箱、邮箱验证码，再验证谷歌或手机验证码
		// 页面带一个参数表明是谷歌验证还是短信验证:验证类型 1手机 2谷歌
		String verifyType = (String) req.getAttribute("verifyType");
		Map<String, Object> userInfo = SessionUserUtil.getUserInfo(req.getSession());
		// 验证邮箱及邮箱验证码
		StringValidateUtil.validateNotEmpty(email, "email");
		boolean isEmail = StringValidateUtil.isEmail(email);
		if(!isEmail){
			throw new ParamInvalidException("email", "not email ");
		}
		Integer userId = (Integer) userInfo.get("id");
		
		if("1".equals(verifyType)){
			// 1.手机验证码情况
			try {
				userSafeService.verifyPhoneCodeByUserId(userId, phoneCaptcha);
			} catch (BusinessException e) {
				codeMap.put("code", e.getErrorcode());
				codeMap.put("message", e.getMessage());
				obj.clear();
				obj.put("error", codeMap);
				return JsonResponseUtil.buildBusiExcepResonpe(e).toString();
			}
		}
		if("2".equals(verifyType)){
			// 2.谷歌验证码情况
			try {
				userSafeService.verifyGoogleCodeByUserId(userId, googleCaptcha);
			} catch (BusinessException e) {
				codeMap.put("code", e.getErrorcode());
				codeMap.put("message", e.getMessage());
				obj.clear();
				obj.put("error", codeMap);
				return JsonResponseUtil.buildBusiExcepResonpe(e).toString();
			}
		}
		// 3.验证邮箱验证码
			try {
				userSafeService.verifyEmailCodeByUserId(userId, email, emailCaptcha);
				// 验证通过更新数据信息到数据库中
				userSafeService.bindEmailByUserId(userId, email);
				
				result = JsonResponseUtil.buildSucessResonpe(true).toString();
			} catch (BusinessException e) {
				codeMap.put("code", e.getErrorcode());
				codeMap.put("message", e.getMessage());
				obj.clear();
				obj.put("error", codeMap);
				result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
			}
//		return obj.toString();
		return result;
	}
	
	// 实名认证过程：1.主要涉及图片上传的处理
	
	
	
}
*/