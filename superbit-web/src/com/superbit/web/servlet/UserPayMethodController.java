package com.superbit.web.servlet;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.superbit.core.entry.UserPayMethod;
import com.superbit.core.service.UserPayMethodService;
import com.superbit.utils.constantutil.CollectionUtils;
import com.superbit.web.listener.ServiceBeanFactory;
import com.superbit.web.request.UserPayMethodSaveRequest;
import com.superbit.web.utils.JsonResponseUtil;
import com.superbit.web.utils.SessionUserUtil;

import net.sf.json.JSONObject;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月20日 下午1:16:04  
  * @descrip 用户账户设置部分
  */
@Controller
@RequestMapping("/userPayMethod.act")
public class UserPayMethodController {
	
	private UserPayMethodService payMethodService;
	
	/**
	 * @function 构造器注入service对象
	 */
	@PostConstruct
	private void init(){
		payMethodService = ServiceBeanFactory.getBean(UserPayMethodService.class);
	}

	// 账户设置部分：1.根据用户查询用户银行卡、支付宝、微信绑定状态；2.银行卡的绑定与修改；3.支付宝的绑定与修改；4.微信账号的绑定与修改
		
	   /**
		 * 获取用户账户绑定情况，并显示  账户类型、真实姓名、支付账号
		 */
		@ResponseBody
		@RequestMapping(params = "cmd=initUserPayMethod")
		public String initUserPayMethod(HttpServletRequest req, HttpServletResponse resp){
			
			// 从Session中获取用户ID
			Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
			if(null == userId){
				JsonResponseUtil.buildNormalExcepResonpe("US624", "User Session is overdued!");
			}
			
			// 获取用户支付方式
			List<UserPayMethod> result = payMethodService.getUserPayMethodByUserId(userId);			
			return JsonResponseUtil.buildSucessResonpe(result).toString();
		}
		
		/**
		 * 绑定用户银行卡、支付宝、微信
		 */
		@ResponseBody
		@RequestMapping(params = "cmd=bindUserPayMethod")
		public String bindUserPayMethod(HttpServletRequest req, HttpServletResponse resp, UserPayMethodSaveRequest saveRequest){
			
			Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
			if(null == userId){
				JsonResponseUtil.buildNormalExcepResonpe("US624", "User Session is overdued!");
			}
			Map<String, Object> map = CollectionUtils.buildMap(saveRequest);
			payMethodService.saveUserPayMethodByCondition(userId, map);
			String result = JsonResponseUtil.buildSucessResonpe(true).toString();
			return result;
		}
		
		/**
		 * 换绑用户银行卡、支付宝、微信时，查询原有信息
		 */
		@ResponseBody
		@RequestMapping(params = "cmd=getUserPayMethod")
		public String getUserPayMethodByCondition(HttpServletRequest req, HttpServletResponse resp, final Integer payMethod){
			
			Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
			if(null == userId){
				JsonResponseUtil.buildNormalExcepResonpe("US624", "User Session is overdued!");
			}
			if(payMethod <0 || payMethod >3){
				JsonResponseUtil.buildNormalExcepResonpe("US625", "Wrong Pay Method ID!");
			}
			UserPayMethod payMethodDetail = payMethodService.getPayMethodDetailByCondition(userId, payMethod);
			//【为0直接返回，为其他需拼接图片信息一起返回】
			Map<String, Object> result = CollectionUtils.buildMap(payMethodDetail);
			return JsonResponseUtil.buildSucessResonpe(result).toString();
		}
}
