package com.superbit.web.servlet.chat;

import java.util.Set;

import javax.websocket.Endpoint;
import javax.websocket.server.ServerApplicationConfig;
import javax.websocket.server.ServerEndpointConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * websocket启动配置
 * 
 * @author zhangdaoguang
 *
 */
public class ChatConfig implements ServerApplicationConfig {
	private static Logger log = LoggerFactory.getLogger(ChatConfig.class);

	/**
	 * 注解的方式 启动
	 */
	public Set<Class<?>> getAnnotatedEndpointClasses(Set<Class<?>> scan) {
		log.info("------------websoket start-----------------");
		return scan;
	}

	/**
	 * 接口方式启动
	 */
	public Set<ServerEndpointConfig> getEndpointConfigs(
			Set<Class<? extends Endpoint>> arg0) {
		return null;
	}

}
