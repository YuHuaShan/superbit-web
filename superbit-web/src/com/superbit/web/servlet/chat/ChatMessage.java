package com.superbit.web.servlet.chat;

/**
 * 聊天实体类
 * 
 * @author zhangdaoguang
 *
 */
public class ChatMessage {
	/* 订单号 */
	private String orderNumber;
	/* 发送人UID */
	private Integer fromUid;
	/* 接收人UID */
	private Integer toUid;
	/* 消息是否读取，0未读，1已读 */
	private Integer hasRead;
	/* 消息类型0用户消息，1系统消息 */
	private Integer chatType;
	/* 聊天内容 */
	private String content;
	/* 发消息时间 */
	private String sendMessageTime;

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Integer getFromUid() {
		return fromUid;
	}

	public void setFromUid(Integer fromUid) {
		this.fromUid = fromUid;
	}

	public Integer getToUid() {
		return toUid;
	}

	public void setToUid(Integer toUid) {
		this.toUid = toUid;
	}

	public Integer getHasRead() {
		return hasRead;
	}

	public void setHasRead(Integer hasRead) {
		this.hasRead = hasRead;
	}

	public Integer getChatType() {
		return chatType;
	}

	public void setChatType(Integer chatType) {
		this.chatType = chatType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSendMessageTime() {
		return sendMessageTime;
	}

	public void setSendMessageTime(String sendMessageTime) {
		this.sendMessageTime = sendMessageTime;
	}

}
