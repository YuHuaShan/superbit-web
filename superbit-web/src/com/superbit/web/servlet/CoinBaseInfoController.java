package com.superbit.web.servlet;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.superbit.core.entry.Coin_Base_Info;
import com.superbit.service.coin.define.Coin_Base_InfoService;
import com.superbit.web.excep.ErrorCodeConstant;
import com.superbit.web.listener.ServiceBeanFactory;
import com.superbit.web.utils.JsonResponseUtil;
import com.superbit.web.utils.StringPropertiesUtil;

@Controller
@RequestMapping("/coin.act")
public class CoinBaseInfoController {
	private static Logger logger = LoggerFactory.getLogger(CoinBaseInfoController.class);
	
	private Coin_Base_InfoService cbiService;
	@PostConstruct
	private void init(){
		cbiService = ServiceBeanFactory.getBean(Coin_Base_InfoService.class);
	}
	/**
	 * 添加币种
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=addCoinBaseInfo")
	public String addCoinBaseInfo(HttpServletRequest request, 
			String coinCode,
			String coinName,
			String buyOrder,
			String sellOrder,
			String serviceChargeIn,
			String serviceChargeOut,
			String withDrawCharge,
			String coinLimitMin,
			String coinLimitMax,
			String area,
			String coinRechargeDesc,
			String coinWithDrawDesc
			){
		//字段验证
		logger.info("addCoinBaseInfo");
		if(cbiService.verifyCoin_Base_Info("coinCode",coinCode)) {
			return JsonResponseUtil.buildNormalExcepResonpe(ErrorCodeConstant.NOUNIQUE,"币种编码已存在").toString();
		}
		if(cbiService.verifyCoin_Base_Info("coinName",coinName)) {
			return JsonResponseUtil.buildNormalExcepResonpe(ErrorCodeConstant.NOUNIQUE,"币种名已存在").toString();
		}
		
		Coin_Base_Info cbi = new Coin_Base_Info();
		cbi.setCoinId(StringPropertiesUtil.createIdUtil());
		cbi.setCoinStatus(0);
		cbi.setCoinCreateTime(new Date());
		cbi.setTop("0");
		cbi.setSeq(new BigDecimal("100"));
		cbi.setCoinName(coinName);
		cbi.setCoinCode(coinCode);
		cbi.setBuyOrder(new BigDecimal(buyOrder));
		cbi.setSellOrder(new BigDecimal(sellOrder));
		cbi.setServiceChargeIn(new BigDecimal(serviceChargeIn));
		cbi.setServiceChargeOut(new BigDecimal(serviceChargeOut));
		cbi.setWithDrawCharge(new BigDecimal(withDrawCharge));
		cbi.setCoinLimitMax(new BigDecimal(coinLimitMax));
		cbi.setCoinLimitMin(new BigDecimal(coinLimitMin));
		cbi.setCoinRechargeDesc(coinRechargeDesc);
		cbi.setCoinWithDrawDesc(coinWithDrawDesc);
		cbi.setArea(area);
		
		boolean flag = cbiService.addCoin_Base_Info(cbi);
		
		if(flag) {
			return JsonResponseUtil.buildSucessResonpe("ok").toString();
		}else {
			return JsonResponseUtil.buildSucessResonpe("error").toString();
		}
	}
	
	
	/**
	 * 获取币种信息
	 * @param request
	 * @param coinId
	 * @param flag
	 *     flag为0时获取未生效币种信息
	 *     flag为1时获取生效币种信息
	 *     flag为2时获取全部币种信息
	 *     flag为3时获取指定coinId币种信息
	 * @return
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=getCoin_Base_InfoList")
	public String getCoin_Base_InfoList(HttpServletRequest request, 
			String coinId,
			int flag
			){
		
		if(flag==3&&coinId.length()!=32) {
			return JsonResponseUtil.buildNormalExcepResonpe(ErrorCodeConstant.PARAMOUTOFRANGE, "coinId invalid").toString(); 
		}
		List<Coin_Base_Info> cbis = cbiService.getCoin_Base_InfoList(flag, coinId, null, null);
		return JsonResponseUtil.buildSucessResonpe(cbis).toString();
	}
	/**
	 * 删除币种信息
	 * @param request
	 * @param coinId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=deleteCoin_Base_Info")
	public String deleteCoin_Base_Info(HttpServletRequest request, 
			String coinId
			) {
		if(coinId.length()!=32) {
			return JsonResponseUtil.buildNormalExcepResonpe(ErrorCodeConstant.PARAMOUTOFRANGE, "coinId invalid").toString(); 
		}
		try {
			Coin_Base_Info cbi = cbiService.getCoin_Base_InfoList(3, coinId, null, null).get(0);
			cbiService.deleteCoin_Base_Info(cbi);
			return JsonResponseUtil.buildSucessResonpe(true).toString();
		} catch (Exception e) {
			// TODO: handle exception
			return JsonResponseUtil.buildNormalExcepResonpe(ErrorCodeConstant.DELETCOIN, "delete coin error").toString();
		}
	}
	/**
	 * 更新币种充值提现描述
	 * @param session
	 * @param request
	 * @param coinId
	 * @param coinRechargeDesc
	 * @param coinWithDrawDesc
	 * @return
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=updateCoin_Base_Info")
	public String updateCoin_Base_Info(HttpSession session, HttpServletRequest request, 
			String coinId,
			String coinRechargeDesc,
			String coinWithDrawDesc
			) {
		if(coinId.length()!=32) {
			return JsonResponseUtil.buildNormalExcepResonpe(ErrorCodeConstant.PARAMOUTOFRANGE, "coinId invalid").toString(); 
		}
		try {
			Coin_Base_Info cbi = cbiService.getCoin_Base_InfoList(3, coinId, null, null).get(0);
			cbi.setCoinWithDrawDesc(coinWithDrawDesc);
			cbi.setCoinRechargeDesc(coinRechargeDesc);
			cbi.setCoinUpdateTime(new Date());
			cbi.setCoinUpdateUser(StringPropertiesUtil.getUserId(session).toString());
//			cbi.setCoinUpdateUser(userDao.getUserBaseInfoById(StringPropertiesUtil.getUserId(session)).getNickName());
			cbiService.updateCoin_Base_Info(cbi);
			return JsonResponseUtil.buildSucessResonpe(true).toString();
		} catch (Exception e) {
			// TODO: handle exception
			return JsonResponseUtil.buildNormalExcepResonpe(ErrorCodeConstant.UPDATECOIN, "update coinInfo error").toString();
		}
	}
	/**
	 * 币种状态修改，生效失效。目前默认为只能生效，不能做失效处理
	 * @param session
	 * @param request
	 * @param coinId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=effectCoin_Base_Info")
	public String effectCoin_Base_Info(HttpSession session, HttpServletRequest request, 
			String coinId
			) {
		if(coinId.length()!=32) {
			return JsonResponseUtil.buildNormalExcepResonpe(ErrorCodeConstant.PARAMOUTOFRANGE, "coinId invalid").toString(); 
		}
		try {
			Coin_Base_Info cbi = cbiService.getCoin_Base_InfoList(3, coinId, null, null).get(0);
			cbi.setCoinStatus(1);
			cbi.setCoinUpdateTime(new Date());
			cbi.setCoinUpdateUser(StringPropertiesUtil.getUserId(session).toString());
			cbiService.updateCoin_Base_Info(cbi);
			return JsonResponseUtil.buildSucessResonpe(true).toString();
			
		} catch (Exception e) {
			// TODO: handle exception
			return JsonResponseUtil.buildNormalExcepResonpe(ErrorCodeConstant.EFFECTCOIN, "update coinStatus error").toString();
		}
	}
}
