package com.superbit.web.servlet;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.superbit.core.dto.ComplainDetailDto;
import com.superbit.core.dto.OrderDetailDto;
import com.superbit.core.dto.TradeLogListDto;
import com.superbit.core.entry.tradelog.TradeLog;
import com.superbit.core.service.TradeLogService;
import com.superbit.web.listener.ServiceBeanFactory;
import com.superbit.web.request.ComplainSaveRequest;
import com.superbit.web.request.TradeLogGetRequest;
import com.superbit.web.request.TradeLogSaveRequest;
import com.superbit.web.request.UpdateOrderRequest;
import com.superbit.web.response.CommonResponse;
import com.superbit.web.response.ResponseData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 交易记录controller
 * 
 * @author zhangdaoguang
 *
 */
@Controller
@RequestMapping("/trade.act")
public class TradeLogController {
	private static Logger log = LoggerFactory
			.getLogger(TradeLogController.class);

	private TradeLogService tradeLogService;

	@PostConstruct
	private void init() {
		tradeLogService = ServiceBeanFactory.getBean(TradeLogService.class);
	}

	@RequestMapping(params = "cmd=saveTradeLog")
	@ResponseBody
	public CommonResponse<Object> saveTradeLog(TradeLogSaveRequest saveRequest) {
		try {
			log.info("TradeLogController.saveTradeLog() request: {}",
					saveRequest.toString());
			TradeLog tradeLog = this.tradeLogService.getInitTradeLog();
			BeanUtils.copyProperties(tradeLog, saveRequest);
			// 设置手续费,先设置为0
			tradeLog.setPoundageBTC(new BigDecimal(0));
			tradeLogService.saveTradeLog(tradeLog);
			return CommonResponse.createSuccess();
		} catch (Exception e) {
			log.error("TradeLogController.saveTradeLog() exception", e);
			return CommonResponse.createSystemError();
		}
	}

	@RequestMapping(params = "cmd=getTradeLogList")
	@ResponseBody
	public CommonResponse<ResponseData<List<TradeLogListDto>>> getTradeLogList(
			TradeLogGetRequest getRequest) {
		try {
			log.info("TradeLogController.getTradeLogList() request: {}",
					getRequest.toString());
			if (!getRequest.validate()) {
				return CommonResponse.paramError();
			}
			List<TradeLogListDto> dtos = tradeLogService.getTradeLogList(
					getRequest.getUserId(), getRequest.getTradeType(),
					getRequest.getPayStatus(), getRequest.getHasComplain(),
					getRequest.getPayNumber(), getRequest.getCoinCode(),
					getRequest.getPageNo(), getRequest.getPageSize());
			ResponseData<List<TradeLogListDto>> data = new ResponseData<>(dtos);
			return CommonResponse.createPageSuccess(data, dtos.size());
		} catch (Exception e) {
			log.error("TradeLogController.getTradeLogList() exception", e);
			return CommonResponse.createSystemError();
		}
	}

	@RequestMapping(params = "cmd=getOrderDetail")
	@ResponseBody
	public CommonResponse<OrderDetailDto> getOrderDetail(String orderNumber) {
		try {
			log.info("TradeLogController.getOrderDetail() orderNumber: {}",
					orderNumber);
			if (StringUtils.isBlank(orderNumber)) {
				return CommonResponse.paramError();
			}
			OrderDetailDto orderDetail = this.tradeLogService
					.getOrderDetail(orderNumber);
			return CommonResponse.createSuccess(orderDetail);
		} catch (Exception e) {
			log.error("TradeLogController.getOrderDetail() exception", e);
			return CommonResponse.createSystemError();
		}
	}

	@RequestMapping(params = "cmd=updateOrder")
	@ResponseBody
	public CommonResponse<Object> updateOrder(UpdateOrderRequest request) {
		try {
			log.info("TradeLogController.updateOrder() request:{}", request);

			if (!request.validate()) {
				return CommonResponse.paramError();
			}

			this.tradeLogService.updateOrder(request.getOrderNumber(),
					request.getPayStatus(), request.getFundPassword());
			return CommonResponse.createSuccess();
		} catch (Exception e) {
			log.error("TradeLogController.updateOrder() exception", e);
			return CommonResponse.createSystemError();
		}
	}

	@RequestMapping(params = "cmd=insertComplain")
	@ResponseBody
	public CommonResponse<Object> insertComplain(ComplainSaveRequest request) {
		try {
			log.info("TradeLogController.insertComplain() request:{}", request);

			if (!request.validate()) {
				return CommonResponse.paramError();
			}

			this.tradeLogService.saveComplainLog(request.getOrderNumber(),
					request.getUid(), request.getComplainUid(),
					request.getComplainContent());
			return CommonResponse.createSuccess();
		} catch (Exception e) {
			log.error("TradeLogController.insertComplain() exception", e);
			return CommonResponse.createSystemError();
		}
	}

	@RequestMapping(params = "cmd=getComplainResultDetail")
	@ResponseBody
	public CommonResponse<ComplainDetailDto> getComplainResultDetail(
			String orderNumber) {
		try {
			log.info(
					"TradeLogController.getComplainResultDetail() orderNumber:{}",
					orderNumber);

			if (StringUtils.isBlank(orderNumber)) {
				return CommonResponse.paramError();
			}

			ComplainDetailDto resultDto = this.tradeLogService
					.getComplainDetail(orderNumber);
			return CommonResponse.createSuccess(resultDto);
		} catch (Exception e) {
			log.error("TradeLogController.getComplainResultDetail() exception",
					e);
			return CommonResponse.createSystemError();
		}
	}

	@RequestMapping(params = "cmd=cancelComplain")
	@ResponseBody
	public CommonResponse<Object> cancelComplain(String orderNumber) {
		try {
			log.info("TradeLogController.cancelComplain() orderNumber:{}",
					orderNumber);

			if (StringUtils.isBlank(orderNumber)) {
				return CommonResponse.paramError();
			}

			this.tradeLogService.complainCancel(orderNumber);
			return CommonResponse.createSuccess();
		} catch (Exception e) {
			log.error("TradeLogController.cancelComplain() exception", e);
			return CommonResponse.createSystemError();
		}
	}

}
