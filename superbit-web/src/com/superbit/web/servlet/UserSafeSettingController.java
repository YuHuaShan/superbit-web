package com.superbit.web.servlet;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.superbit.core.constant.UserStatusConstant;
import com.superbit.core.entry.UserBaseInfo;
import com.superbit.core.entry.UserSafeInfo;
import com.superbit.core.exception.BusinessException;
import com.superbit.core.exception.ParamInvalidException;
import com.superbit.core.service.LoginService;
import com.superbit.core.service.RegisterService;
import com.superbit.core.service.TradeLogProxy;
import com.superbit.core.service.UserAssetService;
import com.superbit.core.service.UserRealAuthenService;
import com.superbit.core.service.UserSafeService;
import com.superbit.service.util.StringTools;
import com.superbit.utils.mailutil.EmailFailException;
import com.superbit.web.listener.ServiceBeanFactory;
import com.superbit.web.utils.JsonResponseUtil;
import com.superbit.web.utils.SessionUserUtil;
import com.superbit.web.utils.StringValidateUtil;

import net.sf.json.JSONObject;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月22日 下午6:16:30  
  * @descrip 
  */
@Controller
@RequestMapping("/userSafe.act")
public class UserSafeSettingController {

private static Logger log = LoggerFactory.getLogger(TradeLogController.class);
			
	
	private RegisterService registerService; //不需要加Autowired注解
	private LoginService loginService; //不需要加Autowired注解
	private UserSafeService userSafeService; //不需要加Autowired注解
	private UserAssetService userAssertService; //不需要Autowired注解
	private TradeLogProxy tradeLogProxy;
	private UserRealAuthenService realAuthenService;
	
	
	/**
	 * @function 构造器注入service对象
	 */
	@PostConstruct
	private void init(){
		registerService = ServiceBeanFactory.getBean(RegisterService.class);
		loginService = ServiceBeanFactory.getBean(LoginService.class);
		userSafeService = ServiceBeanFactory.getBean(UserSafeService.class);
		userAssertService = ServiceBeanFactory.getBean(UserAssetService.class);
		tradeLogProxy = ServiceBeanFactory.getBean(TradeLogProxy.class);
		realAuthenService = ServiceBeanFactory.getBean(UserRealAuthenService.class);
	}
	
	//显示用户总资产、
	@ResponseBody
	@RequestMapping(params = "cmd=showTotalAssert")
	public String showTotalAssert(HttpServletRequest req, HttpServletResponse resp){
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		Map<String, BigDecimal> totalAsset = userAssertService.getUser_Asset(userId,"BTC");
		return JsonResponseUtil.buildSucessResonpe(totalAsset).toString();
	}
	
	// 用户昵称、安全等级、注册时间、邮箱、手机号、谷歌验证码、登录密码、资金密码、实名认证
	//、交易次数、平均放行时间、最近交易时间、
	@ResponseBody
	@RequestMapping(params = "cmd=showCompInfo")
	public String showCompInfo(HttpServletRequest req, HttpServletResponse resp){
		// 
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		// 总资产
		//Map<String, Object> user_Asset = userAssertService.getUser_Asset(userId);
		Map<Object, Object> result = new HashMap<>();
		Long avgDoneTime = tradeLogProxy.avgDoneTime(userId);
		Timestamp lastTradeTime = tradeLogProxy.getLastTradeTime(userId);
		Integer tradeCount = tradeLogProxy.getTradeCount(userId);
		UserBaseInfo userBaseInfo = loginService.getUserBaseInfo(userId);
		Map<String, Object> userSafeInfo = loginService.getUserSafeInfo(userId);
		result.put("userId", userId);
		result.put("nickname", userBaseInfo.getNickname());
		result.put("avgDoneTime", avgDoneTime);
		result.put("lastTradeTime", lastTradeTime);
		result.put("tradeCount", tradeCount);
		result.put("securityLevel", userSafeInfo.get("securityLevel"));
		result.put("email", userBaseInfo.getEmail());
		result.put("phone", userBaseInfo.getPhone());
		result.put("hasGooglePrivateKey", userSafeInfo.get("googlePrivateKey"));
		result.put("hasLoginPassword", userBaseInfo.getPassword());
		result.put("hasFundPassword", userSafeInfo.get("fundPassword"));
		result.put("authenStatus", userSafeInfo.get("authenStatus"));
		if(UserStatusConstant.AUTHEN_STATUS_FAIL.equals(userSafeInfo.get("authenStatus").toString())){
			// 审核失败时添加失败理由
			String failReason = realAuthenService.getFailReasonByUserId(userId);
			result.put("failReason", failReason);
		}
		
		return JsonResponseUtil.buildSucessResonpe(result).toString();
	}
		
	
	// 查询昵称、设置昵称（需验证昵称是否存在）
	@ResponseBody
	@RequestMapping(params = "cmd=setNicknameAndFundPwd")
	public String setNicknameAndFundPassword(HttpServletRequest req, String nickname, String fundPassword, String reFundPassword){
		
		String result = "";
		//Integer userId = (Integer) req.getSession().getAttribute("userId");
		Integer userId = 6;
		// 1.验证昵称格式，资金密码格式
		boolean condition1 = StringValidateUtil.isNull(nickname);
		boolean condition2 = StringValidateUtil.isNull(fundPassword);
		boolean condition3 = StringValidateUtil.isNull(reFundPassword);
		if(condition1 || condition2 || condition3){
			throw new ParamInvalidException("parameter", " not null");
		}
		 boolean condition4 = StringValidateUtil.isNickname(nickname);
		 boolean condition5 = StringValidateUtil.isFundPassword(fundPassword);
		 boolean condition6 = StringValidateUtil.isFundPassword(fundPassword);
		 if(condition4&&condition5&&condition6){
			 if(!fundPassword.equals(reFundPassword)){
				 return JsonResponseUtil.buildNormalExcepResonpe("US617", "different password").toString();
			 }
			 
			 // 输入通过有效性验证
			 boolean existNickname = userSafeService.checkNickname(nickname);
			// 2.后台验证昵称是否存在，不存在更新昵称和资金密码
			 if(!existNickname){
				 // 昵称存在
				 req.getSession().removeAttribute("nickname");
				 return JsonResponseUtil.buildNormalExcepResonpe("US622", "Nickname exists!").toString();
			 }
			 userSafeService.setNicknameAndFundPassword(userId, nickname, fundPassword);
			 result = JsonResponseUtil.buildSucessResonpe(true).toString();
		 }else{
			 throw new ParamInvalidException("parameter", "The input does not meet the requirements!");
		 }		
		//return obj.toString();
		 return result;
	}
	
	
	// 查询登录密码、修改登录密码
	@ResponseBody
	@RequestMapping(params = "cmd=changePwdByOldPwd")
	public String changePasswordByOldPassword(HttpServletRequest req, String oldPassword, String newPassword, final String rePassword){
		String result = "";
		// 验证密码有效性
		boolean isPassword = StringValidateUtil.isLoginPassword(oldPassword);
		boolean isNewPassword = StringValidateUtil.isLoginPassword(newPassword);
		boolean isRePassword = StringValidateUtil.isLoginPassword(rePassword);
		if(isPassword && isNewPassword && isRePassword){
			// 三个密码有效性验证通过，比较新密码是否一致
			if(!newPassword.equals(rePassword)){
				// 从session中移除确认密码
				req.getSession().removeAttribute("repassword");
				return JsonResponseUtil.buildNormalExcepResonpe("US617", "different password").toString();
			}
			
			// 在service里验证旧密码是否正确；若正确则更新登录密码
			//Integer userId = (Integer) req.getSession().getAttribute("userId");
			Integer userId = 5;
			try {
				userSafeService.changePasswordByOldPassword(userId, oldPassword, newPassword);
				
				result = JsonResponseUtil.buildSucessResonpe(true).toString();
			} catch (BusinessException e) {
				result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();

			}
		}
		return result;
		
	}
	
	// 查询资金密码、修改资金密码
	/**
	 * 用户点击修改资金密码的初始化操作
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=initFundPasswordChange")
	public String initFundPasswordChange(HttpServletRequest req, HttpServletResponse resp){
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		// eamil bindPhone hasGoogleCode
		Map<String, Object> userInfo = SessionUserUtil.getUserInfo(req.getSession());
		boolean hasBind = (boolean) userInfo.get("bindPhone");
		String email = (String) userInfo.get("email");
		boolean hasGoogleCode = (boolean) userInfo.get("hasGoogleCode");
		//String phone = (String) userInfo.get("phone");
		String result = "";

		HashMap<Object, Object> codeMap = new HashMap<>();
		if(! hasBind){
			// 没有手机提示去绑定手机
			codeMap.put("code", 1);
			codeMap.put("message", "bindPhone");

			return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
		}
		// 判断有没有邮箱【有手机】有没有谷歌
		if(null == email && email.isEmpty()){
			// 没有邮箱
			if(hasGoogleCode){
				// 有谷歌
				codeMap.put("code", 2);  // 有谷歌没邮箱
				codeMap.put("message", "hasGoogle");

				return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
			}else if(hasBind){
				// 有手机
				codeMap.put("code", 3);  // 有手机没邮箱
				codeMap.put("message", "hasPhone");
				//return obj.toString();
				return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
			}else{
				
				return JsonResponseUtil.buildNormalExcepResonpe("US753", "The user is not exist!").toString();
			}
		}else{
			// 有邮箱
			if(hasGoogleCode){
				// 有谷歌
				codeMap.put("code", 4);
				codeMap.put("message", "hasGoogleAndEmail");				
				return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
			}else if(hasBind){
				// 有手机
				codeMap.put("code", 5);
				codeMap.put("message", "hasPhoneAndEmail");			
				return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
			}else{
				return JsonResponseUtil.buildNormalExcepResonpe("US753", "The user is not exist!").toString();
			}			
		}
	}
	
	/**
	 * 修改用户资金密码【type 获取页面类型的参数】2 有谷歌没邮箱 3 有手机没邮箱 4有谷歌有邮箱  5 有手机有邮箱
	 * 加传一个参数页面类型	 pageType 
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=changeFundPassword")
	public String changeFundPassword(HttpServletRequest req, HttpServletResponse resp, final String newFundPassword, final String reFundPassword, final String emailCaptcha, final String googleCaptcha, final String phoneCaptcha, final String pageType){

		String result = "";
		// 验证密码
		boolean isPassword = StringValidateUtil.isFundPassword(newFundPassword);
		boolean isRePassword = StringValidateUtil.isFundPassword(reFundPassword);
		if(reFundPassword.contains(" ") || !isPassword || newFundPassword.contains(" ") || !isRePassword){

			return JsonResponseUtil.buildNormalExcepResonpe("US616", "Validate password!").toString();
		}
		// 验证两次密码是否一致
		if(!newFundPassword.equals(reFundPassword)){

			return JsonResponseUtil.buildNormalExcepResonpe("US617", "Different password!").toString();
		}
		String verifyType = pageType.toString();
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		Map<String, Object> userInfo = SessionUserUtil.getUserInfo(req.getSession());
		String email = (String) userInfo.get("email");
		try{
			if("2".equals(verifyType)){
				// 只验证谷歌验证码;保存新密码
				userSafeService.verifyGoogleCodeByUserId(userId, googleCaptcha);
			}else if("3".equals(verifyType)){
				// 只验证手机验证;保存新密码
				userSafeService.verifyPhoneCodeByUserId(userId, phoneCaptcha);
			}else if("4".equals(verifyType)){
				// 验证谷歌和邮箱;保存新密码
				userSafeService.verifyGoogleCodeByUserId(userId, googleCaptcha);
				userSafeService.verifyEmailCodeByUserId(userId, email, emailCaptcha);
			}else if("5".equals(verifyType)){
				// 验证手机和邮箱;保存新密码
				userSafeService.verifyPhoneCodeByUserId(userId, phoneCaptcha);
				userSafeService.verifyEmailCodeByUserId(userId, email, emailCaptcha);
			}
			userSafeService.changeFundPassword(userId, newFundPassword);

			 result = JsonResponseUtil.buildSucessResonpe(true).toString();
		}catch(BusinessException e){

			 result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		}		
		return result;
	}
	
	// 查询谷歌验证、绑定谷歌验证码
	/**
	 * 绑定谷歌验证码之判断手机号，初始化谷歌验证器二维码
	 * 
	 */ 
	@ResponseBody
	@RequestMapping(params = "cmd=initGoogleBind")
	public String initGoogleBind(HttpServletRequest req, HttpServletResponse resp){
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		JSONObject obj = new JSONObject();
		Map<String, Object> codeMap = new HashMap<>();
		String result = "";
		Map<String, Object> userInfo = SessionUserUtil.getUserInfo(req.getSession());
		boolean isBindPhone = (boolean) userInfo.get("bindPhone");
		if(!isBindPhone){
			// 没有绑定手机提示去绑定手机
			codeMap.put("code", 1);
			codeMap.put("message", "bindPhone");
			return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
		}
		// 生成谷歌验证器二维码，存到redis；显示到页面
		try {
			Map<String,Object> map = userSafeService.getGoogleAuth(userId);

			result = JsonResponseUtil.buildSucessResonpe(map).toString();
			
		} catch (BusinessException e) {

			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		}
		return result;
	}
	
	/**
	 * 用户点击绑定谷歌的时候绑定的实现
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=bindGooglePrivateKey")
	public String bindGooglePrivateKey(HttpServletRequest req, HttpServletRequest resp, String googleCaptcha, String phoneCaptcha){
		// 1.验证谷歌验证码和手机验证码
		// 2.保存谷歌私钥到MySQL数据库，删除redis中存储信息			
		StringValidateUtil.validateNotEmpty(googleCaptcha, "googleCaptcha");
		StringValidateUtil.validateNotEmpty(phoneCaptcha, "phoneCaptcha");
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		String result = "";
		
		try {
			userSafeService.verifyGoogleCodeByUserId(userId, googleCaptcha);
			userSafeService.verifyPhoneCodeByUserId(userId, phoneCaptcha);
			userSafeService.bindGoogleAuth(userId);
			// 3.更新session中用户状态
			SessionUserUtil.sendUpdateUserMessage(userId, "hasGoogleCode", true, req.getSession());
			SessionUserUtil.sendUpdateUserMessage(userId, "authType", 3, req.getSession());
			//obj.put("data", true);
			result = JsonResponseUtil.buildSucessResonpe(true).toString();
		} catch (BusinessException e) {
			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		}		
		return result;
	}
	
	/**
	 * 绑定手机之输入手机号
	 * 
	 */ 
	@ResponseBody
	@RequestMapping(params = "cmd=initPhoneBind")
	public String initPhoneBind(HttpServletRequest req, HttpServletResponse resp,String phone, String areaCode){
		StringValidateUtil.validateNotEmpty(phone, "phone");
		phone = phone.replaceAll(" ", "");
		boolean isPhone = StringValidateUtil.isPhone(phone);
		if(!isPhone){
			throw new ParamInvalidException("phone", " incorrect format");
		}

		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		//Integer userId = 5;
		String result = "";
		boolean bool;
		
		try {
			bool = userSafeService.applyForPhoneBind(userId, phone, areaCode);
			result = JsonResponseUtil.buildSucessResonpe(bool).toString();
		} catch (BusinessException e) {
			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		}
		//return obj.toString();
		return result;
	}
	
	/**
	 * 绑定手机之验证验证码
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=bindPhoneByCode")
	public String bindPhoneByCode(HttpServletRequest req, HttpServletResponse resp, String phone, final String captcha){
		StringValidateUtil.validateNotEmpty(captcha, "captcha");
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		//Integer userId = 5;
		String result = "";
		
		try {
			userSafeService.bindPhone(userId, captcha, phone);
			// 更新用户状态 成功后更新session中用户信息
			UserBaseInfo userBaseInfo = loginService.getUserBaseInfo(userId);
			UserSafeInfo userSafeInfo = userSafeService.getUserSafeInfo(userId);
			SessionUserUtil.sendUpdateUserMessage(userId, "phone", this.replacePhone(userBaseInfo.getPhone().toString()), req.getSession());
			SessionUserUtil.sendUpdateUserMessage(userId, "bindPhone", "1", req.getSession());//手机绑定状态：1 -- 已绑定
			// 更改安全级别			
			if(StringTools.isBlank(userSafeInfo.getGooglePrivateKey())){
					SessionUserUtil.sendUpdateUserMessage(userId, "securityLevel", UserStatusConstant.SECURITY_LEVEL_PHONE,req.getSession());
			}
//			obj.put("data", true);
			result = JsonResponseUtil.buildSucessResonpe(true).toString();
		} catch (BusinessException e) {

			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		}
		
		return result;
	}
	
	
	/**
	 * 隐藏手机号码的部分信息
	 * @param phone
	 * @return
	 */
	private Object replacePhone(String phone) {
		if(phone == null || phone.equals("")){
			phone = "";
		}else if(phone.length()<8){
			phone = "********";
		}else{
			String pre = phone.substring(0, phone.length()-8);
			String post = phone.substring(phone.length()-4);
			phone = pre + "****" + post;
		}
		return phone;
	}
	

	/**
	 * 绑定邮箱之输入邮箱:1.绑定邮箱之前判断手机及谷歌是否绑定都不存在去绑定手机；2.存在谷歌验证谷歌；3.存在手机不存在谷歌
	 * 
	 */ 
	@ResponseBody
	@RequestMapping(params = "cmd=initEmailBind")
	public String initEmailBind(HttpServletRequest req, HttpServletResponse resp,String email){
		StringValidateUtil.validateNotEmpty(email, "email");
		email = email.replaceAll(" ", "");
		boolean isEmail = StringValidateUtil.isEmail(email);
		Integer userId = SessionUserUtil.getLoginUserId(req.getSession());
		//有谷歌验证谷歌
		Map<String, Object> userInfo = SessionUserUtil.getUserInfo(req.getSession());
		boolean hasGoogleCode = (boolean) userInfo.get("hasGoogleCode");
		byte bindPhone = (byte) userInfo.get("bindPhone");
		//boolean hasGoogleCode = false;
		//byte bindPhone = 1;
		Map<String, Object> codeMap = new HashMap<String, Object>();
		if(!UserStatusConstant.BIND_PHONE_YES.equals(bindPhone)){
			codeMap.put("code", 1);
			codeMap.put("message", "bindPhone");
			return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
		}
		if(hasGoogleCode){
			codeMap.put("code", 2);
			codeMap.put("message", "verifyGoogle");
			return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
		}
		
		codeMap.put("code", 3);
		codeMap.put("message", "verifyPhone");

		return JsonResponseUtil.buildSucessResonpe(codeMap).toString();
	}
	
	/**
	 * 点击"获取验证码"获取邮箱验证码
	 * 
	 */
	@ResponseBody
	@RequestMapping(params= "cmd=getEmailCode")
	public String getEmailCode(HttpServletRequest req, String email){
		StringValidateUtil.validateNotEmpty(email, "email");
		boolean isEmail = StringValidateUtil.isEmail(email);
		if(!isEmail){
			throw new ParamInvalidException("email", "not email ");
		}

		String result = "";
		try {
			registerService.emailRegister(email);
			result = JsonResponseUtil.buildSucessResonpe(true).toString();
		} catch (BusinessException e) {
			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		} catch (EmailFailException e) {
			result = JsonResponseUtil.buildNormalExcepResonpe("US626", "Email Send Fail!").toString();
		}
		return result;
	}
	
	/**
	 * 点击"获取验证码"获取短信验证码【绑定邮箱】
	 * 点击"获取验证码"获取短信验证码【绑定谷歌】
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=getPhoneCode")
	public String getPhoneCode(HttpServletRequest req){

		String result = "";
		
		Map<String, Object> userInfo = SessionUserUtil.getUserInfo(req.getSession());

		Integer userId = (Integer) userInfo.get("id");
		//Integer userId = 6;
		try {
			
			userSafeService.getPhoneCodeByUserId(userId);

			result = JsonResponseUtil.buildSucessResonpe(true).toString();
		} catch (BusinessException e) {

			result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
		}
		
		return result;
	}
	
	/**
	 * 绑定邮箱之获取验证码【谷歌验证码或手机验证码】
	 */
	@SuppressWarnings("null")
	@ResponseBody
	@RequestMapping(params = "cmd=bindEmailByCode")
	public String bindEmailByCode(HttpServletRequest req, HttpServletResponse resp, String email, final String emailCaptcha, final String phoneCaptcha, final String googleCaptcha){

		String result = "";
		// 验证邮箱、邮箱验证码，再验证谷歌或手机验证码 
		// 页面带一个参数表明是谷歌验证还是短信验证:验证类型 1手机 2谷歌
		//String verifyType = (String) req.getAttribute("verifyType");
		Map<String, Object> userInfo = SessionUserUtil.getUserInfo(req.getSession());
		// 验证邮箱及邮箱验证码
		StringValidateUtil.validateNotEmpty(email, "email");
		boolean isEmail = StringValidateUtil.isEmail(email);
		if(!isEmail){
			throw new ParamInvalidException("email", "not email ");
		}
		Integer userId = (Integer) userInfo.get("id");
		//Integer userId = 6;
		if((null == phoneCaptcha && phoneCaptcha.isEmpty()) && !(null == googleCaptcha && googleCaptcha.isEmpty())){
			// 2.谷歌验证码情况
			try {
					userSafeService.verifyGoogleCodeByUserId(userId, googleCaptcha);
			} catch (BusinessException e) {
					return JsonResponseUtil.buildBusiExcepResonpe(e).toString();
			}
			
		}else if(!(null == phoneCaptcha && phoneCaptcha.isEmpty()) && (null == googleCaptcha && googleCaptcha.isEmpty())){
			// 1.手机验证码情况
			try {
					userSafeService.verifyPhoneCodeByUserId(userId, phoneCaptcha);
			} catch (BusinessException e) {
					return JsonResponseUtil.buildBusiExcepResonpe(e).toString();
			}
		}else{
			return JsonResponseUtil.buildNormalExcepResonpe("US605", "Captcha is error!").toString();
		}
		/*if("1".equals(verifyType)){
			// 1.手机验证码情况
			try {
				userSafeService.verifyPhoneCodeByUserId(userId, phoneCaptcha);
			} catch (BusinessException e) {
				return JsonResponseUtil.buildBusiExcepResonpe(e).toString();
			}
		}
		if("2".equals(verifyType)){
			// 2.谷歌验证码情况
			try {
				userSafeService.verifyGoogleCodeByUserId(userId, googleCaptcha);
			} catch (BusinessException e) {
				return JsonResponseUtil.buildBusiExcepResonpe(e).toString();
			}
		}*/
		// 3.验证邮箱验证码
			try {
				userSafeService.verifyEmailCodeByUserId(userId, email, emailCaptcha);
				// 验证通过更新数据信息到数据库中
				userSafeService.bindEmailByUserId(userId, email);
				
				result = JsonResponseUtil.buildSucessResonpe(true).toString();
			} catch (BusinessException e) {
				
				result = JsonResponseUtil.buildBusiExcepResonpe(e).toString();
			}
		return result;
	}
	
	// 实名认证过程：1.主要涉及图片上传的处理
	
	
}
