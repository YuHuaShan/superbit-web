package com.superbit.web.servlet;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.superbit.core.entry.UserSafeInfo;
import com.superbit.core.entry.User_Asset;
import com.superbit.core.tools.PageListRes;
import com.superbit.service.userasset.service.define.User_AssetService;
import com.superbit.web.listener.ServiceBeanFactory;
import com.superbit.web.utils.JsonResponseUtil;
import com.superbit.web.utils.StringPropertiesUtil;

@Controller
@RequestMapping("/user_asset.act")
public class UserAssetController {
	
	private static Logger logger = LoggerFactory.getLogger(UserAssetController.class);
	
//	private UserSafeInfoProxy userSafeInfoProxy;
	private User_AssetService user_AssetService;
	@PostConstruct
	private void init(){
		user_AssetService = ServiceBeanFactory.getBean(User_AssetService.class);
//		userSafeInfoProxy = ServiceBeanFactory.getBean(UserSafeInfoProxy.class);
	}
	/**
	 * 获取资产列表
	 * @param req
	 * @param userId  用户id
	 * @param pageSize  每页数量
	 * @param pageNo  请求页码 默认为第一页
	 * @param flag  是否隐藏资产为0的记录，flag=0不隐藏，flag=1隐藏。当所有资产都为0时不隐藏
	 * @return
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=getUser_AssetList")
	public String getUser_AssetList(HttpSession session, HttpServletRequest req, 
			int pageSize,
			int pageNo,
			int flag){
		logger.info("cmd=getUser_AssetList");
		Integer userId = StringPropertiesUtil.getUserId(session);
		//资产初始化判断工作，如果有新增生效币种不在用户资产中，则新增一条资产记录
		user_AssetService.initUser_Assets(userId);
		
		
		PageListRes<User_Asset> result = new PageListRes<User_Asset>();
		result.setPageSize(pageSize);
		result.setPageNo(pageNo);
		if(flag==0) {//不隐藏
			result.setTotal(user_AssetService.getUser_AssetTotalCount(userId, 0));
		}else {//隐藏，判断资产
			//获取资产总数大于0的条数
			int total = user_AssetService.getUser_AssetTotalCount(userId, 1);
			//记录总条数，当前页码
			result.setTotal(user_AssetService.getUser_AssetTotalCount(userId, total>0?flag:0));
		}
		//记录当前页码对应内容
		List<User_Asset> user_AssetList = user_AssetService.getUser_AssetList(userId, flag, (pageNo-1)*pageSize, pageSize);
		result.setList(user_AssetList);
		return JsonResponseUtil.buildSucessResonpe(result).toString();
	}
	
	
	
	/**
	 * 获取用户指定资产信息，并验证数据
	 * @param req
	 * @param userId  用户id
	 * @param assetId
	 * @param coinId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=getUser_Asset")
	public String getUser_Asset(HttpSession session, HttpServletRequest req, 
			String assetId,
			String coinId){
		logger.info("cmd=getUser_Asset");
		Integer userId = StringPropertiesUtil.getUserId(session);
		
//		UserSafeInfo usi = userSafeInfoProxy.verificationSafeInfo(userId);
		UserSafeInfo usi = new UserSafeInfo();
		usi.setGooglePrivateKey("googlekeyxxxx");
		usi.setBindPhone(new Byte("0"));

		Map<String, Object> map = new HashMap<String, Object>();
		if(usi.getGooglePrivateKey()!=null) {
			map.put("google", true);
		}else if(usi.getBindPhone()==0) {
			map.put("phone", true);
		}
		
		User_Asset result = user_AssetService.getUser_Asset(userId, assetId, coinId, null);
		map.put("user_asset", result);
		return JsonResponseUtil.buildSucessResonpe(map).toString();
	}
	/**
	 * 统计用户资产信息
	 * @param session
	 * @param req
	 * @param userId
	 * @param stime
	 * @param etime
	 * @return
	 */
	@ResponseBody
	@RequestMapping(params = "cmd=getStatistics_User_Asset")
	public String getStatistics_User_Asset(HttpSession session, HttpServletRequest req, 
			String userId,
			String stime,
			String etime) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date sDate = null;
		Date eDate = null;
		try {
			sDate = stime.length()==19?simpleDateFormat.parse(stime):null;
			eDate = etime.length()==19?simpleDateFormat.parse(etime):null;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<User_Asset> user_Assets = null;
		if(null!=userId&&userId.length()>0) {
			user_Assets = user_AssetService.getSpell_User_Asset(Integer.valueOf(userId), sDate, eDate);
		}else {
			user_Assets = user_AssetService.getSpell_User_Asset(-1, sDate, eDate);
		}
		return JsonResponseUtil.buildSucessResonpe(user_Assets).toString();
	}
}
