package com.superbit.core.exception;

/**
 * 无指定记录异常，  一般产生原因是非常规操作或恶意攻击
 */
public class NoFitRecordExcepiton extends BaseRuntimeException {
	private static final long serialVersionUID = -3761946955970642530L;
	
	private String recid;
	private String className;
	
	public NoFitRecordExcepiton(String recid,String className) {
		super("RT102","record not find -"+className+":"+recid,null);
		this.recid = recid;
		this.className = className;
	}

	public String getRecid() {
		return recid;
	}

	public String getClassName() {
		return className;
	}

}
