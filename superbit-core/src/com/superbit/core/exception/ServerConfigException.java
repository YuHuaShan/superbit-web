package com.superbit.core.exception;

/**
 * 服务配置异常
 */
public class ServerConfigException extends BaseRuntimeException {
	private static final long serialVersionUID = 4561654767010747900L;
	

	public ServerConfigException(String message) {
		super("RT104",message,null);
	}
}
