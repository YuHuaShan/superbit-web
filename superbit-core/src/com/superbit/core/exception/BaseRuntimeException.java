package com.superbit.core.exception;

/**
 * 封装运行时异常
 */
public class BaseRuntimeException extends RuntimeException {
	private static final long serialVersionUID = -6693895810786471043L;
	
	/**
	 * 错误码，相关定义规范请参考《异常定义规范》
	 */
	private String errorcode;

	public BaseRuntimeException(String errorcode,String message,Throwable e) {
		super(message,e);
		this.errorcode = errorcode;
	}

	public String getErrorcode() {
		return errorcode;
	}
}
