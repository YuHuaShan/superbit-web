package com.superbit.core.exception;

/**
 * 服务运行环境异常
 */
public class ServerErrorException extends BaseRuntimeException {
	private static final long serialVersionUID = 4561654767010747900L;
	

	public ServerErrorException(String message,Throwable e) {
		super("RT103",message,e);
	}
}
