package com.superbit.core.exception;

/**
 * 封装业务异常，一般在正常情况下产生， 开发人员需要根据情况分别处理
 */
public class BusinessException extends Exception {
	private static final long serialVersionUID = -1986655358889488649L;
	/**
	 * 错误码，相关定义规范请参考《异常定义规范》
	 */
	private String errorcode;

	public BusinessException(String errorcode,String message,Throwable e) {
		super(message,e);
		this.errorcode = errorcode;
	}

	public String getErrorcode() {
		return errorcode;
	}
}
