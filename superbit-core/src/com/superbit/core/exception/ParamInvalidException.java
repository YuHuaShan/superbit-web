package com.superbit.core.exception;

/**
 * 参数不合法异常， 一般产生原因是 前台验证失效或恶意攻击
 */
public class ParamInvalidException extends BaseRuntimeException {
	private static final long serialVersionUID = -4274419711390943538L;
	
	private String paramName; 
	public ParamInvalidException(String paramName,String cause) {
		super("RT101","param invalid -"+paramName+":"+cause,null);
		this.paramName = paramName;
	}
	public String getParamName() {
		return paramName;
	}
}
