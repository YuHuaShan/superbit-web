package com.superbit.core.entry.tradelog;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 交易记录实体类
 * 
 * @author zhangdaoguang
 */
public class TradeLog implements Serializable {
	private static final long serialVersionUID = -6971237961683941941L;

	private Long id;
	/* 报价ID */
	private Long quoteId;
	/* 订单号 */
	private String orderNumber;
	/* 币种Code */
	private String coinCode;
	/* 币种名称 */
	private String coinName;
	/* 交易类型 */
	private Integer tradeType;
	/* 买家UID */
	private Integer buyserUid;
	/* 卖家UID */
	private Integer sellerUid;
	/* 价格CNY(单价) */
	private BigDecimal unitPrice;
	/* 数量BTC */
	private BigDecimal coinAmount;
	/* 金额CNY(总价) */
	private BigDecimal totalPrice;
	/* 手续费BTC */
	private BigDecimal poundageBTC;
	/* 交易方式： 记录id多个逗号分隔,0银行卡,1支付宝,2微信 */
	private String payType;
	/* 状态,0：未付款 1：已付款 2：已完成 3：买家已取消 4：超时取消 */
	private Integer payStatus;
	/* 0：没申诉，1：有申诉 */
	private Integer hasComplain;
	/* 下单时间 */
	private Timestamp createTime;
	/* 更新时间 */
	private Timestamp updateTime;
	/* 付款时间 */
	private Timestamp payTime;
	/* 放行时间 */
	private Timestamp doneTime;
	/* 放行时间 */
	private Timestamp cancelTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getQuoteId() {
		return quoteId;
	}

	public void setQuoteId(Long quoteId) {
		this.quoteId = quoteId;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getCoinCode() {
		return coinCode;
	}

	public void setCoinCode(String coinCode) {
		this.coinCode = coinCode;
	}

	public String getCoinName() {
		return coinName;
	}

	public void setCoinName(String coinName) {
		this.coinName = coinName;
	}

	public Integer getTradeType() {
		return tradeType;
	}

	public void setTradeType(Integer tradeType) {
		this.tradeType = tradeType;
	}

	public Integer getBuyserUid() {
		return buyserUid;
	}

	public void setBuyserUid(Integer buyserUid) {
		this.buyserUid = buyserUid;
	}

	public Integer getSellerUid() {
		return sellerUid;
	}

	public void setSellerUid(Integer sellerUid) {
		this.sellerUid = sellerUid;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getCoinAmount() {
		return coinAmount;
	}

	public void setCoinAmount(BigDecimal coinAmount) {
		this.coinAmount = coinAmount;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public BigDecimal getPoundageBTC() {
		return poundageBTC;
	}

	public void setPoundageBTC(BigDecimal poundageBTC) {
		this.poundageBTC = poundageBTC;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public Integer getHasComplain() {
		return hasComplain;
	}

	public void setHasComplain(Integer hasComplain) {
		this.hasComplain = hasComplain;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public Timestamp getPayTime() {
		return payTime;
	}

	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}

	public Timestamp getDoneTime() {
		return doneTime;
	}

	public void setDoneTime(Timestamp doneTime) {
		this.doneTime = doneTime;
	}

	public Timestamp getCancelTime() {
		return cancelTime;
	}

	public void setCancelTime(Timestamp cancelTime) {
		this.cancelTime = cancelTime;
	}

}
