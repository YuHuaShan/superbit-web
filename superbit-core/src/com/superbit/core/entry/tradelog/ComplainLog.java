package com.superbit.core.entry.tradelog;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 申诉记录表
 * 
 * @author zhangdaoguang
 *
 */
public class ComplainLog implements Serializable {
	private static final long serialVersionUID = 5782753569192726294L;
	private Long id;
	/* 订单号 */
	private String orderNumber;
	/* 申诉人UID */
	private Integer uid;
	/* 申诉对象UID */
	private Integer complainUid;
	/* 申诉状态 0：申诉中 1：申诉完成 2：申诉失败 */
	private Integer complainStatus;
	/* 申诉理由内容 */
	private String complainContent;
	/* 申诉结果内容 */
	private String resultContent;
	/* 申诉时间 */
	private Timestamp createTime;
	/* 更新(结果)时间 */
	private Timestamp updateTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public Integer getComplainUid() {
		return complainUid;
	}

	public void setComplainUid(Integer complainUid) {
		this.complainUid = complainUid;
	}

	public Integer getComplainStatus() {
		return complainStatus;
	}

	public void setComplainStatus(Integer complainStatus) {
		this.complainStatus = complainStatus;
	}

	public String getComplainContent() {
		return complainContent;
	}

	public void setComplainContent(String complainContent) {
		this.complainContent = complainContent;
	}

	public String getResultContent() {
		return resultContent;
	}

	public void setResultContent(String resultContent) {
		this.resultContent = resultContent;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

}
