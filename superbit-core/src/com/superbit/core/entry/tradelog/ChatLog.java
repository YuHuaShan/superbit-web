package com.superbit.core.entry.tradelog;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 聊天记录实体类
 * 
 * @author zhangdaoguang
 */
public class ChatLog implements Serializable {
	private static final long serialVersionUID = -2214973968685243662L;

	private Long id;
	/* 订单号 */
	private String orderNumber;
	/* 发消息用户UID */
	private Integer fromUid;
	/* 接消息用户UID */
	private Integer toUid;
	/* 发消息内容 */
	private String content;
	/* 0:未读,1已读 */
	private Integer hasRead;
	/* 0:用户消息,1系统消息 */
	private Integer chatType;
	/* 发消息时间 */
	private Timestamp createTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Integer getFromUid() {
		return fromUid;
	}

	public void setFromUid(Integer fromUid) {
		this.fromUid = fromUid;
	}

	public Integer getToUid() {
		return toUid;
	}

	public void setToUid(Integer toUid) {
		this.toUid = toUid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getHasRead() {
		return hasRead;
	}

	public void setHasRead(Integer hasRead) {
		this.hasRead = hasRead;
	}

	public Integer getChatType() {
		return chatType;
	}

	public void setChatType(Integer chatType) {
		this.chatType = chatType;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

}
