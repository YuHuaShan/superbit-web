package com.superbit.core.entry;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
 * 用户充值记录表
 * @author hongzhen
 *
 */
public class User_Recharge implements Serializable{

	private static final long serialVersionUID = 1L;
	private int id;
	/*充值用户id*/
	private int userId;
	/*用户充值资产id*/
	private String assetId;
	/*用户充值币种id*/
	private String coinId;
	/*用户充值币种类型*/
	private String coinType;
	/*用户充值币种名字*/
	private String coinName;
	/*用户充值地址*/
	private String RechargeAddr;
	/*用户充值平台*/
	private String RechargePlat;
	/*用户充值数量*/
	private BigDecimal RechargeAmount;
	/*用户充值手续费*/
	private BigDecimal serviceCharge;
	/*用户充值状态*/
	private int RechargeStatus = 0;
	/*用户充值创建时间*/
	private Date createTime;
	/*用户充值状态最近一次修改时间*/
	private Date updateTime;
	public User_Recharge() {
		super();
	}
	public User_Recharge(int id, int userId, String assetId, String coinId, String coinType, String coinName,
			String rechargeAddr, String rechargePlat, BigDecimal rechargeAmount, BigDecimal serviceCharge,
			int rechargeStatus, Date createTime, Date updateTime) {
		super();
		this.id = id;
		this.userId = userId;
		this.assetId = assetId;
		this.coinId = coinId;
		this.coinType = coinType;
		this.coinName = coinName;
		RechargeAddr = rechargeAddr;
		RechargePlat = rechargePlat;
		RechargeAmount = rechargeAmount;
		this.serviceCharge = serviceCharge;
		RechargeStatus = rechargeStatus;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public String getCoinId() {
		return coinId;
	}
	public void setCoinId(String coinId) {
		this.coinId = coinId;
	}
	public String getCoinType() {
		return coinType;
	}
	public void setCoinType(String coinType) {
		this.coinType = coinType;
	}
	public String getCoinName() {
		return coinName;
	}
	public void setCoinName(String coinName) {
		this.coinName = coinName;
	}
	public String getRechargeAddr() {
		return RechargeAddr;
	}
	public void setRechargeAddr(String rechargeAddr) {
		RechargeAddr = rechargeAddr;
	}
	public String getRechargePlat() {
		return RechargePlat;
	}
	public void setRechargePlat(String rechargePlat) {
		RechargePlat = rechargePlat;
	}
	public BigDecimal getRechargeAmount() {
		return RechargeAmount;
	}
	public void setRechargeAmount(BigDecimal rechargeAmount) {
		RechargeAmount = rechargeAmount;
	}
	public BigDecimal getServiceCharge() {
		return serviceCharge;
	}
	public void setServiceCharge(BigDecimal serviceCharge) {
		this.serviceCharge = serviceCharge;
	}
	public int getRechargeStatus() {
		return RechargeStatus;
	}
	public void setRechargeStatus(int rechargeStatus) {
		RechargeStatus = rechargeStatus;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
