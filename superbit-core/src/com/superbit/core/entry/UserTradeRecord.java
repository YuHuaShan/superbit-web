package com.superbit.core.entry;

import java.io.Serializable;
/**
 * 用户交易完成情况实体类 ：统计用户交易完成率交易次数等
 * @author 赵世栋
 *@version 2018-1-4
 */
public class UserTradeRecord  implements Serializable{

	private static final long serialVersionUID = 677150516964240913L;

	private long id;
	/*用户id*/
	private long userId;
	/*交易成功次数*/
	private int tradeFinishCount;
	/*交易失败次数*/
	private int failTradeCount;
	/*委托人*/
	private String consigner;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public int getTradeFinishCount() {
		return tradeFinishCount;
	}
	public void setTradeFinishCount(int tradeFinishCount) {
		this.tradeFinishCount = tradeFinishCount;
	}
	public String getConsigner() {
		return consigner;
	}
	public void setConsigner(String consigner) {
		this.consigner = consigner;
	}
	public int getFailTradeCount() {
		return failTradeCount;
	}
	public void setFailTradeCount(int failTradeCount) {
		this.failTradeCount = failTradeCount;
	}
	
}
