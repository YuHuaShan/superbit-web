package com.superbit.core.entry;

import java.io.Serializable;
import java.util.Date;

/**
 * 记录币种交易状态表
 * @author 赵世栋
 * @version 2018-1-10
 *
 */
public class CoinTradeBusiType implements Serializable {

	private static final long serialVersionUID = -4120037417540669003L;

	private long id;
	
	private String coinCode;
	/*启用状态 点击生效后默认是开启状态   0：停用状态 1：启用状态*/
	private int isActivate ;
	/*交易状态：0：关闭交易1：开启交易*/
	private int tradeStatus;
	/*币种名称*/
	private String coinName;
	/*创建时间*/
	private Date createTime;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCoinCode() {
		return coinCode;
	}
	public void setCoinCode(String coinCode) {
		this.coinCode = coinCode;
	}
	public int getIsActivate() {
		return isActivate;
	}
	public void setIsActivate(int isActivate) {
		this.isActivate = isActivate;
	}
	public int getTradeStatus() {
		return tradeStatus;
	}
	public void setTradeStatus(int tradeStatus) {
		this.tradeStatus = tradeStatus;
	}
	public String getCoinName() {
		return coinName;
	}
	public void setCoinName(String coinName) {
		this.coinName = coinName;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
