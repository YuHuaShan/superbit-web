package com.superbit.core.entry;

import java.io.Serializable;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月19日 下午2:25:16  
  * @descrip 用户图片信息
  */
public class UserImgInfo implements Serializable{

	
	private static final long serialVersionUID = -7782776840936853164L;
	
	private int id;
	/*用户ID*/
	private int userId;
	/*图片名称*/
	private String imgName;
	/*	图片类型imgType：0 表示身份认证信息 1 表示用户账户信息*/
	private byte imgType;
	/*排序sort：身份认证信息时 0 正面照 1 背面照 2 手持身份证照
	          用户账户信息时 0 支付宝账号 1 微信账号*/
	private byte sort;
	/*图片相对地址*/
	private String imgUrl;
	/*图片数据信息*/
	private String imgData;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getImgName() {
		return imgName;
	}
	public void setImgName(String imgName) {
		this.imgName = imgName;
	}
	public byte getImgType() {
		return imgType;
	}
	public void setImgType(byte imgType) {
		this.imgType = imgType;
	}
	public byte getSort() {
		return sort;
	}
	public void setSort(byte sort) {
		this.sort = sort;
	}	
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getImgData() {
		return imgData;
	}
	public void setImgData(String imgData) {
		this.imgData = imgData;
	}
	
	
	
}
