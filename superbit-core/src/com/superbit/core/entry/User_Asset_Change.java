package com.superbit.core.entry;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
 * 用户资产变更记录表
 * @author hongzhen
 *
 */
public class User_Asset_Change implements Serializable{

	private static final long serialVersionUID = 1L;
	private int id;
	/*用户id*/
	private int userId;
	/*资产id*/
	private String assetId;
	/*币种id*/
	private String coinId;
	/*币种名称*/
	private String coinName;
	/*用户当前币种总额*/
	private BigDecimal coinAmount;
	/*用户当前币种可用额度*/
	private BigDecimal coinBalance;
	/*用户当前币种冻结额度*/
	private BigDecimal coinFreeze;
	/*用户资产冻结结束时间*/
	private Date coinFreezeDate;
	/*用户当前币种每日提现限额*/
	private BigDecimal coinLimit;
	/*用户当前币种状态，0正常，1冻结；默认正常*/
	private int accountStatus = 0;
	/*记录创建时间*/
	private Date creatTime;
	public User_Asset_Change() {
		super();
	}
	
	
	public User_Asset_Change(int id, int userId, String assetId, String coinId, String coinName,
			BigDecimal coinAmount, BigDecimal coinBalance, BigDecimal coinFreeze, Date coinFreezeDate,
			BigDecimal coinLimit, int accountStatus, Date creatTime) {
		super();
		this.id = id;
		this.userId = userId;
		this.assetId = assetId;
		this.coinId = coinId;
		this.coinName = coinName;
		this.coinAmount = coinAmount;
		this.coinBalance = coinBalance;
		this.coinFreeze = coinFreeze;
		this.coinFreezeDate = coinFreezeDate;
		this.coinLimit = coinLimit;
		this.accountStatus = accountStatus;
		this.creatTime = creatTime;
	}


	public Date getCreatTime() {
		return creatTime;
	}


	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}


	public Date getCoinFreezeDate() {
		return coinFreezeDate;
	}
	public void setCoinFreezeDate(Date coinFreezeDate) {
		this.coinFreezeDate = coinFreezeDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public String getCoinId() {
		return coinId;
	}
	public void setCoinId(String coinId) {
		this.coinId = coinId;
	}
	public String getCoinName() {
		return coinName;
	}
	public void setCoinName(String coinName) {
		this.coinName = coinName;
	}
	public BigDecimal getCoinAmount() {
		return coinAmount;
	}
	public void setCoinAmount(BigDecimal coinAmount) {
		this.coinAmount = coinAmount;
	}
	public BigDecimal getCoinBalance() {
		return coinBalance;
	}
	public void setCoinBalance(BigDecimal coinBalance) {
		this.coinBalance = coinBalance;
	}
	public BigDecimal getCoinFreeze() {
		return coinFreeze;
	}
	public void setCoinFreeze(BigDecimal coinFreeze) {
		this.coinFreeze = coinFreeze;
	}
	public BigDecimal getCoinLimit() {
		return coinLimit;
	}
	public void setCoinLimit(BigDecimal coinLimit) {
		this.coinLimit = coinLimit;
	}
	public int getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(int accountStatus) {
		this.accountStatus = accountStatus;
	}
	
	
}
