package com.superbit.core.entry;

import java.io.Serializable;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月4日 下午8:30:14  
  * @descrip 用户系统配置实体类
  */
public class UserAppconfig implements Serializable {


	private static final long serialVersionUID = -3405739529870628826L;
	
	public static final int TYPE_STRING = 0;
	public static final int TYPE_INT = 1;
	public static final int TYPE_LONG = 2;
	public static final int TYPE_DOUBLE = 3;
	
	// Key
	private String key;
	// Value
	private String value;
	// 类型
	private int type;
	// 描述
	private String descrip;
	// 默认值
	private String defValue;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getDescrip() {
		return descrip;
	}
	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}
	public String getDefValue() {
		return defValue;
	}
	public void setDefValue(String defValue) {
		this.defValue = defValue;
	}
	
	
}
