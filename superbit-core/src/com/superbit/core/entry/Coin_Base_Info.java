package com.superbit.core.entry;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
 * 币种表
 * @author hongzhen
 *
 */
public class Coin_Base_Info   implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int id;
	/*币种id，唯一*/
	private String coinId;
	/*币种编码，英文名，唯一*/
	private String coinCode;
	/*币种名称，中文名，唯一*/
	private String coinName;
	/*币种创建时间*/
	private Date coinCreateTime;
	/*币种最近一次修改时间时间*/
	private Date coinUpdateTime;
	/*币种最近一次修改人*/
	private String coinUpdateUser;
	/*币种提现说明*/
	private String coinWithDrawDesc;
	/*币种充值说明*/
	private String coinRechargeDesc;
	/*币种买入手续费*/
	private BigDecimal serviceChargeIn;
	/*币种卖出手续费*/
	private BigDecimal serviceChargeOut;
	/*币种买盘交易手续费*/
	private BigDecimal buyOrder;
	/*币种卖盘交易手续费*/
	private BigDecimal sellOrder;
	/*币种提现手续费*/
	private BigDecimal withDrawCharge;
	/*币种每日提现最小额度*/
	private BigDecimal coinLimitMin;
	/*币种每日提现最大额度*/
	private BigDecimal coinLimitMax;
	/*币种状态，0不可用，1生效，默认为不可用状态*/
	private int coinStatus= 0;
	/*币种区域 1:主区 2：创新区  3：分叉区*/
	private String area;
	/*是否置顶，0不置顶，1置顶，默认为0*/
	private String top;
	/**
	 * 币种排序标识，通过修改seq大小来实现币种排序，对应seq的范围为0+∞， 默认为0。
	 * 插入新币种，seq=最大seq+1；
	 * 移动币种，对应币种seq值需要小于上一个币种，大于下一个币种
	 */
	private BigDecimal seq;
	public Coin_Base_Info() {
		super();
		// TODO Auto-generated constructor stub
	}




	public Coin_Base_Info(int id, String coinId, String coinCode, String coinName, Date coinCreateTime,
			Date coinUpdateTime, String coinUpdateUser, String coinWithDrawDesc, String coinRechargeDesc,
			BigDecimal serviceChargeIn, BigDecimal serviceChargeOut, BigDecimal buyOrder, BigDecimal sellOrder,
			BigDecimal withDrawCharge, BigDecimal coinLimitMin, BigDecimal coinLimitMax, int coinStatus, String area,
			String top, BigDecimal seq) {
		super();
		this.id = id;
		this.coinId = coinId;
		this.coinCode = coinCode;
		this.coinName = coinName;
		this.coinCreateTime = coinCreateTime;
		this.coinUpdateTime = coinUpdateTime;
		this.coinUpdateUser = coinUpdateUser;
		this.coinWithDrawDesc = coinWithDrawDesc;
		this.coinRechargeDesc = coinRechargeDesc;
		this.serviceChargeIn = serviceChargeIn;
		this.serviceChargeOut = serviceChargeOut;
		this.buyOrder = buyOrder;
		this.sellOrder = sellOrder;
		this.withDrawCharge = withDrawCharge;
		this.coinLimitMin = coinLimitMin;
		this.coinLimitMax = coinLimitMax;
		this.coinStatus = coinStatus;
		this.area = area;
		this.top = top;
		this.seq = seq;
	}




	public BigDecimal getWithDrawCharge() {
		return withDrawCharge;
	}



	public void setWithDrawCharge(BigDecimal withDrawCharge) {
		this.withDrawCharge = withDrawCharge;
	}



	public BigDecimal getBuyOrder() {
		return buyOrder;
	}

	public void setBuyOrder(BigDecimal buyOrder) {
		this.buyOrder = buyOrder;
	}

	public BigDecimal getSellOrder() {
		return sellOrder;
	}

	public void setSellOrder(BigDecimal sellOrder) {
		this.sellOrder = sellOrder;
	}

	public String getCoinWithDrawDesc() {
		return coinWithDrawDesc;
	}
	public void setCoinWithDrawDesc(String coinWithDrawDesc) {
		this.coinWithDrawDesc = coinWithDrawDesc;
	}


	public String getArea() {
		return area;
	}





	public void setArea(String area) {
		this.area = area;
	}





	public String getTop() {
		return top;
	}




	public void setTop(String top) {
		this.top = top;
	}




	public BigDecimal getSeq() {
		return seq;
	}




	public void setSeq(BigDecimal seq) {
		this.seq = seq;
	}




	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCoinId() {
		return coinId;
	}

	public void setCoinId(String coinId) {
		this.coinId = coinId;
	}

	public String getCoinCode() {
		return coinCode;
	}

	public void setCoinCode(String coinCode) {
		this.coinCode = coinCode;
	}

	public String getCoinName() {
		return coinName;
	}

	public void setCoinName(String coinName) {
		this.coinName = coinName;
	}

	public Date getCoinCreateTime() {
		return coinCreateTime;
	}

	public void setCoinCreateTime(Date coinCreateTime) {
		this.coinCreateTime = coinCreateTime;
	}

	public Date getCoinUpdateTime() {
		return coinUpdateTime;
	}

	public void setCoinUpdateTime(Date coinUpdateTime) {
		this.coinUpdateTime = coinUpdateTime;
	}

	public String getCoinUpdateUser() {
		return coinUpdateUser;
	}

	

	public BigDecimal getCoinLimitMin() {
		return coinLimitMin;
	}




	public void setCoinLimitMin(BigDecimal coinLimitMin) {
		this.coinLimitMin = coinLimitMin;
	}




	public BigDecimal getCoinLimitMax() {
		return coinLimitMax;
	}




	public void setCoinLimitMax(BigDecimal coinLimitMax) {
		this.coinLimitMax = coinLimitMax;
	}




	public void setCoinUpdateUser(String coinUpdateUser) {
		this.coinUpdateUser = coinUpdateUser;
	}

	public String getCoinRechargeDesc() {
		return coinRechargeDesc;
	}

	public void setCoinRechargeDesc(String coinRechargeDesc) {
		this.coinRechargeDesc = coinRechargeDesc;
	}

	public BigDecimal getServiceChargeIn() {
		return serviceChargeIn;
	}

	public void setServiceChargeIn(BigDecimal serviceChargeIn) {
		this.serviceChargeIn = serviceChargeIn;
	}

	public BigDecimal getServiceChargeOut() {
		return serviceChargeOut;
	}

	public void setServiceChargeOut(BigDecimal serviceChargeOut) {
		this.serviceChargeOut = serviceChargeOut;
	}

	public int getCoinStatus() {
		return coinStatus;
	}

	public void setCoinStatus(int coinStatus) {
		this.coinStatus = coinStatus;
	}




	@Override
	public String toString() {
		return "Coin_Base_Info [id=" + id + ", coinId=" + coinId + ", coinCode=" + coinCode + ", coinName=" + coinName
				+ ", coinCreateTime=" + coinCreateTime + ", coinUpdateTime=" + coinUpdateTime + ", coinUpdateUser="
				+ coinUpdateUser + ", coinWithDrawDesc=" + coinWithDrawDesc + ", coinRechargeDesc=" + coinRechargeDesc
				+ ", serviceChargeIn=" + serviceChargeIn + ", serviceChargeOut=" + serviceChargeOut + ", buyOrder="
				+ buyOrder + ", sellOrder=" + sellOrder + ", withDrawCharge=" + withDrawCharge + ", coinLimitMin="
				+ coinLimitMin + ", coinLimitMax=" + coinLimitMax + ", coinStatus=" + coinStatus + ", area=" + area
				+ ", top=" + top + ", seq=" + seq + "]";
	}

	
	
}
