package com.superbit.core.entry;

import java.io.Serializable;
import java.util.Date;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月19日 下午2:24:38  
  * @descrip 用户实名认证信息
  */
public class UserRealnameAuthen implements Serializable{

	
	private static final long serialVersionUID = -533456096962642981L;
	/*用户ID*/
	private int userId;
	/*昵称*/
	private String nickname;
	/*用户账户*/
	private String userAccount;
	/*提交时间*/
	private Date submitTime;
	/*认证状态0未审核 1已提交未审核(审核中) 2审核失败(理由) 3 审核成功(更改安全信息表状态)*/
	private int authenStatus;
	/*操作人员ID*/
	private int operationId;
	/*操作人姓名*/
	private String operationName;
	/*操作时间*/
	private Date operationTime;
	/*l	operationReason驳回理由：在审核失败时填写*/
	private String operationReason;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}
	public Date getSubmitTime() {
		return submitTime;
	}
	public void setSubmitTime(Date submitTime) {
		this.submitTime = submitTime;
	}
	public int getAuthenStatus() {
		return authenStatus;
	}
	public void setAuthenStatus(int authenStatus) {
		this.authenStatus = authenStatus;
	}
	public int getOperationId() {
		return operationId;
	}
	public void setOperationId(int operationId) {
		this.operationId = operationId;
	}
	public String getOperationName() {
		return operationName;
	}
	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}
	public Date getOperationTime() {
		return operationTime;
	}
	public void setOperationTime(Date operationTime) {
		this.operationTime = operationTime;
	}
	public String getOperationReason() {
		return operationReason;
	}
	public void setOperationReason(String operationReason) {
		this.operationReason = operationReason;
	}
	
	
	
}
