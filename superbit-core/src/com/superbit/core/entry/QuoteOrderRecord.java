package com.superbit.core.entry;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 * 报价订单交易记录
 * @author 赵世栋
 * @version 2018-1-4
 *
 */
public class QuoteOrderRecord implements Serializable {

	private static final long serialVersionUID = 4104503128344708819L;
	/*交易记录id*/
	private long id;
	/*报价订单id*/
	private long quoteOrderId;
	/*交易币种*/
	private int coinType;
	/*交易对象*/
	private String transactionObject;
	/*价格CNY*/
	private BigDecimal priceCNY;
	/*币种数量*/
	private BigDecimal amountCoin;
	/*交易总金额*/
	private BigDecimal tradePrice; 
	/*交易手续费*/
	private BigDecimal poundageBTC;
	/*付款参考号*/
	private int paymentNumber;
	/*交易支付状态 0：等待支付 1：已付款 2：已完成  4：买家取消  5：申诉中  6：申诉成功 7：申诉失败*/
	private int payStatus;
	/*买方用户id*/
	private long buyserUserId;
	/*卖方用户id*/
	private long sellerUserId;
	/*下单时间*/
	private long placeOrderTime;
	/*交易类型 0：买入 1：卖出*/
	private int tradeType;
	/*申诉者名称*/
	private String complainantName;
	/*交易支付方式：卖方的支持项*/
	private int paymentType;
	/*交易产生类型  0： 直接创建 1：买卖盘拉取的*/
	private int tradeCreateType;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getQuoteOrderId() {
		return quoteOrderId;
	}
	public void setQuoteOrderId(long quoteOrderId) {
		this.quoteOrderId = quoteOrderId;
	}
	public int getCoinType() {
		return coinType;
	}
	public void setCoinType(int coinType) {
		this.coinType = coinType;
	}
	public String getTransactionObject() {
		return transactionObject;
	}
	public void setTransactionObject(String transactionObject) {
		this.transactionObject = transactionObject;
	}
	public BigDecimal getPriceCNY() {
		return priceCNY;
	}
	public void setPriceCNY(BigDecimal priceCNY) {
		this.priceCNY = priceCNY;
	}
	public BigDecimal getAmountCoin() {
		return amountCoin;
	}
	public void setAmountCoin(BigDecimal amountCoin) {
		this.amountCoin = amountCoin;
	}
	public BigDecimal getTradePrice() {
		return tradePrice;
	}
	public void setTradePrice(BigDecimal tradePrice) {
		this.tradePrice = tradePrice;
	}
	public BigDecimal getPoundageBTC() {
		return poundageBTC;
	}
	public void setPoundageBTC(BigDecimal poundageBTC) {
		this.poundageBTC = poundageBTC;
	}
	public int getPaymentNumber() {
		return paymentNumber;
	}
	public void setPaymentNumber(int paymentNumber) {
		this.paymentNumber = paymentNumber;
	}
	public int getPayStatus() {
		return payStatus;
	}
	public void setPayStatus(int payStatus) {
		this.payStatus = payStatus;
	}
	public long getBuyserUserId() {
		return buyserUserId;
	}
	public void setBuyserUserId(long buyserUserId) {
		this.buyserUserId = buyserUserId;
	}
	public long getSellerUserId() {
		return sellerUserId;
	}
	public void setSellerUserId(long sellerUserId) {
		this.sellerUserId = sellerUserId;
	}
	public long getPlaceOrderTime() {
		return placeOrderTime;
	}
	public void setPlaceOrderTime(long placeOrderTime) {
		this.placeOrderTime = placeOrderTime;
	}
	public int getTradeType() {
		return tradeType;
	}
	public void setTradeType(int tradeType) {
		this.tradeType = tradeType;
	}
	public String getComplainantName() {
		return complainantName;
	}
	public void setComplainantName(String complainantName) {
		this.complainantName = complainantName;
	}
	public int getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(int paymentType) {
		this.paymentType = paymentType;
	}
	public int getTradeCreateType() {
		return tradeCreateType;
	}
	public void setTradeCreateType(int tradeCreateType) {
		this.tradeCreateType = tradeCreateType;
	}
	
}
