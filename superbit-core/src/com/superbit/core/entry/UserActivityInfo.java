package com.superbit.core.entry;

import java.io.Serializable;
import java.util.Date;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月4日 下午8:01:53  
  * @descrip 用户活动情况实体类
  */
public class UserActivityInfo implements Serializable{

	
	private static final long serialVersionUID = -8443811061542356667L;
	/*用户ID*/
	private int userId;
	/*VIP等级*/
	private byte VIPLevel;
	/*积分*/
	private int score;
	/*更新日期*/
	private Date updateDate;
	
	// 空参构造
	public UserActivityInfo() {
		super();
	}
	// 满参构造
	public UserActivityInfo(int userId, byte vIPLevel, int score, Date updateDate) {
		super();
		this.userId = userId;
		VIPLevel = vIPLevel;
		this.score = score;
		this.updateDate = updateDate;
	}


	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public byte getVIPLevel() {
		return VIPLevel;
	}
	public void setVIPLevel(byte vIPLevel) {
		VIPLevel = vIPLevel;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	
}
