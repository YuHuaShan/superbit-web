package com.superbit.core.entry;

import java.io.Serializable;
/**
 * 公共提现平台表
 * @author hongzhen
 *
 */
public class Common_Platform implements Serializable{

	private static final long serialVersionUID = 1L;
	private int id;
	private String platformId;
	/*平台名称*/
	private int platformName;
	private String platformCode;
	/*是否是极速平台，0为非极速，1为极速，默认为0*/
	private int topSpeed= 0;
	
	public Common_Platform() {
		super();
	}

	public Common_Platform(int id, String platformId, int platformName, String platformCode, int topSpeed) {
		super();
		this.id = id;
		this.platformId = platformId;
		this.platformName = platformName;
		this.platformCode = platformCode;
		this.topSpeed = topSpeed;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public int getPlatformName() {
		return platformName;
	}

	public void setPlatformName(int platformName) {
		this.platformName = platformName;
	}

	public String getPlatformCode() {
		return platformCode;
	}

	public void setPlatformCode(String platformCode) {
		this.platformCode = platformCode;
	}

	public int getTopSpeed() {
		return topSpeed;
	}

	public void setTopSpeed(int topSpeed) {
		this.topSpeed = topSpeed;
	}



	
}
