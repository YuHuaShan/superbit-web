package com.superbit.core.entry;

import java.io.Serializable;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月4日 下午8:26:08  
  * @descrip 用户登录记录实体类
  */
public class UserLoginLog implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2691899729296977449L;
	/*登录日志ID*/
	private int loginLogId;
	/*用户ID*/
	private int userId;
	/*远程IP*/
	private String remoteIP;
	/*登陆方式说明：0web，1手机html，2安卓，3IOS*/
	private byte loginMode;
	/*登录日期*/
	private long loginDate;
	/*登录状态说明：0成功，1失败*/
	private byte loginStatus;
	/*错误码说明：0通过，1用户不存在或密码错误，2用户被锁定*/
	private byte errorCode;
	public int getLoginLogId() {
		return loginLogId;
	}
	public void setLoginLogId(int loginLogId) {
		this.loginLogId = loginLogId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getRemoteIP() {
		return remoteIP;
	}
	public void setRemoteIP(String remoteIP) {
		this.remoteIP = remoteIP;
	}
	public byte getLoginMode() {
		return loginMode;
	}
	public void setLoginMode(byte loginMode) {
		this.loginMode = loginMode;
	}
	public long getLoginDate() {
		return loginDate;
	}
	public void setLoginDate(long loginDate) {
		this.loginDate = loginDate;
	}
	public byte getLoginStatus() {
		return loginStatus;
	}
	public void setLoginStatus(byte loginStatus) {
		this.loginStatus = loginStatus;
	}
	public byte getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(byte errorCode) {
		this.errorCode = errorCode;
	}
	
	
	
}
