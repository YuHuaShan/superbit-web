package com.superbit.core.entry;

import java.io.Serializable;
import java.util.Date;
/**
 * 用户自定义平台信息
 * @author hongzhen
 *
 */
public class User_Withdraw_Plat implements Serializable{

	private static final long serialVersionUID = 1L;
	private int id;
	/*用户id*/
	private int userId;
	/*资产id*/
	private String assetId;
	/*平台id*/
	private String platformId;
	/*平台名称*/
	private String platformName;
	/*平台创建时间*/
	private Date createTime;
	public User_Withdraw_Plat() {
		super();
	}
	
	

	public User_Withdraw_Plat(int id, int userId, String assetId, String platformId, String platformName,
			Date createTime) {
		super();
		this.id = id;
		this.userId = userId;
		this.assetId = assetId;
		this.platformId = platformId;
		this.platformName = platformName;
		this.createTime = createTime;
	}



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getPlatformName() {
		return platformName;
	}
	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	
	
}
