package com.superbit.core.entry;

import java.io.Serializable;

/**
 * 申诉表
 * @author 赵世栋
 * @version 2018-1-4
 *
 */
public class Complain implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	/*申诉者id*/
	private long userId;
	/*交易记录id*/
	private int tradeRecordId;
	/*付款参考号*/
	private int consultNumber;
	/*创建时间*/
	private long createTime;
	/*标题  提交申诉、申诉失败、申诉完成*/
	private String title;
	/*申诉原因*/
	private String complainReason;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public int getTradeRecordId() {
		return tradeRecordId;
	}
	public void setTradeRecordId(int tradeRecordId) {
		this.tradeRecordId = tradeRecordId;
	}
	public int getConsultNumber() {
		return consultNumber;
	}
	public void setConsultNumber(int consultNumber) {
		this.consultNumber = consultNumber;
	}
	public long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getComplainReason() {
		return complainReason;
	}
	public void setComplainReason(String complainReason) {
		this.complainReason = complainReason;
	}
	
	
	
	
	
}
