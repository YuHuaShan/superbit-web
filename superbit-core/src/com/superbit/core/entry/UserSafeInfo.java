package com.superbit.core.entry;

import java.io.Serializable;
import java.util.Date;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月4日 下午7:31:16  
  * @descrip 用户安全相关信息
  */
public class UserSafeInfo implements Serializable {
	

	private static final long serialVersionUID = -8222227341234824405L;
	/*用户ID*/
	private int userId;
	/*资金密码*/
	private String fundPassword;
	/*谷歌私钥*/
	private String googlePrivateKey;
	/*绑定手机0未绑定1已绑定*/
	private byte bindPhone;
	/*安全级别0什么也没有绑定   1中绑定手机    2高绑定谷歌*/
	private byte securityLevel;
	/*登录认证暂未使用 0 未设置  1 已设置*/
	private byte loginAuthentication;
	/*0未审核 1已提交未审核(审核中) 2审核失败(理由) 3 审核成功(更改安全信息表状态)*/
	private byte authenStatus;
	/*冻结状态0活动 1 冻结 2修改登录密码时设置禁止提现*/
	private byte freezeStatus;
	/*冻结截止时间*/
	private Date freezeDeadline;
	/*锁定状态0未锁定，1密码错误锁定，2后台锁定，3频繁访问锁定*/
	private byte lockStatus;	
	/*锁定截止时间*/
	private long lockDeadline;
	/*更新时间*/
	private Date updateDate;
	/*密码等级暂未使用低中高123*/
	private byte passwordLevel;
	
	
	/*
	 * 空参构造
	 */
	public UserSafeInfo() {
		super();
	}
	/*
	 * 满参构造
	 */
	public UserSafeInfo(int userId, String fundPassword, String googlePrivateKey, byte bindPhone, byte securityLevel,
			byte loginAuthentication, byte authenStatus, byte freezeStatus, Date freezeDeadline, byte lockStatus, long lockDeadline,
			Date updateDate, byte passwordLevel) {
		super();
		this.userId = userId;
		this.fundPassword = fundPassword;
		this.googlePrivateKey = googlePrivateKey;
		this.bindPhone = bindPhone;
		this.securityLevel = securityLevel;
		this.loginAuthentication = loginAuthentication;
		this.authenStatus = authenStatus;
		this.freezeStatus = freezeStatus;
		this.freezeDeadline = freezeDeadline;
		this.lockStatus = lockStatus;
		this.lockDeadline = lockDeadline;
		this.updateDate = updateDate;
		this.passwordLevel = passwordLevel;
	}
	
	public byte getAuthenStatus() {
		return authenStatus;
	}
	public void setAuthenStatus(byte authenStatus) {
		this.authenStatus = authenStatus;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getFundPassword() {
		return fundPassword;
	}
	public void setFundPassword(String fundPassword) {
		this.fundPassword = fundPassword;
	}
	public String getGooglePrivateKey() {
		return googlePrivateKey;
	}
	public void setGooglePrivateKey(String googlePrivateKey) {
		this.googlePrivateKey = googlePrivateKey;
	}
	public byte getBindPhone() {
		return bindPhone;
	}
	public void setBindPhone(byte bindPhone) {
		this.bindPhone = bindPhone;
	}
	public byte getSecurityLevel() {
		return securityLevel;
	}
	public void setSecurityLevel(byte securityLevel) {
		this.securityLevel = securityLevel;
	}
	public byte getLoginAuthentication() {
		return loginAuthentication;
	}
	public void setLoginAuthentication(byte loginAuthentication) {
		this.loginAuthentication = loginAuthentication;
	}
	public byte getFreezeStatus() {
		return freezeStatus;
	}
	public void setFreezeStatus(byte freezeStatus) {
		this.freezeStatus = freezeStatus;
	}
	public Date getFreezeDeadline() {
		return freezeDeadline;
	}
	public void setFreezeDeadline(Date freezeDeadline) {
		this.freezeDeadline = freezeDeadline;
	}
	public byte getLockStatus() {
		return lockStatus;
	}
	public void setLockStatus(byte lockStatus) {
		this.lockStatus = lockStatus;
	}
	public long getLockDeadline() {
		return lockDeadline;
	}
	public void setLockDeadline(long lockDeadline) {
		this.lockDeadline = lockDeadline;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public byte getPasswordLevel() {
		return passwordLevel;
	}
	public void setPasswordLevel(byte passwordLevel) {
		this.passwordLevel = passwordLevel;
	}
	
	
	
}
