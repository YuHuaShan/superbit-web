package com.superbit.core.entry;

import java.io.Serializable;

/**
 * 报价订单实体类
 * @author 赵世栋
 * @version 2018-1-4
 *
 */
import java.math.BigDecimal;
import java.util.Date;
public class QuoteOrder implements Serializable {

	private static final long serialVersionUID = -2704119186825992134L;

	/*订单id*/
	private long  Id;
	/*用户id*/
	private int  userId;
	/*最小交易量CNY*/
	private int ALOtransaction;
	/*价格CNY（单价）*/
	private BigDecimal priceCNY  = BigDecimal.ZERO;
	/*币的数量*/
	private BigDecimal amountCoin = BigDecimal.ZERO;
	/*已交易数量*/
	private BigDecimal dealAmount = BigDecimal.ZERO;
	/*报价单总金额*/
	private BigDecimal totalPrice = BigDecimal.ZERO;
	/*剩余金额*/
	private BigDecimal remainPrice = BigDecimal.ZERO;
	/*手续费*/
	private BigDecimal tradeFee = BigDecimal.ZERO;
	/*冻结资金*/
	private BigDecimal freezeAmount;
	/*币种Code*/
	private String coinCode;
	/*报价订单类型 0：买入 1：卖出*/
	private int quoteType;
	/*报价订单的状态 0：未成交 1：部分成交中 2：部分成交 */
	private int quoteStatus;
	/*创建时间*/
	private Date createTime;
	/*支付方式0：银行卡1：支付宝2：微信*/
	private String payType;
	/*是否设置自动  0：不是 1：是*/
	private int isAutoSet;
	/*设置自动后增益指数*/
	private String setValue;
	/*交易码 0：未开始交易 1：部分交易 2：最后一笔交易*/
	private int tradeCode;
	
	
	
	/*不入库*/
	/*当前报价订单已交易进度*/
	private double transactionProgress;
	/*交易完成次数 */
	private int tradeFinishCount;
	/*交易完成率=1- 取消交易次数/总交易次数 */
	private String tradeFinishRate;
	/*是否认证  0：未认证  1：已认证*/
	private int isAuth;
	/*委托人*/
	private String consigner;
	public long getId() {
		return Id;
	}
	public void setId(long id) {
		Id = id;
	}
	

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public BigDecimal getPriceCNY() {
		return priceCNY;
	}
	public void setPriceCNY(BigDecimal priceCNY) {
		this.priceCNY = priceCNY;
	}
	public BigDecimal getAmountCoin() {
		return amountCoin;
	}
	public void setAmountCoin(BigDecimal amountCoin) {
		this.amountCoin = amountCoin;
	}
	public int getALOtransaction() {
		return ALOtransaction;
	}
	public void setALOtransaction(int aLOtransaction) {
		ALOtransaction = aLOtransaction;
	}
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getCoinCode() {
		return coinCode;
	}
	public void setCoinCode(String coinCode) {
		this.coinCode = coinCode;
	}
	public int getQuoteType() {
		return quoteType;
	}
	public void setQuoteType(int quoteType) {
		this.quoteType = quoteType;
	}
	public int getQuoteStatus() {
		return quoteStatus;
	}
	public void setQuoteStatus(int quoteStatus) {
		this.quoteStatus = quoteStatus;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public BigDecimal getDealAmount() {
		return dealAmount;
	}
	public void setDealAmount(BigDecimal dealAmount) {
		this.dealAmount = dealAmount;
	}

	
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public BigDecimal getTradeFee() {
		return tradeFee;
	}
	public void setTradeFee(BigDecimal tradeFee) {
		this.tradeFee = tradeFee;
	}
	public double getTransactionProgress() {
		return transactionProgress;
	}
	public void setTransactionProgress(double transactionProgress) {
		this.transactionProgress = transactionProgress;
	}
	
	public int getIsAutoSet() {
		return isAutoSet;
	}
	public void setIsAutoSet(int isAutoSet) {
		this.isAutoSet = isAutoSet;
	}

	public int getTradeFinishCount() {
		return tradeFinishCount;
	}
	public void setTradeFinishCount(int tradeFinishCount) {
		this.tradeFinishCount = tradeFinishCount;
	}

	public String getTradeFinishRate() {
		return tradeFinishRate;
	}
	public void setTradeFinishRate(String tradeFinishRate) {
		this.tradeFinishRate = tradeFinishRate;
	}
	public String getConsigner() {
		return consigner;
	}
	public void setConsigner(String consigner) {
		this.consigner = consigner;
	}
	public int getIsAuth() {
		return isAuth;
	}
	public void setIsAuth(int isAuth) {
		this.isAuth = isAuth;
	}
	
	public BigDecimal getRemainPrice() {
		return remainPrice;
	}
	public void setRemainPrice(BigDecimal remainPrice) {
		this.remainPrice = remainPrice;
	}
	public BigDecimal getFreezeAmount() {
		return freezeAmount;
	}
	public void setFreezeAmount(BigDecimal freezeAmount) {
		this.freezeAmount = freezeAmount;
	}
	public String getSetValue() {
		return setValue;
	}
	public void setSetValue(String setValue) {
		this.setValue = setValue;
	}
	public int getTradeCode() {
		return tradeCode;
	}
	public void setTradeCode(int tradeCode) {
		this.tradeCode = tradeCode;
	}
	
	
}
