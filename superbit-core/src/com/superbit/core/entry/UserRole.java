package com.superbit.core.entry;

import java.io.Serializable;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月4日 下午8:24:03  
  * @descrip 用户角色实体类
  */
public class UserRole implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7665032594869809898L;
	/*用户角色ID*/
	private int userRoleId;
	/*用户ID*/
	private int userId;
	/*角色*/
	private String role;
	public int getUserRoleId() {
		return userRoleId;
	}
	public void setUserRoleId(int userRoleId) {
		this.userRoleId = userRoleId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	
}
