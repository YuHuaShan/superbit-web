package com.superbit.core.entry;

import java.io.Serializable;
import java.util.Date;
/**
 * 用户提现地址表
 * @author hongzhen
 *
 */
public class User_Withdraw_Addr implements Serializable{

	private static final long serialVersionUID = 1L;
	private int id;
	/*提现用户id*/
	private int userId;
	/*提现资产id*/
	private String assetId;
	/*提现币种id*/
	private String coinId;
	/*提现地址*/
	private String withDrawAddr;
	/*提现创建时间*/
	private Date createTime;
	/*提现状态最近一次修改时间*/
	private Date updateTime;
	public User_Withdraw_Addr() {
		super();
	}

	public User_Withdraw_Addr(int id, int userId, String assetId, String coinId, String withDrawAddr,
			Date createTime, Date updateTime) {
		super();
		this.id = id;
		this.userId = userId;
		this.assetId = assetId;
		this.coinId = coinId;
		this.withDrawAddr = withDrawAddr;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public String getCoinId() {
		return coinId;
	}
	public void setCoinId(String coinId) {
		this.coinId = coinId;
	}

	public String getWithDrawAddr() {
		return withDrawAddr;
	}
	public void setWithDrawAddr(String withDrawAddr) {
		this.withDrawAddr = withDrawAddr;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
}
