package com.superbit.core.entry;

import java.io.Serializable;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月13日 下午7:09:42  
  * @descrip 
  */
public class UserPayMethod implements Serializable{
	
	private static final long serialVersionUID = 7410236060262405023L;
	
	
	private int id;
	/*用户ID*/
	private int userId;
	/*0 银行卡，1 支付宝，2 微信*/
	private int chanelID;  //0 银行卡，1 支付宝，2 微信
	/*真实姓名*/
	private String realName;//真实姓名
	/*支付账号(银行卡号,支付宝,微信)*/
	private String payAccount;
	/*支付二维码(开户行)*/
	private String payQrCode;
	/*二维码存储地址(开户支行)*/
	private String payQrAddr;
	/*绑定时间*/
	private long bindTime;
	/*最近更改时间*/
	private long updateTime;
	
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getChanelID() {
		return chanelID;
	}
	public void setChanelID(int chanelID) {
		this.chanelID = chanelID;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getPayAccount() {
		return payAccount;
	}
	public void setPayAccount(String payAccount) {
		this.payAccount = payAccount;
	}
	public String getPayQrCode() {
		return payQrCode;
	}
	public void setPayQrCode(String payQrCode) {
		this.payQrCode = payQrCode;
	}
	public String getPayQrAddr() {
		return payQrAddr;
	}
	public void setPayQrAddr(String payQrAddr) {
		this.payQrAddr = payQrAddr;
	}
	public long getBindTime() {
		return bindTime;
	}
	public void setBindTime(long bindTime) {
		this.bindTime = bindTime;
	}

	
}
