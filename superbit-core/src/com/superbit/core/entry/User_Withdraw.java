package com.superbit.core.entry;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
 * 用户提现记录表
 * @author hongzhen
 *
 */
public class User_Withdraw implements Serializable{

	private static final long serialVersionUID = 1L;
	private int id;
	/*提现用户id*/
	private int userId;
	/*提现资产id*/
	private String assetId;
	/*提现币种id*/
	private String coinId;
	/*提现币种类型*/
	private String area;
	/*提现币种名字*/
	private String coinCode;
	/*提现币种名字*/
	private String coinName;
	/*提现地址*/
	private String withDrawAddr;
	/*提现平台*/
	private String withDrawPlat;
	/*提现数量*/
	private BigDecimal withDrawAmount;
	/*提现手续费*/
	private BigDecimal serviceCharge;
	/*提现状态，0未处理，1通过，2驳回,3打款中，4数据确认*/
	private int withDrawStatus = 0;
	/*提现创建时间*/
	private Date createTime;
	/*提现状态最近一次修改时间*/
	private Date updateTime;
	public User_Withdraw() {
		super();
	}


	public User_Withdraw(int id, int userId, String assetId, String coinId, String area, String coinCode,
			String coinName, String withDrawAddr, String withDrawPlat, BigDecimal withDrawAmount,
			BigDecimal serviceCharge, int withDrawStatus, Date createTime, Date updateTime) {
		super();
		this.id = id;
		this.userId = userId;
		this.assetId = assetId;
		this.coinId = coinId;
		this.area = area;
		this.coinCode = coinCode;
		this.coinName = coinName;
		this.withDrawAddr = withDrawAddr;
		this.withDrawPlat = withDrawPlat;
		this.withDrawAmount = withDrawAmount;
		this.serviceCharge = serviceCharge;
		this.withDrawStatus = withDrawStatus;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public String getCoinId() {
		return coinId;
	}
	public void setCoinId(String coinId) {
		this.coinId = coinId;
	}

	public String getArea() {
		return area;
	}


	public void setArea(String area) {
		this.area = area;
	}


	public String getCoinCode() {
		return coinCode;
	}


	public void setCoinCode(String coinCode) {
		this.coinCode = coinCode;
	}


	public String getCoinName() {
		return coinName;
	}
	public void setCoinName(String coinName) {
		this.coinName = coinName;
	}
	public String getWithDrawAddr() {
		return withDrawAddr;
	}
	public void setWithDrawAddr(String withDrawAddr) {
		this.withDrawAddr = withDrawAddr;
	}
	public String getWithDrawPlat() {
		return withDrawPlat;
	}
	public void setWithDrawPlat(String withDrawPlat) {
		this.withDrawPlat = withDrawPlat;
	}
	public BigDecimal getWithDrawAmount() {
		return withDrawAmount;
	}
	public void setWithDrawAmount(BigDecimal withDrawAmount) {
		this.withDrawAmount = withDrawAmount;
	}
	public BigDecimal getServiceCharge() {
		return serviceCharge;
	}
	public void setServiceCharge(BigDecimal serviceCharge) {
		this.serviceCharge = serviceCharge;
	}
	public int getWithDrawStatus() {
		return withDrawStatus;
	}
	public void setWithDrawStatus(int withDrawStatus) {
		this.withDrawStatus = withDrawStatus;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
}
