package com.superbit.core.entry;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 交易完成记录 实体类：记录交易完成时的信息
 * @author 赵世栋
 * @version 2018-1-4
 *
 */
public class TradeFinishRecod implements Serializable{

	private static final long serialVersionUID = -7204565944201617917L;

	private long id;
	/*用户id*/
	private long userId;
	/*成交价*/
	private BigDecimal currentRate;
	/*交易的币种*/
	private Integer coinType;
	/*成交时间*/
	private long dealTime;
	/*卖方的userId*/
	private long sellerUserId;
	/*买方的userId*/
	private long buyserUserId;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public BigDecimal getCurrentRate() {
		return currentRate;
	}
	public void setCurrentRate(BigDecimal currentRate) {
		this.currentRate = currentRate;
	}
	public Integer getCoinType() {
		return coinType;
	}
	public void setCoinType(Integer coinType) {
		this.coinType = coinType;
	}
	public long getDealTime() {
		return dealTime;
	}
	public void setDealTime(long dealTime) {
		this.dealTime = dealTime;
	}
	public long getSellerUserId() {
		return sellerUserId;
	}
	public void setSellerUserId(long sellerUserId) {
		this.sellerUserId = sellerUserId;
	}
	public long getBuyserUserId() {
		return buyserUserId;
	}
	public void setBuyserUserId(long buyserUserId) {
		this.buyserUserId = buyserUserId;
	}
	
	
	
}
