package com.superbit.core.entry;

import java.io.Serializable;
import java.util.Date;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月4日 下午7:25:34  
  * @descrip 存储用户基础信息
  */
public class UserBaseInfo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3090739856464047390L;
	/*用户ID*/
	private int userId;
	/*用户昵称*/
	private String nickname;
	/*邮箱*/
	private String email;
	/*密码*/
	private String password;
	/*区号*/
	private String areaCode;
	/*手机号*/
	private String phone;
	/*注册日期*/
	private Date registerDate;
	/*账户类型  1 邮箱  2 手机*/
	private byte accountType;
	
	
	// 空参构造
	public UserBaseInfo() {
		super();
	}
	// 满参构造
	public UserBaseInfo(String nickname, String email, String password, String areaCode, String phone,
			Date registerDate, byte accountType) {
		super();
		this.nickname = nickname;
		this.email = email;
		this.password = password;
		this.areaCode = areaCode;
		this.phone = phone;
		this.registerDate = registerDate;
		this.accountType = accountType;
	}
	// 满参构造
	public UserBaseInfo(int userId, String nickname, String email, String password, String areaCode, String phone,
			Date registerDate, byte accountType) {
		super();
		this.userId = userId;
		this.nickname = nickname;
		this.email = email;
		this.password = password;
		this.areaCode = areaCode;
		this.phone = phone;
		this.registerDate = registerDate;
		this.accountType = accountType;
	}

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	public byte getAccountType() {
		return accountType;
	}
	public void setAccountType(byte accountType) {
		this.accountType = accountType;
	}
	
	

}
