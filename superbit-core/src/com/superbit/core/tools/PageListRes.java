package com.superbit.core.tools;

import java.util.List;

/***
 * 列表分页返回对象
 * @author huyaohua
 *
 * @param <T>
 */
public class PageListRes<T> {

	/**
	 * 页编号 : 第几页
	 */
	private int pageNo = 1;
	/**
	 * 页大小 : 每页的数量
	 */
	private int pageSize = 10;
	/**
	 * 查询结果
	 */
	private List<T> list;
	/**
	 * 总条数
	 */
	private int total;
	/**
	 * 总页数
	 */
	private int totalPages;

	public PageListRes() {}

	public PageListRes(Integer pageNo, Integer pageSize) {
		if(pageNo!=null&&pageNo>0){			
			this.pageNo = pageNo;
		}
		if(pageSize!=null&&pageSize>0){			
			this.pageSize = pageSize;
		}
	}

	/**
	 * 获得当前页的页号,序号从1开始,默认为1.
	 */
	public int getPageNo() {
		return pageNo;
	}
	/**
	 * 获得每页的记录数量,默认为1.
	 */
	public int getPageSize() {
		return pageSize;
	}
	/**
	 * 根据pageNo和pageSize计算当前页第一条记录在总结果集中的位置,序号从1开始.
	 */
	public int getFirst() {
		return ((pageNo - 1) * pageSize) + 1;
	}
	/**获取最后的边界值
	 * @return
	 */
	public int getEnd() {
		return pageNo*pageSize;
	}
	/**
	 * 取得总记录数, 默认值为-1.
	 */
	public int getTotal() {
		return total;
	}
	/**
	 * 设置总记录数.
	 */
	public void setTotal(final int total) {
		this.total = total;
		this.totalPages = this.calcTotalPages();
	}
	/**
	 * 根据pageSize与total计算总页数, 默认值为-1.
	 */
	public int calcTotalPages() {
		if (totalPages > 0) {
			if (total < 0) {
				return -1;
			}
			int pages = total / pageSize;
			return total % pageSize > 0 ? ++pages : pages;
		} else {
			return totalPages;
		}
	}
	/**
	 * 计算偏移量
	 */
	public int calcOffset() {
		return ((pageNo - 1) * pageSize);
	}
	/**
	 * 计算限定数
	 */
	public int calcLimit() {
		return pageSize;
	}

	public static <T> PageListRes<T> createInstance(Integer pageNo, Integer pageSize) {
		return new PageListRes<>(pageNo, pageSize);
	}

	public static <T> PageListRes<T> createInstance(Integer pageNo, Integer pageSize,int total) {
		PageListRes<T> res = new PageListRes<>(pageNo, pageSize);
		res.setTotal(total);
		return res;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
	
}