package com.superbit.core.service;


/**
 * @function 回滚接口
 * @author WangZhenwei
 * @version 2018-01-08 13:33
 */
public interface LongTimeTaskCallback {

	void callback();
	
}
