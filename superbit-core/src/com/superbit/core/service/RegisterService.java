package com.superbit.core.service;

import java.util.Map;

import com.superbit.core.exception.BusinessException;
import com.superbit.utils.mailutil.EmailFailException;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月4日 下午8:34:01  
  * @descrip 注册模块
  */
public interface RegisterService {

	/**
	 * 手机注册
	 */
	void phoneRegister(String areaCode,String phone) throws BusinessException;
	
	/**
	 * 手机注册激活
	 */
	Map<String, Object> activePhone(String phone,String captcha,String password) throws BusinessException;
	
	/**
	 * 邮箱注册
	 */
	void emailRegister(String email) throws BusinessException, EmailFailException;
	
	/**
	 * 邮箱注册激活
	 */
	Map<String,Object> activeEmail(String email,String captcha,String password) throws BusinessException;
	
	/**
	 * 根据邮箱验证邮箱验证码的业务方法
	 * @param email
	 * @param captcha
	 * @return
	 * @throws BusinessException
	 */
	public Map<String, String> verifyCodeByEmail(String email, String captcha) throws BusinessException;
	
	/**
	 * 根据手机号验证手机验证码
	 * @param phone
	 * @param captcha
	 * @return
	 * @throws BusinessException
	 */
	public Map<String, String> verifyCodeByPhone(String phone, String captcha) throws BusinessException;
	
	/**
	 * 发送手机短信【不对外提供】
	 * @param phone			区号+手机号
	 * @param systemId		系统Id
	 * @param moduleId		模板Id
	 * @param moudleName	模板名称
	 * @param accessData	短信内容
	 * @param isSync		是否异步
	 */
	/*void sendMessage(final String areaCode, final String phone, final int systemId, final int moduleId, final String moudleName,
			final Map<String,Object> accessData, boolean isSync);*/
	
	/**
	 * 发送邮件
	 */
	/*void sendEmail(final String email, final String modelName, final int moduleId, 
			final Map<String,Object> accessData,final boolean isSync);*/
}
