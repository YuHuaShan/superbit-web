package com.superbit.core.service;

import java.util.List;
import java.util.Map;

import com.superbit.core.entry.tradelog.ChatLog;

/**
 * 聊天记录服务接口
 * 
 * @author zhangdaoguang
 *
 */
public interface ChatLogService {
	/**
	 * 保存聊天记录
	 */
	Long saveChatLog(ChatLog chatLog);

	/**
	 * 通过订单号获取聊天记录
	 */
	List<ChatLog> getChatLogByOrderNumber(String orderNumber);

	/**
	 * 是否有新消息
	 * 
	 * @param orderNumbers
	 *            订单号列表
	 * @return 订单号 --> 0:有未读消息, 1:没有
	 */
	Map<String, Integer> hasNewMessage(List<String> orderNumbers);

	void updateHasRead(List<String> orderNumbers);
}
