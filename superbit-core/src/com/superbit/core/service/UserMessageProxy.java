package com.superbit.core.service;
/** @author  WangZhenwei 
  * @date 创建时间：2018年1月19日 下午4:55:54  
  * @descrip 
  */
public interface UserMessageProxy {

	/**
	 * 发送短信验证码
	 * @param userId
	 * @param moudleName
	 * @return
	 */
	boolean sendSMS(Integer userId, String moudleName);
	/**
	 * 判断用户验证码是否一致
	 * @param userId  
	 * @param moudleName   模块，如果有用则用
	 * @param context	用户上传验证码内容
	 * @return
	 */
	boolean verficationSMS(Integer userId, String moudleName, String context);
}
