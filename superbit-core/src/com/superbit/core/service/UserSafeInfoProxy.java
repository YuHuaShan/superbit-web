package com.superbit.core.service;

import com.superbit.core.entry.UserSafeInfo;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月19日 下午4:41:57  
  * @descrip 与用户安全信息相关的业务
  */
public interface UserSafeInfoProxy {

	
	/**
	 * 获取用户绑定手机号及谷歌账号状态
	 * @param userId
	 * @return
	 */
	UserSafeInfo verificationSafeInfo(Integer userId);
		
	/**
	 * 返回用户修改登录密码冻结时间
	 * @return 返回冻结时间
	 */
	String assetFreeze(Integer userId);
}
