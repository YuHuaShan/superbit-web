package com.superbit.core.service;

import java.util.Map;

import com.superbit.core.entry.UserSafeInfo;
import com.superbit.core.exception.BusinessException;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月15日 上午10:44:44  
  * @descrip 处理用户安全设置的实现类
  */
public interface UserSafeService {

	// 根据用户旧登录密码设置新登录密码
	void changePasswordByOldPassword(int userId, String oldPassword, String newPassword)  throws BusinessException;
	// 检测该昵称是否存在
	boolean checkNickname(String nickname);
	// 保存昵称和资金密码
	void setNicknameAndFundPassword(Integer userId, String nickname, String fundPassword);
	// 申请手机绑定
	boolean applyForPhoneBind(Integer userId, String phone, String areaCode) throws BusinessException;
	// 绑定手机的业务实现
	void bindPhone(Integer userId, String captcha, String phone) throws BusinessException;
	// 根据用户Id获取用户安全信息
	UserSafeInfo getUserSafeInfo(Integer userId);
	// 根据用户ID向指定手机发送验证码信息
	void getPhoneCodeByUserId(Integer userId) throws BusinessException;
	// 根据用户ID验证手机验证码
	void verifyPhoneCodeByUserId(Integer userId, String phoneCaptcha) throws BusinessException;
	// 根据用户ID验证谷歌验证码
	void verifyGoogleCodeByUserId(Integer userId, String googleCaptcha) throws BusinessException;
	// 根据用户ID验证邮件验证码
	void verifyEmailCodeByUserId(Integer userId, String email, String emailCaptcha) throws BusinessException;
	// 根据用户ID绑定用户的邮箱信息
	void bindEmailByUserId(Integer userId, String email);
	// 生成绑定谷歌验证码，并存入redis
	Map<String, Object> getGoogleAuth(Integer userId) throws BusinessException;
	// 绑定谷歌验证码的业务实现
	void bindGoogleAuth(Integer userId) throws BusinessException;
	// 根据用户ID更新用户资金密码
	void changeFundPassword(Integer userId, String newFundPassword);
	
}
