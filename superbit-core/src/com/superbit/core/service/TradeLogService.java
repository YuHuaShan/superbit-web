package com.superbit.core.service;

import java.util.List;

import com.superbit.core.dto.ComplainDetailDto;
import com.superbit.core.dto.OrderDetailDto;
import com.superbit.core.dto.TradeLogListDto;
import com.superbit.core.entry.tradelog.TradeLog;

/**
 * 交易记录服务类接口
 * 
 * @author zhangdaoguang
 *
 */
public interface TradeLogService {
	/**
	 * 保存交易记录
	 */
	Long saveTradeLog(TradeLog tradeLog);

	/**
	 * 获取基本信息初始化过的交易记录
	 */
	TradeLog getInitTradeLog();

	/**
	 * 获取交易记录列表
	 */
	List<TradeLogListDto> getTradeLogList(Integer userId, Integer tradeType,
			Integer payStatus, Integer hasComplain, Long payNumber,
			String coinCode, Integer pageNo, Integer pageSize);

	/**
	 * 获取订单详情
	 */
	OrderDetailDto getOrderDetail(String orderNumber);

	/**
	 * 更新订单
	 */
	Integer updateOrder(String orderNumber, Integer payStatus,
			String fundPassword);

	/**
	 * 保存申诉记录
	 */
	Long saveComplainLog(String orderNumber, Integer uid, Integer complainUid,
			String complainContent);

	/**
	 * 保存申诉记录
	 */
	ComplainDetailDto getComplainDetail(String orderNumber);

	/**
	 * 取消申诉
	 */
	void complainCancel(String orderNumber);

	/**
	 * 申诉完成
	 */
	void complainDone(String orderNumber);

	/**
	 * 申诉失败
	 */
	void complainFail(String orderNumber);

}
