package com.superbit.core.service;

import com.superbit.core.dto.QuoteOrderDto;
import com.superbit.core.exception.BusinessException;

/**
 * 报价单模块给后台接口
 * @author 赵世栋
 * @date 2018-01-17
 *
 */
public interface QuoteOrderProxy {

	/**
	 * 交易部分点击取消时的接口
	 * @param quoteOrder  报价单信息
	 * @return true :取消成功  false :取消失败
	 */
	
	boolean cancelTradeOrder(QuoteOrderDto quoteOrderDto) throws BusinessException;
	/**
	 * 删除报价单id
	 * @param orderId  报价单id
	 * @return false 失败  true 成功
	 */
	boolean deleteQuoteOrder(long orderId) throws BusinessException;
}
