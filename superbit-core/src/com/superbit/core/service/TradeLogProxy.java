package com.superbit.core.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.superbit.core.entry.tradelog.TradeLog;

/**
 * 交易记录对内提供接口
 * 
 * @author zhangdaoguang
 *
 */
public interface TradeLogProxy {
	/**
	 * 获取用户交易次数
	 * @param userId 用户ID
	 * @return 交易次数
	 */
	Integer getTradeCount(Integer userId);

	/**
	 * 获取用户平均放行时间
	 * @param userId 用户ID
	 * @return 放行时间,单位秒
	 */
	Long avgDoneTime(Integer userId);

	/**
	 * 获取用户最后成交时间
	 * @param userId 用户ID
	 * @return 时间戳
	 */
	Timestamp getLastTradeTime(Integer userId);

	/**
	 * 获取coinCode最新成交单价CNY
	 * @param userId 用户ID
	 * @return 成交单价
	 */
	BigDecimal getNewTradeCNY(String coinCode);

	/**
	 * 获取coinCode上一天最后一笔成交单价CNY
	 * @param userId 用户ID
	 * @return 成交单价
	 */
	BigDecimal getLastDayTradeCNY(String coinCode);
	
	/**
	 * 通过报价ID获取交易记录列表
	 */
	List<TradeLog> getTradeLogListByQuoteId(Long quoteId);
	
	/**
	 * 通过用户ID获取交易记录列表
	 */
	List<TradeLog> getTradeLogListByUserId(Integer userId);
	
	/**
	 * 通过用户ID获取买入的交易记录列表
	 */
	List<TradeLog> getBuyTradeLogListByUserId(Integer userId);
	
	/**
	 * 通过用户ID获取卖出的交易记录列表
	 */
	List<TradeLog> getSellTradeLogListByUserId(Integer userId);
	/**
	 * 通过币种ID获取币种对应人民币价格
	 * @param map  key为coinCode，value为0
	 * @return map key为coinCode，value为该币种对应人民币的这算价格
	 */
	Map<String, BigDecimal> getCoinPrice(Map<String, BigDecimal> map);
}
