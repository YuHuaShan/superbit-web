package com.superbit.core.service;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 用户资产接口
 * 
 * @author hongzhen
 *
 */
public interface UserAssetService {
	/**
	 * 转移资产
	 * 
	 * @param outUserId
	 *            放币用户ID
	 * @param inUserId
	 *            收币用户ID
	 * @param inUserId
	 *            币种ID
	 * @param fundAmount
	 *            转移币数量
	 * @return 结果状态码,0:转移成功,1:转移失败
	 */
	public Integer transferFund(Integer outUserId, Integer inUserId,
			String coinCode,
			BigDecimal fundAmount);
	
	/**
	 * 返回用户总资产，以及对应人民币价格
	 * @param userId
	 * @param coinCode
	 * @return
	 */
	Map<String, BigDecimal> getUser_Asset(int userId, String coinCode);
	/**
	 * 用户资产冻结及解冻
	 * @param userId
	 * @param coinCode
	 * @param amount 正值为冻结，负值为解冻
	 * @return
	 */
	boolean freezeUser_Asset(String userId, String coinCode, BigDecimal amount);
	/**
	 * 根据用户Id和币种码获取对应的余额数
	 * @param userId 用户id
	 * @param coinCode 币种码
	 * @return
	 */
	BigDecimal getCoinAmouts(int userId,String coinCode);
}
