package com.superbit.core.service;

import com.superbit.core.entry.UserTradeRecord;
import com.superbit.core.exception.BusinessException;

/**
 * 交易完成情况业务层
 * @author 赵世栋
 * @date 2018-01-12
 */
public interface UserTradeRecordService {
	/**
	 * 获取用户交易完成记录
	 * @param userId 用户IId
	 * @param coinCode 币种Code
	 * @date 2018-1-12
	 * @return 
	 */
	UserTradeRecord getUserTradeRecordeInfo(long userId);
	/**
	 * 修改用户交易完成记录
	 * @param userTradeRecorde
	 *  @date 2018-1-12
	 * @return
	 */
	void updateUserTradeRecordeInfo(UserTradeRecord userTradeRecorde) throws BusinessException;
	/**
	 * 修改用户交易完成记录
	 * @param userTradeRecorde 
	 *  @date 2018-1-12
	 * @return
	 */
	int saveUserTradeRecordeInfo(UserTradeRecord userTradeRecorde)throws BusinessException;

}
