package com.superbit.core.service;
/** @author  WangZhenwei 
  * @date 创建时间：2018年1月20日 下午5:36:52  
  * @descrip 
  */
public interface UserRealAuthenService {

	//根据用户ID获得用户审核失败的理由
	String getFailReasonByUserId(Integer userId);

	
}
