package com.superbit.core.service;

import java.util.Map;

/**
 * 
 * @author Administrator
 *
 */
public interface TradeFinishRecordService {
	
	/**
	 * 获取指定币种的最新价和指数价及涨幅
	 * @param coinType 币种
	 * @return
	 */
	Map<String,Object> getPublicPriceInfo(Integer coinType);

	/**
	 * 获取指定币种24H内的最低价和最高价
	 * @param coinType 币种
	 * @return
	 */
	Map<String ,Object> get24HPriceInfo(Integer coinType);
	/**
	 * 获取指定币种的走势数据
	 * @param coinType 币种
	 * @return
	 */
	Map<String,Object> getTrendData(Integer coinType);
}
