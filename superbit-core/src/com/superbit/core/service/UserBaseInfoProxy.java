package com.superbit.core.service;
/** @author  WangZhenwei 
  * @date 创建时间：2018年1月19日 下午4:45:08  
  * @descrip 与用户安全信息相关的操作
  */
public interface UserBaseInfoProxy {

	// 根据用户ID获取用户昵称【用户的昵称可以从session中获取，建议】
	String getNickNameByUserId(Integer userId);
}
