package com.superbit.core.service;

import java.util.Date;
import java.util.Map;

import com.superbit.core.entry.UserActivityInfo;
import com.superbit.core.entry.UserBaseInfo;
import com.superbit.core.exception.BusinessException;
import com.superbit.utils.mailutil.EmailFailException;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月4日 下午8:33:24  
  * @descrip 登录模块的接口
  */
public interface LoginService {

	/**
	 * 登录时获取用户账号
	 * @throws BusinessException 
	 */
	Map<String,Object> login(String account, byte type, String password, Map<String, Object> accessData) throws BusinessException; 
	
	/**
	 * 登录时忘记密码发送验证码
	 * @return 
	 */
	UserBaseInfo forgetLoginPassword(String account) throws BusinessException,EmailFailException;
	
	/**
	 * 忘记密码重置密码的方法
	 */
	void changeLoginPassword(Integer userId, String activeCode, String password, Map<String,String> map) throws BusinessException;
	
	/**
	 * 密码错误次数超过5次，锁定用户的方法
	 */
	void lockUser(int userId,byte lockStatus,Date lockDeadline);
	// 鎖定狀態説明：0 未鎖定 1 密碼錯誤鎖定  2 後臺鎖定  3頻繁訪問鎖定
	
	/**
	 * 解锁用户
	 */
	void unlockUser(int userId);

	/**
	 * 根据用户Id获取用户基础信息
	 * @param userId
	 * @return
	 */
	UserBaseInfo getUserBaseInfo(int userId);

	/**
	 * 根据用户Id获取用户安全信息
	 * @param userId
	 * @return
	 */
	Map<String, Object> getUserSafeInfo(int userId);

	/**
	 * 根据用户Id获取用户活动信息
	 * @param userId
	 * @return
	 */
	UserActivityInfo getUserActivityInfo(int userId);

	/**
	 * 设置登录认证
	 * @param userId
	 * @param type
	 */
	void setLoginAuthentication(Integer userId, int type) throws BusinessException;

	/**
	 * 校验登录认证时发送手机验证码
	 * @param userId
	 */
	void checkLoginAuthenticationSendMessage(Integer userId) throws BusinessException;

	/**
	 * 校验登录认证
	 * @param userId
	 * @param captcha
	 */
	void checkLoginAuthentication(Integer userId, String captcha, String pageType) throws BusinessException;

	

	
}
