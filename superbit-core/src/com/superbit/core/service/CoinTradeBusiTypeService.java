package com.superbit.core.service;


import com.superbit.core.tools.PageListRes;
import com.superbit.core.vo.CoinTradeBusiTypeVO;

/**
 * 币种交易流通状态  业务层
 * @author 赵世栋
 * @date 2018-01-12
 *
 */
public interface CoinTradeBusiTypeService {

	/**
	 * 获取交易币种列表
	 * @param areaCode  币区名称
	 * @param coinName  币种名称
	 * @param coinStatus 币种status
	 * @param page   当前页数
	 * @param size   当前页容量
	 * @return 
	 */
	PageListRes<CoinTradeBusiTypeVO> getCoinTradeBusiList(String areaCode,String coinName,String coinStatus, int page,int size);


	/**
	 * 开启交易/停止交易
	 * @param id  
	 * @param status  修改状态   
	 * @param flag  true :交易状态    false  
	 * @return true :操作成功   false :操作失败
	 * 
	 */
	boolean changeTradeStatusById(long id,String status,boolean flag);
	
	
}
