package com.superbit.core.service;
/** @author  WangZhenwei 
  * @date 创建时间：2018年1月13日 上午9:21:58  
  * @descrip 用户角色的业务实现
  */
public interface UserRoleService {

	// 根据用户Id获取用户角色
	String[] getUserRoles(int userId);

}
