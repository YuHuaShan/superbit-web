package com.superbit.core.service;


/**
 * @function 回滚抽象类
 * @author WangZhenwei
 * @version 2018-01-08 13:36
 */
public abstract class LongTimeCallbackService {
	
	public void makeRemoteCallAndUnknowWhenFinish(final LongTimeTaskCallback callback, boolean isSync) {
		if(!isSync) {
			callback.callback();
		}else {
			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					callback.callback();
				}
			});
			t.start();
		}
	}

}
