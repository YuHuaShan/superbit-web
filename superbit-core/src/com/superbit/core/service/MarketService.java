package com.superbit.core.service;

import java.util.List;

import com.superbit.core.exception.BusinessException;
import com.superbit.core.vo.CoinTypeVO;

/**
 * 市场 相关业务接口
 * @author 赵世栋
 * @date:2018-01-16
 */
public interface MarketService {

	/**
	 * 获取市场行情信息
	 * @return
	 */
	List<CoinTypeVO>getCoinMarketInfo()throws BusinessException;
}
