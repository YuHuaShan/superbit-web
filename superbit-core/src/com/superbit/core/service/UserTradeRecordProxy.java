package com.superbit.core.service;
/**
 * 交易记录完成 
 * 
 * @author 赵世栋
 *
 */
public interface UserTradeRecordProxy {
	/**
	 * 交易次数加1
	 * @param userId 用户id
	 */
	void addTradCount(long userId);
	/**
	 * 交易次数减1
	 * @param userId 用户id
	 */
	void reduceTradCount(long userId);
}
