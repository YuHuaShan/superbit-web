package com.superbit.core.service;

import java.util.List;

import com.superbit.core.entry.UserPayMethod;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月19日 下午4:48:18  
  * @descrip 与用户支付信息相关的操作
  */
public interface UserPayMethodProxy {

	// 根据用户ID获取用户支付方式【交易系统=后】
		List<UserPayMethod> getUserPayMethodByUserId(Integer userId);
}
