package com.superbit.core.service;

import java.util.List;
import java.util.Map;

import com.superbit.core.entry.QuoteOrderRecord;

/**
 * 报价交易业务层
 * @author 赵世栋
 * @version 2018-1-4
 *
 */
public interface QuoteOrderRecordService {
	/**
	 * 
	 * @param userId 用户Id
	 * @param queryStr 查询参考号
	 * @param showType 卖出/买入
	 * @param payStatus 支付状态查询 0：等待支付 1：已付款 2：已完成  4：买家取消
	 * @param complainStatus 是否勾选申诉中 0：未勾选  1：勾选
	 * @param pageNumber 当前页数
	 * @param pageSize   页容量
	 * @return
	 */
	List<QuoteOrderRecord>getQuoteOrderRecordList(Integer userId,String queryStr,String showType,Integer payStatus,String complainStatus,
			Integer pageNumber,Integer pageSize);
	/**
	 * 撤销报价交易
	 * @param paymentNumber 付款参考号
	 * @param payStatus 付款状态
	 */
	void cancelQuoteOrderRecord(Integer paymentNumber,Integer payStatus );
	/**
	 * 去支付
	 * @param userId 用户id
	 * @param quoteRecordId 交易订单号
	 * @param payStatus 支付状态
	 * @return 订单信息和卖家收款方式
	 */
	Map<String ,Object>toPay(Integer userId,Integer quoteRecordId,Integer payStatus);
	/**
	 * 申诉
	 * @param userId 用户Id
	 * @param quoteRecordId 报价交易Id
	 * @param payStatus 支付状态
	 */
	void toComplain(Integer userId,Integer quoteRecordId,Integer payStatus);
}
