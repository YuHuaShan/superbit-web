package com.superbit.core.service;

import java.math.BigDecimal;
import java.util.Map;

import com.superbit.core.exception.BusinessException;
import com.superbit.core.tools.PageListRes;
import com.superbit.core.vo.QuoteOrderVO;

/**
 * 报价订单业务层
 * @author 赵世栋
 * @version 2018-1-4
 */
public interface QuoteOrderService {

	/**
	 * 买入报价
	 * @param priceCNY 价格
	 * @param indexPrice 指数价
	 * @param amountCoin 数量
	 * @param quotePrice 总金额
	 * @param ALOtransaction 最小交易量
	 * @param payType 支付方式
	 * @param coinCode 币种码
	 * @param sellerUserId 卖家userId
	 * @param buyserUserId 买家userId
	 * @param quoteId 报价订单Id
	 * @param isAutoSetting 是否设置自动定价 0：未设置 1：设置
	 * @param setValue 自动增益值
	 * @throws BusinessException
	 */
	boolean buyinQuoteOrder(BigDecimal indexprice,double priceCNY,BigDecimal amountCoin,BigDecimal quotePrice,int ALOtransaction,
			String payType,String coinCode,int buyserUserId,String sellerUserId,long quoteId,int isAutoSetting,String setValue)throws BusinessException;
	/**
	 * 卖出
	 * @param priceCNY价格
	 * @param indexPrice 指数价
	 * @param amountCoin 数量
	 * @param quotePrice 总金额
	 * @param ALOtransaction 最小交易量
	 * @param payType 支付方式
	 * @param coinType 币种Id
	 * @param sellerUserId 卖家userId
	 * @param buyserUserId 买家userId
	 * @param quoteId 报价订单Id
	 * @param isAutoSetting 是否设置自动定价 0：未设置 1：设置
	 * @param setValue 自动增益值
	 * @throws BusinessException
	 */
	boolean selloutQuoteOrder(BigDecimal indexprice,double priceCNY, BigDecimal amountCoin,BigDecimal quotePrice,int ALOtransaction,
			String payType,String coinCode,String buyserUserId,int sellerUserId,long quoteId,int isAutoSetting,String setValue)throws BusinessException;
	/**
	 * 撤销报价订单
	 * @param quoteId 报价Id
	 * @param quoteStatus  报价订单状态
	 * @throws BusinessException
	 * @return true 撤销成功    false 撤销失败
	 */
	boolean cancelQuoteOrder(long quoteId,int quoteStatus,long userId)throws BusinessException;
	/**
	 * 获取挂单列表
	 * @param userId 用户Id
	 * @param queryStr 查询参数  查询字段 all :全部   buy： 买入  sell：卖出
	 * @param pageNumber 当前页数 
	 * @param pageSize  页容量
	 * @param coinCode 币种码
	 * @return
	 */
	PageListRes<QuoteOrderVO>getQuoteOrderList(int userId,String coinCode,String queryStr,Integer pageNumber,Integer pageSize);
	/**
	 * 获取指定币种的大盘信息
	 * @param coinCode 币种码
	 * @param dataSize 返回数据的数量
	 * @param quoteType 报价类型  0：买 1：卖  该字段取消于2018-01-11 由前端进行匹配
	 * @return
	 */
	Map<String ,Object> getGrailData(String coinCode,Integer dateSize);
	/**
	 * 修改设置自动增益的报价单
	 * @date 2018-01-16
	 * @return true :修改成功   false ：修改失败
	 */
	boolean updateAutoSettingQuoteOrder()throws BusinessException;
}
