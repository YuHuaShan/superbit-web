package com.superbit.core.service;
/**
 * 申诉业务层
 * @author 赵世栋
 * @version 2018-1-4
 *
 */
public interface ComplainService {
	
	/**
	 * 创建申诉
	 * @param complainReason 申诉原因
	 * @param userId 创建者Id
	 * @param consultNumber 付款参考号
	 * @param tradeRecodeId 交易记录Id
	 * @param title     申诉标题
	 */
	void createComplain(String complainReason,Integer userId,Integer consultNumber,Integer tradeRecodeId,String title);
	/**
	 * 回复申诉
	 * @param complainReason 申诉结果
	 * @param adminId 评审者Id
	 * @param consultNumber 付款参考号
	 * @param tradeRecodeId 交易记录Id
	 * @param title 申诉标题
	 */
	void replyComplain(String complainReason,Integer adminId,Integer consultNumber,Integer tradeRecodeId,String title);
}
