package com.superbit.core.service;

import java.util.List;
import java.util.Map;

import com.superbit.core.entry.UserPayMethod;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月20日 上午10:56:06  
  * @descrip 用户支付方式的业务类
  */
public interface UserPayMethodService {

	// 根据用户ID获取用户支付信息
	public List<UserPayMethod> getUserPayMethodByUserId(Integer userId);
	// 根据用户ID保存用户支付信息
	public void saveUserPayMethodByCondition(Integer userId, Map<String, Object> map);
	// 根据用户ID和支付编号查询账户支付详情
	public UserPayMethod getPayMethodDetailByCondition(Integer userId, int payMethod); 
		

}
