package com.superbit.core.constant;
/** @author  WangZhenwei 
  * @date 创建时间：2018年1月20日 下午2:16:38  
  * @descrip 
  */
public interface UserStatusConstant {

	/* 身份认证状态, 0 ：未提交，1：已提交未认证 ，2： 认证失败 ,3： 认证成功 */
	final Byte AUTHEN_STATUS_INIT = 0;
	final Byte AUTHEN_STATUS_SUBMIT = 1;
	final Byte AUTHEN_STATUS_FAIL = 2;
	final Byte AUTHEN_STATUS_SUCCESS = 3;

	/* 用户绑定付款方式类型, 0 ：银行卡，1： 支付宝，2： 微信 */
	final Integer PAY_METHOD_CARD = 0;
	final Integer PAY_METHOD_ALI = 1;
	final Integer PAY_METHOD_WX = 2;
	
	/*用户协议是否勾选*/
	final Integer USER_PROTOCOL_BLANK = 0;
	final Integer USER_PROTOCOL_SELECT = 1;
	
	/*验证码位数6位*/
	final Integer CHECK_CODE_LENGTH = 6;
	
	/*用户账户类型 1邮箱 2 手机*/
	final Byte USER_ACCOUNT_EMAIL = 1;
	final Byte USER_ACCOUNT_PHONE = 2;
	
	/*用户是否绑定手机0未绑定 1已绑定*/
	final Byte BIND_PHONE_NO = 0;
	final Byte BIND_PHONE_YES = 1;
	
	/*安全等级说明 0低什么也没有 1手机 中  2谷歌私钥 高*/
	final Byte SECURITY_LEVEL_NONE = 0;
	final Byte SECURITY_LEVEL_PHONE = 1;
	final Byte SECURITY_LEVEL_GOGGLE = 2;
	
	/*冻结状态说明：0 活动 1冻结 2修改登录密码*/
	final Byte USER_STATUS_ACTIVE = 0;
	final Byte USER_STATUS_FREEZE = 1;
	final Byte USER_STATUS_RESETPWD = 2;
	
	/*用户锁定状态锁定说明0未锁定，1密码错误锁定，2后台锁定，3频繁访问锁定*/
	final Byte LOCK_STATUS_NONE = 0;
	final Byte LOCK_STATUS_WRONGPWD = 1;
	final Byte LOCK_STATUS_ADMIN = 2;
	final Byte LOCK_STATUS_FREQ_ACCESS = 3;
	
	/*登录方式说明：0web，1手机html，2安卓，3IOS*/
	final Byte LOGIN_MODE_WEB = 0;
	final Byte LOGIN_MODE_PHONE_WEB = 1;
	final Byte LOGIN_MODE_ANDROID = 2;
	final Byte LOGIN_MODE_IOS = 3;
	
	/*登录状态说明0成功，1失败*/
	final Byte LOGIN_STATUS_SUCCESS = 0;
	final Byte LOGIN_STATUS_FAIL = 1;
	
	/*图片类型0 表示身份认证信息 1 表示用户账户信息*/
	final Byte IMG_TYPE_AUTH = 0;
	final Byte IMG_TYPE_ACCOUNT = 1;
	
	/*身份认证信息时 0 正面照 1 背面照 2 手持身份证照*/
	final Byte IMG_AUTH_FRONT = 0;
	final Byte IMG_AUTH_BACK = 1;
	final Byte IMG_AUTH_HAND = 2;
}
