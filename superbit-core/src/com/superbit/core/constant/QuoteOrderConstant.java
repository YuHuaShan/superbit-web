package com.superbit.core.constant;
/**
 * 报价单模块 常量维护
 * @author 赵世栋
 * @date 2018-01-17
 *
 */
public interface QuoteOrderConstant {

	//深度方向
	final int DEEP_LOW = 1;
	final int DEEP_HIGHT = 2;
	
	//买入
	final int DIRECTION_BUY = 0;
	//卖出
	final int DIRECTION_SELL = 1;
	/*报价订单的状态 0：未成交 1：部分成交中 2：部分成交 */
	final int QUOTE_STATUS_NODEAL = 0;
	final int QUOTE_STATUS_DEALING = 1;
	final int QUOTE_STATUS_PART_DEAL = 2;
	//设置报价单自动增益 0：没有设置  1：设置自动增益
	final int QUOTE_SET_NO_AUTO = 0;
	final int QUOTE_SET_AUTO = 1;
	//交易码 用于维护和交易部分衔接 0：未开始交易   1：交易中  2：最后一笔交易
	final int QUOTE_TRADESTATUS_NO = 0;
	final int QUOTE_TRADESTATUS_DOING = 1;
	final int QUOTE_TRADESTATUS_LAST = 2;
	//交易时对应CNY的最低要求
	final int QUOTE_INIT_COUNT = 1000;//报价单对应的最初CNY数
	
	//报价单是否设置自动增益  0：未设置（已勾选）  1：设置（勾选）
	final int QUOTE_ORDER_NO_SETAUTO = 0;
	final int QUOTE_ORDER_YES_SETAUTO = 1;
	/*费用相关*/
	//交易费
	final double QUOTE_TRADE_FEE = 0;
	//精确位数(CNY)
	final int QUOTE_CNY_ACCURAY = 8;
	//币数量的精确度(COIN)
	final int QUOTE_COIN_ACCURY = 2;
	//后台的都显示10
	final int QUOTE_DATA_ACCURY = 10;
	//误差取值  用于前端和后台计算精度丢失
	final double QUOTE_DEVIATION = 0.001;
	
	
	/*	
	 * 1开头和3开头的错误属于系统错误， 上文中有明确提示的进行提示， 无明确提示的统一提示 "未定义错误"
	 * 2开头的属于业务异常， 每种Code都有相应异常提示， 针对每一个URL需要分别处理， 请向开发人员索取文档
	 * */
	static final String PARAMISINVALID = "QO500";//参数不合法
	
	
	
}
