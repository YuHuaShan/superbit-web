package com.superbit.core.constant;

/**
 * 交易记录常量
 * 
 * @author zhangdaoguang
 *
 */
public interface TradeLogConstants {
	/* 交易状态 0：未付款，1：已付款，2：已完成(放币)，3：买家已取消，4：超时取消 */
	final Integer PAY_STATUS_NOT_PAY = 0;
	final Integer PAY_STATUS_HAVE_PAY = 1;
	final Integer PAY_STATUS_DONE = 2;
	final Integer PAY_STATUS_BUY_CANCEL = 3;
	final Integer PAY_STATUS_TIMEOUT_CANCEL = 4;

	/* 申诉状态 0：没申诉，1：有申诉 */
	final Integer HAVE_COMPLAIN_NO = 0;
	final Integer HAVE_COMPLAIN_YES = 1;

	/* 交易类型 0：买入, 1：卖出 */
	final Integer TRADE_TYPE_BUY = 0;
	final Integer TRADE_TYPE_SELL = 1;

	/* 是否有未读消息, 0:有未读消息, 1:没有 */
	final Integer HAVE_NEW_MSG = 0;
	final Integer NOT_HAVE_NEW_MSG = 1;
	
	/* 消息是否读取，0未读，1已读  */
	final Integer NOT_HAVE_READ = 0;
	final Integer HAVE_READ = 1;
	
	/* 消息类型， 0:用户消息,1系统消息 */ 
	final Integer CHAT_TYPE_USER = 0;
	final Integer CHAT_TYPE_SYSTEM = 1;
	final Integer SYSTEM_ID = -1;

	final String BUY_NAME = "买家";
	final String SELL_NAME = "卖家";

	/* 付款方式类型, 0 ：银行卡, 1： 支付宝, 2： 微信 */
	final Integer CNANEL_ID_CARD = 0;
	final Integer CNANEL_ID_ALI = 1;
	final Integer CNANEL_ID_WX = 2;

	/* 申诉状态, 0 ：申诉中，1：申诉完成 ，2： 申诉失败 ,3： 取消 */
	final Integer COMPLAIN_STATUS_ING = 0;
	final Integer COMPLAIN_STATUS_DONE = 1;
	final Integer COMPLAIN_STATUS_FAIL = 2;
	final Integer COMPLAIN_STATUS_CANEL = 3;

	/* 未付款超时时间为5分钟 */
	final long AUTO_CANCEL_TIME = 5 * 60 * 1000L;

	/* 更改订单状态, 1:交易记录不存在,2: 资金密码不正确,3: 资金转移失败 */
	final Integer TRADE_LOG_NOT_EXIST = 1;
	final Integer FUND_PASSWORD_ILLEGAL = 2;
	final Integer FUND_TRANSFER_FAIL = 3;

	/* 交易记录查询类型, 0:全部,1: 买入列表,2: 卖出 */
	final Integer QUERY_TYPE_ALL = 0;
	final Integer QUERY_TYPE_BUY = 1;
	final Integer QUERY_TYPE_SELL = 2;
}
