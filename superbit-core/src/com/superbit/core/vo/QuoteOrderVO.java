package com.superbit.core.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.superbit.core.entry.QuoteOrder;

/**
 * 报价单的vo
 * @author 赵世栋
 * @date 2018-01-15
 */
public class QuoteOrderVO implements Serializable{

	private static final long serialVersionUID = 2493323869085534939L;
	private long quoteId; 
	private int quoteType;
	private BigDecimal amountCoin;
	private BigDecimal priceCNY;
	private int ALOtransaction;
	private BigDecimal totalPrice;
	private BigDecimal dealAmount;
	private double transactionProgress;
	private String payType;
	private Date createTime;
	private int tradeStatus;
	
	
	public QuoteOrderVO() {
		super();
	}
	public QuoteOrderVO(QuoteOrder order){
		this.quoteId = order.getId();
		this.quoteType = order.getQuoteType();
		this.priceCNY = order.getPriceCNY();
		this.ALOtransaction = order.getALOtransaction();
		this.totalPrice = order.getTotalPrice();
		this.dealAmount = order.getDealAmount();
		this.transactionProgress = order.getTransactionProgress();
		this.payType =order.getPayType();
		this.createTime = order.getCreateTime();
		this.tradeStatus = order.getQuoteStatus();
		this.amountCoin = order.getAmountCoin();
	}
	public long getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(long quoteId) {
		this.quoteId = quoteId;
	}
	public int getQuoteType() {
		return quoteType;
	}
	public void setQuoteType(int quoteType) {
		this.quoteType = quoteType;
	}
	public BigDecimal getPriceCNY() {
		return priceCNY;
	}
	public void setPriceCNY(BigDecimal priceCNY) {
		this.priceCNY = priceCNY;
	}
	public int getALOtransaction() {
		return ALOtransaction;
	}
	public void setALOtransaction(int aLOtransaction) {
		ALOtransaction = aLOtransaction;
	}
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	public BigDecimal getDealAmount() {
		return dealAmount;
	}
	public void setDealAmount(BigDecimal dealAmount) {
		this.dealAmount = dealAmount;
	}
	public double getTransactionProgress() {
		return transactionProgress;
	}
	public void setTransactionProgress(double transactionProgress) {
		this.transactionProgress = transactionProgress;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public int getTradeStatus() {
		return tradeStatus;
	}
	public void setTradeStatus(int tradeStatus) {
		this.tradeStatus = tradeStatus;
	}
	public BigDecimal getAmountCoin() {
		return amountCoin;
	}
	public void setAmountCoin(BigDecimal amountCoin) {
		this.amountCoin = amountCoin;
	}
	
}
