package com.superbit.core.vo;

import java.io.Serializable;
/**
 * 币种VO
 * @author 赵世栋
 *
 */
public class CoinTypeVO implements Serializable{
	
	private static final long serialVersionUID = 880483254284814541L;
	/*币种id*/
	private int coinId;
	/*币名称*/
	private String coinName;
	/*币种码*/
	private String coinCode;
	/*币区*/
	private String coinArea;
	/*涨幅*/
	private String increase;
	/*最新成交价*/
	private String lastestPrice;
	
	public int getCoinId() {
		return coinId;
	}
	public void setCoinId(int coinId) {
		this.coinId = coinId;
	}
	public String getCoinName() {
		return coinName;
	}
	public void setCoinName(String coinName) {
		this.coinName = coinName;
	}
	public String getCoinCode() {
		return coinCode;
	}
	public void setCoinCode(String coinCode) {
		this.coinCode = coinCode;
	}
	public String getCoinArea() {
		return coinArea;
	}
	public void setCoinArea(String coinArea) {
		this.coinArea = coinArea;
	}
	public String getIncrease() {
		return increase;
	}
	public void setIncrease(String increase) {
		this.increase = increase;
	}
	public String getLastestPrice() {
		return lastestPrice;
	}
	public void setLastestPrice(String lastestPrice) {
		this.lastestPrice = lastestPrice;
	}
	

}