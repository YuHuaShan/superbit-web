package com.superbit.core.vo;

import com.superbit.core.entry.CoinTradeBusiType;

public class CoinTradeBusiTypeVO {
	/*id*/
	private long id;
	/*启用状态 点击生效后默认是开启状态   0：停用状态 1：启用状态*/
	private int isActivate ;
	/*交易状态：0：关闭交易1：开启交易*/
	private int tradeStatus;
	/*币种名称*/
	private String coinName;
	
	public CoinTradeBusiTypeVO(CoinTradeBusiType coinTrade) {
		this.id = coinTrade.getId();
		this.isActivate = coinTrade.getIsActivate();
		this.tradeStatus = coinTrade.getTradeStatus();
		this.coinName = coinTrade.getCoinName();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getIsActivate() {
		return isActivate;
	}
	public void setIsActivate(int isActivate) {
		this.isActivate = isActivate;
	}
	public int getTradeStatus() {
		return tradeStatus;
	}
	public void setTradeStatus(int tradeStatus) {
		this.tradeStatus = tradeStatus;
	}
	public String getCoinName() {
		return coinName;
	}
	public void setCoinName(String coinName) {
		this.coinName = coinName;
	}
	
}
