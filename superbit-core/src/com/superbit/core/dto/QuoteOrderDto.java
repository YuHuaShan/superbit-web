package com.superbit.core.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 接收交易模块回退报价订单
 * @date 2018-01-17
 * @author 赵世栋
 *
 */
public class QuoteOrderDto {
	
	private long quoteId;
	/* 订单号 */
	private String orderNumber;
	/* 价格CNY(单价) */
	private BigDecimal unitPrice;
	/* 数量BTC */
	private BigDecimal coinAmount;
	/* 金额CNY(总价) */
	private BigDecimal totalPrice;
	/* 手续费BTC */
	private BigDecimal poundageBTC;
	/* 下单时间 */
	private Timestamp createTime;
	/*币种码*/
	private String coinCode;
	/*买方用户Id*/
	private int buyUserId;
	/*买方用户Id*/
	private int sellUserId;
	
	public long getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(long quoteId) {
		this.quoteId = quoteId;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
	public BigDecimal getCoinAmount() {
		return coinAmount;
	}
	public void setCoinAmount(BigDecimal coinAmount) {
		this.coinAmount = coinAmount;
	}
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	public BigDecimal getPoundageBTC() {
		return poundageBTC;
	}
	public void setPoundageBTC(BigDecimal poundageBTC) {
		this.poundageBTC = poundageBTC;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	
	
	public int getBuyUserId() {
		return buyUserId;
	}
	public void setBuyUserId(int buyUserId) {
		this.buyUserId = buyUserId;
	}
	public int getSellUserId() {
		return sellUserId;
	}
	public void setSellUserId(int sellUserId) {
		this.sellUserId = sellUserId;
	}
	public String getCoinCode() {
		return coinCode;
	}
	public void setCoinCode(String coinCode) {
		this.coinCode = coinCode;
	}
	@Override
	public String toString() {
		return "QuoteOrderDto [quoteId=" + quoteId + ", orderNumber=" + orderNumber + ", unitPrice=" + unitPrice
				+ ", coinAmount=" + coinAmount + ", totalPrice=" + totalPrice + ", poundageBTC=" + poundageBTC
				+ ", createTime=" + createTime + ", buyUserId=" + buyUserId + ", sellUserId=" + sellUserId + "]";
	}

	
}
