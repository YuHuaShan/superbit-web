package com.superbit.core.dto;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 订单详情dto
 * 
 * @author zhangdaoguang
 *
 */
public class OrderDetailDto {
	/* 金额CNY(总价) */
	private BigDecimal totalPrice;
	/* 数量BTC */
	private BigDecimal coinAmount;
	/* 价格CNY(单价) */
	private BigDecimal unitPrice;
	/* 手续费BTC */
	private BigDecimal poundageBTC;
	/* 付款参考号 */
	private Long payNumber;
	/* 状态,0：未付款 1：已付款 2：已完成 3：买家已取消 4：超时取消 */
	private Integer payStatus;
	/* 确认支付时间(对应payStatus=1) */
	private String payTime;
	/* 放行时间(对应payStatus=2) */
	private String doneTime;
	/* 申诉时间 */
	private String complainTime;
	/* 微信相关数据 */
	private Map<String, String> wx;
	/* 支付宝相关数据 */
	private Map<String, String> ali;
	/* 银行卡相关数据 */
	private Map<String, String> card;

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public BigDecimal getCoinAmount() {
		return coinAmount;
	}

	public void setCoinAmount(BigDecimal coinAmount) {
		this.coinAmount = coinAmount;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getPoundageBTC() {
		return poundageBTC;
	}

	public void setPoundageBTC(BigDecimal poundageBTC) {
		this.poundageBTC = poundageBTC;
	}

	public Long getPayNumber() {
		return payNumber;
	}

	public void setPayNumber(Long payNumber) {
		this.payNumber = payNumber;
	}

	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public String getPayTime() {
		return payTime;
	}

	public void setPayTime(String payTime) {
		this.payTime = payTime;
	}

	public String getDoneTime() {
		return doneTime;
	}

	public void setDoneTime(String doneTime) {
		this.doneTime = doneTime;
	}

	public String getComplainTime() {
		return complainTime;
	}

	public void setComplainTime(String complainTime) {
		this.complainTime = complainTime;
	}

	public Map<String, String> getWx() {
		return wx;
	}

	public void setWx(Map<String, String> wx) {
		this.wx = wx;
	}

	public Map<String, String> getAli() {
		return ali;
	}

	public void setAli(Map<String, String> ali) {
		this.ali = ali;
	}

	public Map<String, String> getCard() {
		return card;
	}

	public void setCard(Map<String, String> card) {
		this.card = card;
	}

}
