package com.superbit.core.dto;

/**
 * 申诉详情dto
 * 
 * @author zhangdaoguang
 */
public class ComplainDetailDto {
	/* 申诉状态 0：申诉中 1：申诉完成 2：申诉失败 */
	private Integer complainStatus;
	/* 申诉理由内容 */
	private String complainContent;
	/* 申诉结果内容 */
	private String resultContent;
	/* 申诉时间 */
	private String createTime;
	/* 更新(结果)时间 */
	private String updateTime;

	public Integer getComplainStatus() {
		return complainStatus;
	}

	public void setComplainStatus(Integer complainStatus) {
		this.complainStatus = complainStatus;
	}

	public String getComplainContent() {
		return complainContent;
	}

	public void setComplainContent(String complainContent) {
		this.complainContent = complainContent;
	}

	public String getResultContent() {
		return resultContent;
	}

	public void setResultContent(String resultContent) {
		this.resultContent = resultContent;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

}
