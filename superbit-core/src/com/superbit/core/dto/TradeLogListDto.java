package com.superbit.core.dto;

import java.math.BigDecimal;

import com.superbit.core.constant.TradeLogConstants;
import com.superbit.core.entry.tradelog.TradeLog;
import com.superbit.utils.date.DateFormateUtils;

/**
 * 交易记录列表dto
 * 
 * @author zhangdaoguang
 *
 */
public class TradeLogListDto {
	private String orderNumber;
	/* 币种Code */
	private String coinCode;
	/* 币种名称 */
	private String coinName;
	/* 交易类型 */
	private Integer tradeType;
	/* 交易对象UID */
	private Integer tradeUserId;
	/* 交易对象名称 */
	private String tradeUserName;
	/* 价格CNY(单价) */
	private BigDecimal unitPrice;
	/* 数量BTC */
	private BigDecimal coinAmount;
	/* 金额CNY(总价) */
	private BigDecimal totalPrice;
	/* 手续费BTC */
	private BigDecimal poundageBTC;
	/* 下单时间 */
	private String createTime;
	/* 交易方式： 记录id多个逗号分隔,0银行卡,1支付宝,2微信 */
	private String payType;
	/* 付款参考号 */
	private Long payNumber;
	/* 状态,0：未付款 1：已付款 2：已完成 3：买家已取消 4：超时取消 */
	private Integer payStatus;
	/* 申诉人UID */
	private Integer complainPeopleId;
	/* 申诉人,展示买家、卖家 */
	private String complainPeople;
	/* 申诉者姓名 */
	private String complainPeopleName;
	/* 申诉状态：0申诉中,1申诉完成 ,2申诉失败 */
	private Integer complainStatus;
	private Integer hasNewMessage; // 聊天图标：0有未读消息，1没有
	private Integer buyserUid;
	private Integer sellerUid;

	public TradeLogListDto(TradeLog log) {
		this.orderNumber = log.getOrderNumber();
		this.coinCode = log.getCoinCode();
		this.coinName = log.getCoinName();
		this.tradeType = log.getTradeType();
		if (TradeLogConstants.TRADE_TYPE_BUY.equals(log.getTradeType())) {
			this.tradeUserId = log.getSellerUid();
		} else if (TradeLogConstants.TRADE_TYPE_SELL.equals(log.getTradeType())) {
			this.tradeUserId = log.getBuyserUid();
		}
		this.unitPrice = log.getUnitPrice();
		this.coinAmount = log.getCoinAmount();
		this.totalPrice = log.getTotalPrice();
		this.poundageBTC = log.getPoundageBTC();
		this.createTime = DateFormateUtils.formate(log.getCreateTime());
		this.payType = log.getPayType();
		this.payNumber = log.getId();
		this.payStatus = log.getPayStatus();
		this.buyserUid = log.getBuyserUid();
		this.sellerUid = log.getSellerUid();
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getCoinCode() {
		return coinCode;
	}

	public void setCoinCode(String coinCode) {
		this.coinCode = coinCode;
	}

	public String getCoinName() {
		return coinName;
	}

	public void setCoinName(String coinName) {
		this.coinName = coinName;
	}

	public Integer getTradeType() {
		return tradeType;
	}

	public void setTradeType(Integer tradeType) {
		this.tradeType = tradeType;
	}

	public Integer getTradeUserId() {
		return tradeUserId;
	}

	public void setTradeUserId(Integer tradeUserId) {
		this.tradeUserId = tradeUserId;
	}

	public String getTradeUserName() {
		return tradeUserName;
	}

	public void setTradeUserName(String tradeUserName) {
		this.tradeUserName = tradeUserName;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getCoinAmount() {
		return coinAmount;
	}

	public void setCoinAmount(BigDecimal coinAmount) {
		this.coinAmount = coinAmount;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public BigDecimal getPoundageBTC() {
		return poundageBTC;
	}

	public void setPoundageBTC(BigDecimal poundageBTC) {
		this.poundageBTC = poundageBTC;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public Long getPayNumber() {
		return payNumber;
	}

	public void setPayNumber(Long payNumber) {
		this.payNumber = payNumber;
	}

	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public Integer getComplainPeopleId() {
		return complainPeopleId;
	}

	public void setComplainPeopleId(Integer complainPeopleId) {
		this.complainPeopleId = complainPeopleId;
	}

	public String getComplainPeople() {
		return complainPeople;
	}

	public void setComplainPeople(String complainPeople) {
		this.complainPeople = complainPeople;
	}

	public String getComplainPeopleName() {
		return complainPeopleName;
	}

	public void setComplainPeopleName(String complainPeopleName) {
		this.complainPeopleName = complainPeopleName;
	}

	public Integer getComplainStatus() {
		return complainStatus;
	}

	public void setComplainStatus(Integer complainStatus) {
		this.complainStatus = complainStatus;
	}

	public Integer getHasNewMessage() {
		return hasNewMessage;
	}

	public void setHasNewMessage(Integer hasNewMessage) {
		this.hasNewMessage = hasNewMessage;
	}

	public Integer getBuyserUid() {
		return buyserUid;
	}

	public void setBuyserUid(Integer buyserUid) {
		this.buyserUid = buyserUid;
	}

	public Integer getSellerUid() {
		return sellerUid;
	}

	public void setSellerUid(Integer sellerUid) {
		this.sellerUid = sellerUid;
	}

}
