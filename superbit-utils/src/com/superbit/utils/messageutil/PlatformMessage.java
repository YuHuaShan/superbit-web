package com.superbit.utils.messageutil;

import java.io.Serializable;

public class PlatformMessage implements Serializable{
	private static final long serialVersionUID = -8085483466902536831L;
	public static final int STATUS_UNREAD = 0 ; //未读  
	public static final int STATUS_READED = 1 ; //已读
	public static final int STATUS_DELETE = 2 ; //已删除

	/**
	 * 记录ID
	 */
	private int id;
	/**
	 * 用户ID
	 */
	private int userid;
	/**
	 * 业务类型（英文）
	 */
	private String type;
	/**
	 * 业务类型（中文）
	 */
	private String typename;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 附加信息
	 */
	private String remarkdata;
	/**
	 * 状态
	 */
	private int status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getRemarkdata() {
		return remarkdata;
	}
	public void setRemarkdata(String remarkdata) {
		this.remarkdata = remarkdata;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
	
	
}
