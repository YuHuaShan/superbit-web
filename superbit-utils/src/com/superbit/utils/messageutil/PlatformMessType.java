package com.superbit.utils.messageutil;


public enum PlatformMessType {
		/**
		 * 系统公告
		 */
		TYPE_BROADCAST("broadcast","系统公告"),   
		
	; //上方定义枚举内容
	
	/**
	 * 业务类型（英文）
	 */
	private String type;
	/**
	 * 业务类型（中文）
	 */
	private String typeName;
	
	private  PlatformMessType(String type,String typeName){
		this.type  = type;
		this.typeName = typeName;
	}
	public String getType() {
		return type;
	}
	public String getTypeName() {
		return typeName;
	}
	
}
