package com.superbit.utils.messageutil;

import java.util.List;
import java.util.Map;

public interface PlatformMessageService {
	
	/**发送站内信
	 * @param userid
	 * @param type
	 * @param title
	 * @param content
	 * @param remarkdata
	 */
	void sendPlatformMessage(int userid,String type,String title,String content,Map<String,String> remarkdata);
	
	
	/**获取站内信列表
	 * @param userid
	 * @param type
	 * @param status
	 * @return
	 */
	List<PlatformMessage> listPlatformMessage(int userid,String type,Integer status);
	
	/**获取站内信详细信息， 并标记为已读
	 * @param userid
	 * @param recid
	 * @return
	 */
	PlatformMessage readMessageContent(int userid,int recid);
}
