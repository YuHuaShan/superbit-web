package com.superbit.utils.mobileutil;

import java.io.Serializable;
import java.util.Date;

/**
 * 短信发送记录
 */
public class MobileMessRecord implements Serializable {
	
	private static final long serialVersionUID = -6923084113022894087L;
	
	//记录id
	private long id;
	//手机号
	private String phoneNum;
	//短信发送通道id
	private int smsId;
	//发送状态：1发送中，2发送失败，3发送成功
	private int state;
	//发送时间
	private Date createTime;
	//短信通道返回id，用来查询短信发送状态
	private String returnId;
	//短信通道返回结果
	private String message;
	//验证次数
	private int count;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public int getSmsId() {
		return smsId;
	}
	public void setSmsId(int smsId) {
		this.smsId = smsId;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getReturnId() {
		return returnId;
	}
	public void setReturnId(String returnId) {
		this.returnId = returnId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

}
