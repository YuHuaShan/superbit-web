package com.superbit.utils.mobileutil;

import java.util.List;
import java.util.Map;

/**
 * 短信发送组件
 *
 */
public interface MobileMessService {
	
	/**发送短信
	 * @param phonenum 目标手机号码（国家代码+手机号）
	 * @param moudleName 业务模型名称（验证码code:***）
	 * @param messageData 内容变量
	 * @return 发送状态   0 成功，1电话地址错误，2服务配置错误，3内容变量缺失
	 */
	int sendMessage(final String phonenum,String moudleName, boolean isSync, Map<String,Object> messageData);
	
	/**
	 * 获取短信发送记录数量
	 * @param phonenum
	 * @param smsId
	 * @param state
	 * @return
	 */
	int countSmsMessRecord(String phonenum,Integer smsId,Integer state);
	
	/**
	 * 获取短信发送记录
	 * @param phonenum
	 * @param smsId
	 * @param state
	 * @param start
	 * @param size
	 * @return
	 */
	List<MobileMessRecord> getSmsMessRecords(String phonenum,Integer smsId,Integer state,int start ,int size);
	
	/**
	 * 短信注册处理器
	 * @param smsService
	 */
	void addSmsService(int smsId, SmsService smsService);
}
