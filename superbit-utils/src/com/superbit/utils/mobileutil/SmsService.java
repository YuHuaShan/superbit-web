package com.superbit.utils.mobileutil;

import java.util.Map;

/**
 * 短信接口
 * @author zhangxu
 *
 */
public interface SmsService {
	
	/**发送短信
	 * @param phonenum 目标手机号码（国家代码+手机号）
	 * @param moudleName 业务模型名称（验证码code:***）
	 * @param messageData 内容变量
	 */
	Map<String,String> smsSend(String phonenum, String moudleName, Map<String, Object> messageData);
}
