package com.superbit.utils.mailutil;

import java.util.Map;

/**
 * 邮件发送组件
 *
 */
public interface EmailSendService {
	/**发送邮件
	 * @param address 目标邮件地址
	 * @param moudleName 业务模型名称
	 * @param messageData 内容变量
	 */
	void sendMessage(String address,String moudleName,boolean isSync,Map<String,Object> messageData)throws EmailFailException;
	
	
	/**发送邮件
	 * @param address 目标邮件地址
	 * @param moudleName 业务模型名称
	 * @param messageData 内容变量
	 * @param callBack 回掉函数
	 */
	void sendMessage(String address,String moudleName,boolean isSync,Map<String,Object> messageData,CallBack callBack)throws EmailFailException;
	
}
