package com.superbit.utils.dbutil;


/**
 * @function Redis的Key的工具类
 * @author Wangzhenwei
 * @version 2018-01-06 14:32
 */
public class RedisKeyUtils {
	
	/**
	 * @function 获取手机注册的临时信息的Key
	 * @param phone	手机号
	 * @return
	 */
	public static String getPhoneRegisterTempInfoKey(String phone) {
		/*
		 * 返回值格式：$phoneRegister+{phone}
		 */
		return String.format("$register+%s", phone);
	}
	
	/**
	 * @function 手机注册的临时信息存放一小时
	 * @return
	 */
	public static long getPhoneRegisterTempInfoTime() {
		return 1*60*60;
	}
	
	/**
	 * 手机、邮件验证码有效时间10分钟
	 * @return
	 */
	public static long getCaptchaValidTime() {
		return 1*10*60;
	}
	
	/**
	 * 获取邮箱注册的临时信息的Key
	 * @param email	邮箱
	 * @return
	 */
	public static String getEmailRegisterTempInfoKey(String email) {
		/*
		 * %s是字符串转换符
		 * 返回值格式：$emialRegister@{email}
		 */
		return String.format("$emailregister@%s", email);
	}
	
	/**
	 * 邮箱注册的临时信息存放一小时
	 * @return
	 */
	public static long getEmailRegisterTempInfoTime() {
		return 1*60*60;
	}
	
	/**
	 * 密码输错记录的Key
	 * @param userId	用户Id
	 * @return
	 */
	public static String getPasswordErrorKey(int userId) {
		/*
		 * 返回值格式：$passwordError{userId}
		 */
		return String.format("$passwordError%d", userId);
	}
	
	public static String getLockUsersKey() {
		/*
		 * 返回值格式：$lockUsers
		 */
		return "$lockUsers";
	}
	
	/**
	 * @function 锁定用户2小时
	 * @return
	 */
	public static long lockUserTime() {
		return 2*60*60*1000;
	}
	
	/**
	 * 获取修改登陆密码的临时信息的Key
	 * @param userId	用户Id
	 * @return
	 */
	public static String getRePasswordTempInfoKey(int userId) {
		/*
		 * 返回值格式：$rePassword{userId}
		 */
		return String.format("$rePassword%d", userId);
	}
	
	/**
	 * 修改登陆密码的临时信息存放一小时
	 * @return
	 */
	public static long getRePasswordTempInfoTime() {
		return 1*60*60;
	}
	
	/**
	 * 获取修改资金密码的临时信息的Key
	 * @param userId	用户Id
	 * @return
	 */
	public static String getReFundPasswordTempInfoKey(int userId) {
		/*
		 * 返回值格式：$reFundPassword{userId}
		 */
		return String.format("$reFundPassword%d", userId);
	}
	
	/**
	 * 修改资金密码的临时信息存放一小时
	 * @return
	 */
	public static long getReFundPasswordTempInfoTime() {
		return 1*60*60;
	}

	/**
	 * 修改资金密码，冻结一天
	 * @return
	 */
	public static long getFreezeTime() {
		return 24*60*60*1000;
	}

	/**
	 * 获取绑定手机的临时信息的Key
	 */
	public static String getBindPhoneKey(int userId) {
		/*
		 * 返回值格式：$bindPhone{userId}
		 */
		return String.format("$bindPhone%d", userId);
	}
	
	/**
	 * 绑定手机的临时信息存放一小时
	 * @return
	 */
	public static long getBindPhoneTempInfoTime() {
		return 1*60*60;
	}
	
	/**
	 * 获取换绑手机时旧手机的临时信息的Key
	 * @param userId	用户Id
	 * @return
	 */
	public static String getOldPhoneTempInfoKey(int userId) {
		/*
		 * 返回值格式：$oldPhone{userId}
		 */
		return String.format("$oldPhone%d", userId);
	}
	
	/**
	 * 换绑手机时旧手机的临时信息存放一小时
	 * @return
	 */
	public static long getOldPhoneTempInfoTime() {
		return 1*60*60;
	}
	
	/**
	 * 获取换绑手机时新手机的临时信息的Key
	 * @param userId	用户Id
	 * @return
	 */
	public static String getNewPhoneTempInfoKey(int userId) {
		/*
		 * 返回值格式：$newPhone{userId}
		 */
		return String.format("$newPhone%d", userId);
	}
	
	/**
	 * 换绑手机时旧手机的临时信息存放一小时
	 * @return
	 */
	public static long getNewPhoneTempInfoTime() {
		return 1*60*60;
	}
	
	/**
	 * 获取绑定邮箱的临时信息的Key
	 * @param userId	用户Id
	 * @return
	 */
	public static String getBindEmailTempInfoKey(int userId) {
		/*
		 * 返回值格式：@bindEmail{userId}
		 */
		return String.format("@bindEmail%d", userId);
	}

	/**
	 * 绑定邮箱的临时信息存放一小时
	 * @return
	 */
	public static long getBindEmailTempInfoTime() {
		return 1*60*60;
	}
	
	/**
	 * 获取临时谷歌私钥
	 * @param userId	用户Id
	 * @return
	 */
	public static String getGooglePrivateKey(int userId) {
		/*
		 * 返回值格式：$googlePrivateKey{userId}
		 */
		return String.format("$googlePrivateKey%d", userId);
	}
	
	/**
	 * 临时谷歌私钥存放一小时
	 * @return
	 */
	public static long getGooglePrivateKeyTime() {
		return 1*60*60;
	}
	
	/**
	 * 获取修改安全等级解绑手机时的临时信息的Key
	 * @param userId	用户Id
	 * @return
	 */
	public static String getChangeSecurityLevelTempInfoKey(int userId) {
		/*
		 * 返回值格式：$changeSecurityLevel{userId}
		 */
		return String.format("$changeSecurityLevel%d", userId);
	}
	
	/**
	 * 修改安全等级解绑手机时的临时信息存放一小时
	 * @return
	 */
	public static long getChangeSecurityLevelTempInfoTime() {
		return 1*60*60;
	}
	
	/**
	 * 获取校验安全等级的临时信息的Key
	 * @param userId	用户Id
	 * @return
	 */
	public static String getCheckSecurityLevelTempInfoKey(int userId) {
		/*
		 * 返回值格式：$checkSecurityLevel{userId}
		 */
		return String.format("$checkSecurityLevel%d", userId);
	}
	
	/**
	 * 校验安全等级的临时信息存放一小时
	 * @return
	 */
	public static long getCheckSecurityLevelTempInfoTime() {
		return 1*60*60;
	}
	
	/**
	 * 获取修改登录认证解绑手机的临时信息的Key
	 * @param userId	用户Id
	 * @return
	 */
	public static String getChangeLoginAuthenticationTempInfoKey(int userId) {
		/*
		 * 返回值格式：$changeLoginAuthentication{userId}
		 */
		return String.format("$changeLoginAuthentication%d", userId);
	}
	
	/**
	 * 修改登录认证解绑手机的临时信息存放一小时
	 * @return
	 */
	public static long getChangeLoginAuthenticationTempInfoTime() {
		return 1*60*60;
	}
	
	/**
	 * 获取校验登录认证的临时信息的Key
	 * @param userId	用户Id
	 * @return
	 */
	public static String getCheckLoginAuthenticationTempInfoKey(int userId) {
		/*
		 * 返回值格式：$checkLoginAuthenticationTempInfo{userId}
		 */
		return String.format("$checkLoginAuthentication%d", userId);
	}
	
	/**
	 * 获取校验登录认证的临时信息存放一小时
	 * @return
	 */
	public static long getCheckLoginAuthenticationTempInfoTime() {
		return 1*60*60;
	}
	
	/**
	 * 获取创建API的临时信息的Key
	 * @param userId	用户Id
	 * @return
	 */
	public static String getCreateAPITempInfoKey(int userId) {
		/*
		 * 返回值格式：$createAPI{uesrId}
		 */
		return String.format("$createAPI%d", userId);
	}
	
	/**
	 * 创建API的临时信息存放一小时
	 * @return
	 */
	public static long getCreateAPITempInfoTime() {
		return 1*60*60;
	}
	
	/**
	 * 获取查看用户API详细信息的临时信息的Key
	 * @param userId	用户Id
	 * @return
	 */
	public static String getUserAPIInfoDetailTempInfoKey(int userId) {
		/*
		 * 返回值格式：$userAPIInfoDetail{userId}
		 */
		return String.format("$userAPIInfoDetail%d", userId);
	}
	
	/**
	 * 查看用户API详细信息的临时信息存放一小时
	 * @return
	 */
	public static long getUserAPIInfoDetailTempInfoTime() {
		return 1*60*60;
	}
	
	/**
	 * 获取用户系统配置的Key
	 * @param userId	用户Id
	 * @return
	 */
	public static String getAppConfigKey(String key) {
		/*
		 * 返回值格式：$appConfigKey{key}
		 */
		return String.format("$appConfigKey%s", key);
	}
	
	/**
	 * 获取对外服务手机的临时信息的Key
	 * @param userId	用户Id
	 * @param systemId	系统Id
	 * @param moduleId	模板Id
	 * @return
	 */
	public static String getOutwardPhoneTempInfoKey(int userId, int systemId, int moduleId) {
		/*
		 * 返回值格式：$outwardPhone{userId+systemId+moduleId}
		 */
		return String.format("$outwardPhone%s", userId + "+" + systemId + "+" + moduleId);
	}
	
	/**
	 * 对外服务手机的临时信息存放一小时
	 * @return
	 */
	public static long getOutwardPhoneTempInfoTime() {
		return 1*60*60;
	}
	
	/**
	 * 单点登录的临时信息的Key
	 * @param userId		用户Id
	 * @param systemCode	系统代号
	 * @return
	 */
	public static String getSSOTempInfoKey(int userId, String systemCode) {
		/*
		 * 返回值格式：$SSO{userId+systemCode}
		 */
		return String.format("$SSO%s", userId + "+" + systemCode);
	}
	
	/**
	 * 单点登录的临时信息存放一分钟
	 * @return
	 */
	public static long getSSOTempInfoTime() {
		return 1*60;
	}

	/**
	 * 获取手机登录密码重置的临时信息的Key
	 * @param phone
	 * @return
	 */
	public static String getPhoneResetTempInfoKey(String phone) {
		return String.format("$reset+%s", phone);
	}

	/**
	 * 获取邮箱登录密码重置的临时信息的Key
	 * @param email
	 * @return
	 */
	public static String getEmailResetTempInfoKey(String email) {
		return String.format("$emailreset@%s", email);
	}

	/**
	 * 手机登录重置密码的临时信息存放一小时
	 * @return
	 */
	public static long getPhoneResetTempInfoTime() {
		return 1*60*60;
	}

	/**
	 * 邮箱登录重置密码的临时信息存放一小时
	 * @return
	 */
	public static long getEmailResetTempInfoTime() {
		return 1*60*60;
	}
	
}
