package com.superbit.utils.date;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.util.StringUtils;

/**
 * 日期格式化工具类
 * 
 * @author zhangdaoguang
 *
 */
public class DateFormateUtils {
	public static String formate(Timestamp time) {
		return formate(time, "yyyy/MM/dd HH:mm:ss");
	}

	public static String formate(Timestamp time, String partten) {
		if (time == null || StringUtils.isEmpty(partten)) {
			return null;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(partten);
		String formateTime = sdf.format(time);
		return formateTime;
	}

	public static String formate(Date time, String partten) {
		SimpleDateFormat sdf = new SimpleDateFormat(partten);
		String formateTime = sdf.format(time);
		return formateTime;
	}

	public static String getYesterday() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		Date time = cal.getTime();
		return formate(time, "yyyy-MM-dd 00:00:00");
	}

	public static String getToday() {
		return formate(new Date(), "yyyy-MM-dd 00:00:00");
	}
}
