package com.superbit.utils.constantutil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月8日 下午6:24:10  
  * @descrip 获取固定时间的工具类
  */
public class FixedTime {

	/*
	 * 获取指定格式的当前时间
	 */
	public static Date getCurrentTime(){
	      Date date = new Date();//获得系统时间.
          SimpleDateFormat sdf =   new SimpleDateFormat( " yyyy-MM-dd HH:mm:ss " );
          String nowTime = sdf.format(date);
          Date time = null;
		try {
			time = sdf.parse( nowTime );
		} catch (ParseException e) {
			System.err.print("时间格式转换异常！");
			e.printStackTrace();
		}
       return time;
	}
	
	/*
	 * 获取指定格式的24小时之后时间
	 */
	public static Date getTomorrow(){
		
	     SimpleDateFormat sdf =   new SimpleDateFormat( " yyyy-MM-dd HH:mm:ss " );
	     Date today = new Date();
	        
	     Calendar c = Calendar.getInstance();
	     c.setTime(today);
	     c.add(Calendar.DAY_OF_MONTH, 1);// 今天+1天

	     Date tomorrow = c.getTime();
	     Date t= null;
       try {
		t= sdf.parse(sdf.format(tomorrow));
	} catch (ParseException e) {
		System.err.print("时间格式转换异常！");
		e.printStackTrace();
	}
       return t;
	}
}
