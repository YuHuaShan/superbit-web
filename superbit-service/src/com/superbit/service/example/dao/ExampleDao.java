//package com.superbit.service.example.dao;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//
//import com.superbit.core.entry.QuoteOrderRecord;
//import com.superbit.utils.dbutil.CommonDao;
//
//@Repository
//public class ExampleDao {
//	
//	@Autowired
//	private CommonDao commonDao;
//
//	public QuoteOrderRecord getExampleRecord(int id){
//		QuoteOrderRecord qor = commonDao.getEntry(id, QuoteOrderRecord.class);
//		return qor;
//	}
//}
