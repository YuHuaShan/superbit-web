//package com.superbit.service.example.impl;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.superbit.core.entry.QuoteOrderRecord;
//import com.superbit.service.example.dao.ExampleDao;
//import com.superbit.service.example.define.ExampleService;
//@Service("exampleService")
//public class ExampleServiceImpl implements ExampleService{
//
//	@Autowired
//	private ExampleDao exampleDao;
//
//	@Override
//	public QuoteOrderRecord getExampleRecord(int id) {
//		return exampleDao.getExampleRecord(id);
//	}
//}
