package com.superbit.service.coin.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.superbit.core.entry.Coin_Base_Info;
import com.superbit.service.coin.dao.Coin_Base_InfoDao;
import com.superbit.service.coin.define.Coin_Base_InfoService;


@Service
public class Coin_Base_InfoServiceImpl implements Coin_Base_InfoService{
	
	@Autowired
	private Coin_Base_InfoDao coin_Base_InfoDao;
	@Override
	public boolean addCoin_Base_Info(Coin_Base_Info cbi) {
		// TODO Auto-generated method stub
		return coin_Base_InfoDao.addCoin_Base_Info(cbi);
	}

	@Override
	public boolean deleteCoin_Base_Info(Coin_Base_Info cbi) {
		// TODO Auto-generated method stub
		return coin_Base_InfoDao.deleteCoin_Base_Info(cbi);
	}

	@Override
	public boolean updateCoin_Base_Info(Coin_Base_Info cbi) {
		// TODO Auto-generated method stub
		return coin_Base_InfoDao.updateCoin_Base_Info(cbi);
	}

	@Override
	public List<Coin_Base_Info> getCoin_Base_InfoList(int flag, String coinId, String coinCode, String coinName) {
		// TODO Auto-generated method stub
		return coin_Base_InfoDao.getCoin_Base_InfoList(flag, coinId, coinCode, coinName);
	}

	@Override
	public boolean verifyCoin_Base_Info(String key, String value) {
		// TODO Auto-generated method stub
		return coin_Base_InfoDao.verifyCoin_Base_Info(key, value);
	}

	
	
}
