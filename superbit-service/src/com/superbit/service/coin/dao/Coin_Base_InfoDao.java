package com.superbit.service.coin.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.superbit.core.entry.Coin_Base_Info;
import com.superbit.utils.dbutil.CommonDao;

@Repository
public class Coin_Base_InfoDao {
	private static Logger logger = LoggerFactory.getLogger(Coin_Base_InfoDao.class);
	
	@Autowired
	private CommonDao commonDao;
	/**
	 * 添加币种
	 * @param cbi
	 * @return
	 */
	public boolean addCoin_Base_Info(Coin_Base_Info cbi){
		logger.info("addCoin_Base_Info");
		Serializable result = commonDao.saveEntry(cbi);
		int flag = Integer.valueOf(result.toString());
		if(flag>0) {
			return true;
		}else {
			return false;
		}
		
	}
	/**
	 * 删除币种
	 * @param cbi
	 * @return
	 */
	public boolean deleteCoin_Base_Info(Coin_Base_Info cbi) {
		// TODO Auto-generated method stub
		try {
			commonDao.deleteEntry(Coin_Base_Info.class, cbi.getId());
			return true;
		}catch(Exception e){
			return false;
		}
	}
	/**
	 * 修改币种信息
	 * @param cbi
	 * @return
	 */
	public boolean updateCoin_Base_Info(Coin_Base_Info cbi) {
		// TODO Auto-generated method stub
		try {
			commonDao.updateEntry(cbi);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	/**
	 * 获取币种信息
	 * @param flag //0:获取未生效币种;1:获取生效币种;2:获取所有币种;3:获取指定币种
	 * 	   flag为0时获取未生效币种信息
	 *     flag为1时获取生效币种信息
	 *     flag为2时获取全部币种信息
	 *     flag为3时获取指定coinId币种信息
	 * @param coinId
	 * @param coinCode
	 * @param coinName
	 * @return
	 */
	public List<Coin_Base_Info> getCoin_Base_InfoList(int flag, String coinId, String coinCode, String coinName) {
		// TODO Auto-generated method stub
		List<Criterion> conds = new ArrayList<Criterion>();
		
		Order[] orders = new Order[]{Order.desc("coinUpdateTime")};
		if(flag == 0) {//获取未生效币种
			conds.add(Restrictions.eq("coinStatus", flag));
		}else if(flag ==1 ){//获取生效币种
			conds.add(Restrictions.eq("coinStatus", flag));
		}else if(flag ==2) {//获取所有币种
			
		}else if(flag==3) {//获取指定币种
			Map<String, Object> map = new HashMap<String,Object>();
			if(null!=coinId) {
				map.put("coinId", coinId);
			}
			if(null!=coinCode) {
				map.put("coinCode", coinCode);
			}
			if(null!=coinName) {
				map.put("coinName", coinName);
			}
			return commonDao.getEntrysByField(Coin_Base_Info.class, map, new String[] {"coinUpdateTime"});
		}
		return commonDao.getEntrysByCond(Coin_Base_Info.class, conds.toArray(new Criterion[0]), orders);
	}
	/**
	 * 验证币种参数是否合法（coinCode与coinName是否唯一）
	 * @param key coinCode/coinName
	 * @param value xxx/xxx
	 * @return true 已存在
	 */
	public boolean verifyCoin_Base_Info(String key, String value) {
		// TODO Auto-generated method stub
		List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.eq(key, value));
		
		int i = commonDao.getCount(Coin_Base_Info.class, conds.toArray(new Criterion[0]) );
		if (i>0) {
			return true;
		}
		return false;
	}

}
