package com.superbit.service.coin.define;

import java.util.List;

import com.superbit.core.entry.Coin_Base_Info;

/**
 * 币种业务层
 * @author hongzhen
 *
 */
public interface Coin_Base_InfoService {
	/**
	 * 新增币种
	 * @param cbi
	 * @return
	 */
	boolean addCoin_Base_Info(Coin_Base_Info cbi);
	/**
	 * 验证币种参数是否合法（coinCode与coinName是否唯一）
	 * @param key coinCode/coinName
	 * @param value xxx/xxx
	 * @return true 已存在
	 */
	boolean verifyCoin_Base_Info(String key, String value);
	/**
	 * 删除币种
	 * @param cbi
	 * @return
	 */
	boolean deleteCoin_Base_Info(Coin_Base_Info cbi);
	/**
	 * 币种生效
	 * @param cbi
	 * @return
	 */
	boolean updateCoin_Base_Info(Coin_Base_Info cbi);
	/**
	 * 获取币种信息
	 * @param flag //0:获取未生效币种;1:获取生效币种;2:获取所有币种;3:获取指定币种
	 * 	   flag为0时获取未生效币种信息
	 *     flag为1时获取生效币种信息
	 *     flag为2时获取全部币种信息
	 *     flag为3时获取指定coinId币种信息
	 * @param coinId
	 * @param coinCode
	 * @param coinName
	 * @return
	 */
	List<Coin_Base_Info> getCoin_Base_InfoList(int flag, String coinId, String coinCode, String coinName);
	
	
}
