package com.superbit.service.withdraw.addr.define;

import com.superbit.core.entry.User_Withdraw_Addr;
/**
 * 用户资产提现地址业务层
 * @author hongzhen
 *
 */
public interface User_Withdraw_AddrService {
	/**
	 * 添加提现地址
	 * @param uwa
	 * @return
	 */
	boolean addUser_Withdraw_Addr(User_Withdraw_Addr uwa);
	/**
	 * 修改提现地址
	 * @param uwa
	 * @return
	 */
	boolean updateUser_Withdraw_Addr(User_Withdraw_Addr uwa);
	/**
	 * 删除提现地址
	 * @param uwa
	 * @return
	 */
	boolean deleteUser_Withdraw_Addr(User_Withdraw_Addr uwa);
	/**
	 * 获取提现地址
	 * @param uwa
	 * @return
	 */
	User_Withdraw_Addr getUser_Withdraw_Addr(String userId, String coinId);
}
