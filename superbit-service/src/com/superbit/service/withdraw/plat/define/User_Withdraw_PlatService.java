package com.superbit.service.withdraw.plat.define;

import java.util.List;

import com.superbit.core.entry.User_Withdraw_Plat;

/**
 * 用户自定义平台业务层
 * @author hongzhen
 *
 */
public interface User_Withdraw_PlatService {
	/**
	 * 添加用户自定义平台
	 * @param uwp
	 * @return
	 */
	boolean addUser_Withdraw_Plat(User_Withdraw_Plat uwp);
	/**
	 * 删除用户自定义平台
	 * @param platformId
	 * @return
	 */
	boolean deleteUser_Withdraw_Plat(String platformId);
	/**
	 * 获取用户自定义平台列表
	 * @param userId
	 * @param assetId
	 * @return
	 */
	List<User_Withdraw_Plat> getUser_Withdraw_PlatList(int userId, String assetId);
}
