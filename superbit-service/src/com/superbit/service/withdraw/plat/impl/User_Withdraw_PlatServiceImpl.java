package com.superbit.service.withdraw.plat.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superbit.core.entry.User_Withdraw_Plat;
import com.superbit.service.withdraw.plat.dao.User_Withdraw_PlatDao;
import com.superbit.service.withdraw.plat.define.User_Withdraw_PlatService;
@Service
public class User_Withdraw_PlatServiceImpl implements User_Withdraw_PlatService {
	@Autowired
	private User_Withdraw_PlatDao user_Withdraw_PlatDao;
	@Override
	public boolean addUser_Withdraw_Plat(User_Withdraw_Plat uwp) {
		// TODO Auto-generated method stub
		return user_Withdraw_PlatDao.addUser_Withdraw_Plat(uwp);
	}

	@Override
	public boolean deleteUser_Withdraw_Plat(String platformId) {
		// TODO Auto-generated method stub
		return user_Withdraw_PlatDao.deleteUser_Withdraw_Plat(platformId);
	}

	@Override
	public List<User_Withdraw_Plat> getUser_Withdraw_PlatList(int userId, String assetId) {
		// TODO Auto-generated method stub
		return user_Withdraw_PlatDao.getUser_Withdraw_PlatList(userId, assetId);
	}

}
