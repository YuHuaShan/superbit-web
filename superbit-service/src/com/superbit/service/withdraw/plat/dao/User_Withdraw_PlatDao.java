package com.superbit.service.withdraw.plat.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.superbit.core.entry.User_Withdraw_Plat;
import com.superbit.utils.dbutil.CommonDao;
@Repository
public class User_Withdraw_PlatDao {
	private static Logger logger = LoggerFactory.getLogger(User_Withdraw_PlatDao.class);
	
	@Autowired
	private CommonDao commonDao;
	public boolean addUser_Withdraw_Plat(User_Withdraw_Plat uwp) {
		// TODO Auto-generated method stub
		logger.info("addUser_Withdraw_Plat");
		Serializable result = commonDao.saveEntry(uwp);
		int i = Integer.valueOf(result.toString());
		if(i>0) {
			return true;
		}else {
			return false;
		}
		
	}

	public boolean deleteUser_Withdraw_Plat(String platformId) {
		// TODO Auto-generated method stub
		try {
			logger.info("deleteUser_Withdraw_Plat");
			commonDao.deleteEntrys(User_Withdraw_Plat.class, "platformId='"+platformId+"'");
		}catch (Exception e) {
			// TODO: handle exception
			logger.error(e.toString());
			return false;
		}
		return true;
	}

	public List<User_Withdraw_Plat> getUser_Withdraw_PlatList(int userId, String assetId) {
		// TODO Auto-generated method stub
		logger.info("getUser_Withdraw_PlatList");
		List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.eq("userId", userId));
		conds.add(Restrictions.eq("assetId", assetId));
		Order[] orders = new Order[]{Order.desc("createTime")};
		return commonDao.getEntrysByCond(User_Withdraw_Plat.class, conds.toArray(new Criterion[0]), orders);
	}

}
