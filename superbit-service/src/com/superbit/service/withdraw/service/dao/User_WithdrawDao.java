package com.superbit.service.withdraw.service.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.superbit.core.entry.User_Withdraw;
import com.superbit.utils.dbutil.CommonDao;

@Repository
public class User_WithdrawDao {
private static Logger logger = LoggerFactory.getLogger(User_WithdrawDao.class);
	
	@Autowired
	private CommonDao commonDao;
	public Map<String, String> getUser_WithDraw_Info() {
		// TODO Auto-generated method stub
		return null;
	}


	/**
	 * 获取用户提现记录
	 * @param id
	 * @return
	 */
	public User_Withdraw getUser_Withdraw(int id) {
		// TODO Auto-generated method stub
		return commonDao.getEntry(id, User_Withdraw.class);
	}
	/**
	 * 更新用户提现记录
	 * @param uw
	 * @return
	 */
	public boolean updateUser_Withdraw(User_Withdraw uw) {
		// TODO Auto-generated method stub
		try {
			commonDao.updateEntry(uw);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	public boolean operationUser_Withdraw(int userId, String coinId, String status) {
		// TODO Auto-generated method stub
		return false;
	}
	/**
	 * 保存用户提现记录
	 * @param uw
	 * @return
	 */
	public boolean saveUser_Withdraw(User_Withdraw uw) {
		// TODO Auto-generated method stub
		logger.info("saveUser_Withdraw");
		int i = Integer.valueOf(commonDao.saveEntry(uw).toString());
		if(i>0) {
			return true;
		}
		return false;
	}
	/**
	 * 获取用户提现次数
	 * @param userId
	 * @param coinId
	 * @param status
	 * @param stime
	 * @param etime
	 * @return
	 */
	public int getUser_WithdrawCount(int userId, String assetId, String coinId, String coinCode, int status, Date stime, Date etime) {
		// TODO Auto-generated method stub
		List<Criterion> conds = new ArrayList<Criterion>();
		if(userId>0) {
			conds.add(Restrictions.eq("userId", userId));
		}
		
		if(null!=assetId) {
			conds.add(Restrictions.eq("assetId", assetId));
		}
		
		if(null!=coinId) {
			conds.add(Restrictions.eq("coinId", coinId));
		}	
		
		if(null!=coinCode) {
			conds.add(Restrictions.eq("coinCode", coinCode));
		}
		if(status>=0) {
			conds.add(Restrictions.eq("withDrawStatus", status));
		}
		
		if(null!=stime) {
			conds.add(Restrictions.ge("stime", stime));
		}
		if(null!=etime) {
			conds.add(Restrictions.le("etime", etime));
		}
		return commonDao.getCount(User_Withdraw.class, conds);
	}
	/**
	 * 获取用户提现内容
	 * @param userId
	 * @param coinId
	 * @param status
	 * @param stime
	 * @param etime
	 * @param start
	 * @param end
	 * @param order
	 * @return
	 */
	public List<User_Withdraw> getUser_Withdraws(int userId, String assetId, String coinId, String coinCode, int status, Date stime, Date etime, int start, int end, String order) {
		// TODO Auto-generated method stub
		
		List<Criterion> conds = new ArrayList<Criterion>();
		if(userId>0) {
			conds.add(Restrictions.eq("userId", userId));
		}
		
		if(null!=assetId) {
			conds.add(Restrictions.eq("assetId", assetId));
		}
		
		if(null!=coinId) {
			conds.add(Restrictions.eq("coinId", coinId));
		}	
		
		if(null!=coinCode) {
			conds.add(Restrictions.eq("coinCode", coinCode));
		}
		
		if(status>=0) {
			conds.add(Restrictions.eq("withDrawStatus", status));
		}
		
		if(null!=stime) {
			conds.add(Restrictions.ge("createTime", stime));
		}
		if(null!=etime) {
			conds.add(Restrictions.le("createTime", etime));
		}


		Order[] orders = new Order[]{Order.desc(null!=order?order:"updateTime")};
		
		if(start<0) {
			return commonDao.getEntrysByCond(User_Withdraw.class, conds.toArray(new Criterion[0]), orders);
		}
		return commonDao.getEntrysByCond(User_Withdraw.class, conds.toArray(new Criterion[0]), orders, start, end);
		
	}
	
}
