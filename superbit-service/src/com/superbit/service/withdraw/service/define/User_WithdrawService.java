package com.superbit.service.withdraw.service.define;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.superbit.core.entry.User_Withdraw;
/**
 * 用户资产提现业务层
 * @author hongzhen
 *
 */
public interface User_WithdrawService {
	/**
	 * 返回用户提现所需验证信息，短信验证，谷歌验证，手续费等
	 * @return
	 */
	Map<String, String> getUser_WithDraw_Info();
	
	/**
	 * 通过id获取用户提现记录
	 * @param id
	 * @return
	 */
	User_Withdraw getUser_Withdraw(int id);
	/**
	 * 提现审核
	 * 审核用户提现
	 * @param userId
	 * @param assetId
	 * @param coinId
	 * @param status
	 * @return
	 */
	boolean updateUser_Withdraw(User_Withdraw uw);
	
	/**
	 * 提现操作
	 * 针对审核通过提现记录进行转账
	 * @param userId
	 * @param coinId
	 * @param status
	 * @return
	 */
	boolean operationUser_Withdraw(int userId, String coinId, String status);
	/**
	 * 用户提现记录
	 * @param uw
	 * @return
	 */
	boolean saveUser_Withdraw(User_Withdraw uw);
	/**
	 * 获取用户提现条数，根据参数进行查询，
	 *     userId为空时查询所有用户，
	 *     coinId为空时查询所有币种，
	 *     status为空时查询所有提现状态数据，
	 *     stime,etime为查询时间范围，为空时查询所有时间范围内数据
	 * @param userId
	 * @param coinId
	 * @param status
	 * @param stime
	 * @param etime
	 * @return
	 */
	int getUser_WithdrawCount(int userId, String coinId, int status, Date stime, Date etime);
	/**
	 * 返回用户提现记录，根据参数进行查询
	 *     userId为空时查询所有用户，
	 *     coinId为空时查询所有币种，
	 *     status为空时查询所有提现状态数据，
	 *     stime,etime为查询时间范围，为空时查询所有时间范围内数据
	 *     star,end为分页使用
	 * @param userId
	 * @param coinId
	 * @param status
	 * @param stime
	 * @param etime
	 * @param start
	 * @param end
	 * @return
	 */
	List<User_Withdraw> getUser_Withdraws(int userId, String coinId, int status, Date stime, Date etime, int start, int end, String order);
	/**
	 * 获取今日已提现额度
	 * @param userId
	 * @param assetId  //assetId，coinId，coinCode三个条件并行，按顺序进行判端使用。存在assetid则不使用coinId，coinCode；存在coinId则不使用coinCode
	 * @param coinId
	 * @param coinCode
	 * @return
	 */
	BigDecimal getWithDrawsLimit(int userId, String assetId, String coinId, String coinCode);
}
