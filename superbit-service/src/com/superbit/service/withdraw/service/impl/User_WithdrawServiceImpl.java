package com.superbit.service.withdraw.service.impl;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superbit.core.entry.User_Withdraw;
import com.superbit.service.withdraw.service.dao.User_WithdrawDao;
import com.superbit.service.withdraw.service.define.User_WithdrawService;
@Service
public class User_WithdrawServiceImpl implements User_WithdrawService {
	@Autowired
	private User_WithdrawDao user_WithdrawDao;
	@Override
	public Map<String, String> getUser_WithDraw_Info() {
		// TODO Auto-generated method stub
		return user_WithdrawDao.getUser_WithDraw_Info();
	}

	@Override
	public User_Withdraw getUser_Withdraw(int id) {
		// TODO Auto-generated method stub
		return user_WithdrawDao.getUser_Withdraw(id);
	}

	@Override
	public boolean updateUser_Withdraw(User_Withdraw uw){
		// TODO Auto-generated method stub
		return user_WithdrawDao.updateUser_Withdraw(uw) ;
	}

	@Override
	public boolean operationUser_Withdraw(int userId, String coinId, String status) {
		// TODO Auto-generated method stub
		return user_WithdrawDao.operationUser_Withdraw(userId, coinId, status);
	}

	@Override
	public boolean saveUser_Withdraw(User_Withdraw uw) {
		// TODO Auto-generated method stub
		return user_WithdrawDao.saveUser_Withdraw(uw);
	}

	@Override
	public int getUser_WithdrawCount(int userId, String coinId, int status, Date stime, Date etime) {
		// TODO Auto-generated method stub
		return user_WithdrawDao.getUser_WithdrawCount(userId, null, coinId, null, status, stime, etime);
	}
	@Override
	public List<User_Withdraw> getUser_Withdraws(int userId, String coinId, int status, Date stime, Date etime, int start, int end, String order) {
		// TODO Auto-generated method stub
		return user_WithdrawDao.getUser_Withdraws(userId, null, coinId, null, status, stime, etime, start, end, order);
	}

	@Override
	public BigDecimal getWithDrawsLimit(int userId, String assetId, String coinId, String coinCode) {
		// TODO Auto-generated method stub
		Calendar c = Calendar.getInstance();      
        c.set(Calendar.HOUR_OF_DAY, 0);  
        c.set(Calendar.MINUTE, 0);  
        c.set(Calendar.SECOND, 0);  
        c.set(Calendar.MILLISECOND, 0);  
        Date stime = c.getTime();
        c.add(Calendar.DAY_OF_YEAR, 1);
        Date etime = c.getTime();
		List<User_Withdraw> user_Withdraws = user_WithdrawDao.getUser_Withdraws(userId, assetId, coinId, coinCode, -1, stime, etime, -1, -1, null);
		BigDecimal bigDecimal = new BigDecimal("0");
		for(User_Withdraw uw:user_Withdraws) {
			bigDecimal = bigDecimal.add(uw.getWithDrawAmount());
		}
		return bigDecimal;
	}


}
