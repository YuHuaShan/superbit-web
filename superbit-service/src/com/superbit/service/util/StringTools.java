package com.superbit.service.util;



import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



/**
 * 字符串操作工具类
 * @author 刘鑫
 *
 * @version 2017年3月25日 下午1:59:26
 */
public class StringTools {
	
	/**
	 * 根据pattern对模板字符串进行解析，
	 * 将模板字符串中的宏进行有效提取，
	 * 方便后续替换操作
	 * @param modleString
	 * @param pattern
	 * @return
	 * @throws Exception
	 */
	public static List<String> getMacro(String modleString,String pattern) throws Exception{
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(modleString);
		List<String> subValue = new ArrayList<String>();
		while(m.find()){
			String subPattern = m.group();
			subValue.add(subPattern.replaceAll("\\$", "").replaceAll("\\{", "").replaceAll("\\}", ""));
		}
		return subValue;
	}
	
	public static String getHTMLDom(String modleString,String pattern){
		Pattern p = Pattern.compile(pattern,Pattern.CANON_EQ);
		Matcher m = p.matcher(modleString);
		String dom = "";
		while(m.find()){
			dom = m.group();
		}
		return dom.replaceAll("<.*?>", "");
		
	}
	
	/**
	 * 替换邮件模板
	 * @param content
	 * @param pattern
	 * @param request
	 * @return
	 */
	public static String setLocalCtx(String content,String pattern,String ctx){
		content = content.replaceAll(pattern, ctx);
		return content;
	}
	
	/*public static String getLocalCtx(HttpServletRequest request){
		String ctx = request.getScheme()+"://"+request.getHeader("host")+request.getContextPath();
		return ctx;
	}*/
	
	public static void main(String[] args) {
		try {
//			List<String> modles = getMacro("aaaaa${xxx}bbbb${yyy}cccccc${zzz}","\\$\\{.*?\\}");
//			System.out.println(modles.get(1));
			
//			String title = getHTMLDom("<html><head><title>密码重置</title></head></html>","<title>.*?</title>");
//			System.out.println(title);
			
//			List<Integer> list = new LinkedList<Integer>();
//			list.add(1);
//			list.add(2);
//			list.add(3);
//			list.add(4);
//			ListIterator<Integer> iterator = list.listIterator();
//			for (;iterator.hasNext();) {
//				if(iterator.next()==2){
//					iterator.previous();
//					iterator.add(8);
//					int a = iterator.nextIndex();
//					System.out.println(a);
//					break;
//				}
//			}
//			System.out.println(list.toString());
			
			/*double a = 7104.88;
			a = a - a % 5;
			System.out.println(a);*/
			
//			System.out.println(19.99+20);
//			long a = System.nanoTime();
//			String actualURI = getActualURI("///swap///////btc2cny.act");;
//			System.out.println(actualURI);
//			System.out.println(System.nanoTime()-a);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 判断邮件地址格式是否正确
	 * @param email
	 * @return
	 */
	public static boolean isEmail(String email) {
		String str = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
		Pattern p = Pattern.compile(str);
		Matcher m = p.matcher(email);
		return m.matches();
	}
	
	/**
	 * 判断手机号码格式是否正确
	 * @param email
	 * @return
	 */
	public static boolean isMobile(String mobile) {
		String str = "^(\\+?[0-9]{6,18})$";
		Pattern p = Pattern.compile(str);
		Matcher m = p.matcher(mobile);
		return m.matches();
	}
	
	/**
	 * 判断IP地址格式是否正确
	 * @param ipAddress
	 * @return
	 */
	public static boolean isIpAddress(String ipAddress){
		String str = "^(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])$";
		Pattern p = Pattern.compile(str);
		Matcher m = p.matcher(ipAddress);
		return m.matches();
	}
	
	/**
	 * 判断字符串是否为空，去空格后是否为""
	 * @param str
	 * @return
	 */
	public static boolean isBlank(String str){
		if(null == str){
			return true;
		}
		if(str.trim().equals("")){
			return true;
		}
		if(str.contains("\t")||str.contains("\n")||str.contains("\f")||str.contains("\r")){
			return true;
		}
		return false;
	}
	
	/**
	 * 判断字符串是否不为空，不为空字符串
	 * @param str
	 * @return
	 */
	public static boolean isNotBlank(String str){
		return !isBlank(str);
	}
	
	/**
	 * 获取一些常用的钥，例如公钥，未加密的私钥等
	 * @param obj
	 * @return
	 */
	public static String getUnsafeKey(Object obj){
		long localTime = System.currentTimeMillis();
		String code = CommonUtils.randCode("",10);
		return CommonUtils.EncoderByMd5(obj.toString() + localTime + code);
	}
	
	/**
	 * 此方法为私有算法，不对外公开，
	 * 进行私有算法重组后的Key会比较安全，一般用于私钥加密
	 * @param unSafeKey
	 * @return
	 */
	public static String getSafeKey(String unSafeKey){
		return CommonUtils.EncoderByMd5(unSafeKey);//假设是MD5加密
	}
	
	/**
	 * 获取真实URI，防止斜杠注入
	 * @param uri
	 * @return
	 */
	/*public static String getActualURI(HttpServletRequest req){
		String uri = req.getRequestURI().substring(req.getContextPath().length());
		String[] uris = uri.split("/");
		String actualURI = "";
		for (String string : uris) {
			if(!isBlank(string)){
				actualURI += "/" + string;
			}
		}
		return actualURI.toString();
	}*/

}
