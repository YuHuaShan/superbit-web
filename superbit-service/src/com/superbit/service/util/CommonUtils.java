package com.superbit.service.util;


import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import com.superbit.core.exception.ParamInvalidException;


/** 
 * @author  WangZhenwei 
 * @date 创建时间：2018年1月5日 下午5:26:09  
 * @function 通用工具类
 */
public class CommonUtils {
	
	private static String CODES = "01234567890abcdefghijklmnopqrstuvwxyz";
	
	/**
	 * @function 递归生成随机码，按传入位数递归
	 * @param code 	初始化为""
	 * @param sum	生成字符串的长度
	 * @return
	 */
	public static String randCode(String code,int sum){
		if(code.length()>=sum){
			return code;
		}
		if(sum<=0){
			return code;
		}
		java.util.Random random = new java.util.Random();
		int result=random.nextInt(10);
		code += result;
		return randCode(code,sum--);
	}

	/**
	 * @function MD5加密
	 * @param content	加密内容
	 * @return
	 */
	public static String EncoderByMd5(String content) {
		// 生成一个MD5加密计算摘要
        MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			// 计算md5函数
			md.update(content.getBytes());
	        /*
	         * digest()最后确定返回md5 hash值，返回值为8位字符串。因为md5 hash值是16位的hex值，实际上就是8位的字符
	         * BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值
	         */
        	return new BigInteger(1, md.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("MD5加密出现错误");
		}
	}
	
	/**
	 * @function 递归生成随机串
	 * @param code 	初始化为""
	 * @param sum	生成字符串的长度
	 * @return
	 */
	public static String randString(String code, int sum) {
		if(code.length() >= sum) {
			return code;
		}
		if(sum <= 0) {
			return code;
		}
		char[] code_s = CODES.toCharArray();
		java.util.Random random = new java.util.Random();
		int result = random.nextInt(CODES.length());
		code += code_s[result];
		return randString(code, sum--);
	}
	
	/**
	 * @function 生成UUID
	 * @return
	 */
	public static String getUUIDString() {
		UUID uuid = UUID.randomUUID();  
        String str = uuid.toString();  
        // 去掉"-"符号  
        String temp = str.substring(0, 8) + str.substring(9, 13) + str.substring(14, 18) 
        				+ str.substring(19, 23) + str.substring(24);  
        return temp; 
	}
	
	/**@function 字符串不是整数时抛出异常
	 * @param str
	 * @throws ParamInvalidException
	 */
	public static void validateIsInt(String str,String paramName)throws ParamInvalidException{
		try{
			Integer.parseInt(str);
		}catch(Exception e){
			throw new ParamInvalidException(paramName,"Not Int");
		}
	}
	
	/**
	 * @function 判断对象是否是Long类型
	 * @param str
	 * @return
	 */
	public static boolean isLong(String str) {
		try{
			validateIsLong(str, null);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	/**@function 判断是否是小数（包含整数）
	 * @param str
	 * @return
	 */
	public static boolean isDouble(String str){
		try{
			validateIsDouble(str, null);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	/**@function 字符串不是数字时（整数+小数），抛出异常
	 * @param str
	 * @throws ParamInvalidException
	 */
	public static void validateIsDouble(String str,String paramName)throws ParamInvalidException{
		try{
			Double.parseDouble(str);
		}catch(Exception e){
			throw new ParamInvalidException(paramName, "Not Double");
		}
	}
	/**@function 字符串不是Long类型时抛出异常
	 * @param str
	 * @throws ParamInvalidException
	 */
	public static void validateIsLong(String str, String paramName)throws ParamInvalidException {
		try{
			Long.parseLong(str);
		}catch(Exception e) {
			throw new ParamInvalidException(paramName, "Not Long");
		}
	}
}
