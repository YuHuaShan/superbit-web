package com.superbit.service.util;

import java.util.UUID;


public class StringPropertiesUtil {
	/**
	 * 生成32位uuid
	 * @return
	 */
	public static String createIdUtil() {
		return createIdUtil(32);
	}
	/**
	 * 生成指定长度uuid
	 * @param length
	 * @return
	 */
	public static String createIdUtil(int length) {
		length = length>0?length:8;
		return UUID.randomUUID().toString().replaceAll("-", "").substring(0, length);
	}
}
