package com.superbit.service.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;

public class GenerateUtils {
	/**
	 * 生成订单号
	 * 
	 * @return system.currentTime()+服务器号（2位）+随机数（3位）
	 */
	public static String genOrderNumber() {
		String ip = getIp();
		String serverId = ip.substring(ip.length() - 2).replace('.', '0');
		// system.currentTime()+服务器号（2位）+随机数（3位）
		StringBuilder sb = new StringBuilder();
		sb.append(System.currentTimeMillis());
		if (StringUtils.isNotEmpty(serverId)) {
			sb.append(serverId);
		} else {
			sb.append("99");
		}
		for (int i = 0; i < 3; i++) {
			// 随机生成3位数
			sb.append(RandomUtils.nextInt(9));
		}
		return sb.toString();
	}

	private static String getIp() {
		try {
			InetAddress inetAddress = InetAddress.getLocalHost();
			return inetAddress.getHostAddress(); // get the ip address
		} catch (UnknownHostException e) {
			System.out.println("unknown host!");
		}
		return "";
	}
}
