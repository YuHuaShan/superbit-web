package com.superbit.service.mq.impl;

import java.util.Map;

import javax.annotation.Resource;
import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import com.superbit.core.entry.Coin_Base_Info;
import com.superbit.core.entry.User_Asset;
import com.superbit.service.mq.define.RechargePickupMQService;


@Service
public class RechargePickupMQServiceImpl implements RechargePickupMQService{
	private Log logger = LogFactory.getLog(this.getClass());
	
//	@Resource(name = "jmsTemplate4Wallet")
    private JmsTemplate jmsTemplate;
	
//	@Value("${walletService.upname}")
	private String destinationName;


	@Override
	public void sendAddressMessage(int coinType,final  int messageType, final Object obj) {
		try{
			Destination destination = new ActiveMQQueue(destinationName+coinType);
			jmsTemplate.send(destination, new MessageCreator() {
				@Override
				public Message createMessage(Session session) throws JMSException {
					return createMessageByObj(messageType, obj, session);
				}
	
			});
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}
	}
	
	private Message createMessageByObj(int messageType, Object obj, Session session) throws JMSException{
		/*
		 * 1.请求新的充值地址
		 * 2.发出提现请求
		 * 3.获取系统（钱包）总容量
		 */
		if(messageType==1){
			return applyNewChargeAddress(obj, session);
		}else if(messageType==2){
			return applyPickup(obj, session);
		}else if(messageType==3){
			return getWithdrawCount(obj, session);
		}else{
			return null;
		}
	}
	
	/**发送请求新的充值地址的消息
	 * @param obj
	 * @param session
	 * @return
	 */
	private Message applyNewChargeAddress(Object obj, Session session) {
		try {
			User_Asset userAsset = (User_Asset) obj ;
			BytesMessage bm = session.createBytesMessage();
			bm.writeByte((byte) 1);
			bm.writeInt(userAsset.getId());
			bm.writeInt(userAsset.getUserId());
			return bm;
		} catch (JMSException e) {
			return null;
		}
	}
	
	/**发送获取钱包(系统)总容量的消息
	 * @param obj
	 * @param session
	 * @return
	 */
	private Message getWithdrawCount(Object obj, Session session) {
		try {
			Coin_Base_Info coinType = (Coin_Base_Info) obj;
			BytesMessage bm = session.createBytesMessage();
			bm.writeByte((byte) 3);
			bm.writeByte((byte) coinType.getId());
			return bm;
		} catch (JMSException e) {
			return null;
		}
	}
	
	/**发送提现请求的消息
	 * @param obj
	 * @param session
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Message applyPickup(Object obj, Session session){
		try {
			Map<String, String> broadcast = (Map<String, String>) obj;
			BytesMessage bm = session.createBytesMessage();
			bm.writeByte((byte) 2);
			bm.writeBytes(broadcast.get("broadcastId").getBytes());
			bm.writeByte((byte) '\0');
			bm.writeBytes(broadcast.get("receiveBlockInfo").getBytes());
			bm.writeByte((byte) '\0');
			return bm;
		} catch (JMSException e) {
			return null;
		}
	}
	
}
