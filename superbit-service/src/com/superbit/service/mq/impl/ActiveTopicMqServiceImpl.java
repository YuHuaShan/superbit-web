package com.superbit.service.mq.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.superbit.service.mq.define.ActiveTopicMqService;
import com.superbit.service.mq.define.MQManageHandler;


@Service
public class ActiveTopicMqServiceImpl implements ActiveTopicMqService {

	private Map<String,MQManageHandler> messageHandlerMap = new HashMap<String,MQManageHandler>();
	
	@Override
	public void addMessageSender(String mqType, MQManageHandler handler) {
		messageHandlerMap.put(mqType, handler);
	}

	@Override
	public void dealWithMessage(String mqType, String messagetype, Object obj) {
		MQManageHandler messHandler = this.getMessHandler(mqType);
		if(messHandler==null){			
			//找不到对应处理器，不进行处理
		}else{			
			messHandler.handle(messagetype, obj);
		}
		
	}
	
	private MQManageHandler getMessHandler(String mqType){
		return messageHandlerMap.get(mqType);
	}

}
