package com.superbit.service.mq.dao;

import org.springframework.stereotype.Repository;

@Repository(value="ServiceStatusListener")
public class ServiceStatusListener {
	
	private boolean mysqlWorking = true;
	private boolean mqworking = true;
	private boolean redisworking = true;
	
	public boolean isAllServerOnWorking(){
		return mysqlWorking&&mqworking&&redisworking;
	}
	
	public boolean isRedisOnWorking(){
		return redisworking;
	}
	
	public boolean isMQOnWorking(){
		return mqworking;
	}
	
	public boolean isMySqlOnWorking(){
		return mysqlWorking;
	}
	
	public void setRedisWorkingStates(boolean bn){
		this.redisworking = bn;
	}
	
	public void setMQWorkingStates(boolean bn){
		this.mqworking = bn;
	}
	
	public void setMySqlWorkingStates(boolean bn){
		this.mysqlWorking = bn;
	}
}
