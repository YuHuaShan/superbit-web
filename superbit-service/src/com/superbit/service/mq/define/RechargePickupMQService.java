package com.superbit.service.mq.define;

public interface RechargePickupMQService {
	
	/**根据消息类型向队列发送消息
	 * @param transactionOrder
	 */
	void sendAddressMessage(int coinType,int messageType,Object messageobj);
	
}
