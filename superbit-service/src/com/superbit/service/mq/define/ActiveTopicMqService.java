package com.superbit.service.mq.define;

public interface ActiveTopicMqService {
	
	public void addMessageSender(String mqType,MQManageHandler handler);
	
	public void dealWithMessage(String mqType,String messagetype,Object obj);

}
