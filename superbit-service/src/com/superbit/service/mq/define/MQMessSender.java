package com.superbit.service.mq.define;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

public interface MQMessSender{
	//发现消息类型
	int TYPE_ADDBUYORDER = 1; //下买单
	int TYPE_ADDSELLORDER = 2; //下卖单
	int TYPE_CANCELORDRE = 3; //撤销
	int TYPE_PARAMSET = 4;//设置
	
	/**设定队列名称
	 * @return
	 */
	String getDestinationName();
	
	/**将业务bean转成消息
	 * @param obj
	 * @return
	 */
	Message createMessageByObj(int messageType,Object obj,Session session) throws JMSException;
}
