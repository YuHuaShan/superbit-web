package com.superbit.service.mq.define;

import javax.jms.Message;

public interface ActiveMqService {

	/**根据交易类型名称向队列发送消息
	 * @param matchType
	 * @param transactionOrder
	 */
	void sendOrderMessage(int matchType,int messageType,Object transactionOrder);
	
	/**根据matchType和messageType处理消息回传结果
	 * @param matchType
	 * @param messageType
	 * @param message
	 */
	void dealWithMessage(int matchType,int messageType,Message message);
	
	/**添加消息发送器
	 * @param matchType
	 * @param sender
	 */
	void addMessageSender(int matchType,MQMessSender sender);
	
	/**添加消息处理器
	 * @param matchType
	 * @param handler
	 */
	void addMessageHandler(int matchType,MQMessageHandler handler);
}
