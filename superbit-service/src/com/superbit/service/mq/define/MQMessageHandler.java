package com.superbit.service.mq.define;

import javax.jms.Message;

public interface MQMessageHandler {
	
	//撮合结果类型
	int TYPE_PARTIALTRADE = 1; //部分成交
	int TYPE_ALLTRADE = 2; //全部成交
	int TYPE_CANCELORDER = 3; //撤销成功
	int TYPE_TRADEINFO = 4; //最新成交信息
	int TYPE_DEEPENUPDATE = 5; //更新深度信息
	int TYPE_PARAMSETINFO = 6; //设置结果输出
	
	/**处理从撮合系统发过来的结果
	 * @param tradeType
	 * @param message
	 */
	void handle(int tradeType, Message message);
}
