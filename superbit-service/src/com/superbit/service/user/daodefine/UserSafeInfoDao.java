package com.superbit.service.user.daodefine;

import com.superbit.core.entry.UserSafeInfo;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月19日 下午5:12:06  
  * @descrip 
  */
public interface UserSafeInfoDao {

	// 跟新用户安全信息
	void updateUserSafeInfo(UserSafeInfo userSafeInfo);
	// 根据用户Id获取用户安全信息
	UserSafeInfo getUserSafeInfoById(int userId);
	// 判断用户是否被锁定，返回用户安全信息对象
	UserSafeInfo userIsLocked(int userId);
	// 向数据库保存用户安全信息
	void saveUserSafeInfo(UserSafeInfo userSafeInfo);
	
	
	
	// 前台锁定【对MySQL和redis都做操作】
	void lockUser(int userId, long lockDeadline);
	// 绑定手机时更新用户安全信息
	void updatePhoneInfoById(UserSafeInfo safeInfo);
	// 更改登录密码冻结提现状态
	void freezeUserForResetPwd(UserSafeInfo safeInfo);
}
