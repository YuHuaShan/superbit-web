package com.superbit.service.user.daodefine;

import com.superbit.core.entry.UserRealnameAuthen;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月22日 下午4:22:28  
  * @descrip 
  */
public interface UserRealAuthenDao {

	// 根据用户ID获取驳回理由
	UserRealnameAuthen getFailReasonByUserId(Integer userId);

}
