package com.superbit.service.user.daodefine;

import com.superbit.core.entry.UserBaseInfo;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月19日 下午5:12:33  
  * @descrip 
  */
public interface UserBaseInfoDao {

	// 根据用户昵称查询用户个数
	int countUserInfoByNickname(String nickname);
	// 根据用户Id更新用户信息
	void updateUserBaseInfoByUserId(UserBaseInfo userBaseInfo);
	// 根据用户Id获取用户基本信息
	UserBaseInfo getUserBaseInfoById(int userId);
	// 根据账户及账户类型获取用户基本信息
	UserBaseInfo getUserBaseInfoByAccount(String account, byte accountType);
	// 根据邮箱查询用户个数
	int countUserByEmail(String email);
	// 向数据库中持久化用户基础信息
	Integer saveUserBaseInfo(UserBaseInfo userBaseInfo);
	// 根据账号获取用户基本信息
	UserBaseInfo getUserBaseInfoByAccount(String account);
	// 根据手机号查询用户个数
	int countUserByPhone(String phone);
	// 根据用户ID更新绑定手机信息
	void bindPhoneInfoById(UserBaseInfo baseInfo);
	// 根据用户ID更新邮箱信息
	void updateEmailInfoByUserId(UserBaseInfo userBaseInfo);
	// 根据用户ID修改用户登录密码
	void updateLoginPwdInfoByUserId(UserBaseInfo userInfo);
}
