package com.superbit.service.user.daodefine;

import java.util.List;

import com.superbit.core.entry.UserPayMethod;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月20日 上午11:08:06  
  * @descrip 
  */
public interface UserPayMethodDao {

	// 根据用户ID查询用户支付信息
	List<UserPayMethod> queryUserPayMethodByUserId(Integer userId);
	// 保存用户的支付信息
	void savePayMethod(UserPayMethod payMethod);
	// 根据用户ID以及支付编号，查询用户支付信息
	UserPayMethod getPayMethodDetail(Integer userId, Integer payMethodCard);

}
