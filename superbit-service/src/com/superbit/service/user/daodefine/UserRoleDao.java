package com.superbit.service.user.daodefine;

import java.util.List;
import java.util.Map;

import com.superbit.core.entry.UserRole;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月13日 上午9:35:08  
  * @descrip 
  */
public interface UserRoleDao {

	// 获取用户角色
	List<UserRole> getUserRoles(Map<String, Object> map, String[] order);

}
