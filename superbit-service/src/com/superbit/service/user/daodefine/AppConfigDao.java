package com.superbit.service.user.daodefine;


import java.util.List;

import com.superbit.core.entry.UserAppconfig;
import com.superbit.core.exception.BusinessException;



/**
 * @function 用户系统配置数据层接口
 * @author WangZhenwei
 * @version 2017-09-28 10:53
 */
public interface AppConfigDao {
	
	/**
	 * 获取用户系统配置对象列表
	 * @return
	 */
	List<UserAppconfig> getAppConfigInfos();
	
	/**
	 * 获取用户系统单项配置对象
	 * @param key	配置的键
	 * @return
	 */
	UserAppconfig getAppConfigInfo(String key);
	
	/**
	 * 获取用户系统配置的Value
	 * @param key	配置的键
	 * @return
	 */
	String getAppConfigValue(String key);
	String getAppConfigValue(String key, String defvalue);
	int getAppConfigIntValue(String key);
	int getAppConfigIntValue(String key,int defvalue);
	long getAppConfigLongValue(String key);
	long getAppConfigLongValue(String key,long defvalue);
	double getAppConfigDoubleValue(String key);
	double getAppConfigDoubleValue(String key,double defvalue);
	
	/**
	 * 添加用户系统配置信息
	 * @param key		配置的键
	 * @param value		配置的值
	 * @param type		类型
	 * @param descrip	描述
	 * @param defvalue	默认值
	 * @throws BusinessException
	 */
	void addAppConfig(String key, String value, int type, String descrip, String defvalue); 
	
	
	/**
	 * 更新用户系统配置信息
	 * @param key		配置的键
	 * @param value		配置的值
	 * @param descrip	描述
	 * @throws BusinessException 
	 */
	void updateAppConfig(String key, String value, String descrip) throws BusinessException;
	
	/**
	 * 重新加载配置信息
	 * @param key	配置的键
	 */
	void reloadAppConfig(String key);
	
}
