package com.superbit.service.user.daodefine;

import java.util.Map;


/** @author  WangZhenwei 
  * @date 创建时间：2018年1月5日 下午6:34:04  
  * @descrip 
  */
public interface UserRedisDao {

	
	/**
	 * 针对redis数据库的操作
	 * 
	 */
	// 获取手机注册的临时信息
	Map<String, String> getPhoneRegisterTempInfo(String phone);
	// 验证码验证次数增加
	void addVerifyCount(String key);
	// 删除手机临时注册信息
	void removePhoneRegisterTempInfo(String phone);
	// 将手机注册的临时信息保存到redis库中
	void savePhoneRegisterTempInfo(Map<String, String> tempInfo);
	
	// 获取邮箱注册的临时信息
	Map<String, String> getEmailRegisterTempInfo(String email);
	// 将邮箱注册的临时信息保存到redis中
	void saveEmailRegisterTempInfo(Map<String, String> tempInfo);
	// 删除邮箱注册的临时信息
	void removeEmailRegisterTempInfo(String email);
	// 增加密码错误次数
	long addPasswordErrorTimes(int userId);
	// 删除密码输入错误次数统计
	void dropPasswordErrorTimesCount(int userId);
	// 获取校验登录认证的临时信息
	Map<String, String> getCheckLoginAuthenticationTempInfo(Integer userId);
	// 保存校验登录认证的临时信息
	void saveCheckLoginAuthenticationTempInfo(Integer userId, Map<String, String> tempInfo);
	// 验证码错误次数增加
	void incVerifyCount(String key);
	// 删除校验登录认证的临时信息
	void removeCheckLoginAuthentication(Integer userId);
	// 获取账户为手机时密码重置的临时信息
	Map<String, String> getPhoneResetTempInfo(String phone);
	// 保存账号为手机时密码重置的临时信息
	void savePhoneResetTempInfo(Map<String, String> tempInfo);
	// 获取账户为邮箱时密码重置的临时信息
	Map<String, String> getEmailResetTempInfo(String email);
	// 保存账号为手机时密码重置的临时信息
	void saveEmailResetTempInfo(Map<String, String> tempInfo);
	// 删除用户重置的邮箱临时信息
	void removeEmailResetTempInfo(String email);
	// 删除用户重置的手机临时信息
	void removePhoneResetTempInfo(String email);
	// 设置谷歌私钥到redis中
	void setGoogleSecret(Integer userId, String secret);
	// 获取redis中谷歌私钥
	String getGoogleSecret(Integer userId);
	// 删除redis中的谷歌私钥
	void dropGoogleSecret(Integer userId);
	
	
	
	
	
}
