package com.superbit.service.user.daodefine;

import com.superbit.core.entry.UserActivityInfo;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月19日 下午5:13:02  
  * @descrip 
  */
public interface UserActivityInfoDao {

	// 根据用户Id获取用户活动信息
	UserActivityInfo getUserActivityInfoById(int userId);
	// 向数据库保存用户活动信息
	void saveUserActivityInfo(UserActivityInfo userActivityInfo);
}
