package com.superbit.service.user.daodefine;

import com.superbit.core.entry.UserLoginLog;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月19日 下午5:14:19  
  * @descrip 
  */
public interface UserLoginLogDao {

	/**
	 * 保存登录日志对象
	 * @param loginLog	登录日志对象
	 */
	void saveLoginLog(UserLoginLog loginLog);
}
