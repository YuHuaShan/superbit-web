package com.superbit.service.user.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superbit.core.entry.UserRole;
import com.superbit.core.service.UserRoleService;
import com.superbit.service.user.daodefine.UserRoleDao;
import com.superbit.utils.constantutil.CollectionUtils;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月13日 上午9:29:16  
  * @descrip 用户角色的业务实现
  */
@Service
public class UserRoleServiceImpl implements UserRoleService{
	
	@Autowired
	private UserRoleDao userRoleDao;

	/**
	 * 
	 * 根据用户Id获取用户角色
	 */
	@SuppressWarnings("unused")
	@Override
	public String[] getUserRoles(int userId) {
		Map<String, Object> map = CollectionUtils.buildMap("userId", userId);
		List<UserRole> userRoleList = userRoleDao.getUserRoles(map, null);
		return null;
	}

}
