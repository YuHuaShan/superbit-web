package com.superbit.service.user.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superbit.core.entry.UserBaseInfo;
import com.superbit.core.service.UserBaseInfoProxy;
import com.superbit.service.user.daodefine.UserBaseInfoDao;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月19日 下午8:20:46  
  * @descrip 
  */
@Service
public class UserBaseInfoProxyImpl implements UserBaseInfoProxy{

	@Autowired
	private UserBaseInfoDao userDao;
	
	/**
	 * 根据用户昵称查询用户ID
	 */
	@Override
	public String getNickNameByUserId(Integer userId) {
		UserBaseInfo userBaseInfo = userDao.getUserBaseInfoById(userId);	
		if(userBaseInfo==null){
			return "";
		}
		return userBaseInfo.getNickname();
	}

}
