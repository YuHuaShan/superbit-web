package com.superbit.service.user.impl;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superbit.core.constant.UserStatusConstant;
import com.superbit.core.entry.UserPayMethod;
import com.superbit.core.service.UserPayMethodService;
import com.superbit.service.user.daodefine.UserPayMethodDao;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月20日 上午11:05:38  
  * @descrip 
  */
@Service
@Transactional
public class UserPayMethodServiceImpl implements UserPayMethodService{

	@Autowired
	private UserPayMethodDao payMethodDao;
	/**
	 * 根据用户ID获取用户支付信息
	 */
	@Override
	public List<UserPayMethod> getUserPayMethodByUserId(Integer userId) {	
		return payMethodDao.queryUserPayMethodByUserId(userId);
	}
	
	/**
	 * 根据用户ID保存用户支付信息
	 */
	@Override
	public void saveUserPayMethodByCondition(Integer userId, Map<String, Object> map) {
		// 区别那种方式分别封装
		String chanelID = map.get("chanelID").toString();
		UserPayMethod payMethod = new UserPayMethod();
		if("0".equals(chanelID)){
			// 银行卡
			payMethod.setUserId(userId);
			payMethod.setChanelID(UserStatusConstant.PAY_METHOD_CARD);
			payMethod.setRealName(map.get("realName").toString());
			payMethod.setPayAccount(map.get("payAccount").toString());
			payMethod.setPayQrCode(map.get("payQrCode").toString());
			payMethod.setPayQrAddr(map.get("payQrAddr").toString());
			payMethod.setBindTime(System.currentTimeMillis());

		}else{
			// 支付宝或微信
			payMethod.setUserId(userId);
			payMethod.setChanelID(Integer.parseInt(map.get("chanelID").toString()));
			payMethod.setRealName(map.get("realName").toString());
			payMethod.setPayAccount(map.get("payAccount").toString());
			
		}
		
		payMethodDao.savePayMethod(payMethod);
	}

	/**
	 * 根据用户ID及支付编号获取用户支付详细信息【如果是微信或支付宝，需要将二维码图片展示】
	 */
	@Override
	public UserPayMethod getPayMethodDetailByCondition(Integer userId, int payMethod) {
		return payMethodDao.getPayMethodDetail(userId, payMethod);	
	}

}
