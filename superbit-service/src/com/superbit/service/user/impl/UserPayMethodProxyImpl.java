package com.superbit.service.user.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superbit.core.entry.UserPayMethod;
import com.superbit.core.service.UserPayMethodProxy;
import com.superbit.service.user.daodefine.UserPayMethodDao;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月19日 下午8:18:48  
  * @descrip 
  */
@Service
public class UserPayMethodProxyImpl implements UserPayMethodProxy{

	@Autowired
	private UserPayMethodDao payMethodDao;
	
	/**
	 * 根据用户ID获取用户的支付方式
	 */
	@Override
	public List<UserPayMethod> getUserPayMethodByUserId(Integer userId) {		
		return payMethodDao.queryUserPayMethodByUserId(userId);
	}

}
