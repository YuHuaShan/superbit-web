package com.superbit.service.user.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superbit.core.constant.UserStatusConstant;
import com.superbit.core.entry.UserRealnameAuthen;
import com.superbit.core.service.UserRealAuthenService;
import com.superbit.service.user.daodefine.UserRealAuthenDao;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月20日 下午5:37:55  
  * @descrip 
  */
@Transactional
@Service
public class UserRealAuthenServiceImpl implements UserRealAuthenService{

	@Autowired
	private UserRealAuthenDao authenDao;
	
	/**
	 * 根据用户ID查询用户审核失败的驳回理由
	 */
	@Override
	public String getFailReasonByUserId(Integer userId) {
		UserRealnameAuthen realnameAuthen = authenDao.getFailReasonByUserId(userId);
		if(UserStatusConstant.AUTHEN_STATUS_FAIL.equals(realnameAuthen.getAuthenStatus())){
			return realnameAuthen.getOperationReason().toString();
		}
		return null;
	}

}
