package com.superbit.service.user.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superbit.core.entry.UserSafeInfo;
import com.superbit.core.service.UserSafeInfoProxy;
import com.superbit.service.user.daodefine.UserSafeInfoDao;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月19日 下午8:15:50  
  * @descrip 其他系统调用用户
  */
@Service
public class UserSafeInfoProxyImpl implements UserSafeInfoProxy{

	@Autowired
	private UserSafeInfoDao safeInfoDao;
	
	/**
	 * 根据用户ID获取用户指定安全信息
	 */
	@Override
	public UserSafeInfo verificationSafeInfo(Integer userId) {
		return safeInfoDao.getUserSafeInfoById(userId);
	}

	/**
	 * 返回用户修改登录密码冻结时间
	 */
	@Override
	public String assetFreeze(Integer userId) {
		UserSafeInfo userSafeInfo = safeInfoDao.getUserSafeInfoById(userId);
		if(userSafeInfo.getFreezeStatus() == 2){
			return userSafeInfo.getFreezeDeadline().toString();
		}
		return null;
	}

}
