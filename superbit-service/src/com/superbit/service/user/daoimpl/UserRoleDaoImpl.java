package com.superbit.service.user.daoimpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.superbit.core.entry.UserRole;
import com.superbit.service.user.daodefine.UserRoleDao;
import com.superbit.utils.dbutil.CommonDao;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月13日 上午9:35:29  
  * @descrip 用户角色的实现
  */
@Repository
public class UserRoleDaoImpl implements UserRoleDao{

	@Autowired
	private CommonDao commonDao;

	/**
	 * 根据用户Id获取用户角色
	 */
	@Override
	public List<UserRole> getUserRoles(Map<String, Object> condition, String[] order) {
		return commonDao.getEntrysByField(UserRole.class, condition, order);
	}
	
	
}
