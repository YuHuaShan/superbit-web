package com.superbit.service.user.daoimpl;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.superbit.core.entry.UserPayMethod;
import com.superbit.service.user.daodefine.UserPayMethodDao;
import com.superbit.utils.dbutil.CommonDao;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月20日 上午11:08:33  
  * @descrip 操作用户支付信息的DAO实现
  */
@Repository
public class UserPayMethodDaoImpl implements UserPayMethodDao{

	@Autowired
	private CommonDao commonDao;

	/**
	 * 根据用户ID查询用户支付信息
	 */
	@Override
	public List<UserPayMethod> queryUserPayMethodByUserId(Integer userId) {
		Criterion[] criteria = new Criterion[]{Restrictions.eq("userId", userId)};
		List<UserPayMethod> list = commonDao.getEntrysByCond(UserPayMethod.class, criteria, null);		
		return list;
	}

	/**
	 * 保存支付信息到数据库的方法,保存之前需查询有无该信息有则先删除
	 */
	@Override
	public void savePayMethod(UserPayMethod payMethod) {
		Criterion[] criteria = new Criterion[]{Restrictions.eq("userId", payMethod.getUserId()), Restrictions.eq("chanelID", payMethod.getChanelID())};
		List<UserPayMethod> list = commonDao.getEntrysByCond(UserPayMethod.class, criteria, null);
		if(list != null && list.size() >0){
			for (int i = 0; i < list.size(); i++) {
				commonDao.deleteEntrys(UserPayMethod.class, "userId="+payMethod.getUserId()+" and chanelID="+payMethod.getChanelID());
			}
		}
		
		commonDao.saveEntry(payMethod);
	}

	/**
	 * 根据用户ID及支付编号查询支付信息
	 */
	@Override
	public UserPayMethod getPayMethodDetail(Integer userId, Integer payMethodCard) {
		/*List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.eq("orderNumber", orderNumber));
		List<TradeLog> resultList = commonDao.getEntrysByCond(TradeLog.class,
				conds.toArray(new Criterion[0]), null);*/
		Criterion[] criteria = new Criterion[]{Restrictions.eq("userId", userId), Restrictions.eq("chanelID", payMethodCard)};             
		List<UserPayMethod> payMethods = commonDao.getEntrysByCond(UserPayMethod.class, criteria, null);	
		if(null == payMethods || payMethods.isEmpty()){
			return null;
		}else{
			return payMethods.get(0);
		}	
	}
}
