package com.superbit.service.user.daoimpl;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import com.superbit.service.user.daodefine.UserRedisDao;
import com.superbit.utils.dbutil.RedisKeyUtils;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月5日 下午6:34:31  
  * @descrip 用户系统对Redis数据库的操作实现
  */
@Repository
public class UserRedisDaoImpl implements UserRedisDao{

	
	
	@Resource(name="redisTemplate4String")
	private RedisTemplate<String,String> redisTemplate;



	
	/**
	 * 对Redis数据库的操作
	 * 
	 */
	/**
	 * 获取redis数据库中手机注册的临时信息
	 */
	@Override
	public Map<String, String> getPhoneRegisterTempInfo(String phone) {
		HashOperations<String, String, String> operation = redisTemplate.opsForHash();
		String tempInfoKey = RedisKeyUtils.getPhoneRegisterTempInfoKey(phone);
		return operation.entries(tempInfoKey);
	}

	/**
	 * 验证码验证次数增加
	 * 
	 */
	@Override
	public void addVerifyCount(String key) {
		HashOperations<String, String, String> hashOperation = redisTemplate.opsForHash();
		String phoneMessageVerifyCountStr = hashOperation.get(key, "phoneMessageVerifyCount");
		if(phoneMessageVerifyCountStr == null){
			return;
		}
		Integer phoneMessageVerifyCount = Integer.parseInt(phoneMessageVerifyCountStr) + 1;
		hashOperation.put(key, "phoneMessageVerifyCount", phoneMessageVerifyCount + "");		
	}

	/**
	 * 删除手机临时注册信息
	 * 
	 */
	@Override
	public void removePhoneRegisterTempInfo(String phone) {
		String tempInfoKey = RedisKeyUtils.getPhoneRegisterTempInfoKey(phone);
		redisTemplate.delete(tempInfoKey);
	}

	/**
	 * 向redis数据库中保存注册临时信息，并设置过期时间
	 * 
	 */
	@Override
	public void savePhoneRegisterTempInfo(Map<String, String> tempInfo) {
		// 将数据转换成hash结构的数据
		HashOperations<String, Object, Object> operation = redisTemplate.opsForHash();
		// 创建此Hash的key
		String tempInfoKey = RedisKeyUtils.getPhoneRegisterTempInfoKey(tempInfo.get("phone"));
		// 存库
		operation.putAll(tempInfoKey, tempInfo);
		// 为hash的key设置过期时间：key,过期时间,计时单位
		redisTemplate.expire(tempInfoKey, RedisKeyUtils.getPhoneRegisterTempInfoTime(), TimeUnit.SECONDS);
	}

	/**
	 * 获取邮箱临时注册信息
	 */
	@Override
	public Map<String, String> getEmailRegisterTempInfo(String email) {
		HashOperations<String, String, String> operation = redisTemplate.opsForHash();
		String tempInfoKey = RedisKeyUtils.getEmailRegisterTempInfoKey(email);
		return operation.entries(tempInfoKey);
	}

	/**
	 * 保存邮件注册的临时信息
	 * 
	 */
	@Override
	public void saveEmailRegisterTempInfo(Map<String, String> tempInfo) {
		HashOperations<String, Object, Object> operation = redisTemplate.opsForHash();
		String tempInfoKey = RedisKeyUtils.getEmailRegisterTempInfoKey(tempInfo.get("email")+"");
		operation.putAll(tempInfoKey, tempInfo);
		//邮件注册的临时信息在Redis中存放一小时
		redisTemplate.expire(tempInfoKey, RedisKeyUtils.getEmailRegisterTempInfoTime(), TimeUnit.SECONDS);
	}

	/**
	 * 移除redis数据库中的邮箱注册临时信息
	 */
	@Override
	public void removeEmailRegisterTempInfo(String email) {
		String tempInfoKey = RedisKeyUtils.getEmailRegisterTempInfoKey(email);
		redisTemplate.delete(tempInfoKey);
	}

	/**
	 * 增加密码输入错误次数(5次)
	 */
	@Override
	public long addPasswordErrorTimes(int userId) {
		ValueOperations<String, String> operation = redisTemplate.opsForValue();
		String key = RedisKeyUtils.getPasswordErrorKey(userId);
		return operation.increment(key, 1);
	}

	/**
	 * 删除密码输入错误次数统计
	 */
	@Override
	public void dropPasswordErrorTimesCount(int userId) {
		String key = RedisKeyUtils.getPasswordErrorKey(userId);
		redisTemplate.delete(key);
	}

	
	
	
	

	/**
	 * 获取校验登录认证的临时信息
	 */
	@Override
	public Map<String, String> getCheckLoginAuthenticationTempInfo(Integer userId) {
		HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
		String tempInfoKey = RedisKeyUtils.getCheckLoginAuthenticationTempInfoKey(userId);		
		return hashOperations.entries(tempInfoKey);
	}

	/**
	 * 保存校验登录认证的临时信息
	 */
	@Override
	public void saveCheckLoginAuthenticationTempInfo(Integer userId, Map<String, String> tempInfo) {
		HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
		String tempInfoKey = RedisKeyUtils.getCheckLoginAuthenticationTempInfoKey(userId);
		hashOperations.putAll(tempInfoKey, tempInfo);
		// 校验登录认证的临时信息在Redis中存放一小时
		redisTemplate.expire(tempInfoKey, RedisKeyUtils.getCheckLoginAuthenticationTempInfoTime(), TimeUnit.SECONDS);	
	}

	/**
	 * 验证码错误次数增加
	 */
	@Override
	public void incVerifyCount(String key) {
		HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
		String verifyCount = hashOperations.get(key, "verifyCount");
		if(verifyCount == null){
			return;
		}
		hashOperations.increment(key, "verifyCount", 1);
	}

	/**
	 * 删除校验登录认证的临时信息
	 */
	@Override
	public void removeCheckLoginAuthentication(Integer userId) {
		String tempInfoKey = RedisKeyUtils.getCheckLoginAuthenticationTempInfoKey(userId);
		redisTemplate.delete(tempInfoKey);		
	}

	/**
	 * 
	 * 获取手机登录密码重置的临时信息
	 */
	@Override
	public Map<String, String> getPhoneResetTempInfo(String phone) {
		HashOperations<String, String, String> operation = redisTemplate.opsForHash();
		String tempInfoKey = RedisKeyUtils.getPhoneResetTempInfoKey(phone);
		return operation.entries(tempInfoKey);
	}

	/**
	 * 保存手机登录密码重置的临时信息
	 */
	@Override
	public void savePhoneResetTempInfo(Map<String, String> tempInfo) {
		// 将数据转换成hash结构的数据
		HashOperations<String, Object, Object> operation = redisTemplate.opsForHash();
		// 创建此Hash的key
		String tempInfoKey = RedisKeyUtils.getPhoneResetTempInfoKey(tempInfo.get("phone"));
		// 存库
		operation.putAll(tempInfoKey, tempInfo);
		// 为hash的key设置过期时间：key,过期时间,计时单位
		redisTemplate.expire(tempInfoKey, RedisKeyUtils.getPhoneResetTempInfoTime(), TimeUnit.SECONDS);
	}

	/**
	 * 
	 * 获取邮箱登录密码重置的临时信息
	 */
	@Override
	public Map<String, String> getEmailResetTempInfo(String email) {
		HashOperations<String, String, String> operation = redisTemplate.opsForHash();
		String tempInfoKey = RedisKeyUtils.getEmailResetTempInfoKey(email);
		return operation.entries(tempInfoKey);
	}

	@Override
	public void saveEmailResetTempInfo(Map<String, String> tempInfo) {
		// 将数据转换成hash结构的数据
		HashOperations<String, Object, Object> operation = redisTemplate.opsForHash();
		// 创建此Hash的key
		String tempInfoKey = RedisKeyUtils.getEmailResetTempInfoKey(tempInfo.get("email"));
		// 存库
		operation.putAll(tempInfoKey, tempInfo);
		// 为hash的key设置过期时间：key,过期时间,计时单位
		redisTemplate.expire(tempInfoKey, RedisKeyUtils.getEmailResetTempInfoTime(), TimeUnit.SECONDS);
	}

	/**
	 * 删除邮箱密码重置临时信息
	 */
	@Override
	public void removeEmailResetTempInfo(String email) {
		String tempInfoKey = RedisKeyUtils.getEmailResetTempInfoKey(email);
		redisTemplate.delete(tempInfoKey);
	}
	
	/**
	 * 删除手机密码重置临时信息
	 */
	@Override
	public void removePhoneResetTempInfo(String phone) {
		String tempInfoKey = RedisKeyUtils.getPhoneResetTempInfoKey(phone);
		redisTemplate.delete(tempInfoKey);
	}

	/**
	 * 设置谷歌私钥
	 */
	@Override
	public void setGoogleSecret(Integer userId, String secret) {
		ValueOperations<String, String> operation = redisTemplate.opsForValue();
		String googlePrivateKey = RedisKeyUtils.getGooglePrivateKey(userId);
		operation.set(googlePrivateKey, secret);
		long privateKeyTime = RedisKeyUtils.getGooglePrivateKeyTime();
		// 设置过期时间
		redisTemplate.expire(googlePrivateKey, privateKeyTime, TimeUnit.SECONDS);
	}

	/**
	 * 根据用户ID从redis中获取用户谷歌私钥
	 */
	@Override
	public String getGoogleSecret(Integer userId) {
		ValueOperations<String, String> operation = redisTemplate.opsForValue();
		String googlePrivateKey = RedisKeyUtils.getGooglePrivateKey(userId);
		String secret = operation.get(googlePrivateKey);
		return secret;
	}

	/**
	 * 根据用户ID删除redis中的谷歌私钥
	 */
	@Override
	public void dropGoogleSecret(Integer userId) {
		String googlePrivateKey = RedisKeyUtils.getGooglePrivateKey(userId);
		redisTemplate.delete(googlePrivateKey);
	}


}
