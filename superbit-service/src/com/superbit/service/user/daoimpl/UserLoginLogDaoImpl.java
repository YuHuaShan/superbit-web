package com.superbit.service.user.daoimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.superbit.core.entry.UserLoginLog;
import com.superbit.service.user.daodefine.UserLoginLogDao;
import com.superbit.utils.dbutil.CommonDao;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月12日 下午3:54:01  
  * @descrip 登录日志数据层实现
  */
@Repository
public class UserLoginLogDaoImpl implements UserLoginLogDao{

	@Autowired
	private CommonDao commonDao;
	
	/**
	 * 保存登录日志对象
	 */
	@Override
	public void saveLoginLog(UserLoginLog loginLog) {
		commonDao.saveEntry(loginLog);
	}

}
