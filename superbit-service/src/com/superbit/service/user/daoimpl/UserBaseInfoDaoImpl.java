package com.superbit.service.user.daoimpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.superbit.core.entry.UserBaseInfo;
import com.superbit.service.user.daodefine.UserBaseInfoDao;
import com.superbit.utils.dbutil.CommonDao;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月19日 下午5:28:53  
  * @descrip 
  */
@Repository
public class UserBaseInfoDaoImpl implements UserBaseInfoDao{

	@Autowired
	private CommonDao commonDao;
	
	/**
	 * 根据昵称查询用户个数
	 */
	@Override
	public int countUserInfoByNickname(String nickname) {
		 Criterion[] criteria = new Criterion[]{Restrictions.eq("nickname", nickname)};
		return commonDao.getCount(UserBaseInfo.class, criteria);
	}

	/**
	 * 根据用户Id更新用户基本信息
	 */
	@Override
	public void updateUserBaseInfoByUserId(UserBaseInfo userBaseInfo) {
		Map<String, Object> map = new HashMap<>();
		map.put("nickname", userBaseInfo.getNickname());
		String condition = "USER_ID="+userBaseInfo.getUserId();
		commonDao.updateEntrys(UserBaseInfo.class, map, condition);
		//commonDao.updateEntry(userBaseInfo);
	}

	/**
	 * 根据用户Id获取用户基本信息
	 */
	@Override
	public UserBaseInfo getUserBaseInfoById(int userId) {
		return commonDao.getEntry(userId, UserBaseInfo.class);
	}

	/**
	 *根据账号、账号类型获取用户基本信息 
	 */
	@Override
	public UserBaseInfo getUserBaseInfoByAccount(String account, byte accountType) {
		Criterion[] criteria = null;
		// 账户类型有两种：1邮箱 2手机
		if(accountType == 1){
			criteria = new Criterion[]{Restrictions.eq("email", account)};
		}else{
			criteria = new Criterion[]{Restrictions.eq("phone", account)};
		}
		
		List<UserBaseInfo> userBases = commonDao.getEntrysByCond(UserBaseInfo.class, criteria, null, 0, 1);
		if(null == userBases || userBases.isEmpty()){
			return null;
		}else{
			return userBases.get(0);
		}		
	}

	/**
	 * 根据邮箱统计注册人数
	 */
	@Override
	public int countUserByEmail(String email) {
		Criterion[] criteria = new Criterion[]{Restrictions.eq("email", email)};
		return commonDao.getCount(UserBaseInfo.class, criteria);
	}

	/**
	 * 向数据库中保存用户注册的基础信息
	 * 
	 */
	@Override
	public Integer saveUserBaseInfo(UserBaseInfo userBaseInfo) {		
		return (Integer) commonDao.saveEntry(userBaseInfo);
	}

	/**
	 * 根据账号获取用户基本信息
	 */
	@Override
	public UserBaseInfo getUserBaseInfoByAccount(String account) {
		Criterion[] criteria0 = new Criterion[]{Restrictions.eq("email", account)};
		Criterion[] criteria1 = new Criterion[]{Restrictions.eq("phone", account)};
		List<UserBaseInfo> userBaseInfoList0 = commonDao.getEntrysByCond(UserBaseInfo.class, criteria0, null, 0, 1);
		List<UserBaseInfo> userBaseInfoList1 = commonDao.getEntrysByCond(UserBaseInfo.class, criteria1, null, 0, 1);
		if(userBaseInfoList0 == null || userBaseInfoList0.isEmpty()){
			if(userBaseInfoList1 == null || userBaseInfoList1.isEmpty()){
				return null;
			}
			return userBaseInfoList1.get(0);
		}else{
			return userBaseInfoList0.get(0);
		}
	}

	/**
	 * 根据手机统计注册人数
	 */
	@Override
	public int countUserByPhone(String phone) {		
        Criterion[] criteria = new Criterion[]{Restrictions.eq("phone", phone)};
		return commonDao.getCount(UserBaseInfo.class, criteria);
	}

	/**
	 * 根据用户ID更新用户绑定手机信息
	 */
	@Override
	public void bindPhoneInfoById(UserBaseInfo baseInfo) {
		Map<String, Object> map = new HashMap<>();
		map.put("areaCode", baseInfo.getAreaCode());
		map.put("phone", baseInfo.getPhone());
		String condition = "USER_ID="+baseInfo.getUserId();
		commonDao.updateEntrys(UserBaseInfo.class, map, condition);
	}

	/**
	 * 根据用户ID更新用户邮箱信息
	 */
	@Override
	public void updateEmailInfoByUserId(UserBaseInfo userBaseInfo) {
		Map<String, Object> map = new HashMap<>();
		map.put("email", userBaseInfo.getEmail());
		String condition = "USER_ID="+userBaseInfo.getUserId();
		commonDao.updateEntrys(UserBaseInfo.class, map, condition);
	}

	/**
	 * 根据用户ID更新用户登录密码
	 */
	@Override
	public void updateLoginPwdInfoByUserId(UserBaseInfo userInfo) {
		Map<String, Object> map = new HashMap<>();
		map.put("password", userInfo.getPassword());
		String condition = "USER_ID="+userInfo.getUserId();
		commonDao.updateEntrys(UserBaseInfo.class, map, condition);
	}

}
