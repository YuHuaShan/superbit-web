package com.superbit.service.user.daoimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.superbit.core.entry.UserRealnameAuthen;
import com.superbit.service.user.daodefine.UserRealAuthenDao;
import com.superbit.utils.dbutil.CommonDao;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月22日 下午4:22:59  
  * @descrip 
  */
@Repository
public class UserRealAuthenDaoImpl implements UserRealAuthenDao{

	@Autowired
	private CommonDao commonDao;
	
	/**
	 * 根据用户ID获取用户实名认证的对象
	 */
	@Override
	public UserRealnameAuthen getFailReasonByUserId(Integer userId) {
		return commonDao.getEntry(userId, UserRealnameAuthen.class);
	}

}
