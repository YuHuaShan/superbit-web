package com.superbit.service.user.daoimpl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.superbit.core.entry.UserSafeInfo;
import com.superbit.service.user.daodefine.UserSafeInfoDao;
import com.superbit.utils.constantutil.CollectionUtils;
import com.superbit.utils.dbutil.CommonDao;
import com.superbit.utils.dbutil.RedisKeyUtils;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月19日 下午5:28:31  
  * @descrip 
  */
@Repository
public class UserSafeInfoDaoImpl implements UserSafeInfoDao{

	
	@Autowired
	private CommonDao commonDao;
	
	@Resource(name="redisTemplate4String")
	private RedisTemplate<String,String> redisTemplate;
	
	/**
	 * 根据用户ID更新用户安全信息
	 */
	@Override
	public void updateUserSafeInfo(UserSafeInfo userSafeInfo) {
		Map<String, Object> map = new HashMap<>();
		map.put("fundPassword", userSafeInfo.getFundPassword().toString());
		String condition = "USER_ID="+userSafeInfo.getUserId();
		commonDao.updateEntrys(UserSafeInfo.class, map, condition);
		//commonDao.updateEntry(userSafeInfo);
	}

	/**
	 * 	根据ID获取用户安全信息
	 * @param userId
	 */
	@Override
	public UserSafeInfo getUserSafeInfoById(int userId) {
		return commonDao.getEntry(userId, UserSafeInfo.class);
	}

	/**
	 * 判断用户是否被锁定，返回用户安全信息对象
	 */
	@Override
	public UserSafeInfo userIsLocked(int userId) {
		UserSafeInfo userSafeInfo = this.getUserSafeInfoById(userId);
		// 判断用户是否锁定：0未锁定 1 密码错误锁定 2后台锁定 3频繁访问锁定
		if(userSafeInfo.getLockStatus() == 0){
			return userSafeInfo;
		}else{
			// 该用户的锁定时间小于系统当前时间
			if(userSafeInfo.getLockDeadline() < System.currentTimeMillis()){
				this.lockUser(userId, (byte)0, 0);  // 解锁
			}
			return userSafeInfo;
		}
	}

	/**
	 * 向数据库保存用户安全信息
	 */
	@Override
	public void saveUserSafeInfo(UserSafeInfo userSafeInfo) {
		commonDao.saveEntry(userSafeInfo);
	}
	
	/**
	 * 前台锁定【同时对MySQL、redis做操作】
	 */
	@Override
	public void lockUser(int userId, long lockDeadline) {
		// 密码输入错误5次锁定
		this.lockUser(userId, (byte)1, lockDeadline);
	}

	/**
	 * 前台锁定、后台锁定、解锁【MySQL、Redis数据库一起操作】
	 * @param userId         用户Id
	 * @param lockStatus     用户锁定状态：0未锁定，1密码错误锁定，2后台锁定，3频繁访问锁定
	 * @param lockDeadline   锁定截止时间
	 */
	public void lockUser(int userId, byte lockStatus, long lockDeadline) {
		String lockUsersKey = RedisKeyUtils.getLockUsersKey();
		// 未锁定状态
		if(lockStatus == 0){
			redisTemplate.opsForHash().delete(lockUsersKey, userId + "");
			commonDao.updateEntry(UserSafeInfo.class, CollectionUtils.buildMap("lockDeadline", 0, "lockStatus", (byte)0), userId);
			return;
		}else{
			redisTemplate.opsForHash().put(lockUsersKey, userId + "", lockDeadline + "");
			commonDao.updateEntry(UserSafeInfo.class, CollectionUtils.buildMap("lockDeadline", lockDeadline, "lockStatus", lockStatus), userId);
		}
		
	}

	/**
	 * 根据用户ID更新手机相关信息
	 */
	@Override
	public void updatePhoneInfoById(UserSafeInfo safeInfo) {
		Map<String, Object> map = new HashMap<>();
		map.put("bindPhone", safeInfo.getBindPhone());
		map.put("securityLevel", safeInfo.getSecurityLevel());
		String condition = "USER_ID="+safeInfo.getUserId();
		commonDao.updateEntrys(UserSafeInfo.class, map, condition);
	}

	/**
	 * 根据用户ID冻结用户提现状态
	 */
	@Override
	public void freezeUserForResetPwd(UserSafeInfo safeInfo) {
		Map<String, Object> map = new HashMap<>();
		map.put("freezeStatus", safeInfo.getFreezeStatus());
		map.put("freezeDeadline", safeInfo.getFreezeDeadline());
		String condition = "USER_ID="+safeInfo.getUserId();
		commonDao.updateEntrys(UserSafeInfo.class, map, condition);
	}

}
