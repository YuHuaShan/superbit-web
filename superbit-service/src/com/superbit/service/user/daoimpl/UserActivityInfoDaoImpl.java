package com.superbit.service.user.daoimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.superbit.core.entry.UserActivityInfo;
import com.superbit.service.user.daodefine.UserActivityInfoDao;
import com.superbit.utils.dbutil.CommonDao;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月19日 下午6:05:01  
  * @descrip 
  */
@Repository
public class UserActivityInfoDaoImpl implements UserActivityInfoDao{

	@Autowired
	private CommonDao commonDao;
	/**
	 * 根据用户Id获取用户活动信息
	 */
	@Override
	public UserActivityInfo getUserActivityInfoById(int userId) {
		return commonDao.getEntry(userId, UserActivityInfo.class);
	}

	/**
	 * 向数据库保存用户活动信息
	 */
	@Override
	public void saveUserActivityInfo(UserActivityInfo userActivityInfo) {
		commonDao.saveEntry(userActivityInfo);
	}

}
