package com.superbit.service.user.daoimpl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.superbit.core.entry.UserAppconfig;
import com.superbit.core.exception.BusinessException;
import com.superbit.service.user.daodefine.AppConfigDao;
import com.superbit.service.util.CommonUtils;
import com.superbit.utils.dbutil.CommonDao;
import com.superbit.utils.dbutil.RedisKeyUtils;

/** @author  WangZhenwei 
  * @date 创建时间：2018年1月8日 下午6:11:24  
  * @descrip 用户系统配置的DAO实现
  */
@Repository
public class AppConfigDaoImpl implements AppConfigDao{

	@Autowired
	private CommonDao commonDao;
	
	@Resource(name="redisTemplate4String")
	private RedisTemplate<String,String> redisTemplate;
	
	@Override
	public List<UserAppconfig> getAppConfigInfos() {
		
		return null;
	}

	@Override
	public UserAppconfig getAppConfigInfo(String key) {
		
		return null;
	}

	@Override
	public String getAppConfigValue(String key) {
		String redisKey = RedisKeyUtils.getAppConfigKey(key);
		String value = redisTemplate.opsForValue().get(redisKey);
		if(value == null) {
			UserAppconfig appConfig = commonDao.getEntry(key, UserAppconfig.class);
			if(appConfig != null) {				
				value = appConfig.getValue();
				redisTemplate.opsForValue().set(redisKey, value);
			}
		}
		return value;
	}

	@Override
	public String getAppConfigValue(String key, String defvalue) {
		
		return null;
	}

	@Override
	public int getAppConfigIntValue(String key) {
		String value = getAppConfigValue(key);
		if(value == null) {
			return 0;
		}
		return Integer.parseInt(value);
	}

	@Override
	public int getAppConfigIntValue(String key, int defvalue) {
		String value = getAppConfigValue(key);
		if(value==null){
			addAppConfig(key, defvalue+"", UserAppconfig.TYPE_INT, "", defvalue+"");
			return defvalue;
		}else{			
			return Integer.parseInt(value);
		}
	}

	@Override
	public long getAppConfigLongValue(String key) {
		
		return 0;
	}

	@Override
	public long getAppConfigLongValue(String key, long defvalue) {
		
		return 0;
	}

	@Override
	public double getAppConfigDoubleValue(String key) {
		
		return 0;
	}

	@Override
	public double getAppConfigDoubleValue(String key, double defvalue) {
		
		return 0;
	}

	@Override
	public void addAppConfig(String key, String value, int type, String descrip, String defvalue)
			{
		if(type == 1) {
			CommonUtils.validateIsInt(value, "value");
		}
		if(type == 2) {
			CommonUtils.validateIsLong(value, "value");
		}
		if(type == 3) {
			CommonUtils.validateIsDouble(value, "value");
		}
		
		UserAppconfig appConfig = new UserAppconfig();
		appConfig.setKey(key);
		appConfig.setValue(value);
		appConfig.setType(type);
		appConfig.setDescrip(descrip);
		appConfig.setDefValue(defvalue);
		
		commonDao.saveEntry(appConfig);
		String redisKey = RedisKeyUtils.getAppConfigKey(appConfig.getKey());
		redisTemplate.opsForValue().set(redisKey, value);
		
	}

	@Override
	public void updateAppConfig(String key, String value, String descrip) throws BusinessException {
		
		
	}

	@Override
	public void reloadAppConfig(String key) {
		
		
	}

}
