package com.superbit.service.userrecharge.define;

import java.util.Date;
import java.util.List;

import com.superbit.core.entry.User_Recharge;
/**
 * 用户充值业务层
 * @author hongzhen
 *
 */
public interface User_RechargeService {
	/**
	 * 发送生成用户对应的币种充值地址请求
	 * @param userId
	 * @param coinId
	 * @return
	 */
	boolean sendRechargeAddressMessage(int userId, String coinId);

	/**
	 * 获取用户充值信息
	 * @param userId
	 * @return
	 */
	List<User_Recharge> getUser_Recharge(int userId, String coinId, int status, Date stime, Date etime, int start, int end, String order);
	
	/**
	 * 获取用户充值信息数量
	 * @param userId
	 * @return
	 */
	int getUser_RechargeCount(int userId, String coinId, int status, Date stime, Date etime);
	/**
	 * 用户充值记录
	 * @param ur
	 * @return
	 */
	boolean saveUser_Recharge(User_Recharge ur);
}
