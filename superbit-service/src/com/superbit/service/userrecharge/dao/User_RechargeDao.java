package com.superbit.service.userrecharge.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.superbit.core.entry.User_Recharge;
import com.superbit.utils.dbutil.CommonDao;
@Repository
public class User_RechargeDao{

	private static Logger logger = LoggerFactory.getLogger(User_RechargeDao.class);
	@Autowired
	private CommonDao commonDao;
	/**
	 * 获取用户充值记录
	 * @param userId
	 * @param coinId
	 * @param status
	 * @param stime
	 * @param etime
	 * @param start
	 * @param end
	 * @param order
	 * @return
	 */
	public List<User_Recharge> getUser_Recharge(int userId, String coinId, int status, Date stime, Date etime, int start, int end, String order) {
		// TODO Auto-generated method stub
		logger.info("getUser_Recharge");
		List<Criterion> conds = new ArrayList<Criterion>();
		if(userId>0) {
			conds.add(Restrictions.eq("userId", userId));
		}
		
		if(null!=coinId) {
			conds.add(Restrictions.eq("coinId", coinId));
		}
		
		if(status>=0) {
			conds.add(Restrictions.eq("RechargeStatus", status));
		}
		
		if(null!=stime) {
			conds.add(Restrictions.ge("stime", stime));
		}
		if(null!=etime) {
			conds.add(Restrictions.le("etime", etime));
		}


		Order[] orders = new Order[]{Order.desc(null!=order?order:"updateTime")};
		
		if(start<0) {
			return commonDao.getEntrysByCond(User_Recharge.class, conds.toArray(new Criterion[0]), orders);
		}
		return commonDao.getEntrysByCond(User_Recharge.class, conds.toArray(new Criterion[0]), orders, start, end);
	}
	/**
	 * 获取用户充值记录条数
	 * @param userId
	 * @param coinId
	 * @param status
	 * @param stime
	 * @param etime
	 * @return
	 */
	public int getUser_RechargeCount(int userId, String coinId, int status, Date stime, Date etime) {
		// TODO Auto-generated method stub
		List<Criterion> conds = new ArrayList<Criterion>();
		if(userId>0) {
			conds.add(Restrictions.eq("userId", userId));
		}
		
		if(null!=coinId) {
			conds.add(Restrictions.eq("coinId", coinId));
		}
		
		if(status>=0) {
			conds.add(Restrictions.eq("RechargeStatus", status));
		}
		
		if(null!=stime) {
			conds.add(Restrictions.ge("stime", stime));
		}
		if(null!=etime) {
			conds.add(Restrictions.le("etime", etime));
		}
		return commonDao.getCount(User_Recharge.class, conds);
	}
	
	public boolean saveUser_Recharge(User_Recharge ur) {
		// TODO Auto-generated method stub
		int i = Integer.valueOf(commonDao.saveEntry(ur).toString());
		if(i>0) {
			return true;
		}
		return false;
	}
}
