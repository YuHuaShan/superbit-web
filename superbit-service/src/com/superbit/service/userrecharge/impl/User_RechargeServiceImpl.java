package com.superbit.service.userrecharge.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superbit.core.entry.Coin_Base_Info;
import com.superbit.core.entry.User_Asset;
import com.superbit.core.entry.User_Recharge;
import com.superbit.service.coin.dao.Coin_Base_InfoDao;
import com.superbit.service.mq.define.RechargePickupMQService;
import com.superbit.service.userasset.service.dao.User_AssetDao;
import com.superbit.service.userrecharge.dao.User_RechargeDao;
import com.superbit.service.userrecharge.define.User_RechargeService;
@Service
public class User_RechargeServiceImpl implements User_RechargeService{
	@Autowired
	private User_RechargeDao user_RechargeDao;
	@Autowired
	private User_AssetDao user_AssetDao;
	@Autowired
	private Coin_Base_InfoDao coin_Base_InfoDao;
	@Autowired
	private RechargePickupMQService rechargePickupService;
	
	@Override
	public boolean sendRechargeAddressMessage(int userId, String coinId) {
		// TODO Auto-generated method stub
		try {
			User_Asset user_Asset = user_AssetDao.getUser_Asset(userId, coinId, null);
			Coin_Base_Info coin_Base_Info = coin_Base_InfoDao.getCoin_Base_InfoList(3, coinId, null, null).get(0);
			rechargePickupService.sendAddressMessage(coin_Base_Info.getId() , 1, user_Asset);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	@Override
	public List<User_Recharge> getUser_Recharge(int userId, String coinId, int status, Date stime, Date etime, int start, int end, String order) {
		// TODO Auto-generated method stub
		return user_RechargeDao.getUser_Recharge(userId, coinId, status, stime, etime, start, end, order);
	}

	@Override
	public int getUser_RechargeCount(int userId, String coinId, int status, Date stime, Date etime) {
		// TODO Auto-generated method stub
		return user_RechargeDao.getUser_RechargeCount(userId, coinId, status, stime, etime);
	}

	@Override
	public boolean saveUser_Recharge(User_Recharge ur) {
		// TODO Auto-generated method stub
		return user_RechargeDao.saveUser_Recharge(ur);
	}

}
