package com.superbit.service.platform.define;

import java.util.List;

import com.superbit.core.entry.Common_Platform;
/**
 * 公共提现平台
 * @author hongzhen
 *
 */
public interface Common_PlatformService {
	/**
	 * 添加公共提现平台
	 * @param cp
	 * @return
	 */
	boolean addCommon_Platform(Common_Platform cp);
	
	/**
	 * 删除公共提现平台
	 * @param cp
	 * @return
	 */
	boolean deleteCommon_Platform(Common_Platform cp);
	/**
	 * 获取所有提现平台
	 * @return
	 */
	List<Common_Platform> getCommon_Platforms();
}
