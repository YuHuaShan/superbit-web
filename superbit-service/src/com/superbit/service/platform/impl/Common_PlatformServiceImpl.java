package com.superbit.service.platform.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superbit.core.entry.Common_Platform;
import com.superbit.service.platform.dao.Common_PlatformDao;
import com.superbit.service.platform.define.Common_PlatformService;
@Service
public class Common_PlatformServiceImpl implements Common_PlatformService {

	@Autowired
	private Common_PlatformDao common_PlatformDao;
	@Override
	public boolean addCommon_Platform(Common_Platform cp) {
		// TODO Auto-generated method stub
		return common_PlatformDao.addCommon_Platform(cp);
	}

	@Override
	public boolean deleteCommon_Platform(Common_Platform cp) {
		// TODO Auto-generated method stub
		return common_PlatformDao.deleteCommon_Platform(cp);
	}

	@Override
	public List<Common_Platform> getCommon_Platforms() {
		// TODO Auto-generated method stub
		return common_PlatformDao.getCommon_Platforms();
	}

}
