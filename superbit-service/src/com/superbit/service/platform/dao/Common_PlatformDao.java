package com.superbit.service.platform.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.superbit.core.entry.Common_Platform;
import com.superbit.utils.dbutil.CommonDao;
@Repository
public class Common_PlatformDao {
	private static Logger logger = LoggerFactory.getLogger(Common_PlatformDao.class);
	
	@Autowired
	private CommonDao commonDao;
	public boolean addCommon_Platform(Common_Platform cp) {
		// TODO Auto-generated method stub
		int i = Integer.valueOf(commonDao.saveEntry(cp).toString());
		if(i>0) {
			return true;
		}
		return false;
	}

	public boolean deleteCommon_Platform(Common_Platform cp) {
		// TODO Auto-generated method stub
		
		try {
			commonDao.deleteEntry(Common_Platform.class, cp.getId());
			return true;
		}catch(Exception e) {
			logger.error(e.toString());
			return false;
		}
		
	}

	public List<Common_Platform> getCommon_Platforms() {
		// TODO Auto-generated method stub
		return commonDao.getEntrysByCond(Common_Platform.class, null, null);
	}
}
