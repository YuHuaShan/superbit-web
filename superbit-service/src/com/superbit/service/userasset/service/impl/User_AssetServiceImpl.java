package com.superbit.service.userasset.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superbit.core.entry.Coin_Base_Info;
import com.superbit.core.entry.User_Asset;
import com.superbit.service.coin.dao.Coin_Base_InfoDao;
import com.superbit.service.userasset.service.dao.User_AssetDao;
import com.superbit.service.userasset.service.define.User_AssetService;
import com.superbit.service.util.StringPropertiesUtil;

@Service
public class User_AssetServiceImpl implements User_AssetService {

	@Autowired
	private User_AssetDao user_AssetDao;
	
	@Autowired
	private Coin_Base_InfoDao coin_Base_InfoDao;
	@Override
	public int getUser_AssetTotalCount(int userId, int flag) {
		// TODO Auto-generated method stub
		return user_AssetDao.getUser_AssetTotalCount(userId, flag);
	}

	@Override
	public List<User_Asset> getUser_AssetList(int userId, int flag, int start, int end) {
		// TODO Auto-generated method stub
		return user_AssetDao.getUser_AssetList(userId, flag, start, end);
	}


	@Override
	public Map<String, BigDecimal> getUser_Asset(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean updateUser_Asset_Amount(int userId, int flag, String coinId, String coinCode, BigDecimal amount){
		// TODO Auto-generated method stub
//		int userId, String assetId, String coinId
		User_Asset uAsset = user_AssetDao.getUser_Asset(userId, coinId, coinCode);
		return user_AssetDao.updateUser_Asset_Amount(uAsset, flag, amount);
	}

	@Override
	public User_Asset getUser_Asset(int userId, String assetId, String coinId, String coinCode) {
		// TODO Auto-generated method stub
		return user_AssetDao.getUser_Asset(userId, assetId, coinId, coinCode);
	}

	@Override
	public boolean initUser_Assets(int userId) {
		// TODO Auto-generated method stub
		boolean result = true;
		//获取用户所有资产
		List<User_Asset> uAssets = user_AssetDao.getUser_AssetList(userId, 0, -1, 0);
		//获取所有已生效币种
		List<Coin_Base_Info> coin_Base_Infos = coin_Base_InfoDao.getCoin_Base_InfoList(1, null, null, null);
		//比较是否有新增已生效币种未添加入用户资产，若有则新增该资产
		for(Coin_Base_Info cbi : coin_Base_Infos) {
			boolean flag = false;
			for(User_Asset ua : uAssets) {
				if(cbi.getCoinId().equals(ua.getCoinId())) {					
					flag = true;
					break;
				}
			}
			if(!flag) {
				//该生效币种不在用户资产中，需要初始化操作
				User_Asset uAsset = new User_Asset();
				uAsset.setUserId(userId);
				uAsset.setAssetId(StringPropertiesUtil.createIdUtil());
				uAsset.setCoinId(cbi.getCoinId());
				uAsset.setCoinName(cbi.getCoinName());
				uAsset.setCoinCode(cbi.getCoinCode());
				uAsset.setCoinAmount(new BigDecimal("0"));
				uAsset.setCoinBalance(new BigDecimal("0"));
				uAsset.setCoinFreeze(new BigDecimal("0"));
				uAsset.setCoinFreezeDate(new Date());
				uAsset.setAccountStatus(0);
				result = user_AssetDao.addUser_Asset(uAsset);
			}
		}
		return result;
	}

	@Override
	public boolean updateUser_Asset(int id, Map<String, Object> map) {
		// TODO Auto-generated method stub
		return user_AssetDao.updateUser_Asset(map, id);
	}

	@Override
	public List<User_Asset> getSpell_User_Asset(int userId, Date stime, Date etime) {
		// TODO Auto-generated method stub
		return null;
	}

}
