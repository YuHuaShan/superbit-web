package com.superbit.service.userasset.service.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superbit.core.entry.Coin_Base_Info;
import com.superbit.core.entry.User_Asset;
import com.superbit.core.service.TradeLogProxy;
import com.superbit.core.service.UserAssetService;
import com.superbit.service.coin.dao.Coin_Base_InfoDao;
import com.superbit.service.userasset.service.dao.User_AssetDao;
import com.superbit.service.util.StringPropertiesUtil;
/**
 * 后端接口
 * @author hongzhen
 *
 */
@Service
public class UserAssetServiceImpl implements UserAssetService {
	@Autowired
	private User_AssetDao user_AssetDao;
	@Autowired
	private Coin_Base_InfoDao coin_Base_InfoDao;
	@Autowired
	private TradeLogProxy tradeLogProxy;
	@Override
	public Integer transferFund(Integer outUserId, Integer inUserId,
			String coinCode, BigDecimal fundAmount) {
		// TODO Auto-generated method stub
		//转移资产发生在冻结资产中，对剩余资产不用处理，需要处理总资产，以及冻结资产
		//TODO 读取两个用户该币种对应的资产信息
		User_Asset uaIn = user_AssetDao.getUser_Asset(inUserId, null, coinCode);
		User_Asset uaOut = user_AssetDao.getUser_Asset(outUserId, null, coinCode);
		Coin_Base_Info coin_Base_Info = coin_Base_InfoDao.getCoin_Base_InfoList(3, null, coinCode, null).get(0);
		if(uaOut.getCoinFreeze().compareTo(fundAmount)>=0) {
			//TODO 对转入的账户没有该币种则需要创建，有则需要修改资产信息
			if(uaIn==null) {//创建该用户资产信息
				User_Asset ua = new User_Asset();
				ua.setAssetId(StringPropertiesUtil.createIdUtil());
				ua.setCoinId(coin_Base_Info.getCoinId());
				ua.setCoinCode(coin_Base_Info.getCoinCode());
				ua.setCoinName(coin_Base_Info.getCoinName());
				ua.setCoinAmount(fundAmount);
				ua.setCoinBalance(fundAmount);
				ua.setCoinFreeze(new BigDecimal("0"));
				ua.setAccountStatus(0);
				user_AssetDao.addUser_Asset(ua);
			}else {//修改转入用户资产信息
				uaIn.setCoinAmount(uaIn.getCoinAmount().add(fundAmount));
				uaIn.setCoinBalance(uaIn.getCoinBalance().add(fundAmount));
				user_AssetDao.updateUser_Asset(uaIn);
			}
			//修改转出用户资产信息
			uaOut.setCoinAmount(uaOut.getCoinAmount().subtract(fundAmount));
			uaOut.setCoinFreeze(uaOut.getCoinFreeze().subtract(fundAmount));
			user_AssetDao.updateUser_Asset(uaOut);
			return 0;
		}else {
			return 1;
		}	}

	@Override
	public Map<String, BigDecimal> getUser_Asset(int userId, String coinCode) {
		// TODO Auto-generated method stub
		//获取用户总资产信息
		Map<String, User_Asset> uassets = user_AssetDao.getUser_Asset(userId);
		Map<String, BigDecimal> map = new HashMap<String, BigDecimal>();
		for(String key:uassets.keySet()) {
			map.put(key, uassets.get(key).getCoinAmount());
		}
		//获取币种对应的人民币价格
		Map<String, BigDecimal>  results = tradeLogProxy.getCoinPrice(map);

		BigDecimal total = new BigDecimal("0");
		//计算总人民币价格
		for(String key:map.keySet()) {
			BigDecimal value = map.get(key).multiply(results.get(key));
			total = total.add(value);
		}
		//计算人民币折算为对应币种的个数
		BigDecimal coins = total.divide(results.get(coinCode), 8, BigDecimal.ROUND_HALF_UP);
		
		map.clear();
		map.put("coins", coins);
		map.put("CNY", total);
		
		return map;
	}

	@Override
	public boolean freezeUser_Asset(String userId, String coinCode,
			BigDecimal amount) {
		// TODO Auto-generated method stub
		return user_AssetDao.updateUser_Asset_Amount(user_AssetDao.getUser_Asset(Integer.valueOf(userId), null, coinCode),
				0, amount);
	}

	@Override
	public BigDecimal getCoinAmouts(int userId, String coinCode) {
		// TODO Auto-generated method stub
		return user_AssetDao.getCoinAmouts(userId, coinCode);
	}

}
