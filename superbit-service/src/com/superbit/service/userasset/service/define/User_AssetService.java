package com.superbit.service.userasset.service.define;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.superbit.core.entry.User_Asset;
/**
 * 用户资产管理层
 * @author hongzhen
 *
 */
public interface User_AssetService {
	/**
	 * 用户资产初始化
	 * @param userId
	 * @return
	 */
	boolean initUser_Assets(int userId);
	/**
	 * 返回用户资产记录数
	 * flag为0时获取全部币种
	 * flag为1时获取资产大于0的币种
	 * @param userId
	 * @param flag
	 * @return
	 */
	int getUser_AssetTotalCount(int userId, int flag);
	/**
	 * 返回用户资产列表
	 * flag为0时获取全部币种
	 * flag为1时获取资产大于0的币种
	 * @param userId
	 * @param flag
	 * @param start  用于分页   start<0时不分页
	 * @param end   用于分页
	 * @return
	 */
	List<User_Asset> getUser_AssetList(int userId, int flag, int start, int end);

	/**
	 * 获取用户总资产
	 * @param userId
	 * @return
	 */
	Map<String, BigDecimal> getUser_Asset(String userId);
	/**
	 *  冻结用户某种币种金额，或修改用户的固定资产
	 * @param userId
	 * @param coinId
	 * @param coinCode  优先使用coinId
	 * @param flag 0 资产冻结/解冻，1充值
	 * @param amount 正负值  正数为充值或冻结，负值为解冻
	 * 
	 * @return
	 * @throws UserAssetException 
	 */
	boolean updateUser_Asset_Amount(int userId, int flag, String coinId, String coinCode, BigDecimal amount);
	
	
	/**
	 * 获取用户指定资产币种信息 
	 * @param userId
	 * @param assetId   资产id与币种id存在一个即可，同时存在则只使用资产id
	 * @param coinId
	 * @param coinCode
	 * @return
	 */
	User_Asset getUser_Asset(int userId, String assetId, String coinId, String coinCode);
	
	/**
	 * 修改用户资产信息
	 * @param Id  主键id
	 * @param map
	 * @return
	 */
	boolean updateUser_Asset(int id, Map<String, Object> map);
	
	/**
	 * 获取一段时间内某个或所有用户已生效币种资产，
	 * @param userId   当userId为负值时获取所有用户生效币种资产之和
	 * @param stime
	 * @param etime
	 * @return
	 */
	List<User_Asset> getSpell_User_Asset(int userId, Date stime, Date etime);
}
