package com.superbit.service.userasset.change.define;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.superbit.core.entry.User_Asset_Change;

/**
 * 用户资产变更业务层
 * @author hongzhen
 *
 */
public interface User_Asset_ChangeService {
	/**
	 * 获取某一用户某一种已生效币种在过去某一时间点的资产
	 * flag为0时获取全部币种信息
	 * flag为1时获取生效币种信息
	 * @param stime
	 * @param etime
	 * @param flag
	 * @return
	 */
	List<User_Asset_Change> getUser_Asset_ChangeList(Date stime, Date etime, String flag);
	
	/**
	 * 获取所有用户某一种已生效币种在过去某一时间点的资产之和
	 * flag为0时获取全部币种信息
	 * flag为1时获取生效币种信息
	 * @param stime
	 * @param etime
	 * @param flag
	 * @return
	 */
	BigDecimal getTotalAmount(Date stime, Date etime, String flag);
	/**
	 * 用户资产变更记录
	 * @param uac
	 * @return
	 */
	boolean saveUser_Asset_Change(User_Asset_Change uac);
}
