package com.superbit.service.match.daodefine;

import com.superbit.core.entry.UserTradeRecord;

/**
 * 用户交易完成记录 数据层
 * @author 赵世栋
 * @date 2018-1-10
 */
public interface UserTradeRecordDao {

	/**
	 * 获取用户交易完成记录
	 * @param userId 用户IId
	 * @param coinCode 币种Code
	 * @date 2018-1-11
	 * @return 
	 */
	UserTradeRecord getUserTradeRecordeInfo(long userId);
	/**
	 * 修改用户交易完成记录
	 * @param userTradeRecorde
	 *  @date 2018-1-11
	 * @return
	 */
	void updateUserTradeRecordeInfo(UserTradeRecord userTradeRecorde);
	/**
	 * 修改用户交易完成记录
	 * @param userTradeRecorde 
	 *  @date 2018-1-11
	 * @return
	 */
	long saveUserTradeRecordeInfo(UserTradeRecord userTradeRecorde);
	
}
