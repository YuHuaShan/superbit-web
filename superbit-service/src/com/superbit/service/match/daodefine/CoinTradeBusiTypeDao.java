package com.superbit.service.match.daodefine;

import java.util.List;

import com.superbit.core.entry.CoinTradeBusiType;

/**
 * 币种交易流程 数据层
 * @author 赵世栋
 * @date 2018-01-10
 */
public interface CoinTradeBusiTypeDao {

	List<CoinTradeBusiType> getCoinTradeBusiList(String areaCode, String coinName, String coinStatus, int page,int size);
	/**
	 * 获取指定币种交易状态
	 * @param coinCode 币种码
	 * @return 0：停止交易  1：开启交易
	 */
	CoinTradeBusiType getCoinTradeBusiStatus(String coinCode);
	/**
	 * 添加币种流程
	 * @param coinTrade
	 * @return 对应的id
	 */
	int saveCoinTradeBusi(CoinTradeBusiType coinTrade);
	/**
	 * 修改流程状态
	 * @param coinTrade
	 */
	void updateCoinTradeBusi(CoinTradeBusiType coinTrade);
	/**
	 * 获取详情
	 * @param id
	 * @return
	 */
	CoinTradeBusiType getCoinTradeBusi(long id);
	
	
}
