package com.superbit.service.match.daodefine;

import java.util.List;
import java.util.Map;

import com.superbit.core.entry.tradelog.ComplainLog;

/**
 * 申诉记录DAO
 * 
 * @author zhangdaoguang
 *
 */
public interface ComplainLogDao {
	long saveComplainLog(ComplainLog complainLog);

	void updateComplainLog(ComplainLog complainLog);

	ComplainLog getComplainLogByOrdernumber(String orderNumber);

	Map<String, ComplainLog> getComplainLogByOrdernumbers(List<String> orderNums);
}
