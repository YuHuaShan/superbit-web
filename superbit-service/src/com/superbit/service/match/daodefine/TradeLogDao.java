package com.superbit.service.match.daodefine;

import java.math.BigDecimal;
import java.util.List;

import com.superbit.core.entry.tradelog.TradeLog;

/**
 * 交易记录DAO
 * 
 * @author zhangdaoguang
 *
 */
public interface TradeLogDao {
	/**
	 * 保存交易记录
	 */
	long saveTradeLog(TradeLog tradeLog);

	/**
	 * 更新交易记录
	 */
	void updateTradeLog(TradeLog tradeLog);

	/**
	 * 获取交易列表
	 * 
	 * @param tradeType
	 *            交易类型,必传
	 * @param payStatus
	 *            当前状态
	 * @param coinCode
	 *            币种code,必传
	 * @param pageNo
	 *            页号,必传
	 * @param pageSize
	 *            每页大小,必传
	 * @return 交易列表
	 */
	List<TradeLog> getTradeLogList(Integer userId, Integer tradeType,
			Integer payStatus, String coinCode, Integer hasComplain,
			Integer pageNo, Integer pageSize);

	List<TradeLog> getTradeLogListByUserId(Integer userId, Integer type);

	List<TradeLog> getTradeLogListByQuoteId(Long quoteId);

	/**
	 * 通过id获取交易记录
	 */
	TradeLog getTradeLogById(Long id);

	/**
	 * 通过订单号获取交易记录
	 */
	TradeLog getTradeLogByOrderNum(String orderNumber);

	/**
	 * 根据用户ID获取交易次数
	 */
	Integer getTradeCount(Integer userId);

	/**
	 * 根据条件获取交易记录已成交的列表
	 */
	List<TradeLog> getTradeLogDoneList(String coinCode, String startTime,
			String endTime);

	/**
	 * 根据币种code获取最新成交价
	 */
	BigDecimal getNewTradeCNYByCoincode(String coinCode);

}
