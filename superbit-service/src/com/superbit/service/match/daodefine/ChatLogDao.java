package com.superbit.service.match.daodefine;

import java.util.List;
import java.util.Map;

import com.superbit.core.entry.tradelog.ChatLog;

/**
 * 聊天记录Dao
 * 
 * @author zhangdaoguang
 *
 */
public interface ChatLogDao {
	long saveChatLog(ChatLog chatLog);

	Long saveSystemChatLog(String orderNumber);

	List<ChatLog> getChatLogByOrderNumber(String orderNumber);

	/**
	 * 是否有新消息
	 * 
	 * @param orderNumbers
	 *            订单号列表
	 * @return 订单号 --> 0:有未读消息, 1:没有
	 */
	Map<String, Integer> hasNewMessage(List<String> orderNumbers);

	void updateHasRead(List<String> orderNumbers);

}
