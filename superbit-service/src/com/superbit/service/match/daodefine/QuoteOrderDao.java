package com.superbit.service.match.daodefine;

import java.util.List;

import com.superbit.core.entry.QuoteOrder;
/**
 * 报价订单对应的数据层
 * @author 赵世栋
 * @date 2018-01-08
 */
public interface QuoteOrderDao {
	/**
	 * 保存报价单
	 * @param quoteOrder
	 * @return
	 */
	long saveQuoteOrder(QuoteOrder quoteOrder);
	/**
	 * 获取买卖盘数据
	 * @param coinCode 币种码
	 * @param quoteType 买入 0 卖出 1
	 * @param dataSize 数据量
	 * @return
	 */
	List<QuoteOrder> getGrailData(String coinCode,Integer quoteType,Integer dateSize);
	/**
	 * 获取指定用户的报价单
	 * @param userId 用户Id
	 * @param coinCode 币种码
	 * @param pageNumber 当前页数
	 * @param pagesize  页容量
	 * @param quoteType 报价单状态 0:买入  1：卖出 
	 * @return
	 */
	List<QuoteOrder> getQuoteOrderList(int userId,String coinCode,int quoteType,int pageNumber,int pagesize);
	/**
	 * 获取指定报价信息
	 * @param quoteId
	 * @return
	 */
	QuoteOrder getQuoteOrderInfo(long quoteId);
	
	/**
	 * 删除对应的报价单
	 * @param quoteId
	 */
	void deleteQuoteOrder(long quoteId);
	/**
	 * 修改报价信息
	 * @param quoteOrder
	 */
	void updateQuoteOrder(QuoteOrder quoteOrder);
	/**
	 * 获取报价单总数量
	 * @param userId   用户Id
	 * @param coinCode 币种码
	 * @param quoteType 报价单类型   0:买入   1：卖出
	 * @return
	 */
	int getQuoteOrderCount(int userId,String coinCode,int quoteType);
	/**
	 * 获取所有的设置自动增益的报价单
	 * @return
	 */
	List<QuoteOrder> getQuoteIdsWithAutoSettig();
	

}
