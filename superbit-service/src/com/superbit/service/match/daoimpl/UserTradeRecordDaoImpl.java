package com.superbit.service.match.daoimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.superbit.core.entry.UserTradeRecord;
import com.superbit.service.match.daodefine.UserTradeRecordDao;
import com.superbit.utils.dbutil.CommonDao;
/**
 * 用户完成交易时记录
 * @author 赵世栋
 * @date 2018-01-11
 */
@Repository(value = "userTradeRecordDao")
public class UserTradeRecordDaoImpl  implements UserTradeRecordDao{

	@Autowired
	private CommonDao commonDao;
	/**
	 * 获取用户交易完成记录
	 */
	@Override
	public UserTradeRecord getUserTradeRecordeInfo(long userId) {

		return this.commonDao.getEntry(userId, UserTradeRecord.class);
	}

	@Override
	public void updateUserTradeRecordeInfo(UserTradeRecord userTradeRecorde) {
		this.commonDao.updateEntry(userTradeRecorde);
	}

	@Override
	public long saveUserTradeRecordeInfo(UserTradeRecord userTradeRecorde) {
		return (long) this.commonDao.saveEntry(userTradeRecorde);
	}

}
