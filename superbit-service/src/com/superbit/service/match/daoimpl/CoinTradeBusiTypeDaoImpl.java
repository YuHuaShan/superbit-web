package com.superbit.service.match.daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.superbit.core.entry.CoinTradeBusiType;
import com.superbit.service.match.daodefine.CoinTradeBusiTypeDao;
import com.superbit.utils.dbutil.CommonDao;
/**
 * 币种流通状态维护  数据层
 * @author 赵世栋
 * @date 2018-01-13
 */
@Repository(value = "coinTradeBusiTypeDao")
public class CoinTradeBusiTypeDaoImpl implements CoinTradeBusiTypeDao{

	@Autowired
	private CommonDao commonDao;
	@Override
	public List<CoinTradeBusiType> getCoinTradeBusiList(String areaCode, String coinName, String coinStatus, int page,int size){
		List<Criterion> conds = new ArrayList<Criterion>();
		if(!StringUtils.isEmpty(coinName)){
			conds.add(Restrictions.eq("coinCode", areaCode));
		}
		if(!StringUtils.isEmpty(coinName)){
			conds.add(Restrictions.eq("coinStatus", coinStatus));
		}
		Order[] orders = new Order[]{Order.desc("createTime")};
		return this.commonDao.getEntrysByCond(CoinTradeBusiType.class, conds.toArray(new Criterion[0]), orders, page, size);
	}
	/**
	 * 获取对应币种状态
	 */
	@Override
	public CoinTradeBusiType getCoinTradeBusiStatus(String coinCode) {
		List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.eq("coinCode", coinCode));
		List<CoinTradeBusiType> list = this.commonDao.getEntrysByCond(CoinTradeBusiType.class,conds.toArray(new Criterion[0]),null);
		if(list == null||list.size()== 0){
			return null;
		}
		return list.get(0);
	}

	@Override
	public int saveCoinTradeBusi(CoinTradeBusiType coinTrade) {
		return (int)this.commonDao.saveEntry(coinTrade);
	}
	@Override
	public void updateCoinTradeBusi(CoinTradeBusiType coinTrade) {
		this.commonDao.updateEntry(coinTrade);
	}
	@Override
	public CoinTradeBusiType getCoinTradeBusi(long id) {
		return this.commonDao.getEntry(id, CoinTradeBusiType.class);
	}

}
