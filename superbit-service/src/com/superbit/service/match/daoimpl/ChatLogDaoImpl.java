package com.superbit.service.match.daoimpl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.superbit.core.constant.TradeLogConstants;
import com.superbit.core.entry.tradelog.ChatLog;
import com.superbit.core.entry.tradelog.TradeLog;
import com.superbit.service.match.daodefine.ChatLogDao;
import com.superbit.service.match.daodefine.TradeLogDao;
import com.superbit.utils.dbutil.CommonDao;

/**
 * 聊天记录Dao实现类
 * 
 * @author zhangdaoguang
 *
 */
@Repository(value = "chatLogDao")
public class ChatLogDaoImpl implements ChatLogDao {
	@Autowired
	private CommonDao commonDao;

	@Autowired
	private TradeLogDao tradeLogDao;

	@Override
	public long saveChatLog(ChatLog chatLog) {
		return (long) commonDao.saveEntry(chatLog);
	}

	@Override
	public Long saveSystemChatLog(String orderNumber) {
		TradeLog tradeLog = tradeLogDao.getTradeLogByOrderNum(orderNumber);
		if (tradeLog == null) {
			return null;
		}
		ChatLog chatLog = new ChatLog();
		chatLog.setOrderNumber(orderNumber);
		chatLog.setFromUid(TradeLogConstants.SYSTEM_ID);
		chatLog.setToUid(tradeLog.getSellerUid());
		chatLog.setContent(String.valueOf(tradeLog.getPayStatus()));
		chatLog.setHasRead(TradeLogConstants.NOT_HAVE_READ);
		chatLog.setChatType(TradeLogConstants.CHAT_TYPE_SYSTEM);
		chatLog.setCreateTime(new Timestamp(System.currentTimeMillis()));
		return (long) commonDao.saveEntry(chatLog);
	}

	@Override
	public List<ChatLog> getChatLogByOrderNumber(String orderNumber) {
		List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.eq("orderNumber", orderNumber));
		List<ChatLog> resultList = commonDao.getEntrysByCond(ChatLog.class,
				conds.toArray(new Criterion[0]), null);
		return resultList;
	}

	@Override
	public Map<String, Integer> hasNewMessage(List<String> orderNumbers) {
		List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.in("orderNumber", orderNumbers));
		conds.add(Restrictions.eq("hasRead", TradeLogConstants.HAVE_NEW_MSG));
		List<ChatLog> resultList = commonDao.getEntrysByCond(ChatLog.class,
				conds.toArray(new Criterion[0]), null);
		Map<String, Integer> retMap = new HashMap<String, Integer>();
		for (String orderNumber : orderNumbers) {
			retMap.put(orderNumber, TradeLogConstants.NOT_HAVE_NEW_MSG);
		}

		for (ChatLog chatLog : resultList) {
			String orderNumber = chatLog.getOrderNumber();
			retMap.put(orderNumber, TradeLogConstants.HAVE_NEW_MSG);
		}
		return retMap;
	}

	@Override
	public void updateHasRead(List<String> orderNumbers) {
		List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.in("orderNumber", orderNumbers));
		List<ChatLog> resultList = commonDao.getEntrysByCond(ChatLog.class,
				conds.toArray(new Criterion[0]), null);
		for (ChatLog log : resultList) {
			if (!TradeLogConstants.HAVE_READ.equals(log.getHasRead())) {
				log.setHasRead(TradeLogConstants.HAVE_READ);
				commonDao.updateEntry(log);
			}
		}
	}

}
