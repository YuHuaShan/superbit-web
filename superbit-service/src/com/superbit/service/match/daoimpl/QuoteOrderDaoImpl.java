package com.superbit.service.match.daoimpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.superbit.core.constant.QuoteOrderConstant;
import com.superbit.core.entry.QuoteOrder;
import com.superbit.service.match.daodefine.QuoteOrderDao;
import com.superbit.utils.dbutil.CommonDao;

@Repository(value = "quoteOrderDao")
public class QuoteOrderDaoImpl implements QuoteOrderDao {
	
	@Autowired
	private CommonDao commonDao;
	
	
	
	@Override
	public long saveQuoteOrder(QuoteOrder quoteOrder) {
		 return (long) commonDao.saveEntry(quoteOrder);
	}
	
	@Override
	public List<QuoteOrder> getGrailData(String coinCode, Integer quoteType,Integer dataSize) {
		List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.eq("quoteType", quoteType));
		conds.add(Restrictions.eq("coinCode", coinCode));
		conds.add(Restrictions.ne("tradeCode", QuoteOrderConstant.QUOTE_TRADESTATUS_LAST));//添加交易状态
		Order[] orders = new Order[]{Order.desc("createTime")};
		List<QuoteOrder> list = commonDao.getEntrysByCond(QuoteOrder.class, conds.toArray(new Criterion[0]), orders, 0, dataSize);
		if(dataSize == 7){
			/*报价订单类型 0：买入 1：卖出*/
			if(quoteType ==QuoteOrderConstant.DIRECTION_BUY ){
				//大到小
				Collections.sort(list, new Comparator<QuoteOrder>() {
					@Override
					public int compare(QuoteOrder o1, QuoteOrder o2) {
						//TODO 需要修改维护
						return o2.getPriceCNY().compareTo(o1.getPriceCNY());
					}
				});
			}
			if(quoteType == QuoteOrderConstant.DIRECTION_SELL){
				//小到大
				Collections.sort(list, new Comparator<QuoteOrder>() {
					@Override
					public int compare(QuoteOrder o1, QuoteOrder o2) {
						//TODO 需要重新组装返回数据
						return o1.getPriceCNY().compareTo(o2.getPriceCNY());
					}
				});
			}
		}
		
		return list;
	}

	@Override
	public QuoteOrder getQuoteOrderInfo(long quoteId) {
		QuoteOrder order = this.commonDao.getEntry(quoteId, QuoteOrder.class);
		return order;
	}

	@Override
	public void deleteQuoteOrder(long quoteId) {
		this.commonDao.deleteEntry(QuoteOrder.class, quoteId);
	}

	@Override
	public void updateQuoteOrder(QuoteOrder quoteOrder) {
	this.commonDao.updateEntry(quoteOrder);	
	}
	
	@Transactional
	@Override
	public List<QuoteOrder> getQuoteOrderList(int userId, String coinCode, int quoteType,int pageNumber, int pagesize) {
		List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.eq("userId", userId));
		conds.add(Restrictions.eq("coinCode", coinCode));
		conds.add(Restrictions.ne("tradeCode", QuoteOrderConstant.QUOTE_TRADESTATUS_LAST));//添加交易状态
		if(quoteType == 0 ||quoteType == 1){
		conds.add(Restrictions.eq("quoteType", quoteType));
		}
		Order[] orders = new Order[]{Order.desc("createTime")};
		return commonDao.getEntrysByCond(QuoteOrder.class, conds.toArray(new Criterion[0]), orders, pageNumber, pagesize);
	}
	@Override
	public int getQuoteOrderCount(int userId, String coinCode, int quoteType) {
		List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.eq("userId", userId));
		conds.add(Restrictions.ne("tradeCode", QuoteOrderConstant.QUOTE_TRADESTATUS_LAST));//添加交易状态
		if(quoteType == 0 ||quoteType ==1){
		conds.add(Restrictions.eq("quoteType", quoteType));
		}
		if(coinCode != null){
			conds.add(Restrictions.eq("coinCode", coinCode));
		}
		return commonDao.getCount(QuoteOrder.class, conds.toArray(new Criterion[0]));
	}
	@Override
	public List<QuoteOrder> getQuoteIdsWithAutoSettig() {
		List<Criterion> conds =  new ArrayList<Criterion>();
		conds.add(Restrictions.eq("isAutoSet", QuoteOrderConstant.QUOTE_SET_AUTO));
		conds.add(Restrictions.ne("tradeCode", QuoteOrderConstant.QUOTE_TRADESTATUS_LAST));//添加交易状态
		Order[] orders = new Order[]{Order.desc("createTime")};
		return this.commonDao.getEntrysByCond(QuoteOrder.class, conds.toArray(new Criterion[0]), orders);
	}

	

}
