package com.superbit.service.match.daoimpl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.superbit.core.constant.TradeLogConstants;
import com.superbit.core.entry.tradelog.TradeLog;
import com.superbit.core.tools.PageListRes;
import com.superbit.service.match.daodefine.TradeLogDao;
import com.superbit.utils.dbutil.CommonDao;

/**
 * 交易记录DAO实现类
 * 
 * @author zhangdaoguang
 *
 */
@Repository(value = "tradeLogDao")
public class TradeLogDaoImpl implements TradeLogDao {
	@Autowired
	private CommonDao commonDao;

	@Override
	public long saveTradeLog(TradeLog tradeLog) {
		return (long) commonDao.saveEntry(tradeLog);
	}

	@Override
	public void updateTradeLog(TradeLog tradeLog) {
		commonDao.updateEntry(tradeLog);
	}

	@Override
	public List<TradeLog> getTradeLogList(Integer userId, Integer tradeType,
			Integer payStatus, String coinCode, Integer hasComplain,
			Integer pageNo, Integer pageSize) {
		List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.eq("tradeType", tradeType));
		if (TradeLogConstants.TRADE_TYPE_BUY.equals(tradeType)) {
			conds.add(Restrictions.eq("buyserUid", userId));
		} else if (TradeLogConstants.TRADE_TYPE_SELL.equals(tradeType)) {
			conds.add(Restrictions.eq("sellerUid", userId));
		}

		if (payStatus != null) {
			conds.add(Restrictions.eq("payStatus", payStatus));
		}
		conds.add(Restrictions.eq("coinCode", coinCode));
		if (hasComplain != null) {
			conds.add(Restrictions.eq("hasComplain", hasComplain));
		}

		Order[] orders = new Order[] { Order.desc("createTime") };

		PageListRes<TradeLog> page = PageListRes.createInstance(pageNo,
				pageSize);
		List<TradeLog> resultList = commonDao.getEntrysByCond(TradeLog.class,
				conds.toArray(new Criterion[0]), orders, page.getFirst() - 1,
				page.getPageSize());
		return resultList;
	}

	@Override
	public TradeLog getTradeLogById(Long id) {
		return commonDao.getEntry(id, TradeLog.class);
	}

	@Override
	public TradeLog getTradeLogByOrderNum(String orderNumber) {
		List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.eq("orderNumber", orderNumber));
		List<TradeLog> resultList = commonDao.getEntrysByCond(TradeLog.class,
				conds.toArray(new Criterion[0]), null);
		if (resultList != null) {
			return resultList.get(0);
		}
		return null;
	}

	@Override
	public Integer getTradeCount(Integer userId) {
		List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.or(Restrictions.eq("buyserUid", userId),
				Restrictions.eq("sellerUid", userId)));
		return commonDao.getCount(Integer.class, conds);
	}

	@Override
	public List<TradeLog> getTradeLogListByUserId(Integer userId, Integer type) {
		List<Criterion> conds = new ArrayList<Criterion>();
		if (TradeLogConstants.QUERY_TYPE_ALL.equals(type)) {
			conds.add(Restrictions.or(Restrictions.eq("buyserUid", userId),
					Restrictions.eq("sellerUid", userId)));
		} else if (TradeLogConstants.QUERY_TYPE_BUY.equals(type)) {
			conds.add(Restrictions.eq("buyserUid", userId));
		} else if (TradeLogConstants.QUERY_TYPE_SELL.equals(type)) {
			conds.add(Restrictions.eq("sellerUid", userId));
		}
		List<TradeLog> resultList = commonDao.getEntrysByCond(TradeLog.class,
				conds.toArray(new Criterion[0]), null);
		return resultList;
	}

	@Override
	public List<TradeLog> getTradeLogDoneList(String coinCode,
			String startTime, String endTime) {
		List<Criterion> conds = new ArrayList<Criterion>();
		if (coinCode != null) {
			conds.add(Restrictions.eq("coinCode", coinCode));
		}

		conds.add(Restrictions.eq("payStatus",
				TradeLogConstants.PAY_STATUS_DONE));

		if (startTime != null) {
			conds.add(Restrictions.ge("doneTime", Timestamp.valueOf(startTime)));
		}

		if (endTime != null) {
			conds.add(Restrictions.le("doneTime", Timestamp.valueOf(endTime)));
		}
		Order[] orders = new Order[] { Order.desc("doneTime") };
		List<TradeLog> resultList = commonDao.getEntrysByCond(TradeLog.class,
				conds.toArray(new Criterion[0]), orders);
		return resultList;
	}

	@Override
	public BigDecimal getNewTradeCNYByCoincode(String coinCode) {
		List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.eq("coinCode", coinCode));
		conds.add(Restrictions.eq("payStatus",
				TradeLogConstants.PAY_STATUS_DONE));
		Order[] orders = new Order[] { Order.desc("doneTime") };
		List<TradeLog> resultList = commonDao.getEntrysByCond(TradeLog.class,
				conds.toArray(new Criterion[0]), orders, 0, 1);
		if (CollectionUtils.isNotEmpty(resultList)) {
			return resultList.get(0).getUnitPrice();
		}
		return new BigDecimal(0);
	}

	@Override
	public List<TradeLog> getTradeLogListByQuoteId(Long quoteId) {
		List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.eq("quoteId", quoteId));
		Order[] orders = new Order[] { Order.desc("createTime") };
		List<TradeLog> resultList = commonDao.getEntrysByCond(TradeLog.class,
				conds.toArray(new Criterion[0]), orders);
		return resultList;
	}

}
