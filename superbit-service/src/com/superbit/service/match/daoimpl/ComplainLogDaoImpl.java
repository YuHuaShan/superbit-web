package com.superbit.service.match.daoimpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.superbit.core.entry.tradelog.ComplainLog;
import com.superbit.service.match.daodefine.ComplainLogDao;
import com.superbit.utils.dbutil.CommonDao;

/**
 * 申诉记录DAO实现类
 * 
 * @author zhangdaoguang
 *
 */
@Repository(value = "complainLogDao")
public class ComplainLogDaoImpl implements ComplainLogDao {
	@Autowired
	private CommonDao commonDao;

	@Override
	public long saveComplainLog(ComplainLog complainLog) {
		return (long) commonDao.saveEntry(complainLog);
	}

	@Override
	public void updateComplainLog(ComplainLog complainLog) {
		commonDao.updateEntry(complainLog);
	}

	@Override
	public ComplainLog getComplainLogByOrdernumber(String orderNumber) {
		List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.eq("orderNumber", orderNumber));
		List<ComplainLog> resultList = commonDao.getEntrysByCond(
				ComplainLog.class, conds.toArray(new Criterion[0]), null);

		if (resultList != null) {
			return resultList.get(0);
		}
		return null;
	}

	@Override
	public Map<String, ComplainLog> getComplainLogByOrdernumbers(
			List<String> orderNums) {
		Map<String, ComplainLog> resultMap = new HashMap<String, ComplainLog>();
		if (CollectionUtils.isEmpty(orderNums)) {
			return resultMap;
		}

		List<Criterion> conds = new ArrayList<Criterion>();
		conds.add(Restrictions.in("orderNumber", orderNums));
		List<ComplainLog> resultList = commonDao.getEntrysByCond(
				ComplainLog.class, conds.toArray(new Criterion[0]), null);
		for (ComplainLog complainLog : resultList) {
			resultMap.put(complainLog.getOrderNumber(), complainLog);
		}
		return resultMap;
	}

}
