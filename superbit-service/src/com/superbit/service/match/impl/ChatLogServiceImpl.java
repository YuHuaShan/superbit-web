package com.superbit.service.match.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.superbit.core.constant.TradeLogConstants;
import com.superbit.core.entry.tradelog.ChatLog;
import com.superbit.core.service.ChatLogService;
import com.superbit.service.match.daodefine.ChatLogDao;

/**
 * 聊天记录服务接口实现类
 * 
 * @author zhangdaoguang
 *
 */
@Service("chatLogService")
@Transactional
public class ChatLogServiceImpl implements ChatLogService {
	private static Logger log = LoggerFactory
			.getLogger(ChatLogServiceImpl.class);

	@Autowired
	private ChatLogDao chatLogDao;

	@Override
	public Long saveChatLog(ChatLog chatLog) {
		if (chatLog.getHasRead() == null) {
			chatLog.setHasRead(TradeLogConstants.NOT_HAVE_READ);
		}

		if (chatLog.getChatType() == null) {
			chatLog.setChatType(TradeLogConstants.CHAT_TYPE_USER);
		}
		return chatLogDao.saveChatLog(chatLog);
	}

	@Override
	public List<ChatLog> getChatLogByOrderNumber(String orderNumber) {
		log.info(
				"ChatLogServiceImpl.getChatLogByOrderNumber(), orderNumber:{}",
				orderNumber);
		return chatLogDao.getChatLogByOrderNumber(orderNumber);
	}

	@Override
	public Map<String, Integer> hasNewMessage(List<String> orderNumbers) {
		return chatLogDao.hasNewMessage(orderNumbers);
	}

	@Override
	public void updateHasRead(List<String> orderNumbers) {
		chatLogDao.updateHasRead(orderNumbers);
	}

}
