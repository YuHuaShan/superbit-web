package com.superbit.service.match.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.superbit.core.entry.CoinTradeBusiType;
import com.superbit.core.service.CoinTradeBusiTypeService;
import com.superbit.core.tools.PageListRes;
import com.superbit.core.vo.CoinTradeBusiTypeVO;
import com.superbit.service.match.daodefine.CoinTradeBusiTypeDao;
@Service("coinTradeBusiTypeService")
public class CoinTradeBusiTypeServiceImpl  implements CoinTradeBusiTypeService{

	@Autowired
	private CoinTradeBusiTypeDao coinTradeBusiTypeDao;
	
	@Override
	public PageListRes<CoinTradeBusiTypeVO> getCoinTradeBusiList(String areaCode, String coinName, String coinStatus, int pageNumber,int pageSize) {
		List<CoinTradeBusiTypeVO> result =new ArrayList<CoinTradeBusiTypeVO>();
		PageListRes<CoinTradeBusiTypeVO> pageResp = PageListRes.createInstance(pageNumber, pageSize);
		
		List<CoinTradeBusiType> list = this.coinTradeBusiTypeDao.getCoinTradeBusiList(areaCode,coinName,coinStatus,
				pageResp.getFirst()-1,pageResp.getPageSize());
		for (CoinTradeBusiType coinTradeBusiType : list) {
			CoinTradeBusiTypeVO vo =new CoinTradeBusiTypeVO(coinTradeBusiType);
			result.add(vo);
		}
		pageResp.setTotal(list.size() > 0 ? 0:list.size());
		pageResp.setList(result);
		return pageResp;
	}

	@Transactional
	@Override
	public boolean changeTradeStatusById(long id, String status, boolean flag) {
		CoinTradeBusiType coinTrade = this.coinTradeBusiTypeDao.getCoinTradeBusi(id);
		
		if(flag){
			// 开启交易/停止交易
			coinTrade.setTradeStatus("start".equals(status)? 1:0);
			
		}else{
			//停用/启用
			coinTrade.setTradeStatus("start".equals(status)? 1:0);
			coinTrade.setIsActivate("start".equals(status)? 1:0);
		}
		int cid = this.coinTradeBusiTypeDao.saveCoinTradeBusi(coinTrade);
		if(cid>0){
			return true;
		}
		return false;
	}

}
