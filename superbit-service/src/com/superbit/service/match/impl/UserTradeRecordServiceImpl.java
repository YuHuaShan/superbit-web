package com.superbit.service.match.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superbit.core.entry.UserTradeRecord;
import com.superbit.core.exception.BusinessException;
import com.superbit.core.service.UserTradeRecordService;
import com.superbit.service.match.daodefine.UserTradeRecordDao;
@Service
public class UserTradeRecordServiceImpl implements UserTradeRecordService{

	@Autowired
	private UserTradeRecordDao userTradeRecordDao;
	
	@Override
	public UserTradeRecord getUserTradeRecordeInfo(long userId) {
	
		return this.userTradeRecordDao.getUserTradeRecordeInfo(userId);
	}

	@Override
	public void updateUserTradeRecordeInfo(UserTradeRecord userTradeRecorde) throws BusinessException {
		
		
	}

	@Override
	public int saveUserTradeRecordeInfo(UserTradeRecord userTradeRecorde) throws BusinessException {
		// TODO Auto-generated method stub
		return 0;
	}

}
