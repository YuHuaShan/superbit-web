package com.superbit.service.match.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superbit.core.entry.UserTradeRecord;
import com.superbit.core.service.UserBaseInfoProxy;
import com.superbit.core.service.UserTradeRecordProxy;
import com.superbit.service.match.daodefine.UserTradeRecordDao;

/**
 * 提供内部交易完成接口接口
 * 
 * @author 赵世栋
 * @date 2018-01-17
 *
 */
@Service(value = "userTradeRecordProxy")
public class UserTradeRecordProxyImpl implements UserTradeRecordProxy {

	@Autowired
	private UserTradeRecordDao userTradeRecordDao;

	@Autowired
	private UserBaseInfoProxy userBaseInfoProxy;

	/**
	 * 添加交易次数
	 */
	@Override
	public void addTradCount(long userId) {
		UserTradeRecord userTradeRecord = this.userTradeRecordDao
				.getUserTradeRecordeInfo(userId);
		if (userTradeRecord == null) {
			// 不存在
			UserTradeRecord userTradeRecordnew = new UserTradeRecord();
			userTradeRecordnew.setConsigner(userBaseInfoProxy
					.getNickNameByUserId(Integer.valueOf(userId + "")));
			userTradeRecordnew.setUserId(userId);
			userTradeRecordnew.setTradeFinishCount(1);
			userTradeRecordnew.setFailTradeCount(0);
			this.userTradeRecordDao
					.saveUserTradeRecordeInfo(userTradeRecordnew);
		} else {
			// 存在
			userTradeRecord.setTradeFinishCount(userTradeRecord
					.getTradeFinishCount() + 1);
			this.userTradeRecordDao.updateUserTradeRecordeInfo(userTradeRecord);
		}
	}

	/**
	 * 减少交易次数
	 */
	@Override
	public void reduceTradCount(long userId) {
		UserTradeRecord userTradeRecord = this.userTradeRecordDao
				.getUserTradeRecordeInfo(userId);
		if (userTradeRecord == null) {
			// 不存在
			UserTradeRecord userTradeRecordnew = new UserTradeRecord();
			userTradeRecordnew.setConsigner(userBaseInfoProxy
					.getNickNameByUserId(Integer.valueOf(userId + "")));
			userTradeRecordnew.setUserId(userId);
			userTradeRecordnew.setTradeFinishCount(0);
			userTradeRecordnew.setFailTradeCount(1);
			this.userTradeRecordDao
					.saveUserTradeRecordeInfo(userTradeRecordnew);
		} else {
			// 存在
			userTradeRecord.setFailTradeCount(userTradeRecord
					.getFailTradeCount() + 1);
			;
			this.userTradeRecordDao.updateUserTradeRecordeInfo(userTradeRecord);
		}
	}

}
