package com.superbit.service.match.impl;

import java.math.BigDecimal;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superbit.core.constant.QuoteOrderConstant;
import com.superbit.core.dto.QuoteOrderDto;
import com.superbit.core.entry.QuoteOrder;
import com.superbit.core.exception.BusinessException;
import com.superbit.core.service.QuoteOrderProxy;
import com.superbit.service.match.daodefine.QuoteOrderDao;
import com.superbit.service.userasset.service.define.User_AssetService;
/**
 * 报价模块提供给交易模块部分接口
 * @author 赵世栋
 * @date 2018-01-17
 */
@Service(value = "quoteOrderProxy")
public class QuoteOrderProxyImpl implements QuoteOrderProxy {

	@Autowired
	private QuoteOrderDao quoteOrderDao;
	@Autowired
	private User_AssetService  user_AssetService;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Override
	public boolean cancelTradeOrder(QuoteOrderDto quoteOrderDto)throws BusinessException {
		logger.info("交易模块退单开始："+quoteOrderDto.toString());
		//TODO 交易模块订单回退逻辑 买方点击“取消”
		QuoteOrder order = this.quoteOrderDao.getQuoteOrderInfo(quoteOrderDto.getQuoteId());
		if(order == null){
			//此时买单被取消 对应的币种直接退回卖方账号   ：只有买家可以点击取消
			logger.info("报价单被取消资金退回订单号，用户id,币种，币数量："+Arrays.asList(new Object[]{quoteOrderDto.getQuoteId(),quoteOrderDto.getBuyUserId(),quoteOrderDto.getClass(),quoteOrderDto.getCoinAmount()}));
			//将交易金额退回给卖方
			user_AssetService.updateUser_Asset_Amount(quoteOrderDto.getSellUserId(), 1, null, quoteOrderDto.getCoinCode(), quoteOrderDto.getCoinAmount());
			//TODO 报价单被取消剩余金额直接退回账户
			logger.info("报价单被退回成功.....");
			
		}else{
			QuoteOrder quoteOrderNew = new QuoteOrder();
			BigDecimal remain = order.getRemainPrice().add(quoteOrderDto.getTotalPrice());
			BigDecimal totalPrice = order.getTotalPrice();
			quoteOrderNew.setALOtransaction(order.getALOtransaction());
			quoteOrderNew.setAmountCoin(order.getAmountCoin().add(quoteOrderDto.getCoinAmount()).setScale(8,BigDecimal.ROUND_HALF_UP));
			quoteOrderNew.setCoinCode(order.getCoinCode());
			quoteOrderNew.setConsigner(order.getConsigner());
			quoteOrderNew.setCreateTime(order.getCreateTime());
			quoteOrderNew.setDealAmount(order.getDealAmount());
			quoteOrderNew.setFreezeAmount(order.getFreezeAmount());
			quoteOrderNew.setId(order.getId());
			quoteOrderNew.setIsAutoSet(order.getIsAutoSet());
			quoteOrderNew.setPayType(order.getPayType());
			quoteOrderNew.setPriceCNY(order.getPriceCNY());
			quoteOrderNew.setQuoteStatus(order.getQuoteStatus());
			quoteOrderNew.setQuoteType(order.getQuoteType());
			quoteOrderNew.setRemainPrice(remain);
			quoteOrderNew.setSetValue(order.getSetValue());
			quoteOrderNew.setTotalPrice(totalPrice);
			if(remain.compareTo(totalPrice) == 0){
				quoteOrderNew.setTradeCode(QuoteOrderConstant.QUOTE_TRADESTATUS_NO);
			}else{
			quoteOrderNew.setTradeCode(QuoteOrderConstant.QUOTE_TRADESTATUS_DOING);
			}
			quoteOrderNew.setTradeFee(order.getTradeFee());
			quoteOrderNew.setUserId(order.getUserId());
			this.quoteOrderDao.updateQuoteOrder(quoteOrderNew);
			logger.info("正常取消交易完成......");
			//TODO 如果是取消报价单的情况下直接回账户
			return true;
		}
		
		
		return false;
	}

	@Override
	public boolean deleteQuoteOrder(long orderId)throws BusinessException {
		QuoteOrder order = this.quoteOrderDao.getQuoteOrderInfo(orderId);
		if(order != null && QuoteOrderConstant.QUOTE_TRADESTATUS_LAST == order.getTradeCode()){
			this.quoteOrderDao.deleteQuoteOrder(orderId);
			return true;
		}
		return false;
	}

}
