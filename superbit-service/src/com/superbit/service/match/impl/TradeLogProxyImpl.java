package com.superbit.service.match.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superbit.core.constant.TradeLogConstants;
import com.superbit.core.entry.tradelog.TradeLog;
import com.superbit.core.service.TradeLogProxy;
import com.superbit.service.match.daodefine.TradeLogDao;
import com.superbit.utils.date.DateFormateUtils;

/**
 * 交易记录对内提供接口实现类
 * 
 * @author zhangdaoguang
 *
 */
@Service(value = "tradeLogProxy")
public class TradeLogProxyImpl implements TradeLogProxy {
	@Autowired
	private TradeLogDao tradeLogDao;

	@Override
	public Integer getTradeCount(Integer userId) {
		return this.tradeLogDao.getTradeCount(userId);
	}

	@Override
	public Long avgDoneTime(Integer userId) {
		List<TradeLog> tradeLogList = this.tradeLogDao.getTradeLogListByUserId(
				userId, TradeLogConstants.QUERY_TYPE_SELL);
		Long totalTime = 0L;
		Integer totalCount = 0;
		for (TradeLog tradeLog : tradeLogList) {
			if (tradeLog.getDoneTime() == null || tradeLog.getPayTime() == null) {
				continue;
			}
			long diff = tradeLog.getDoneTime().getTime()
					- tradeLog.getPayTime().getTime();
			totalCount++;
			totalTime += diff;
		}
		Long second = totalTime / (totalCount * 1000);
		return second;
	}

	@Override
	public Timestamp getLastTradeTime(Integer userId) {
		List<TradeLog> tradeLogList = this.tradeLogDao.getTradeLogListByUserId(
				userId, TradeLogConstants.QUERY_TYPE_ALL);
		Timestamp timeStamp = null;
		for (TradeLog log : tradeLogList) {
			if (log.getDoneTime() == null) {
				continue;
			}
			if (timeStamp == null) {
				timeStamp = log.getDoneTime();
			} else {
				if (timeStamp.before(log.getDoneTime())) {
					timeStamp = log.getDoneTime();
				}
			}
		}
		return timeStamp;
	}

	@Override
	public BigDecimal getNewTradeCNY(String coinCode) {
		return this.tradeLogDao.getNewTradeCNYByCoincode(coinCode);
	}

	@Override
	public BigDecimal getLastDayTradeCNY(String coinCode) {
		List<TradeLog> tradeList = this.tradeLogDao.getTradeLogDoneList(
				coinCode, null,
				DateFormateUtils.getToday());//修改于2018-01-23   zhaosdDateFormateUtils.getYesterday()
		if (CollectionUtils.isNotEmpty(tradeList)) {
			return tradeList.get(0).getUnitPrice();
		}
		return new BigDecimal(0);
	}

	@Override
	public List<TradeLog> getTradeLogListByQuoteId(Long quoteId) {
		return this.tradeLogDao.getTradeLogListByQuoteId(quoteId);
	}

	@Override
	public List<TradeLog> getTradeLogListByUserId(Integer userId) {
		return this.tradeLogDao.getTradeLogListByUserId(userId,
				TradeLogConstants.QUERY_TYPE_ALL);
	}

	@Override
	public List<TradeLog> getBuyTradeLogListByUserId(Integer userId) {
		return this.tradeLogDao.getTradeLogListByUserId(userId,
				TradeLogConstants.QUERY_TYPE_BUY);
	}

	@Override
	public List<TradeLog> getSellTradeLogListByUserId(Integer userId) {
		return this.tradeLogDao.getTradeLogListByUserId(userId,
				TradeLogConstants.QUERY_TYPE_SELL);
	}

	@Override
	public Map<String, BigDecimal> getCoinPrice(Map<String, BigDecimal> map) {
		// TODO Auto-generated method stub
		return null;
	}

}
